import * as React from "react"
import { Text } from "react-native"
import { Provider as ReduxProvider } from "react-redux"

import configureStore from "./src/store/configureStore"
import AppNavigator from "./src/navigation/AppNavigator"

const store = configureStore()

function App() {
	Text.defaultProps = Text.defaultProps || {}
	Text.defaultProps.allowFontScaling = false

	return (
		<ReduxProvider store={store}>
			<AppNavigator />
		</ReduxProvider>
	)
}

export default App
