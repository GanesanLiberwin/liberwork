import React, { useEffect } from "react"
import { Image, StyleSheet, SafeAreaView } from "react-native"
import AsyncStorage from "@react-native-async-storage/async-storage"
import { useDispatch } from "react-redux"

import * as authActions from "../store/actions/authActions"
import * as profileActions from "../store/actions/profileActions"
import * as accountActions from "../store/actions/accountActions"
import * as workActions from "../store/actions/workActions"
import * as searchActions from "../store/actions/searchActions"
import { IMAGE } from "../constants/Image"

const StartupScreen = props => {
	const dispatch = useDispatch()
	useEffect(() => {
		let mounted = true
		const tryLogin = async () => {
			if (mounted) {
				let lstStoreData = [
					"isAccount",
					"accountList",
					"accountData",
					"isProfile",
					"profileData",
					"userData",
					"orgDetail",
					"draftWorkData",
					"draftCardData",
					"WorkDrafts",
					"searchedSkill"
				]
				let isAccount = false
				let isProfile = false

				AsyncStorage.multiGet(lstStoreData, (err, stores) => {
					stores.map((result, i, store) => {
						let key = store[i][0]
						let val = store[i][1]
						// console.log(key, val)
						if (val) {
							switch (key) {
								case "isAccount":
									isAccount = true
									dispatch(accountActions.accountUpdated(true))
									dispatch(profileActions.profileUpdated(true))
									dispatch(authActions.userUpdated(true))
									break

								case "accountList":
									const accountList = JSON.parse(val)
									if (accountList && accountList.length > 0) {
										dispatch(accountActions.loadExistingAccount(accountList, accountList[0].id, true))
									}
									break

								case "accountData":
									const accountData = JSON.parse(val)
									if (accountData) {
										dispatch(accountActions.loadAccountData(accountData, true))
									}
									break

								case "isProfile":
									isProfile = true
									if (!isAccount) {
										dispatch(profileActions.profileUpdated(true))
										dispatch(authActions.userUpdated(true))
									}
									break

								case "profileData":
									const profileData = JSON.parse(val)
									if (profileData) {
										dispatch(profileActions.getProfileData(profileData, true))
									}
									break

								case "userData":
									const userData = JSON.parse(val)
									if (userData) {
										dispatch(
											authActions.loadUserData(
												{
													userId: userData.userId,
													accesstoken: userData.accesstoken,
													userName: userData.userName,
													userNameType: userData.userNameType
												},
												true
											)
										)
									}
									if (!isProfile) {
										dispatch(authActions.userUpdated(true))
									}
									break
								case "orgDetail":
									const orgDetail = JSON.parse(val)
									if (orgDetail) {
										dispatch(authActions.loadOrgDetails(orgDetail))
									}
									break

								case "draftWorkData":
									const draftWorkData = JSON.parse(val)
									if (draftWorkData && draftWorkData.draft) {
										dispatch(workActions.loadDraftData(draftWorkData.draft))
									}
									break
								case "draftCardData":
									const draftCardData = JSON.parse(val)
									if (draftCardData && draftCardData.draftCard) {
										dispatch(workActions.loadDraftCardData(draftCardData.draftCard))
									}
									break
								case "searchedSkill":
									const searchedSkill = JSON.parse(val)
									if (searchedSkill && searchedSkill.data) {
										dispatch(searchActions.loadSearchedSkills(searchedSkill.data, true))
									}
									break

								case "WorkDrafts":
									const draftsData = JSON.parse(val)
									if (draftsData) {
										dispatch(workActions.loadWorkDrafts(draftsData))
									}
									break
								default:
									break
							}
						}
					})
					dispatch(authActions.setAutoLoginAttempt())
				})
			}
		}
		tryLogin()
		return () => (mounted = false)
	}, [dispatch])

	return (
		<SafeAreaView style={styles.screen}>
			<Image style={{ height: 110, width: 110 }} source={IMAGE.ICON_APP} resizeMode="contain" />
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "#fff"
	}
})

export default StartupScreen
