import React, { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import {
	View,
	Image,
	ImageBackground,
	Text,
	StyleSheet,
	TouchableOpacity,
	TouchableWithoutFeedback,
	KeyboardAvoidingView,
	Button,
	ActivityIndicator,
	Platform
} from "react-native"

import * as authActions from "../../store/actions/authActions"

import { IMAGE } from "../../constants/Image"
import Input from "../../components/Input"
import Colors from "../../constants/Colors"
import Regex from "../../constants/Regex"
import { LinearGradient } from "expo-linear-gradient"

const AuthenticationScreen = () => {
	const isLoading = useSelector(state => state.apiCallsInProgress > 0)
	const [isSignIn, setIsSignIn] = useState(true)
	// const [isSignup, setIsSignup] = useState(false)
	const [isNext, setIsNext] = useState(false)
	const [isFinalSignup, setIsFinalSignup] = useState(false)
	const [isEditable, setIsEditable] = useState(true)
	const [isDialCodePickerEnable, setIsDialCodePickerEnable] = useState(false)
	const [dialCode, setDialCode] = useState("+91")
	const [userName, setUserName] = useState("")
	const [password, setPassword] = useState("")
	const [userNameType, setUserNameType] = useState("")
	const [OTP, setOTP] = useState("")
	// const [error, setError] = useState(false)
	const [errorMessage, setErrorMessage] = useState({})
	const [userNameError, setUserNameError] = useState("")
	const [passwordError, setPasswordError] = useState("")
	const [OTPError, setOTPError] = useState("")

	const dispatch = useDispatch()

	const onForgotPassword = () => {
		alert("Yet to be designed")
	}

	//Dial code Picker data response from Dial code Picker
	const onDial_CodeSelected = receivedDialCode => {
		setDialCode(receivedDialCode)
	}

	const resetErrorMessages = () => {
		setErrorMessage({})
		setUserNameError("")
		setPasswordError("")
		setOTPError("")
	}

	// Method to check whether the user ID is number or Alphanumeric to enable/disable phonepicker component
	//UserName Field change and Validation
	const userNameTextChangeHandler = text => {
		setErrorMessage({})
		if (text) {
			if (isNaN(text)) {
				if (Regex.email.test(text)) {
					setUserNameType("E")
					setIsDialCodePickerEnable(false) // Alpha numeric
					setUserNameError("")
				} else {
					setUserNameError("Please enter a valid Email ID")
					setIsDialCodePickerEnable(false)
				}
			} else {
				setUserNameType("M")
				setIsDialCodePickerEnable(true) // numeric
				setUserNameError("")
			}
		} else {
			setIsDialCodePickerEnable(false)
			setUserNameError("")
		}
		setUserName(text.toLowerCase())
	}

	//Password field change and Validation
	const passwordTextChangeHandler = text => {
		setErrorMessage({})
		if (text.length < 6) {
			setPasswordError("Password length appears less than 6")
		} else {
			setPassword(text)
			setPasswordError("")
		}
	}
	//OTP field change and Validation
	const onOTPTextChangeHandler = text => {
		setErrorMessage({})
		if (text.length < 4) {
			setOTPError("OTP length appears less than 4")
		} else {
			setOTP(text)
			setOTPError("")
		}
	}

	//Sing up flow NEXT button click event
	const onSignUpNavigateHandler = () => {
		resetErrorMessages()
		setUserName("")
		setDialCode("+91")
		setIsDialCodePickerEnable(false)
		setIsNext(true)
		setIsSignIn(false)
		setIsEditable(true)
	}
	//Login toggle function
	const onLoginNavigateHandler = () => {
		setPassword("")
		resetErrorMessages()
		setUserName("")
		setDialCode("+91")
		setIsDialCodePickerEnable(false)
		setIsNext(false)
		setIsFinalSignup(false)
		setIsSignIn(true)
	}

	//Is user Exists and domain validation for Username Field
	const onNextHandler = async () => {
		if (userNameError) {
			return
		}
		let newUsername
		resetErrorMessages()

		const isValidUserName = userName.length > 0 ? true : false
		if (!isValidUserName) {
			setUserNameError("User name is empty!")
			return
		}
		if (userNameType === "M") {
			newUsername = dialCode.concat(userName)
			// setUserName(newUsername)
		}

		try {
			let strUserName = userNameType === "M" ? newUsername : userName
			dispatch(authActions.isUserExist(strUserName, userNameType))
				.then(() => {
					setErrorMessage({})
					setIsFinalSignup(true)
					setIsNext(false)
					setIsSignIn(false)
					setPasswordError("")
					setIsEditable(false)
					setUserName(strUserName)
				})
				.catch(error => {
					setErrorMessage({ onApiCall: error.message })
					setIsEditable(true)
				})
		} catch (err) {
			setErrorMessage({ onApiCall: err.message })
		}
	}

	//SignUp data Validation and sign up API call
	const onSignUpHandler = async () => {
		if (userNameError || passwordError || OTPError) {
			return
		}

		const isValidPassword = password.length > 0 ? true : false
		const isValidUserName = userName.length > 0 ? true : false
		const isOTP = OTP.length > 0 ? true : false

		if (!isValidUserName) {
			setUserNameError("User name is empty!")
		}

		if (!isOTP) {
			setOTPError("OTP is empty!")
		}

		if (!isValidPassword) {
			setPasswordError("Password is empty!")
		}

		if (!isValidUserName || !isValidPassword || !isOTP) {
			return
		}

		try {
			dispatch(authActions.createUser(userName, password, userNameType, dialCode, OTP))
				.then(() => {
					setErrorMessage({})
				})
				.catch(error => {
					setErrorMessage({ onApiCall: error.message })
				})
		} catch (err) {
			setErrorMessage({ onApiCall: err.message })
		}
	}

	//SignIn data validation and Signin API call
	const onSignInHandler = async () => {
		let newUsername
		resetErrorMessages()
		const isValidPassword = password.length > 0 ? true : false
		const isValidUserName = userName.length > 0 ? true : false

		if (!isValidUserName) {
			setUserNameError("User name is emptyy!")
		}

		if (!isValidPassword) {
			setPasswordError("Password is empty!")
		}

		if (!isValidUserName || !isValidPassword) {
			return
		}
		if (userNameType === "M") {
			newUsername = dialCode.concat(userName)
		}

		try {
			dispatch(authActions.loginUser(userNameType === "M" ? newUsername : userName, password, userNameType))
				.then(() => {
					setErrorMessage({})
				})
				.catch(error => {
					setErrorMessage({ onApiCall: error.message })
				})
		} catch (err) {
			setErrorMessage({ onApiCall: err.message })
		}
	}

	return (
		<ImageBackground source={IMAGE.ICON_LIBERWIN_BG} style={styles.imageBackgroundContainer}>
			<KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == "android" && "height"}>
				<Image style={styles.logoStyle} source={IMAGE.ICON_LIBERWIN_DM} resizeMode="contain" />
				<View style={{ flex: 1, justifyContent: "center", alignSelf: "center" }}>
					<LinearGradient
						style={{ borderRadius: 7, alignSelf: "center" }}
						colors={["rgba(255,255,255,.5)", "rgba(255,255,255,.5)", "rgba(7, 176, 249, 0.34)"]}
					>
						<View style={styles.cardContainer}>
							{isSignIn && (
								<View style={{ padding: 10, paddingBottom: 25 }}>
									<View style={styles.usernameContainer}>
										<Input
											id="userName"
											//label="E-Mail"
											icon={{
												name: "account",
												type: "MaterialCommunityIcons",
												color: "rgba(0,0,0,0.7)",
												size: 18
											}}
											keyboardType="default"
											pickerType="DialCodePicker"
											PickerEnable={isDialCodePickerEnable}
											pickerWhenToEnable={isSignIn}
											onPickerSelected={onDial_CodeSelected}
											TextInputEnable
											textHidden={false}
											autoCapitalize="none"
											autoCorrect={false}
											placeholder={"email or mobile number"}
											errorText={userNameError}
											// value={userName}
											onChangeText={userNameTextChangeHandler}
											//onBlur={userNameTextChangeHandler}
										/>
									</View>
									<View style={styles.passwordContainer}>
										<Input
											id="password"
											//label="Password"
											icon={{
												name: "lock",
												type: "MaterialCommunityIcons",
												color: "rgba(0,0,0,0.7)",
												size: 18
											}}
											keyboardType="default"
											TextInputEnable
											securedTextIcon
											textHidden
											autoCapitalize="none"
											autoCorrect={false}
											placeholder={"password"}
											errorText={passwordError}
											// value={password}
											onChangeText={passwordTextChangeHandler}
										/>
									</View>
								</View>
							)}

							{isNext && (
								<View style={{ padding: 10, paddingBottom: 20 }}>
									<View style={styles.usernameContainer}>
										<Input
											id="userName"
											//label="E-Mail"
											icon={{
												name: "account",
												type: "MaterialCommunityIcons",
												color: "rgba(0,0,0,0.7)",
												size: 19
											}}
											keyboardType="default"
											pickerType="DialCodePicker"
											PickerEnable={isDialCodePickerEnable}
											pickerWhenToEnable={isNext}
											onPickerSelected={onDial_CodeSelected}
											TextInputEnable
											textHidden={false}
											autoCapitalize="none"
											autoCorrect={false}
											editable={isEditable}
											placeholder={"email or mobile number"}
											errorText={userNameError}
											// value={userName}
											onChangeText={userNameTextChangeHandler}
											//onBlur={userNameTextChangeHandler}
										/>
									</View>
								</View>
							)}

							{isFinalSignup && (
								<View style={{ padding: 10, paddingBottom: 25 }}>
									<View style={styles.usernameContainer}>
										<Input
											id="userName"
											//label="E-Mail"
											icon={{
												name: "account",
												type: "MaterialCommunityIcons",
												color: "rgba(0,0,0,0.7)",
												size: 19
											}}
											keyboardType="default"
											pickerType="DialCodePicker"
											PickerEnable={isDialCodePickerEnable}
											pickerWhenToEnable={isNext}
											onPickerSelected={onDial_CodeSelected}
											TextInputEnable
											textHidden={false}
											autoCapitalize="none"
											autoCorrect={false}
											editable={isEditable}
											placeholder={"email or mobile number"}
											errorText={userNameError}
											value={userName}
											//onChangeText={userNameTextChangeHandler}
											//onBlur={userNameTextChangeHandler}
										/>
									</View>
									<View style={styles.passwordContainer}>
										<Input
											id="OTP"
											icon={{
												name: "email-check",
												type: "MaterialCommunityIcons",
												color: "rgba(0,0,0,0.7)",
												size: 18
											}}
											TextInputEnable
											otpResend
											textHidden={false}
											keyboardType="numeric"
											autoCapitalize="none"
											autoCorrect={false}
											placeholder={"OTP received in email"}
											errorText={OTPError}
											onChangeText={onOTPTextChangeHandler}
										/>
									</View>
									<View style={styles.passwordContainer}>
										<Input
											id="password"
											//label="Password"
											icon={{
												name: "lock",
												type: "MaterialCommunityIcons",
												color: "rgba(0,0,0,0.7)",
												size: 18
											}}
											keyboardType="default"
											TextInputEnable
											securedTextIcon
											textHidden
											autoCapitalize="none"
											autoCorrect={false}
											placeholder={"enter a password"}
											errorText={passwordError}
											// value={password}
											onChangeText={passwordTextChangeHandler}
										/>
									</View>
								</View>
							)}
						</View>
					</LinearGradient>
					{errorMessage.onApiCall ? (
						<View style={{ marginTop: 10, alignSelf: "center" }}>
							<Text style={{ color: Colors.red }}>{errorMessage.onApiCall}</Text>
						</View>
					) : null}
					<View style={{ marginTop: 10 }}>
						{isLoading ? (
							<ActivityIndicator size="small" color="rgba(0,0,0,0.5)" />
						) : isSignIn ? (
							<View style={{ alignSelf: "center", width: "100%" }}>
								<Button title="Sign In" onPress={onSignInHandler} color={Colors.blue2} />
								<TouchableWithoutFeedback onPress={onForgotPassword}>
									<Text style={styles.textItalics}>Forgot Password?</Text>
								</TouchableWithoutFeedback>
							</View>
						) : isNext ? (
							<Button title="Next" onPress={onNextHandler} color={Colors.blue2} />
						) : isFinalSignup ? (
							<Button title="Sign Up" onPress={onSignUpHandler} color={Colors.blue2} />
						) : null}
					</View>
				</View>
				<View style={{ bottom: "10%", alignSelf: "center" }}>
					{isSignIn ? (
						<View>
							<Text style={styles.textItalics}>Don't have a Liberwin account?</Text>
							<TouchableOpacity onPress={onSignUpNavigateHandler}>
								<Text style={styles.textButton}>Sign Up</Text>
							</TouchableOpacity>
						</View>
					) : (
						<View>
							<Text style={styles.textItalics}>Already have a Liberwin account?</Text>
							<TouchableOpacity onPress={onLoginNavigateHandler}>
								<Text style={styles.textButton}>Sign In</Text>
							</TouchableOpacity>
						</View>
					)}
				</View>
			</KeyboardAvoidingView>
		</ImageBackground>
	)
}

const styles = StyleSheet.create({
	imageBackgroundContainer: {
		/* justifyContent: "flex-start",
		alignItems: "center", */
		//width: "100%",
		height: "100%"
	},
	logoStyle: {
		height: 50,
		width: 130,
		//position: "absolute",
		top: "10%",
		alignSelf: "center"
	},
	cardContainer: {
		marginTop: 10,
		maxWidth: "90%",
		width: "90%",
		minHeight: 70,
		//shadowOpacity: 0.26,
		//elevation: 1,
		//padding: 6,
		borderRadius: 7,
		borderLeftColor: "rgba(7, 176, 249, 0.7)",
		borderRightColor: "rgba(7, 176, 249, 0.7)",
		borderTopColor: "rgba(7, 176, 249, 0.7)",
		//borderBottomColor: Color.blue2,
		borderEndWidth: 2,
		borderStartWidth: 2,
		//borderBottomWidth: 1,
		borderTopWidth: 5
	},
	usernameContainer: {
		flexDirection: "row",
		alignItems: "center",
		borderBottomColor: Colors.bluegrey,
		borderBottomWidth: 1
	},
	passwordContainer: {
		flexDirection: "row",
		alignItems: "baseline",
		borderBottomColor: Colors.bluegrey,
		borderBottomWidth: 1
	},
	OTPContainer: {
		borderBottomColor: Colors.bluegrey,
		borderBottomWidth: 1
	},
	textButton: {
		color: Colors.blue2,
		fontSize: 16,
		marginTop: 10,
		alignSelf: "center"
	},
	textItalics: {
		fontStyle: "italic",
		marginTop: 10,
		alignSelf: "center"
	}
})

export default AuthenticationScreen
