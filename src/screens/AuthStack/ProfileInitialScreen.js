import React, { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { StyleSheet, Text, View, TouchableOpacity, ScrollView, ActivityIndicator, SafeAreaView } from "react-native"
import DatePicker from "@react-native-community/datetimepicker"
import { CustomHeader } from "../../components/CustomHeader"
import Input from "../../components/Input"
import Colors from "../../constants/Colors"
import Picker from "../../components/Picker"
import Regex from "../../constants/Regex"
import { DateToString2, DateToDBString, DifferenceInYears } from "../../components/Utilities/DateFormat"

import * as profileActions from "../../store/actions/profileActions"
import * as authActions from "../../store/actions/authActions"

export const ProfileInitialScreen = props => {
	const dispatch = useDispatch()
	const isLoading = useSelector(state => state.apiCallsInProgress > 0)
	const userId = useSelector(state => state.auth.userId)

	const [profileData, setProfileData] = useState({
		userId: userId,
		firstName: "",
		lastName: "",
		dob: DateToDBString(new Date()),
		nationality: "India",
		personalEmail: "",
		mobileNumber: "",
		gender: ""
	})

	const [profileError, setProfileError] = useState({
		firstName: "",
		lastName: "",
		dob: "",
		personalEmail: "",
		mobileNumber: "",
		gender: ""
	})

	const [isDialCodePickerEnable, setIsDialCodePickerEnable] = useState(true)
	const [dialCode, setDialCode] = useState("+91")
	const [errorText, setErrorText] = useState("Vijay")
	const [errorMessage, setErrorMessage] = useState({})
	//const [showDatePicker, setShowDatePicker] = useState(false)
	const [show, setShow] = useState(false)
	const [date, setDate] = useState(new Date())
	const [today, setToday] = useState(new Date())

	// Gender selection
	const onMalePress = () => {
		setProfileData({ ...profileData, gender: "Male" })
		setProfileError({ ...profileError, gender: "" })
	}
	const onFemalePress = () => {
		setProfileData({ ...profileData, gender: "Female" })
		setProfileError({ ...profileError, gender: "" })
	}
	const onTransPress = () => {
		setProfileData({ ...profileData, gender: "Trans" })
		setProfileError({ ...profileError, gender: "" })
	}
	// on Confirm button Click - Vlidation &Profile Creation
	const onConfirmPersonalData = () => {
		if (
			profileError.firstName ||
			profileError.lastName ||
			profileError.dob ||
			profileError.mobileNumber ||
			profileError.personalEmail ||
			profileError.gender
		) {
			return
		}

		if (DifferenceInYears(new Date(), new Date(profileData.dob)) < 16) {
			setProfileError({ ...profileError, dob: "Your age has to be over 16 to register" })
			return
		}

		let isPersonalEmail = true
		let isMobileNumber = true
		const isFirstName = profileData.firstName.length > 0 ? true : false
		const isLastName = profileData.lastName.length > 0 ? true : false
		const isGender = profileData.gender.length > 0 ? true : false

		if (!isFirstName) {
			setProfileError({ ...profileError, firstName: "Please enter first name" })
			return
		}
		if (!isLastName) {
			setProfileError({ ...profileError, lastName: "Please enter last name" })
			return
		}
		if (profileData.personalEmail) {
			if (!Regex.email.test(profileData.personalEmail.toLowerCase())) {
				isPersonalEmail = profileData.personalEmail.length > 0 ? true : false
				setProfileError({ ...profileError, personalEmail: "Please enter a valid email" })
				return
			}
		}
		if (profileData.mobileNumber) {
			if (isNaN(profileData.mobileNumber)) {
				isMobileNumber = profileData.mobileNumber.length > 0 ? true : false
				setProfileError({ ...profileError, mobileNumber: "Please enter a valid mobile number" })
				return
			}
		}
		if (!isGender) {
			setProfileError({ ...profileError, gender: "Please select a gender" })
			return
		}

		if (!isFirstName || !isLastName || !isPersonalEmail || !isMobileNumber || !isGender) {
			return
		}

		if (!profileData.userId || profileData.userId === "") {
			console.log("UserId is empty")
			return
		}

		try {
			dispatch(profileActions.createProfile(profileData))
				.then(() => {
					setErrorMessage({})
				})
				.catch(error => {
					setErrorMessage({ onApiCall: error.message })
				})
		} catch (err) {
			setErrorMessage({ onApiCall: err.message })
		}
	}

	//Country Picker data response from Country Picker
	const onCountryNameSelected = receivedCountry => {
		if (receivedCountry) {
			setProfileData({ ...profileData, nationality: receivedCountry })
		}
	}

	//Dial code Picker data response from Dial code Picker
	const onDial_CodeSelected = receivedDialCode => {
		if (receivedDialCode) {
			setDialCode(receivedDialCode)
		}
	}
	// First name Validation
	const firstNameChangeHandler = text => {
		if (text) {
			if (text.length >= 1 && text.length <= 50) {
				if (!Regex.name.test(text)) {
					setProfileError({ ...profileError, firstName: "Invalid name format" })
				} else {
					setProfileError({ ...profileError, firstName: "" })
				}
			} else if (text.length === 0) {
				setProfileError({ ...profileError, firstName: "" })
			} else {
				setProfileError({ ...profileError, firstName: "Max 50 letters allowed" })
			}
		}
		setProfileData({ ...profileData, firstName: text })
	}
	// Last name Validation
	const lastNameChangeHandler = text => {
		if (text) {
			if (text.length >= 1 && text.length <= 50) {
				if (!Regex.name.test(text)) {
					setProfileError({ ...profileError, lastName: "Invalid name format" })
				} else {
					setProfileError({ ...profileError, lastName: "" })
				}
			} else if (text.length === 0) {
				setProfileError({ ...profileError, lastName: "" })
			} else {
				setProfileError({ ...profileError, lastName: "Max 50 letters allowed" })
			}
		}
		setProfileData({ ...profileData, lastName: text })
	}
	//Email Validation
	const altEmailChangeHandler = text => {
		if (text) {
			if (Regex.email.test(text.toLowerCase())) {
				setProfileData({ ...profileData, personalEmail: text.toLowerCase() })
				setProfileError({ ...profileError, personalEmail: "" })
			} else {
				setProfileError({ ...profileError, personalEmail: "Invalid email ID" })
			}
		} else {
			setProfileError({ ...profileError, personalEmail: "" })
		}
	}
	//Mobile Number validation
	const mobileTextChangeHandler = text => {
		if (text) {
			if (!isNaN(text)) {
				setProfileData({ ...profileData, mobileNumber: text })
				setProfileError({ ...profileError, mobileNumber: "" })
			} else {
				setProfileError({ ...profileError, mobileNumber: "Invalid mobile number format" })
			}
		} else {
			setProfileError({ ...profileError, mobileNumber: "" })
		}
	}

	const onChange = (event, selectedDate) => {
		const birthDate = selectedDate || date
		setShow(Platform.OS === "ios")
		setDate(birthDate)

		if (DifferenceInYears(new Date(), birthDate) < 16) {
			setProfileError({ ...profileError, dob: "Your age has to be over 16 to register" })
		} else {
			setProfileError({ ...profileError, dob: "" })
		}

		setProfileData({ ...profileData, dob: DateToDBString(birthDate) })
	}

	const showDatePicker = () => {
		setShow(!show)
	}
	return (
		<SafeAreaView style={{ flex: 1 }}>
			<CustomHeader title="home" isHome={true} isSearch={false} isNotification={false} />
			<View style={styles.formContainer}>
				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={styles.textInstructionsContainer}>
						<TouchableOpacity onPress={() => dispatch(authActions.logout())}>
							<Text style={styles.textInstructions}>{"please provide the below information"}</Text>
							<Text style={styles.textInstructions}>{"as per valid national id"}</Text>
						</TouchableOpacity>
					</View>
					<View style={styles.inputTextContainer}>
						<Input
							id="firstName"
							label="first name*"
							icon
							TextInputEnable
							keyboardType="default"
							autoCapitalize="none"
							autoCorrect={false}
							placeholder={"please enter first name"}
							errorText={profileError.firstName}
							// value={userName}
							onChangeText={firstNameChangeHandler}
						/>
					</View>
					<View style={styles.inputTextContainer}>
						<Input
							id="lastName"
							label="last name*"
							icon
							TextInputEnable
							keyboardType="default"
							autoCapitalize="none"
							autoCorrect={false}
							placeholder={"please enter last name"}
							errorText={profileError.lastName}
							// value={userName}
							onChangeText={lastNameChangeHandler}
						/>
					</View>
					<View style={{ ...styles.inputTextContainer }}>
						<View style={{ paddingBottom: 7 }}>
							<Text>birth date*</Text>
						</View>
						<View style={{ width: "100%", paddingBottom: 7, marginLeft: 10 }}>
							<TouchableOpacity style={{}} onPress={showDatePicker}>
								{/* <TextInput value={profileData.dob} editable={false} /> */}
								<Text>{DateToString2(new Date(profileData.dob))}</Text>
							</TouchableOpacity>
						</View>
						{show && (
							<DatePicker
								testID="dateTimePicker"
								timeZoneOffsetInMinutes={0}
								value={date}
								mode="date"
								maximumDate={today}
								is24Hour={true}
								display="default"
								onChange={onChange}
							/>
						)}

						{profileError.dob.length > 0 && <Text style={styles.errorText}>{profileError.dob}</Text>}
					</View>
					<View style={{ ...styles.inputTextContainer, justifyContent: "space-between", paddingBottom: 5 }}>
						<Text style={{ paddingBottom: 7 }}>nationality*</Text>
						<Picker pickerType="CountryPicker" onPickerSelected={onCountryNameSelected}></Picker>
						{profileError.nationality > 0 && <Text>{profileError.nationality}</Text>}
					</View>
					<View style={styles.inputTextContainer}>
						<Input
							id="personalEmail"
							label="alternate email"
							icon
							TextInputEnable
							keyboardType="default"
							autoCapitalize="none"
							autoCorrect={false}
							placeholder={"please enter alternate email"}
							errorText={profileError.personalEmail}
							// value={userName}
							onChangeText={altEmailChangeHandler}
						/>
					</View>
					<View style={styles.inputTextContainer}>
						<Input
							id="mobileNumber"
							label="mobile number"
							icon
							keyboardType="numeric"
							pickerType="DialCodePicker"
							PickerEnable={isDialCodePickerEnable}
							pickerWhenToEnable
							TextInputEnable
							errorText={profileError.mobileNumber}
							placeholder={"please enter mobile number"}
							// value={userName}
							onPickerSelected={onDial_CodeSelected}
							onChangeText={mobileTextChangeHandler}
							//onBlur={userNameTextChangeHandler}
						/>
					</View>
					<Text style={{ paddingLeft: 10, paddingTop: 5 }}>gender*</Text>
					<View style={styles.genderContainer}>
						<TouchableOpacity
							style={{ ...styles.gender, backgroundColor: profileData.gender === "Male" ? Colors.blue1 : Colors.bluegrey }}
							onPress={onMalePress}
						>
							<Text style={{ color: profileData.gender === "Male" ? Colors.white : Colors.black }}>MALE</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={{ ...styles.gender, backgroundColor: profileData.gender === "Female" ? Colors.blue1 : Colors.bluegrey }}
							onPress={onFemalePress}
						>
							<Text style={{ color: profileData.gender === "Female" ? Colors.white : Colors.black }}>FEMALE</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={{ ...styles.gender, backgroundColor: profileData.gender === "Trans" ? Colors.blue1 : Colors.bluegrey }}
							onPress={onTransPress}
						>
							<Text style={{ color: profileData.gender === "Trans" ? Colors.white : Colors.black }}>TRANS</Text>
						</TouchableOpacity>
					</View>
					{profileError.gender.length > 0 && <Text style={styles.errorText}>{profileError.gender}</Text>}
				</ScrollView>
				<View style={{ marginHorizontal: "5%" }}>
					{isLoading ? (
						<ActivityIndicator size="small" color="rgba(0,0,0,0.5)" style={{ marginVertical: "2%" }} />
					) : (
						<View style={{ marginVertical: "2%", borderRadius: 5, backgroundColor: Colors.green1 }}>
							<TouchableOpacity onPress={() => onConfirmPersonalData()} style={{}}>
								<Text style={styles.buttonText}>CREATE PROFILE</Text>
							</TouchableOpacity>
						</View>
					)}
				</View>
			</View>
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	formContainer: {
		flex: 1,
		justifyContent: "center"
	},
	inputTextContainer: {
		paddingLeft: 10,
		paddingVertical: "1%",
		borderBottomWidth: 1,
		borderBottomColor: Colors.bluegrey
	},
	buttonContainer: {
		//width: "95%",
		marginVertical: "2%",
		//flexDirection: "row"
		alignSelf: "center"
	},
	textInstructionsContainer: {
		alignSelf: "center",
		paddingVertical: "5%"
	},
	textInstructions: {
		textAlign: "center",
		fontWeight: "400",
		fontSize: 16,
		fontStyle: "italic",
		marginTop: 5
	},
	genderContainer: {
		flexDirection: "row",
		justifyContent: "space-evenly",
		paddingLeft: 10,
		width: "60%",
		paddingTop: "2%"
	},
	gender: {
		padding: 5,
		borderRadius: 5,
		width: "30%",
		alignItems: "center",
		justifyContent: "center"
	},
	errorText: {
		color: Colors.red,
		fontSize: 12,
		paddingLeft: 1
	},
	buttonText: {
		fontSize: 18,
		fontWeight: "bold",
		alignSelf: "center",
		padding: "2%",
		color: "#fff"
	}
})

export default ProfileInitialScreen
