import React from "react"
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, ScrollView } from "react-native"
import { useSelector } from "react-redux"

import { CustomHeader } from "../../components/CustomHeader"
import Colors from "../../constants/Colors"
import { MaterialIcons } from "@expo/vector-icons"

const AccountScreen = ({ navigation }) => {
	// let lstAccounts = useSelector(state => state.account.lstAccounts)
	const isAccount = useSelector(state => state.isAccount)
	const account = useSelector(state => state.account.accountDetails)
	const orgDetail = useSelector(state => state.orgDetail)
	let orgUnitName = ""
	if (orgDetail && orgDetail.orgUnits && orgDetail.orgUnits.length > 0) {
		const selOrgDetail = orgDetail.orgUnits.find(org => org.orgUnitId === account.orgUnitId)
		if (selOrgDetail && selOrgDetail.orgUnitName) {
			orgUnitName = selOrgDetail.orgUnitName
		}
	}

	return (
		<SafeAreaView style={styles.containerStyle}>
			<CustomHeader title="account" isHome={false} isSearch={true} isNotification={true} navigation={navigation} orgId={account.orgId} />
			{isAccount ? (
				<ScrollView showsVerticalScrollIndicator={false}>
					<View style={{ flex: 1, marginTop: 20, width: "90%", alignSelf: "center" }}>
						<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginBottom: 20 }}>
							<View style={{ alignItems: "center", flex: 1 }}>
								<Text style={styles.orgNameTextStyle}>{orgDetail.orgName ? orgDetail.orgName : ""}</Text>
								<Text style={{ marginTop: 10 }}>{account.accountEmail ? account.accountEmail : ""}</Text>
							</View>
							<TouchableOpacity onPress={() => navigation.navigate("AccountCreate", { isEdit: true })}>
								<MaterialIcons style={styles.editIconStyle} name="edit" />
							</TouchableOpacity>
						</View>
						<View style={styles.textContainer}>
							<Text>organization unit</Text>
							<Text style={styles.textStyle}>{orgUnitName}</Text>
						</View>
						<View style={styles.textContainer}>
							<Text>employee id </Text>
							<Text style={styles.textStyle}>{account.orgPersonId ? account.orgPersonId : ""}</Text>
						</View>
						<View style={styles.textContainer}>
							<Text>organization role</Text>
							<Text style={styles.textStyle}>{account.orgPersonRole ? account.orgPersonRole : ""}</Text>
						</View>
						<View style={styles.textContainer}>
							<Text>supervisor email</Text>
							<Text style={styles.textStyle}>{account.supervisorEmail ? account.supervisorEmail : ""}</Text>
						</View>
						<View style={styles.textContainer}>
							<Text>employee location</Text>
							<Text style={styles.textStyle}>{account.personLocation ? account.personLocation : ""}</Text>
						</View>
						<View style={styles.textContainer}>
							<Text>worker type</Text>
							<Text style={styles.textStyle}>{account.workerType ? account.workerType : ""}</Text>
						</View>
					</View>
				</ScrollView>
			) : (
				<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
					<Text style={{ fontWeight: "bold" }}>You have not created an account</Text>
				</View>
			)}
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	containerStyle: {
		flex: 1
	},
	orgNameTextStyle: {
		//marginTop: 5,
		fontSize: 20,
		fontWeight: "500",
		alignSelf: "center"
	},
	editIconStyle: {
		fontSize: 25,
		color: "rgba(0,0,0,0.5)"
		//marginRight: 20
	},
	textContainer: {
		paddingLeft: 10,
		paddingVertical: "2%",
		borderBottomWidth: 1,
		borderBottomColor: Colors.bluegrey
	},
	textStyle: {
		marginTop: 10,
		marginLeft: 5,
		fontSize: 16
	},
	blockViewStyle: {
		flexDirection: "row",
		justifyContent: "space-around",
		width: "90%",
		alignSelf: "center"
	}
})

export default AccountScreen
