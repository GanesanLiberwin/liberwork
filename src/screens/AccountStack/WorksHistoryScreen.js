import React, { useState, useEffect } from "react"
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, FlatList, ActivityIndicator } from "react-native"
import { useSelector, useDispatch } from "react-redux"
import { LinearGradient } from "expo-linear-gradient"

import { CustomHeader2 } from "../../components/CustomHeader2"
import ClosedWorkCard from "../../components/Work/ClosedWorkCard"

import * as workFlowActions from "../../store/actions/workFlowActions"
import * as workActions from "../../store/actions/workActions"
import { pageLabelJson } from "../../constants/PageLabelJson"

const WorksHistoryScreen = ({ navigation }) => {
	const dispatch = useDispatch()
	const isLoading = useSelector(state => state.apiCallsInProgress > 0)

	const orgId = useSelector(state => state.account.accountDetails.orgId)
	const accountId = useSelector(state => state.account.selectedAccountId)
	const ClosedWorks = useSelector(state => state.works.closedWorks)

	const [refreshing, setRefreshing] = useState(false)

	// { 	postedWorks: [], 	delegatedWorks: [],	allocatedWorks: [] }
	const [workRole, setWorkRole] = useState("postedWorks")

	useEffect(() => {
		getServerData()
	}, [dispatch, accountId])

	const getServerData = () => {
		if (accountId !== "") {
			try {
				let closedWorksCount = 0
				if (ClosedWorks) {
					if (ClosedWorks.postedWorks) {
						closedWorksCount += ClosedWorks.postedWorks.length
					}
					if (ClosedWorks.delegatedWorks) {
						closedWorksCount += ClosedWorks.delegatedWorks.length
					}
					if (ClosedWorks.allocatedWorks) {
						closedWorksCount += ClosedWorks.allocatedWorks.length
					}
				}
				if (closedWorksCount === 0) {
					dispatch(workActions.getClosedWorks(accountId)) //same used in onRefresh...not working here
				}
			} catch (err) {}
		}
	}

	const onRefresh = () => {
		setRefreshing(true)
		dispatch(workActions.getClosedWorks(accountId))
		setRefreshing(false)
	}

	const OnShowWorks = segment => {
		switch (segment) {
			case "posted":
				setWorkRole("postedWorks")
				break
			case "delegated":
				setWorkRole("delegatedWorks")
				break
			case "allocated":
				setWorkRole("allocatedWorks")
				break
			default:
				break
		}
	}

	const NavigateWork = item => {
		dispatch(workFlowActions.updateWorkFlowId(item.workId))
		navigation.navigate("WorkDetail", { item: item })
	}

	const OnFinish = () => {
		navigation.goBack()
	}

	return (
		<SafeAreaView style={{ flex: 1 }}>
			<CustomHeader2 title="work history" isLogo={false} isClose={false} isBack={true} navigation={navigation} onPress={OnFinish} orgId={orgId} />
			<View style={styles.blockViewStyle}>
				<LinearGradient
					style={{ ...styles.linearGradient, width: "33%", borderRadius: 5 }}
					colors={workRole === "postedWorks" ? ["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.1)"] : ["rgba(0, 0, 0, 0.3)", "rgba(0, 0, 0, 0.05)"]}
				>
					<TouchableOpacity onPress={() => OnShowWorks("posted")}>
						<Text style={styles.headerText}>posted</Text>
					</TouchableOpacity>
				</LinearGradient>
				<LinearGradient
					style={{ ...styles.linearGradient, width: "30%", borderRadius: 5 }}
					colors={
						workRole === "delegatedWorks" ? ["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.1)"] : ["rgba(0, 0, 0, 0.3)", "rgba(0, 0, 0, 0.05)"]
					}
				>
					<TouchableOpacity onPress={() => OnShowWorks("delegated")}>
						<Text style={styles.headerText}>delegated</Text>
					</TouchableOpacity>
				</LinearGradient>
				<LinearGradient
					style={{ ...styles.linearGradient, width: "30%", borderRadius: 5 }}
					colors={
						workRole === "allocatedWorks" ? ["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.1)"] : ["rgba(0, 0, 0, 0.3)", "rgba(0, 0, 0, 0.05)"]
					}
				>
					<TouchableOpacity onPress={() => OnShowWorks("allocated")}>
						<Text style={styles.headerText}>worked</Text>
					</TouchableOpacity>
				</LinearGradient>
			</View>
			{isLoading ? (
				<ActivityIndicator size="small" color="rgba(0,0,0,0.5)" />
			) : (
				<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
					<FlatList
						showsVerticalScrollIndicator={false}
						refreshing={refreshing}
						onRefresh={() => onRefresh()}
						data={
							workRole === "postedWorks"
								? ClosedWorks.postedWorks
								: workRole === "delegatedWorks"
								? ClosedWorks.delegatedWorks
								: ClosedWorks.allocatedWorks
						}
						keyExtractor={item => item.workId}
						ListEmptyComponent={() => <Text style={styles.emptyTextStyle}>{pageLabelJson.Work.NoWorks}</Text>}
						renderItem={({ item, index }) => (
							<TouchableOpacity onPress={() => NavigateWork(item)}>
								<ClosedWorkCard header={item.workId} category={item.functionalDomain} status={item.currentStatus} date={item.currentStatusDate} />
							</TouchableOpacity>
						)}
					/>
				</View>
			)}
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	emptyTextStyle: {
		marginVertical: 10,
		marginLeft: 25,
		fontWeight: "300",
		fontStyle: "italic"
	},
	blockViewStyle: {
		flexDirection: "row",
		justifyContent: "space-between",
		marginTop: 15,
		marginBottom: 10,
		marginHorizontal: "5%"
	},
	linearGradient: {
		borderRadius: 5
	},
	headerText: {
		fontSize: 15,
		alignSelf: "center",
		padding: "2%"
	}
})

export default WorksHistoryScreen
