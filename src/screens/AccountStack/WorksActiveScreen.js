import React, { useState, useEffect } from "react"
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, FlatList, ActivityIndicator } from "react-native"
import { useSelector, useDispatch } from "react-redux"
import { LinearGradient } from "expo-linear-gradient"

import { CustomHeader2 } from "../../components/CustomHeader2"
import ClosedWorkCard from "../../components/Work/ClosedWorkCard"

import * as workFlowActions from "../../store/actions/workFlowActions"
import * as workActions from "../../store/actions/workActions"
import { pageLabelJson } from "../../constants/PageLabelJson"
import { workStatusJson } from "../../constants/PageJson"

const WorksActiveScreen = ({ navigation }) => {
	const dispatch = useDispatch()
	const isLoading = useSelector(state => state.apiCallsInProgress > 0)

	const orgId = useSelector(state => state.account.accountDetails.orgId)
	const accountId = useSelector(state => state.account.selectedAccountId)

	const postedWorks = useSelector(state => state.works.lstAvailableWorks.filter(w => !w.delegateWork))
	const delegatedWorks = useSelector(state => state.works.lstAvailableWorks.filter(w => w.delegateWork))
	const allocatedWorks = useSelector(state =>
		state.works.lstEligibleWorks.filter(
			w =>
				w.allocationStatus === workStatusJson.Allocated &&
				w.currentStatus != workStatusJson.Closed &&
				w.currentStatus != workStatusJson.Cancelled &&
				w.currentStatus != workStatusJson.OptedOut
		)
	)

	const [refreshing, setRefreshing] = useState(false)

	// { 	postedWorks: [], 	delegatedWorks: [],	allocatedWorks: [] }
	const [workRole, setWorkRole] = useState("postedWorks")

	useEffect(() => {
		//getServerData()
	}, [dispatch, accountId])

	const getServerData = () => {
		if (accountId !== "") {
			try {
				dispatch(workActions.getAvailableWorks(accountId))
			} catch (err) {}
			try {
				dispatch(workActions.getEligibleWorks(accountId))
			} catch (err) {}
		}
	}

	const onRefresh = () => {
		setRefreshing(true)
		getServerData()
		setRefreshing(false)
	}

	const OnShowWorks = segment => {
		switch (segment) {
			case "posted":
				setWorkRole("postedWorks")
				break
			case "delegated":
				setWorkRole("delegatedWorks")
				break
			case "allocated":
				setWorkRole("allocatedWorks")
				break
			default:
				break
		}
	}

	const NavigateWork = item => {
		switch (item.currentStatus) {
			case workStatusJson.Draft:
				dispatch(workActions.updateDraftId(item.workId))
				navigation.navigate("WorkDraft")
				break

			case workStatusJson.BidEnded:
			case workStatusJson.Published:
			case workStatusJson.Posted:
				dispatch(workFlowActions.updateWorkFlowId(item.workId, ""))
				navigation.navigate("WorkBid", { item: item })
				break

			default:
				dispatch(workFlowActions.updateWorkFlowId(item.workId, ""))
				navigation.navigate("WorkDetail", { item: item })
				break
		}
	}

	const OnFinish = () => {
		navigation.goBack()
	}

	return (
		<SafeAreaView style={{ flex: 1 }}>
			<CustomHeader2 title="work active" isLogo={false} isClose={false} isBack={true} navigation={navigation} onPress={OnFinish} orgId={orgId} />
			<View style={styles.blockViewStyle}>
				<LinearGradient
					style={{ ...styles.linearGradient, width: "33%", borderRadius: 5 }}
					colors={workRole === "postedWorks" ? ["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.1)"] : ["rgba(0, 0, 0, 0.3)", "rgba(0, 0, 0, 0.05)"]}
				>
					<TouchableOpacity onPress={() => OnShowWorks("posted")}>
						<Text style={styles.headerText}>posted</Text>
					</TouchableOpacity>
				</LinearGradient>
				<LinearGradient
					style={{ ...styles.linearGradient, width: "30%", borderRadius: 5 }}
					colors={
						workRole === "delegatedWorks" ? ["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.1)"] : ["rgba(0, 0, 0, 0.3)", "rgba(0, 0, 0, 0.05)"]
					}
				>
					<TouchableOpacity onPress={() => OnShowWorks("delegated")}>
						<Text style={styles.headerText}>delegated</Text>
					</TouchableOpacity>
				</LinearGradient>
				<LinearGradient
					style={{ ...styles.linearGradient, width: "30%", borderRadius: 5 }}
					colors={
						workRole === "allocatedWorks" ? ["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.1)"] : ["rgba(0, 0, 0, 0.3)", "rgba(0, 0, 0, 0.05)"]
					}
				>
					<TouchableOpacity onPress={() => OnShowWorks("allocated")}>
						<Text style={styles.headerText}>working</Text>
					</TouchableOpacity>
				</LinearGradient>
			</View>
			{isLoading ? (
				<ActivityIndicator size="small" color="rgba(0,0,0,0.5)" />
			) : (
				<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
					<FlatList
						showsVerticalScrollIndicator={false}
						refreshing={refreshing}
						onRefresh={() => onRefresh()}
						data={workRole === "postedWorks" ? postedWorks : workRole === "delegatedWorks" ? delegatedWorks : allocatedWorks}
						keyExtractor={item => item.workId}
						ListEmptyComponent={() => <Text style={styles.emptyTextStyle}>{pageLabelJson.Work.NoWorks}</Text>}
						renderItem={({ item, index }) => (
							<TouchableOpacity onPress={() => NavigateWork(item)}>
								<ClosedWorkCard header={item.workId} category={item.functionalDomain} status={item.currentStatus} date={item.plannedEndDate} />
							</TouchableOpacity>
						)}
					/>
				</View>
			)}
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	emptyTextStyle: {
		marginVertical: 10,
		marginLeft: 25,
		fontWeight: "300",
		fontStyle: "italic"
	},
	blockViewStyle: {
		flexDirection: "row",
		justifyContent: "space-between",
		marginTop: 15,
		marginBottom: 10,
		marginHorizontal: "5%"
	},
	linearGradient: {
		borderRadius: 5
	},
	headerText: {
		fontSize: 15,
		alignSelf: "center",
		padding: "2%"
	}
})

export default WorksActiveScreen
