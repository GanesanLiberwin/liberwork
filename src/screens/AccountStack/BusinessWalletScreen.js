import React, { useCallback } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useFocusEffect } from "@react-navigation/native"

import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, FlatList, ScrollView } from "react-native"
import { CustomHeader2 } from "../../components/CustomHeader2"
import Colors from "../../constants/Colors"
import WalletCard from "../../components/Wallet/WalletCard"
import WalletTransCard from "../../components/Wallet/WalletTransCard"
import WalletTransferCard from "../../components/Wallet/WalletTransferCard"
import AccountValidation from "../../components/Account/AccountValidation"

import * as accountActions from "../../store/actions/accountActions"

const BusinessWalletScreen = ({ navigation }) => {
	const dispatch = useDispatch()

	const accountValidated = useSelector(state => state.account.accountVerified.isValid)

	const orgId = useSelector(state => state.account.accountDetails.orgId)

	const curBalance = useSelector(state =>
		state.wallets.BusinessWallet && state.wallets.BusinessWallet.balance ? state.wallets.BusinessWallet.balance : 0
	)
	const resBalance = useSelector(state =>
		state.wallets.BusinessWallet && state.wallets.BusinessWallet.reservedAmount ? state.wallets.BusinessWallet.reservedAmount : 0
	)
	const results = useSelector(state =>
		state.wallets.BusinessWallet && state.wallets.BusinessWallet.trxHistory ? state.wallets.BusinessWallet.trxHistory : []
	)

	useFocusEffect(
		useCallback(() => {
			return () => {
				dispatch(accountActions.clearAccountVerifiedData())
			}
		}, [])
	)

	const OnFinish = () => {
		navigation.goBack()
	}

	return (
		<SafeAreaView style={styles.containerStyle}>
			<CustomHeader2 title="business wallet" isLogo={false} isClose={false} isBack={true} orgId={orgId} navigation={navigation} onPress={OnFinish} />
			<ScrollView showsVerticalScrollIndicator={false}>
				<View style={{ marginVertical: 25, width: "90%", alignSelf: "center" }}>
					<WalletCard curBalance={curBalance} resBalance={resBalance} subHeader1="Reserve" subHeader2="Total" />
				</View>
				<View style={{ flex: 1, width: "90%", alignSelf: "center", minHeight: "30%" }}>
					<Text style={{ ...styles.headerText, marginLeft: 15 }}>Transfer To</Text>
					{curBalance > 0 ? <AccountValidation /> : <Text style={styles.emptyTextStyle}>Insufficient balance and please load Wallet</Text>}
					{accountValidated && <WalletTransferCard type="business" />}
				</View>
			</ScrollView>
			<View style={{ marginVertical: 25, width: "90%", alignSelf: "center" }}>
				<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
					<Text style={{ ...styles.headerText, marginLeft: 15 }}>Recent</Text>
					<TouchableOpacity onPress={() => navigation.navigate("WalletTrans", { type: "business" })}>
						<Text style={{ ...styles.headerText, fontWeight: "500", color: Colors.blue2, marginRight: 15 }}>more</Text>
					</TouchableOpacity>
				</View>
				<View>
					<FlatList
						data={results}
						keyExtractor={result => result.date.toString()}
						ListEmptyComponent={() => <Text style={styles.emptyTextStyle}>There are no recent transactions</Text>}
						renderItem={({ item, index }) => (
							<WalletTransCard type={item.type} reason={item.reason} date={item.date} transAmount={item.amount} balance={item.balance} />
						)}
					/>
				</View>
			</View>
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	containerStyle: {
		flex: 1
	},
	/* bodyViewStyle: {
		flex: 1,
		alignItems: "center"
	}, */
	headerText: {
		color: "rgba(0,0,0,0.5)",
		fontSize: 14,
		fontStyle: "italic"
	},
	emptyTextStyle: {
		marginTop: 10,
		marginLeft: 25
	}
})

export default BusinessWalletScreen
