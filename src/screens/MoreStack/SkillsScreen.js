import React, { useState, useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, TouchableHighlight, Alert, Modal, ActivityIndicator, ScrollView } from "react-native"
import { Octicons } from "@expo/vector-icons"
import { LinearGradient } from "expo-linear-gradient"

import { CustomHeader2 } from "../../components/CustomHeader2"
import SkillCard from "../../components/Skill/SkillCard"
import Search from "../../components/Utilities/Search"
import Colors from "../../constants/Colors"

import * as searchActions from "../../store/actions/searchActions"
import * as profileActions from "../../store/actions/profileActions"

const SkillsScreen = ({ navigation }) => {
	const dispatch = useDispatch()
	const isLoading = useSelector(state => state.apiCallsInProgress > 0)

	let userId = useSelector(state => state.auth.userId)
	const profileId = useSelector(state => state.account.accountDetails.profileId)
	const ProfileSkills = useSelector(state => state.profile.lstProfileSkills)

	const NumberOfSelection = 15
	const SelectedSkills = useSelector(state => state.search.lstSkills)
	const [modalVisible, setModalVisible] = useState(false)

	useEffect(() => {
		if (userId !== "") {
			try {
				dispatch(profileActions.getProfileSkills(userId, true))
			} catch (err) {}
		}
	}, [dispatch, userId])

	const showAlert = () =>
		Alert.alert(
			"Select Skill",
			"Atleast one skill with MUST required",
			[
				{
					text: "OK"
				}
			],
			{ cancelable: false }
		)

	const clearSelectSkills = () => {
		dispatch(searchActions.clearSearchSkills())
	}

	const onModalClose = () => {
		if (!SelectedSkills.some(s => s.required === true) && SelectedSkills.length != 0) {
			showAlert()
		} else {
			setModalVisible(!modalVisible)
			dispatch(searchActions.clearFetchSkills())
		}
	}

	const onModalOpen = () => {
		setModalVisible(true)
	}

	const onAddSkills = async () => {
		try {
			let lstSkills = []
			let skillIndex = -1
			SelectedSkills.map((item, index) => {
				skillIndex = -1
				if (ProfileSkills && ProfileSkills.length > 0) {
					skillIndex = ProfileSkills.findIndex(s => s.skillId === item.id)
				}
				//filter existing profile skills
				if (skillIndex === -1) {
					lstSkills.push({ skillId: item.id, skillName: item.skill })
				}
			})

			if (lstSkills.length > 0) {
				dispatch(profileActions.addSkillToProfile(profileId, lstSkills, ""))
					.then(data => {
						// clearSearchSkills on add success
						// clearSelectSkills()
					})
					.catch(error => {
						// error msg
					})
			}
		} catch (err) {
			// error msg
		}
	}

	const OnFinish = () => {
		navigation.goBack()
	}

	return (
		<SafeAreaView style={{ flex: 1 }}>
			<CustomHeader2 title="skills" isLogo={true} isClose={false} isBack={true} navigation={navigation} onPress={OnFinish} />
			<View style={{ flexDirection: "row", marginHorizontal: 20, marginTop: "4%", alignItems: "center", alignSelf: "center" }}>
				<TouchableOpacity onPress={() => onModalOpen()}>
					<LinearGradient style={styles.linearGradient} colors={["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.5)"]}>
						<Octicons name="plus" style={styles.iconPlusStyle} />
					</LinearGradient>
					{<Text style={{ fontSize: 13, fontStyle: "italic", marginTop: 5 }}>skills</Text>}
				</TouchableOpacity>
				{SelectedSkills.length > 0 && (
					<View style={{ flex: 1 }}>
						<Text style={{ ...styles.headerText, marginLeft: 15 }}>Selected skills</Text>
						<View style={styles.itemList}>
							{SelectedSkills.map((item, index) => {
								return (
									<Text key={item.skill + index} style={{ margin: 7, fontSize: 12, borderWidth: 1, borderRadius: 5, padding: 2 }}>
										{item.skill}
									</Text>
								)
							})}
						</View>
						<View style={{ flexDirection: "row", justifyContent: "center" }}>
							<TouchableOpacity
								style={{ borderRadius: 5, padding: 4, backgroundColor: Colors.orange1, marginHorizontal: 10 }}
								onPress={() => clearSelectSkills()}
							>
								<Text style={{ ...styles.buttonTextStyle, color: "#fff" }}>CLEAR</Text>
							</TouchableOpacity>
							{isLoading ? (
								<ActivityIndicator size="small" color="rgba(0,0,0,0.5)" />
							) : (
								<TouchableOpacity style={{ borderRadius: 5, padding: 4, backgroundColor: Colors.blue2 }} onPress={() => onAddSkills()}>
									<Text style={{ ...styles.buttonTextStyle, color: "#fff" }}>ADD TO PROFILE</Text>
								</TouchableOpacity>
							)}
						</View>
					</View>
				)}
			</View>

			<ScrollView style={{ marginTop: "2%" }}>
				{ProfileSkills.map((item, index) => {
					return (
						<TouchableOpacity
							key={item.skillName + index}
							style={{ width: "90%", alignSelf: "center" }}
							onPress={() => navigation.navigate("SkillDetail", { item: item })}
						>
							<SkillCard
								skill={item.skillName}
								score={item.score ? item.score : 0}
								numberOfWorks={item.workCount ? item.workCount : 0}
								numberOfHours={item.hoursWorked ? item.hoursWorked : 0}
								isValidated={item.verified}
							/>
						</TouchableOpacity>
					)
				})}
			</ScrollView>
			<View style={styles.centeredView}>
				<Modal
					animationType="slide"
					transparent={true}
					visible={modalVisible}
					onRequestClose={() => {
						setModalVisible(!modalVisible)
					}}
				>
					<View style={styles.centeredView}>
						<View style={styles.modalView}>
							<TouchableHighlight style={{ marginVertical: 10, alignSelf: "flex-end" }} onPress={() => onModalClose()}>
								<Text style={styles.buttonTextStyle}>CLOSE</Text>
							</TouchableHighlight>
							<Search numberOfSelections={NumberOfSelection} searchType="Skills" selectedList={SelectedSkills} preferenceSelection={false} />
						</View>
					</View>
				</Modal>
			</View>
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	modalView: {
		width: "90%",
		height: "70%",
		backgroundColor: Colors.BGDark,
		borderRadius: 7,
		padding: 10,
		alignItems: "center"
	},
	buttonTextStyle: {
		color: Colors.TextLight,
		//fontSize: 12,
		fontWeight: "bold",
		textAlign: "center"
	},
	linearGradient: {
		height: 32,
		width: 32,
		borderRadius: 16,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "center"
	},
	iconPlusStyle: {
		fontSize: 25,
		color: "#fff"
	},
	itemList: {
		flexDirection: "row",
		justifyContent: "center",
		marginVertical: 15,
		flexWrap: "wrap"
	},
	headerText: {
		textAlign: "center",
		fontSize: 14,
		fontStyle: "italic"
	}
})

export default SkillsScreen
