import React, { useEffect } from "react"
import { View, Image, Text, SafeAreaView, StyleSheet, TouchableOpacity, ScrollView } from "react-native"
import { useSelector, useDispatch } from "react-redux"

import { CustomHeader2 } from "../../components/CustomHeader2"
import SkillCard from "../../components/Skill/SkillCard"
import { DateToString2 } from "../../components/Utilities/DateFormat"

import { MaterialIcons } from "@expo/vector-icons"
import { IMAGE } from "../../constants/Image"
import Colors from "../../constants/Colors"

import * as profileActions from "../../store/actions/profileActions"

const ProfileScreen = ({ navigation }) => {
	const dispatch = useDispatch()
	let userId = useSelector(state => state.auth.userId)

	const ProfileSkills = useSelector(state => state.profile.lstProfileSkills.filter(s => s.verified))

	useEffect(() => {
		if (userId !== "") {
			try {
				dispatch(profileActions.getProfile(userId, true))
			} catch (err) {}
			try {
				dispatch(profileActions.getProfileSkills(userId, false))
			} catch (err) {}
		}
	}, [dispatch, userId])

	let firstName = useSelector(state => state.profile.firstName)
	let lastName = useSelector(state => state.profile.lastName)
	let dateOfBirth = useSelector(state => state.profile.dob)
	let nationality = useSelector(state => state.profile.nationality)
	let personalEmail = useSelector(state => state.profile.personalEmail)
	let gender = useSelector(state => state.profile.gender)
	let userName = useSelector(state => state.auth.userName)

	const OnFinish = () => {
		navigation.goBack()
	}

	return (
		<SafeAreaView style={styles.containerStyle}>
			<CustomHeader2 title="profile" isLogo={true} isClose={false} isBack={true} navigation={navigation} onPress={OnFinish} />
			<View style={styles.profileBlock}>
				<Image style={styles.profileImageStyle} source={IMAGE.ICON_PROFILE} resizeMode="contain" />
				<View style={styles.profileUserNameStyle}>
					<Text style={{ marginBottom: 10, fontSize: 18 }}>
						{firstName} {lastName}
					</Text>
					<Text>{userName}</Text>
				</View>
				<TouchableOpacity>
					<MaterialIcons style={styles.editIconStyle} name="edit" />
				</TouchableOpacity>
			</View>
			<ScrollView showsVerticalScrollIndicator={false}>
				<View style={{ marginTop: 25, width: "90%", alignSelf: "center" }}>
					<View style={styles.textContainer}>
						<Text>date of birth</Text>
						<Text style={styles.textStyle}>{dateOfBirth && DateToString2(new Date(dateOfBirth))}</Text>
					</View>
					<View style={styles.textContainer}>
						<Text>nationality</Text>
						<Text style={styles.textStyle}>{nationality}</Text>
					</View>
					<View style={styles.textContainer}>
						<Text>personal email</Text>
						<Text style={styles.textStyle}>{personalEmail}</Text>
					</View>
					<View style={styles.textContainer}>
						<Text>gender</Text>
						<Text style={styles.textStyle}>{gender}</Text>
					</View>
				</View>
				{ProfileSkills.length > 0 && (
					<View style={{ width: "90%", alignSelf: "center" }}>
						<View style={styles.textContainer}>
							<Text>top skills</Text>
							{ProfileSkills.map((item, index) => {
								return (
									<TouchableOpacity key={item.skillName + index} style={{}} onPress={() => navigation.navigate("Skills")}>
										<SkillCard
											skill={item.skillName}
											score={item.score ? item.score : 0}
											numberOfWorks={item.workCount ? item.workCount : 0}
											numberOfHours={item.hoursWorked ? item.hoursWorked : 0}
											isValidated={item.verified}
										/>
									</TouchableOpacity>
								)
							})}
						</View>
					</View>
				)}
			</ScrollView>
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	containerStyle: {
		flex: 1
	},
	profileBlock: {
		padding: 10, //for android
		alignItems: "center",
		flexDirection: "row",
		marginHorizontal: 10
		//backgroundColor: "grey"
	},
	profileImageStyle: {
		//marginLeft: 20,
		height: 70,
		width: 70
	},
	profileUserNameStyle: {
		flex: 1,
		marginHorizontal: 5,
		alignItems: "center"
	},
	editIconStyle: {
		fontSize: 25,
		color: "rgba(0,0,0,0.5)"
		//marginRight: 20
	},
	textContainer: {
		paddingHorizontal: 10,
		paddingVertical: "2%",
		borderBottomWidth: 1,
		borderBottomColor: Colors.bluegrey
	},
	textStyle: {
		marginTop: 10,
		marginLeft: 5,
		fontSize: 16
	},
	headerText: {
		marginHorizontal: 10,
		fontStyle: "italic"
	}
})

export default ProfileScreen
