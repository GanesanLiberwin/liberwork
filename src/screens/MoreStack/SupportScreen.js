import React from "react"
import { View, Text, SafeAreaView } from "react-native"
import { CustomHeader2 } from "../../components/CustomHeader2"

const SupportScreen = ({ navigation }) => {
	const OnFinish = () => {
		navigation.goBack()
	}

	return (
		<SafeAreaView style={{ flex: 1 }}>
			<CustomHeader2 title="support" isLogo={true} isClose={false} isBack={true} navigation={navigation} onPress={OnFinish} />
			<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
				<Text style={{ fontWeight: "bold" }}>Support Screen</Text>
			</View>
		</SafeAreaView>
	)
}

export default SupportScreen
