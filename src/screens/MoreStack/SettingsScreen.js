import React from "react"
import { View, Image, Text, SafeAreaView } from "react-native"
import { CustomHeader2 } from "../../components/CustomHeader2"

const SettingsScreen = ({ navigation }) => {
	const OnFinish = () => {
		navigation.goBack()
	}

	return (
		<SafeAreaView style={{ flex: 1 }}>
			<CustomHeader2 title="settings" isLogo={true} isClose={false} isBack={true} navigation={navigation} onPress={OnFinish} />
			<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
				<Text style={{ fontWeight: "bold" }}>Settings Screen</Text>
			</View>
		</SafeAreaView>
	)
}

export default SettingsScreen
