import React, { useState, useEffect } from "react"
import { View, Image, Text, SafeAreaView, StyleSheet, TouchableOpacity, Platform, StatusBar } from "react-native"
import { useSelector, useDispatch } from "react-redux"

import { Ionicons, Octicons } from "@expo/vector-icons"
import { LinearGradient } from "expo-linear-gradient"
import Colors from "../../constants/Colors"
import { IMAGE } from "../../constants/Image"

import * as authActions from "../../store/actions/authActions"

const MoreScreen = ({ navigation }) => {
	const dispatch = useDispatch()

	const isAccount = useSelector(state => state.isAccount)
	const account = useSelector(state => state.account.accountDetails)

	let firstName = useSelector(state => state.profile.firstName)
	let lastName = useSelector(state => state.profile.lastName)
	let userName = useSelector(state => state.auth.userName)
	let userNameType = useSelector(state => state.auth.userNameType)
	let lstAccounts = useSelector(state => state.account.lstAccounts)

	const [isAccountCreated, setIsAccountCreated] = useState(false)

	useEffect(() => {
		if (userNameType === "E" && lstAccounts && lstAccounts.length > 0) {
			const selAccount = lstAccounts.find(acc => acc.accountEmail === userName)
			if (selAccount) {
				setIsAccountCreated(true)
			}
		}
	}, [lstAccounts])

	return (
		<SafeAreaView style={styles.containerStyle}>
			<TouchableOpacity style={styles.profileBlock} onPress={() => navigation.navigate("Profile")}>
				<Image style={styles.profileImageStyle} source={IMAGE.ICON_PROFILE} resizeMode="contain" />
				<View style={styles.profileUserNameStyle}>
					<Text style={{ marginBottom: 10, fontSize: 18 }}>
						{firstName} {lastName}
					</Text>
					<Text>{userName}</Text>
				</View>
				<Ionicons style={styles.forwardIconStyle} name="ios-arrow-forward" />
			</TouchableOpacity>
			<View style={styles.bodyViewStyle}>
				<View style={{ position: "absolute", top: 30 }}>
					<View style={{ flexDirection: "row", justifyContent: "space-around", width: "100%" }}>
						{isAccount && (
							<LinearGradient
								style={{ borderRadius: 5 }}
								colors={["rgba(255, 255, 255, 1)", "rgba(255, 255, 255, 0.7)", "rgba(255, 255, 255, 0.5)", Colors.BlueCard]}
							>
								<TouchableOpacity onPress={() => navigation.navigate("account")}>
									<View style={styles.accountCard}>
										<Text numberOfLines={1} style={{ alignSelf: "flex-end", padding: 2, fontSize: 8, backgroundColor: Colors.green1, color: "#fff" }}>
											ACTIVE
										</Text>
										<View style={{ flex: 1, justifyContent: "center" }}>
											<Text numberOfLines={1} style={{ textAlign: "center", fontSize: 18 }}>
												{account.orgId}
											</Text>
										</View>

										<Text numberOfLines={1} style={{ textAlign: "center", fontSize: 13, fontStyle: "italic", padding: 2 }}>
											enterprise
										</Text>
									</View>
								</TouchableOpacity>
							</LinearGradient>
						)}
						<View pointerEvents={isAccountCreated ? "none" : "auto"} style={{ alignSelf: "center" }}>
							<TouchableOpacity onPress={() => navigation.navigate("AccountCreate", { isEdit: false })}>
								<LinearGradient style={styles.linearGradient} colors={["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.5)"]}>
									<Octicons name="plus" style={styles.iconPlusStyle} />
								</LinearGradient>
								<Text style={{ fontSize: 13, fontStyle: "italic", marginTop: 5 }}>account</Text>
							</TouchableOpacity>
						</View>
					</View>
				</View>
				<View style={{ position: "absolute", bottom: 20 }}>
					<View style={styles.blockViewStyle}>
						<LinearGradient
							style={{ ...styles.linearGradient, width: "30%", borderRadius: 5 }}
							colors={["rgba(0, 0, 0, 0.3)", "rgba(0, 0, 0, 0.05)"]}
						>
							<TouchableOpacity onPress={() => navigation.navigate("Skills")}>
								<Text style={{ fontSize: 18 }}>skills</Text>
							</TouchableOpacity>
						</LinearGradient>
						<LinearGradient
							style={{ ...styles.linearGradient, width: "30%", borderRadius: 5 }}
							colors={["rgba(0, 0, 0, 0.3)", "rgba(0, 0, 0, 0.05)"]}
						>
							<TouchableOpacity onPress={() => navigation.navigate("Settings")}>
								<Text style={{ fontSize: 18 }}>settings</Text>
							</TouchableOpacity>
						</LinearGradient>
						<LinearGradient
							style={{ ...styles.linearGradient, width: "30%", borderRadius: 5 }}
							colors={["rgba(0, 0, 0, 0.3)", "rgba(0, 0, 0, 0.05)"]}
						>
							<TouchableOpacity onPress={() => navigation.navigate("Support")}>
								<Text style={{ fontSize: 18 }}>support</Text>
							</TouchableOpacity>
						</LinearGradient>
					</View>
					<View style={{ flexDirection: "row", marginTop: 60 }}>
						<View style={{ flex: 1, padding: 5 }}>
							<TouchableOpacity onPress={() => dispatch(authActions.logout())}>
								<Text>SIGNOUT</Text>
							</TouchableOpacity>
						</View>
						<View style={{ padding: 5 }}>
							<Text>app version 1.0.0</Text>
						</View>
					</View>
				</View>
			</View>
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	containerStyle: {
		flex: 1,
		...Platform.select({
			android: {
				marginTop: StatusBar.currentHeight
			}
		})
	},
	profileBlock: {
		padding: 5, //for android
		alignItems: "center",
		flexDirection: "row",
		marginHorizontal: 20
		//backgroundColor: "grey"
	},
	profileImageStyle: {
		//marginLeft: 20,
		height: 70,
		width: 70
	},
	profileUserNameStyle: {
		flex: 1,
		marginHorizontal: 5,
		alignItems: "center"
	},
	forwardIconStyle: {
		fontSize: 27
		//marginRight: 20
	},
	linearGradient: {
		height: 32,
		width: 32,
		borderRadius: 16,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "center"
	},
	bodyViewStyle: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
		//backgroundColor: "grey"
	},
	blockViewStyle: {
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
		//width: "85%",
		//alignSelf: "center"
	},
	accountCard: {
		shadowOffset: { width: 1, height: 1 },
		shadowColor: "#333",
		shadowOpacity: 0.3,
		shadowRadius: 5,
		height: 100,
		width: 100
	},
	iconPlusStyle: {
		fontSize: 25,
		color: "#fff"
	},
	menuCardStyle: {
		backgroundColor: "rgba(0, 0, 0, 0.05)",
		borderRadius: 5,
		padding: 10,
		width: "30%",
		alignItems: "center"
	}
})

export default MoreScreen
