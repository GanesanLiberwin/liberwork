import React from "react"
import { View, Text, SafeAreaView } from "react-native"
import { CustomHeader2 } from "../../components/CustomHeader2"

const NotificationsScreen = ({ navigation }) => {
	const OnFinish = () => {
		navigation.goBack()
	}

	return (
		<SafeAreaView style={{ flex: 1 }}>
			<CustomHeader2 title="notifications" isLogo={true} isClose={true} isBack={false} navigation={navigation} onPress={OnFinish} />
			<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
				<Text style={{ fontWeight: "bold" }}>Notifications Screen</Text>
			</View>
		</SafeAreaView>
	)
}

export default NotificationsScreen
