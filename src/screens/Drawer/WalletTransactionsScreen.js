import React, { useState, useCallback } from "react"
import { useDispatch, useSelector } from "react-redux"
import { View, FlatList, Text, SafeAreaView, StyleSheet, TouchableOpacity } from "react-native"
import { useFocusEffect } from "@react-navigation/native"

import { CustomHeader2 } from "../../components/CustomHeader2"
import WalletTransCard from "../../components/Wallet/WalletTransCard"

import * as walletActions from "../../store/actions/walletActions"
import Colors from "../../constants/Colors"

const WalletTransactionsScreen = ({ navigation, route }) => {
	const dispatch = useDispatch()
	const isLoading = useSelector(state => state.apiCallsInProgress > 0)

	const walletType = route.params.type

	const orgId = useSelector(state => state.account.accountDetails.orgId)
	const walletsHistory = useSelector(state => state.wallets.trxHistory)
	const walletAccountId = useSelector(state => state.wallets.accountId)
	const [transactionsPeriod, setTransactionsPeriod] = useState("recent")

	let recentTransactions = []
	if (walletType === "business") {
		recentTransactions = useSelector(state =>
			state.wallets.BusinessWallet && state.wallets.BusinessWallet.trxHistory ? state.wallets.BusinessWallet.trxHistory : []
		)
	} else {
		recentTransactions = useSelector(state =>
			state.wallets.PersonalWallet && state.wallets.PersonalWallet.trxHistory ? state.wallets.PersonalWallet.trxHistory : []
		)
	}

	useFocusEffect(
		useCallback(() => {
			// getAllTransactions()
			getRecentTransactions()
			return () => {
				dispatch(walletActions.clearAllTransaction())
			}
		}, [])
	)

	const getRecentTransactions = () => {
		setTransactionsPeriod("recent")
	}

	const getAllTransactions = () => {
		try {
			if (walletsHistory && walletsHistory.length > 0) {
				setTransactionsPeriod("all")
			} else {
				dispatch(walletActions.getTransaction(walletType === "business" ? walletAccountId + "_WB" : walletAccountId + "_WP"))
					.then(() => {
						// setErrorMessage({})
					})
					.catch(error => {
						// setErrorMessage({ onApiCall: error.message })
					})
				setTransactionsPeriod("all")
			}
		} catch (err) {
			// setErrorMessage({ onApiCall: err.message })
		}
	}

	const OnFinish = () => {
		navigation.goBack()
	}

	return (
		<SafeAreaView style={styles.containerStyle}>
			<CustomHeader2
				title={walletType + " transactions"}
				isLogo={false}
				isClose={true}
				isBack={false}
				orgId={orgId}
				navigation={navigation}
				onPress={OnFinish}
			/>
			<View style={{ flex: 1, width: "90%", alignSelf: "center", marginTop: "5%" }}>
				<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
					<TouchableOpacity onPress={() => getRecentTransactions()}>
						<Text style={{ ...styles.headerText, marginLeft: 15, color: transactionsPeriod === "recent" ? "rgba(0,0,0,0.5)" : Colors.blue2 }}>
							Recent
						</Text>
					</TouchableOpacity>
					{isLoading ? (
						<Text style={{ ...styles.headerText, marginRight: 15, color: "rgba(0,0,0,0.5)" }}>loading</Text>
					) : (
						<TouchableOpacity onPress={() => getAllTransactions()}>
							<Text style={{ ...styles.headerText, marginRight: 15, color: transactionsPeriod === "all" ? "rgba(0,0,0,0.5)" : Colors.blue2 }}>
								last 6 months
							</Text>
						</TouchableOpacity>
					)}
				</View>
				{transactionsPeriod === "recent" ? (
					<FlatList
						data={recentTransactions}
						keyExtractor={recentTransactions => recentTransactions.date.toString()}
						ListEmptyComponent={() => <Text style={styles.emptyTextStyle}>There are no recent transactions</Text>}
						renderItem={({ item, index }) => (
							<WalletTransCard type={item.type} reason={item.reason} date={item.date} transAmount={item.amount} balance={item.balance} />
						)}
					/>
				) : (
					<FlatList
						showsVerticalScrollIndicator={false}
						data={walletsHistory}
						keyExtractor={walletsHistory => walletsHistory.date.toString()}
						ListEmptyComponent={() => <Text style={styles.emptyTextStyle}>There are no transactions</Text>}
						renderItem={({ item, index }) => (
							<WalletTransCard type={item.type} reason={item.reason} date={item.date} transAmount={item.amount} balance={item.balance} />
						)}
					/>
				)}
			</View>
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	containerStyle: {
		flex: 1
	},
	headerText: {
		//color: "rgba(0,0,0,0.5)",
		fontSize: 14,
		fontStyle: "italic"
	},
	emptyTextStyle: {
		marginTop: 10,
		marginLeft: 25
	}
})

export default WalletTransactionsScreen
