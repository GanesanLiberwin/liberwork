import React, { useState, useCallback } from "react"
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, ScrollView, ActivityIndicator } from "react-native"
import { useDispatch, useSelector } from "react-redux"
import { useFocusEffect } from "@react-navigation/native"

import { CustomHeader2 } from "../../components/CustomHeader2"
import WorkSummary from "../../components/Work/WorkSummary"
import CountdownTimer from "../../components/Utilities/CountdownTimer"
import Colors from "../../constants/Colors"
import { GetUnixTime } from "../../components/Utilities/DateFormat"

import * as workFlowActions from "../../store/actions/workFlowActions"
import * as accountActions from "../../store/actions/accountActions"

import { workStatusJson, workRoleJson } from "../../constants/PageJson"

const WorkBidScreen = ({ navigation, route }) => {
	const dispatch = useDispatch()
	const isLoading = useSelector(state => state.apiCallsInProgress > 0)

	const workId = useSelector(state => state.workFlow.workId)
	const workFlow = useSelector(state => state.workFlow)
	const workRole = useSelector(state => state.workFlow.workRole)
	const workDetails = useSelector(state => state.workFlow.workDetails)
	const workStatus = useSelector(state => state.workFlow.workDetails.currentStatus)

	const [BidDone, setBidDone] = useState(false)
	const [errorMessage, setErrorMessage] = useState({})

	const account = useSelector(state => state.account.accountDetails)
	const workbrief = route.params.item
	category = workbrief.functionalDomain.toLowerCase()

	useFocusEffect(
		useCallback(() => {
			getWorkDataFromServer()
			if (workbrief.allocationStatus === "Applied") {
				setBidDone(true)
			} else {
				setBidDone(false)
			}

			return () => {
				dispatch(workFlowActions.updateWorkFlowId(""))
				dispatch(workFlowActions.clearWorkFlowData())
				dispatch(accountActions.clearAccountVerifiedData())
				setErrorMessage({})
				//setBidDone(true)
			}
		}, [workId])
	)

	const getWorkDataFromServer = () => {
		if (workId !== "") {
			try {
				dispatch(workFlowActions.getWorkByWorkId(workId, account && account.id ? account.id : ""))
					.then(() => {
						setErrorMessage({})
					})
					.catch(error => {
						setErrorMessage({ onApiCall: error.message })
					})
			} catch (err) {
				setErrorMessage({ onApiCall: err.message })
			}
		}
	}

	const OnCancel = () => {
		if (workId !== "" && account && account.id) {
			try {
				dispatch(workFlowActions.cancelWork(workId, account.id, workRole, workDetails.rewardUnits, workStatusJson.Cancelling))
			} catch (err) {
				setErrorMessage({ onApiCall: err.message })
			}
		}
	}

	const OnRemove = () => {
		dispatch(workFlowActions.applyWork(workId, account.id, "decline", ""))
		// "action": "decline", "allocationStatus": "Applied"
		// need to remove from store
		OnFinish()
	}

	const OnOptOut = () => {
		if (workbrief.biddingEndDateTime - GetUnixTime(new Date()) > 0) {
			dispatch(workFlowActions.applyWork(workId, account.id, "decline", ""))
			// "action": "decline", "allocationStatus": "Applied"
			// need to remove from store
			OnFinish()
		}
	}

	const OnBid = () => {
		if (workbrief.biddingEndDateTime - GetUnixTime(new Date()) > 0) {
			//dispatch bid api call
			// "action": "apply", "allocationStatus": "Applied"
			dispatch(workFlowActions.applyWork(workId, account.id, "apply", ""))
			// "allocationStatus": "Published" = > "Applied",
			setBidDone(true)
		}
	}

	const OnFinish = () => {
		navigation.goBack()
	}

	/*    
   currentStatus === "Cancelling" || currentStatus === "OptingOut" => "Cancelling"/"OptingOut"
      workRole === "Poster" || "Del-O" => CANCEL
      workRole === "Del-E" /"Del-R" => currentStatus === "Published" ? "BIDDING IN PROGRESS" : "WORK POSTED"
      workRole === "Worker"
        BidDone => OPT-OUT
        REMOVE, Timer
  */

	return (
		<SafeAreaView style={styles.containerStyle}>
			<CustomHeader2 title={category} isClose={true} isBack={false} orgId={account.orgId} navigation={navigation} onPress={OnFinish} />

			<ScrollView showsVerticalScrollIndicator={false}>
				<View style={{ flex: 1, marginTop: "2%" }}>
					{workDetails.workId === "" ? (
						<ActivityIndicator size="small" color="rgba(0,0,0,0.5)" />
					) : (
						<WorkSummary
							showRewardSkills={true}
							showWorkType={true}
							showWorkDesc={true}
							rewardUnits={workDetails.rewardUnits}
							rewardModel={workDetails.rewardModel}
							rewardType={workDetails.rewardType}
							skills={workDetails.skills}
							industryDomain={workDetails.industryDomain}
							industryDomainExp={workDetails.industryExperience}
							workForce={workDetails.workforceType}
							isBeginner={workDetails.beginnerAllowed}
							workSelection={workDetails.selectionModel}
							workType={workDetails.workType}
							isRemote={workDetails.remoteWorking}
							workLocation={workDetails.workLocation}
							workCategory={workDetails.functionalDomain}
							functionalExp={workDetails.functionalExperience}
							description={workDetails.description}
							outcome={workDetails.outcome}
							effort={workDetails.estimatedEfforts}
							attachments={workDetails.attachments}
							workStartDate={workDetails.plannedStartDate}
							workEndDate={workDetails.plannedEndDate}
							isCritical={workDetails.criticalWork}
							statusDate={
								workStatus === workStatusJson.Published || workStatus === workStatusJson.BidEnded
									? workDetails.biddingEndDateTime
									: workDetails.plannedEndDate
							}
							status={workStatus}
							statusHead={workStatus === workStatusJson.Published || workStatus === workStatusJson.BidEnded ? "Bid Ending" : "Ending"}
							posterAccountId={workDetails.accountId}
							delegateAccountId={workDetails.delegateAccountId}
							delegatePermission={workDetails.delegatePermission}
							workRole={workRole}
						/>
					)}
				</View>
			</ScrollView>
			{errorMessage.onApiCall ? (
				<View style={{ marginTop: 10, alignSelf: "center" }}>
					<Text style={{ color: Colors.red }}>{errorMessage.onApiCall}</Text>
				</View>
			) : null}
			{isLoading && workDetails.workId != "" ? (
				<ActivityIndicator size="small" color="rgba(0,0,0,0.5)" />
			) : (
				<View style={{ marginHorizontal: "5%" }}>
					{workStatus === workStatusJson.Cancelling || workStatus === workStatusJson.OptingOut ? (
						<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.GreyInactive }}>
							<Text style={{ ...styles.buttonText, color: "#000" }}>{workStatus.toUpperCase()}</Text>
						</View>
					) : workRole === workRoleJson.Poster || workRole === workRoleJson.delegateOwner ? (
						<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.orange1 }}>
							<TouchableOpacity onPress={() => OnCancel()}>
								<Text style={styles.buttonText}>CANCEL</Text>
							</TouchableOpacity>
						</View>
					) : workRole === workRoleJson.delegateEdit || workRole === workRoleJson.delegateRead ? (
						<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.GreyInactive }}>
							<Text style={{ ...styles.buttonText, color: "#000" }}>{workStatus === "Published" ? "BIDDING IN PROGRESS" : "WORK POSTED"}</Text>
						</View>
					) : workRole === workRoleJson.Worker ? (
						BidDone ? (
							<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.orange1 }}>
								<TouchableOpacity onPress={() => OnOptOut()}>
									<Text style={styles.buttonText}>OPT-OUT</Text>
								</TouchableOpacity>
							</View>
						) : (
							<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
								<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.orange1 }}>
									<TouchableOpacity onPress={() => OnRemove()}>
										<Text style={styles.buttonText}>REMOVE</Text>
									</TouchableOpacity>
								</View>
								<View style={{ width: "70%", borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.green1 }}>
									<TouchableOpacity onPress={() => OnBid()}>
										<Text style={{ ...styles.buttonText, padding: "3%" }}>{<CountdownTimer bidDate={workbrief.biddingEndDateTime} />}</Text>
									</TouchableOpacity>
								</View>
							</View>
						)
					) : null}
				</View>
			)}
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	containerStyle: {
		flex: 1
	},
	linearGradient: {
		borderRadius: 5,
		marginHorizontal: "5%"
	},
	buttonText: {
		fontSize: 18,
		fontWeight: "bold",
		alignSelf: "center",
		padding: "2%",
		color: "#fff"
	}
})

export default WorkBidScreen
