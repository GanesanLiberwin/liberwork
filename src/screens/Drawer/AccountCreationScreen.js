import React, { useState, useCallback } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useFocusEffect } from "@react-navigation/native"

import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, ScrollView, Button, Platform, ActivityIndicator, ActionSheetIOS } from "react-native"
import { Picker } from "@react-native-picker/picker"

import { CustomHeader2 } from "../../components/CustomHeader2"
import Colors from "../../constants/Colors"
import * as AccountData from "../../constants/Account"
import Input from "../../components/Input"
import Regex from "../../constants/Regex"
import * as accountActions from "../../store/actions/accountActions"

const AccountCreationScreen = ({ navigation, route }) => {
	const dispatch = useDispatch()
	const isEdit = route.params.isEdit
	const account = useSelector(state => state.account.accountDetails)

	const isLoading = useSelector(state => state.apiCallsInProgress > 0)
	const profile = useSelector(state => state.profile)
	const orgDetail = useSelector(state => state.orgDetail)
	const userId = useSelector(state => state.auth.userId)
	const userName = useSelector(state => state.auth.userName)
	const userNameType = useSelector(state => state.auth.userNameType)

	const [accountName, setAccountName] = useState(userNameType === "E" ? userName : "")
	const [accountNameTemp, setAccountNameTemp] = useState(userNameType === "E" ? userName : "")

	const [orgUnits, setOrgUnits] = useState(orgDetail && orgDetail.orgUnits ? orgDetail.orgUnits : [])
	const [orgName, setOrgName] = useState(orgDetail.orgName ? orgDetail.orgName : "")
	const [orgId, setOrgId] = useState(orgDetail.orgId ? orgDetail.orgId : "")

	const [OTP, setOTP] = useState("")
	const [selectedOrgUnit, setSelectedOrgUnit] = useState("001")
	const [selectedWorkerType, setSelectedWorkerType] = useState("Employee")
	const [selectedOrgRole, setSelectedOrgRole] = useState("Employee")
	const [empID, setEmpID] = useState("")
	const [supEmail, setSupEmail] = useState("")
	const [empLocation, setEmpLocation] = useState("")

	const [OTPError, setOTPError] = useState("")
	const [accountNameError, setAccountNameError] = useState("")
	const [empIDError, setEmpIDError] = useState("")
	const [supEmailError, setSupEmailError] = useState("")
	const [empLocationError, setEmpLocationError] = useState("")

	const [isNextPage, setIsNextPage] = useState(userNameType === "M" ? true : false)
	const [isVerifyPage, setIsVerifyPage] = useState(userNameType === "M" ? false : false)
	const [isOrgDataPage, setIsOrgDataPage] = useState(userNameType === "M" ? false : true)

	const [topMargin, setTopMargin] = useState("40%")
	const [isEditable, setIsEditable] = useState(true)
	const [errorMessage, setErrorMessage] = useState({})

	useFocusEffect(
		useCallback(() => {
			if (orgDetail) {
				setOrgName(orgDetail.orgName)
				setOrgId(orgDetail.orgId)
				setOrgUnits(orgDetail.orgUnits)
			}

			setSelectedOrgUnit(account.orgUnitId != "" ? account.orgUnitId : "001")
			setSelectedWorkerType(account.workerType != "" ? account.workerType : "Employee")
			setSelectedOrgRole(account.orgPersonRole != "" ? account.orgPersonRole : "Employee")
			setEmpID(account.orgPersonId)
			setSupEmail(account.supervisorEmail)
			setEmpLocation(account.personLocation)

			return () => {
				initializeFields()
			}
		}, [account])
	)

	const initializeFields = () => {
		setSelectedOrgRole("Employee")
		setSelectedOrgUnit("001")
		setSelectedWorkerType("Employee")
		setEmpID("")
		setSupEmail("")
		setEmpLocation("")

		setIsNextPage(userNameType === "M" ? true : false)
		setIsVerifyPage(userNameType === "M" ? false : false)
		setIsOrgDataPage(userNameType === "M" ? false : true)
		setTopMargin("40%")
		setAccountNameTemp("")
		setOTP("")

		setErrorMessage({})
		setAccountNameError("")
		setOTPError("")
		setEmpIDError("")
		setSupEmailError("")
		setEmpLocationError("")
	}

	// Method to check whether the user ID is number or Alphanumeric to enable/disable phonepicker component
	//UserName Field change and Validation
	const accountUserNameChangeHandler = text => {
		setErrorMessage({})
		if (text) {
			if (isNaN(text)) {
				if (Regex.email.test(text)) {
					setAccountNameError("")
				} else {
					setAccountNameError("Please enter a valid Email ID")
				}
			} else {
				setAccountNameError("")
			}
		} else {
			setAccountNameError("")
		}
		setAccountNameTemp(text.toLowerCase())
	}

	const onOTPTextChangeHandler = text => {
		setErrorMessage({})
		if (text) {
			setErrorMessage({})
			if (text.length < 4) {
				setOTPError("OTP length appears less than 4")
			} else {
				setOTP(text)
				setOTPError("")
			}
		} else {
			setOTPError("")
			setOTP("")
		}
	}
	const orgUnitChangeHandler = text => {
		setErrorMessage({})
		setSelectedOrgUnit(text)
	}

	const orgRoleChangeHandler = text => {
		setErrorMessage({})
		setSelectedOrgRole(text)
	}

	const orgWorkTypeChangeHandler = text => {
		setErrorMessage({})
		setSelectedWorkerType(text)
	}

	const empIdChangeHandler = text => {
		setErrorMessage({})
		if (Regex.alphaNumeric.test(text) && text != "") {
			setEmpID(text)
			setEmpIDError("")
		} else {
			setEmpID(text)
			setEmpIDError("Please enter valid employee id")
		}
	}

	const empLocationChangeHandler = text => {
		setErrorMessage({})
		setEmpLocation(text)
	}

	const supEmailChangeHandler = text => {
		setErrorMessage({})
		if (Regex.email.test(text) && text != accountName && text != "") {
			setSupEmail(text)
			setSupEmailError("")
		} else {
			setSupEmail(text)
			setSupEmailError("Please enter a valid email other than yours")
		}
	}

	//Is user Exists and domain validation for Username Field
	const onVerifyHandler = async () => {
		if (!OTP) {
			setOTPError("Please enter the OTP received by account email")
			return
		}
		setTopMargin("0%")
		setIsVerifyPage(false)
		setIsOrgDataPage(true)
		setIsNextPage(false)
	}

	//Verify IsAccountExisting by API call
	const onNextHandler = async () => {
		if (!accountNameTemp) {
			setAccountNameError("please enter account email ID")
			return
		}

		try {
			dispatch(accountActions.isAccountAvailable(accountNameTemp))
				.then(() => {
					setErrorMessage({})
					setIsVerifyPage(true)
					setIsNextPage(false)
					setIsEditable(false)
					setAccountName(accountNameTemp)
				})
				.catch(error => {
					setErrorMessage({ onApiCall: error.message })
				})
		} catch (err) {
			setErrorMessage({ onApiCall: err.message })
		}
	}

	const onFinishHandler = () => {
		if (!empID) {
			setEmpIDError("Please enter valid employee id")
		}

		if (!supEmail) {
			setSupEmailError("Please enter a valid email other than yours")
		}

		if (empIDError != "" || supEmailError != "") {
			return
		}
		try {
			dispatch(
				isEdit
					? accountActions.updateAccount(selectedOrgUnit, selectedWorkerType, empID, supEmail.toLowerCase(), selectedOrgRole, empLocation, account.id)
					: accountActions.createAccount(
							userId,
							accountName,
							orgId,
							selectedOrgUnit,
							empID,
							selectedOrgRole,
							supEmail.toLowerCase(),
							empLocation,
							selectedWorkerType,
							profile.firstName,
							profile.lastName
					  )
			)
				.then(() => {
					//initializeFields()
					setAccountName("")
					setOrgName("")
					navigation.navigate(isEdit ? "account" : "home")
				})
				.catch(error => {
					setErrorMessage({ onApiCall: error.message })
				})
		} catch (err) {
			setErrorMessage({ onApiCall: err.message })
		}
	}

	const OnFinish = () => {
		//initializeFields()
		navigation.goBack()
	}

	const onPressOrgUnit = () =>
		ActionSheetIOS.showActionSheetWithOptions(
			{
				options: [...orgUnits.map(item => item.orgUnitName), "Cancel"],
				cancelButtonIndex: orgUnits.length,
				title: "Orgaization Unit"
			},
			buttonIndex => {
				orgUnits.filter((units, key) => key === buttonIndex).map(filteredUnit => setSelectedOrgUnit(filteredUnit.orgUnitId))
			}
		)

	const onPressWorkerType = () =>
		ActionSheetIOS.showActionSheetWithOptions(
			{
				options: [...AccountData.orgWorkTypes, "Cancel"],
				cancelButtonIndex: AccountData.orgWorkTypes.length,
				title: "Worker Type"
			},
			buttonIndex => {
				AccountData.orgWorkTypes.filter((types, key) => key === buttonIndex).map(filteredType => setSelectedWorkerType(filteredType))
			}
		)

	const onPressOrgRole = () =>
		ActionSheetIOS.showActionSheetWithOptions(
			{
				options: [...AccountData.orgRoles, "Cancel"],
				cancelButtonIndex: AccountData.orgRoles.length,
				title: "Organization Role"
			},
			buttonIndex => {
				AccountData.orgRoles.filter((roles, key) => key === buttonIndex).map(filteredRole => setSelectedOrgRole(filteredRole))
			}
		)

	return (
		<SafeAreaView style={{ flex: 1 }}>
			<CustomHeader2
				title={isEdit ? "edit account" : "create account"}
				isLogo={true}
				isClose={true}
				isBack={false}
				navigation={navigation}
				onPress={OnFinish}
			/>
			<View style={styles.accountContainer}>
				{isNextPage ? (
					<View>
						<View style={{ ...styles.inputTextContainer, marginBottom: 15 }}>
							<Input
								id="accountName"
								icon={{
									name: "mail",
									type: "Feather",
									color: Colors.black,
									size: 20
								}}
								keyboardType="default"
								TextInputEnable
								textHidden={false}
								autoCapitalize="none"
								autoCorrect={false}
								placeholder={"enter your corporate email"}
								errorText={accountNameError}
								value={accountNameTemp}
								onChangeText={accountUserNameChangeHandler}
							/>
						</View>
						<Button title="NEXT" onPress={onNextHandler} color={Colors.blue2} />
					</View>
				) : isVerifyPage ? (
					<View>
						<View style={{ ...styles.inputTextContainer, marginBottom: 15 }}>
							<Input
								id="accountName"
								icon={{
									name: "mail",
									type: "Feather",
									color: Colors.black,
									size: 20
								}}
								keyboardType="default"
								TextInputEnable
								textHidden={false}
								autoCapitalize="none"
								autoCorrect={false}
								// isEditable={isEditable}
								placeholder={"enter your corporate email"}
								errorText={""}
								value={accountName}
							/>
						</View>
						<View style={{ ...styles.inputTextContainer, marginBottom: 15 }}>
							<Input
								id="OTP"
								icon={{
									name: "mail",
									type: "Feather",
									color: Colors.black,
									size: 20
								}}
								TextInputEnable
								textHidden={false}
								keyboardType="numeric"
								autoCapitalize="none"
								autoCorrect={false}
								placeholder={"enter the code received by email"}
								errorText={OTPError}
								onChangeText={onOTPTextChangeHandler}
							/>
						</View>
						<Button title="VERIFY" onPress={onVerifyHandler} color={Colors.blue2} />
					</View>
				) : isOrgDataPage ? (
					<ScrollView showsVerticalScrollIndicator={false}>
						<View>
							<View style={styles.orgNameContainer}>
								<Text style={styles.orgNameTextStyle}>{orgName}</Text>
								<Text style={{ marginTop: 10, fontSize: 14, alignSelf: "center" }}>{accountName}</Text>
							</View>
							<View></View>
							<View style={styles.inputTextContainer}>
								<Text>organization unit*</Text>
								{Platform.OS === "ios" ? (
									<TouchableOpacity style={{ paddingVertical: "3%", marginLeft: 5 }} onPress={onPressOrgUnit}>
										<Text>{orgUnits.filter(units => units.orgUnitId === selectedOrgUnit).map(filteredUnit => filteredUnit.orgUnitName)}</Text>
									</TouchableOpacity>
								) : (
									<Picker
										mode="dropdown"
										selectedValue={selectedOrgUnit}
										style={{ width: "100%" }}
										onValueChange={(itemValue, itemIndex) => orgUnitChangeHandler(itemValue)}
									>
										{orgUnits.map((item, key) => (
											<Picker.Item label={item.orgUnitName} value={item.orgUnitId} key={key} />
										))}
									</Picker>
								)}
							</View>
							<View style={styles.inputTextContainer2}>
								<Input
									id="employeeId"
									label="employee id*"
									icon
									TextInputEnable
									keyboardType="default"
									autoCapitalize="none"
									autoCorrect={false}
									value={empID}
									placeholder={"please enter your employee id"}
									errorText={empIDError}
									onChangeText={empIdChangeHandler}
								/>
							</View>
							<View style={styles.inputTextContainer}>
								<Text>organization role*</Text>
								{Platform.OS === "ios" ? (
									<TouchableOpacity style={{ paddingVertical: "3%", marginLeft: 5 }} onPress={onPressOrgRole}>
										<Text>{selectedOrgRole}</Text>
									</TouchableOpacity>
								) : (
									<Picker
										mode="dropdown"
										selectedValue={selectedOrgRole}
										style={{ width: "100%" }}
										onValueChange={(itemValue, itemIndex) => orgRoleChangeHandler(itemValue)}
									>
										{AccountData.orgRoles.map((item, key) => (
											<Picker.Item label={item} value={item} key={key} />
										))}
									</Picker>
								)}
							</View>
							<View style={styles.inputTextContainer2}>
								<Input
									id="supervisorEmail"
									label="supervisor email*"
									icon
									TextInputEnable
									keyboardType="default"
									autoCapitalize="none"
									autoCorrect={false}
									value={supEmail}
									placeholder={"please enter supervisor email"}
									errorText={supEmailError}
									onChangeText={supEmailChangeHandler}
								/>
							</View>
							<View style={styles.inputTextContainer2}>
								<Input
									id="employeeLocation"
									label="employee location"
									icon
									TextInputEnable
									keyboardType="default"
									autoCapitalize="none"
									autoCorrect={false}
									maxLength={60}
									value={empLocation}
									placeholder={"please enter your location"}
									errorText={empLocationError}
									onChangeText={empLocationChangeHandler}
								/>
							</View>
							<View style={styles.inputTextContainer}>
								<Text>worker type*</Text>
								{Platform.OS === "ios" ? (
									<TouchableOpacity style={{ paddingVertical: "3%", marginLeft: 5 }} onPress={onPressWorkerType}>
										<Text>{selectedWorkerType}</Text>
									</TouchableOpacity>
								) : (
									<Picker
										mode="dropdown"
										selectedValue={selectedWorkerType}
										style={{ width: "100%" }}
										onValueChange={(itemValue, itemIndex) => orgWorkTypeChangeHandler(itemValue)}
									>
										{AccountData.orgWorkTypes.map((item, key) => (
											<Picker.Item label={item} value={item} key={key} />
										))}
									</Picker>
								)}
							</View>
							<View style={{ marginTop: 25 }}>
								{isLoading ? (
									<ActivityIndicator size="small" color="rgba(0,0,0,0.5)" />
								) : (
									<TouchableOpacity onPress={() => onFinishHandler()} style={{ borderRadius: 5, backgroundColor: Colors.green1 }}>
										<Text style={styles.buttonText}>{isEdit ? "UPDATE ACCOUNT" : "CREATE ACCOUNT"}</Text>
									</TouchableOpacity>
								)}
							</View>
						</View>
					</ScrollView>
				) : null}
				{errorMessage.onApiCall ? (
					<View style={{ marginTop: 10, alignSelf: "center" }}>
						<Text style={{ color: Colors.red }}>{errorMessage.onApiCall}</Text>
					</View>
				) : null}
			</View>
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	accountContainer: {
		flex: 1,
		marginTop: 20,
		width: "90%",
		alignSelf: "center",
		justifyContent: "center"
	},
	inputTextContainer: {
		paddingTop: "2%",
		borderBottomWidth: 1,
		borderBottomColor: Colors.bluegrey
	},
	inputTextContainer2: {
		paddingVertical: "2%",
		borderBottomWidth: 1,
		borderBottomColor: Colors.bluegrey
	},
	orgNameContainer: {
		marginBottom: 20
	},
	orgNameTextStyle: {
		marginTop: 5,
		fontSize: 20,
		fontWeight: "500",
		alignSelf: "center"
	},
	errorText: {
		color: Colors.red,
		fontSize: 12,
		paddingLeft: 1
	},
	buttonText: {
		fontSize: 16,
		fontWeight: "bold",
		alignSelf: "center",
		padding: "2%",
		color: "#fff"
	}
})

export default AccountCreationScreen
