import React, { useState, useCallback, useRef } from "react"
import { View, Text, SafeAreaView, StyleSheet, TouchableOpacity, ScrollView, ActivityIndicator } from "react-native"

import { useFocusEffect } from "@react-navigation/native"
import { useDispatch, useSelector } from "react-redux"
import { LinearGradient } from "expo-linear-gradient"

import { CustomHeader2 } from "../../components/CustomHeader2"
import WorkSummary from "../../components/Work/WorkSummary"
import Colors from "../../constants/Colors"
import CommunicationCard from "../../components/Work/CommunicationCard"
import WorkSubmissionCard from "../../components/Work/WorkSubmissionCard"
import WorkClosureCard from "../../components/Work/WorkClosureCard"
import { workStatusJson, workRoleJson } from "../../constants/PageJson"

import * as workFlowActions from "../../store/actions/workFlowActions"
import * as accountActions from "../../store/actions/accountActions"

const WorkDetailScreen = ({ navigation, route }) => {
	const dispatch = useDispatch()
	const scrollViewRef = useRef()
	const isLoading = useSelector(state => state.apiCallsInProgress > 0)

	const workId = useSelector(state => state.workFlow.workId)
	const workFlow = useSelector(state => state.workFlow)
	const workRole = useSelector(state => state.workFlow.workRole)
	const workDetails = useSelector(state => state.workFlow.workDetails)
	const workStatus = useSelector(state => state.workFlow.workDetails.currentStatus)
	const Submission = useSelector(state => state.workFlow.Submission)
	const Closure = useSelector(state => state.workFlow.Closure)

	const [errorMessage, setErrorMessage] = useState({})

	const [isSubmitting, setIsSubmitting] = useState(false)
	const [isClosing, setIsClosing] = useState(false)
	const [showRewardSkills, setShowRewardSkills] = useState(false)
	const [showWorkType, setShowWorkType] = useState(false)
	const [showWorkDesc, setShowWorkDesc] = useState(false)

	const account = useSelector(state => state.account.accountDetails)

	const workbrief = route.params.item
	category = workbrief.functionalDomain.toLowerCase()

	useFocusEffect(
		useCallback(() => {
			getWorkDataFromServer()

			return () => {
				dispatch(workFlowActions.updateWorkFlowId(""))
				dispatch(workFlowActions.clearWorkFlowData())
				dispatch(accountActions.clearAccountVerifiedData())

				setErrorMessage({})
				setShowRewardSkills(false)
				setShowWorkType(false)
				setShowWorkDesc(false)
				setIsClosing(false)
				setIsSubmitting(false)
			}
		}, [workId])
	)

	const getWorkDataFromServer = () => {
		if (workId !== "" && account && account.id) {
			try {
				dispatch(workFlowActions.getWorkByWorkId(workId, account.id))
					.then(() => {
						setErrorMessage({})
					})
					.catch(error => {
						setErrorMessage({ onApiCall: error.message })
					})
			} catch (err) {
				setErrorMessage({ onApiCall: err.message })
			}
		}
	}

	const submitFeedback = () => {
		if (workId !== "" && Closure.posterRating > 0 && account && account.id) {
			try {
				//(workId, accountId, role, rating, feedback)
				dispatch(workFlowActions.addFeedback(workId, account.id, workRole, Closure.posterRating, Closure.posterRatingText, Closure.rewardUnitsFinal))
			} catch (err) {
				setErrorMessage({ onApiCall: err.message })
			}
		}
	}

	const OnShowSegment = segment => {
		switch (segment) {
			case "RewardSkills":
				if (!showRewardSkills) {
					setShowWorkType(false)
					setShowWorkDesc(false)
				}
				setShowRewardSkills(!showRewardSkills)
				break
			case "WorkType":
				if (!showWorkType) {
					setShowRewardSkills(false)
					setShowWorkDesc(false)
				}
				setShowWorkType(!showWorkType)

				break
			case "WorkDesc":
				if (!showWorkDesc) {
					setShowRewardSkills(showWorkDesc)
					setShowWorkType(showWorkDesc)
				}
				setShowWorkDesc(!showWorkDesc)
				break
			default:
				break
		}
	}

	//handles cancel from poster del-o and optout from worker
	//on api success => change status in store to Cancelling
	const OnCancelOptout = (cancelStatus, rewardUnits) => {
		if (workId !== "" && account && account.id) {
			try {
				dispatch(workFlowActions.cancelWork(workId, account.id, workRole, rewardUnits, cancelStatus))
			} catch (err) {
				setErrorMessage({ onApiCall: err.message })
			}
		}
	}

	const OnCancel = () => {
		// // workRole === "Poster" || "Del-O" => Cancelling: "Cancelling",
		if (workId !== "" && account && account.id) {
			OnCancelOptout(workStatusJson.Cancelling, workDetails.rewardUnits)
		}
	}

	const OnOptOut = () => {
		// // workRole === "Worker" => OPT-OUT => OptingOut: "Opting-out"
		if (workId !== "" && account && account.id) {
			let rewardUnits = "0" //  need to add rewardUnits logic
			OnCancelOptout(workStatusJson.OptingOut, rewardUnits)
		}
	}

	// "InProgress"/"Rework"
	const updateWorkStatus = workStatus => {
		if (workId !== "" && account && account.id) {
			// {  "accountId": "", "status": "Rework" }
			// {  "accountId": "", "status": "InProgress" }
			try {
				let payload = { accountId: account.id, status: workStatus }
				dispatch(workFlowActions.updateWorkStatus(workId, workStatus, payload))
			} catch (err) {
				setErrorMessage({ onApiCall: err.message })
			}
		}
	}

	const OnInProgress = () => {
		updateWorkStatus(workStatusJson.Inprogress)
	}

	const OnRework = () => {
		updateWorkStatus(workStatusJson.Rework)
	}

	const OnSubmit = () => {
		if (!isSubmitting) {
			setShowRewardSkills(false)
			setShowWorkType(false)
			setShowWorkDesc(false)
			setIsSubmitting(true)
		} else if (Submission.submission != "") {
			//call submit api
			try {
				// "status": "submitted",
				dispatch(
					workFlowActions.submitWork(
						workId,
						account.id,
						workStatus === workStatusJson.Inprogress ? workStatusJson.Submitted : workStatusJson.Resubmitted,
						Submission.submission,
						Submission.attachments
					)
				)
					.then(() => {
						setIsSubmitting(false)
						setErrorMessage({})
					})
					.catch(error => {
						setErrorMessage({ onApiCall: error.message })
					})
			} catch (err) {
				setErrorMessage({ onApiCall: err.message })
			}
			//
		}
	}

	const OnClose = () => {
		if (!isClosing) {
			setShowRewardSkills(false)
			setShowWorkType(false)
			setShowWorkDesc(false)
			setIsClosing(true)
		} else {
			//call close api
			submitFeedback()
		}
	}

	const OnFinish = () => {
		navigation.goBack()
	}

	/*
    workStatus === Cancelling || OptingOut || Completed => Cancelling || OptingOut || Completed 

    workStatus === "Allocated"
     workRole === "Poster" || "Del-O" => CANCEL
     workRole === "Del-E" ||  "Del-R" => WORK ALLOCATED
     workRole === "Worker" => OPT-OUT , SET IN-PROGRESS

    workStatus === "In-Progress" || workStatus === "Rework"
      workRole === "Poster" || "Del-O" => CANCEL
      workRole === "Del-E" ||  "Del-R" => WORK IN PROGRESS
      workRole === "Worker" => OPT-OUT , "SUBMIT" : "CONFIRM"
    
    workStatus === "Submitted" || workStatus === "Resubmitted"
      workRole === "Poster" || "Del-O" => REWORK, "CLOSE" : "CONFIRM"
      workRole === "Del-E" && workStatus != "Resubmitted" => REWORK
      WORK UNDER REVIEW
  */

	return (
		<SafeAreaView style={styles.containerStyle}>
			<CustomHeader2 title={category} isClose={true} isBack={false} orgId={account.orgId} navigation={navigation} onPress={OnFinish} />
			{workDetails.workId != "" && (
				<View style={styles.blockViewStyle}>
					<LinearGradient
						style={{ ...styles.linearGradient, width: "33%", borderRadius: 5 }}
						colors={showRewardSkills ? ["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.1)"] : ["rgba(0, 0, 0, 0.3)", "rgba(0, 0, 0, 0.05)"]}
					>
						<TouchableOpacity onPress={() => OnShowSegment("RewardSkills")}>
							<Text style={styles.headerText}>reward & skills</Text>
						</TouchableOpacity>
					</LinearGradient>
					<LinearGradient
						style={{ ...styles.linearGradient, width: "30%", borderRadius: 5 }}
						colors={showWorkType ? ["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.1)"] : ["rgba(0, 0, 0, 0.3)", "rgba(0, 0, 0, 0.05)"]}
					>
						<TouchableOpacity onPress={() => OnShowSegment("WorkType")}>
							<Text style={styles.headerText}>work type</Text>
						</TouchableOpacity>
					</LinearGradient>
					<LinearGradient
						style={{ ...styles.linearGradient, width: "30%", borderRadius: 5 }}
						colors={showWorkDesc ? ["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.1)"] : ["rgba(0, 0, 0, 0.3)", "rgba(0, 0, 0, 0.05)"]}
					>
						<TouchableOpacity onPress={() => OnShowSegment("WorkDesc")}>
							<Text style={styles.headerText}>work desc</Text>
						</TouchableOpacity>
					</LinearGradient>
				</View>
			)}
			<ScrollView
				showsVerticalScrollIndicator={false}
				ref={scrollViewRef}
				//onContentSizeChange={() => scrollViewRef.current.scrollToEnd({ animated: true })}
			>
				{workDetails.workId === "" ? (
					<ActivityIndicator size="small" color="rgba(0,0,0,0.5)" />
				) : (
					<View style={{ flex: 1 }}>
						<WorkSummary
							navigation={navigation}
							showRewardSkills={showRewardSkills}
							showWorkType={showWorkType}
							showWorkDesc={showWorkDesc}
							rewardUnits={workDetails.rewardUnits}
							rewardModel={workDetails.rewardModel}
							rewardType={workDetails.rewardType}
							skills={workDetails.skills}
							industryDomain={workDetails.industryDomain}
							industryDomainExp={workDetails.industryExperience}
							workForce={workDetails.workforceType}
							isBeginner={workDetails.beginnerAllowed}
							workSelection={workDetails.selectionModel}
							workType={workDetails.workType}
							isRemote={workDetails.remoteWorking}
							workLocation={workDetails.workLocation}
							workCategory={workDetails.functionalDomain}
							functionalExp={workDetails.functionalExperience}
							description={workDetails.description}
							outcome={workDetails.outcome}
							effort={workDetails.estimatedEfforts}
							attachments={workDetails.attachments}
							workStartDate={workDetails.plannedStartDate}
							workEndDate={workDetails.plannedEndDate}
							isCritical={workDetails.criticalWork}
							statusDate={
								workStatus === workStatusJson.Closed ||
								workStatus === workStatusJson.Cancelled ||
								workStatus === workStatusJson.OptedOut ||
								workStatus === workStatusJson.Completed ||
								workStatus === workStatusJson.Cancelling ||
								workStatus === workStatusJson.OptingOut
									? workDetails.currentStatusDate
									: workDetails.plannedEndDate
							}
							status={workStatus}
							statusHead={
								workStatus === workStatusJson.Closed ||
								workStatus === workStatusJson.Cancelled ||
								workStatus === workStatusJson.OptedOut ||
								workStatus === workStatusJson.Completed ||
								workStatus === workStatusJson.Cancelling ||
								workStatus === workStatusJson.OptingOut
									? ""
									: "Ending"
							}
							posterAccountId={workDetails.accountId}
							delegateAccountId={workDetails.delegateAccountId}
							delegatePermission={workDetails.delegatePermission}
							allocatedTo={workDetails.allocatedTo}
							workRole={workRole}
						/>
						<CommunicationCard
							editable={
								workStatus === workStatusJson.Closed || workStatus === workStatusJson.OptedOut || workStatus === workStatusJson.Cancelled
									? false
									: true
							}
							workRole={workRole}
						/>
						{(Submission.submission != "" || isSubmitting) && (
							<WorkSubmissionCard submission={Submission.submission} attachments={Submission.attachments} editable={isSubmitting ? true : false} />
						)}
						{(Closure.posterRating != 0 || isClosing) && (
							<WorkClosureCard
								rating={Closure.posterRating}
								editable={isClosing ? true : false}
								rewardUnits={workDetails.rewardUnits}
								rewardModel={workDetails.rewardModel}
								rewardType={workDetails.rewardType}
							/>
						)}
					</View>
				)}
			</ScrollView>
			{isLoading && workDetails.workId != "" ? (
				<ActivityIndicator size="small" color="rgba(0,0,0,0.5)" />
			) : (
				<View style={{ marginHorizontal: "5%" }}>
					{workStatus === workStatusJson.Cancelling || workStatus === workStatusJson.OptingOut || workStatus === workStatusJson.Completed ? (
						<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.GreyInactive }}>
							<Text style={{ ...styles.buttonText, color: "#000" }}>{workStatus.toUpperCase()}</Text>
						</View>
					) : workStatus === workStatusJson.Allocated ? (
						workRole === workRoleJson.Poster || workRole === workRoleJson.delegateOwner ? (
							<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.orange1 }}>
								<TouchableOpacity onPress={() => OnCancel()}>
									<Text style={styles.buttonText}>CANCEL</Text>
								</TouchableOpacity>
							</View>
						) : workRole === workRoleJson.delegateEdit || workRole === workRoleJson.delegateRead ? (
							<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.GreyInactive }}>
								<Text style={{ ...styles.buttonText, color: "#000" }}>WORK ALLOCATED</Text>
							</View>
						) : workRole === workRoleJson.Worker ? (
							<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
								<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.orange1 }}>
									<TouchableOpacity onPress={() => OnOptOut()}>
										<Text style={styles.buttonText}>OPT-OUT</Text>
									</TouchableOpacity>
								</View>
								<View style={{ width: "70%", borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.green1 }}>
									<TouchableOpacity onPress={() => OnInProgress()}>
										<Text style={{ ...styles.buttonText, padding: "3%" }}>SET IN-PROGRESS</Text>
									</TouchableOpacity>
								</View>
							</View>
						) : null
					) : workStatus === workStatusJson.Inprogress || workStatus === workStatusJson.Rework ? (
						workRole === workRoleJson.Poster || workRole === workRoleJson.delegateOwner ? (
							<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.orange1 }}>
								<TouchableOpacity onPress={() => OnCancel()}>
									<Text style={styles.buttonText}>CANCEL</Text>
								</TouchableOpacity>
							</View>
						) : workRole === "Del-E" || workRole === "Del-R" ? (
							<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.GreyInactive }}>
								<Text style={{ ...styles.buttonText, color: "#000" }}>WORK IN PROGRESS</Text>
							</View>
						) : workRole === workRoleJson.Worker ? (
							<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
								<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.orange1 }}>
									<TouchableOpacity onPress={() => OnOptOut()}>
										<Text style={styles.buttonText}>OPT-OUT</Text>
									</TouchableOpacity>
								</View>
								<View style={{ width: "70%", borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.green1 }}>
									<TouchableOpacity onPress={() => OnSubmit()}>
										<Text style={{ ...styles.buttonText, padding: "3%" }}>
											{!isSubmitting ? (workStatus === workStatusJson.Inprogress ? "SUBMIT" : "RESUBMIT") : "CONFIRM"}
										</Text>
									</TouchableOpacity>
								</View>
							</View>
						) : null
					) : workStatus === workStatusJson.Submitted || workStatus === workStatusJson.Resubmitted ? (
						workRole === workRoleJson.Poster || workRole === workRoleJson.delegateOwner ? (
							<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
								<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.orange1 }}>
									<TouchableOpacity onPress={() => OnRework()}>
										<Text style={styles.buttonText}>REWORK</Text>
									</TouchableOpacity>
								</View>
								<View style={{ width: "70%", borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.green1 }}>
									<TouchableOpacity onPress={() => OnClose()}>
										<Text style={{ ...styles.buttonText, padding: "3%" }}>{!isClosing ? "CLOSE" : "CONFIRM"}</Text>
									</TouchableOpacity>
								</View>
							</View>
						) : workRole === workRoleJson.delegateEdit && workStatus != workStatusJson.Resubmitted ? (
							<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.orange1 }}>
								<TouchableOpacity onPress={() => OnRework()}>
									<Text style={styles.buttonText}>REWORK</Text>
								</TouchableOpacity>
							</View>
						) : (
							<View style={{ borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.GreyInactive }}>
								<Text style={{ ...styles.buttonText, color: "#000" }}>WORK UNDER REVIEW</Text>
							</View>
						)
					) : null}
				</View>
			)}
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	containerStyle: {
		flex: 1
	},
	blockViewStyle: {
		flexDirection: "row",
		justifyContent: "space-between",
		marginTop: 15,
		marginBottom: 10,
		marginHorizontal: "5%"
	},
	linearGradient: {
		borderRadius: 5
	},
	headerText: {
		fontSize: 15,
		alignSelf: "center",
		padding: "2%"
	},
	buttonText: {
		fontSize: 18,
		fontWeight: "bold",
		alignSelf: "center",
		padding: "2%",
		color: "#fff"
	}
})

export default WorkDetailScreen
