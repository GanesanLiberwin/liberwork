import React from "react"
import { View, Text, SafeAreaView, StyleSheet, ScrollView } from "react-native"
import { CustomHeader2 } from "../../components/CustomHeader2"
import Colors from "../../constants/Colors"

import { DateToString } from "../../components/Utilities/DateFormat"

const SkillDetailScreen = ({ navigation, route }) => {
	const skillbrief = route.params.item
	skillName = skillbrief.skillName

	const OnFinish = () => {
		navigation.goBack()
	}

	return (
		<SafeAreaView style={{ flex: 1 }}>
			<CustomHeader2
				title={skillbrief.skillName.toLowerCase()}
				isLogo={true}
				isClose={true}
				isBack={false}
				navigation={navigation}
				onPress={OnFinish}
			/>
			<ScrollView showsVerticalScrollIndicator={false}>
				{/* <View style={{ marginTop: "5%", alignItems: "center" }}>
					<Text style={styles.skillHeaderText}>{skillbrief.skillName}</Text>
					<Text style={styles.subHeaderText}>{skillbrief.skillName}</Text>
				</View> */}
				<View style={styles.summaryCard}>
					<View style={{ flexDirection: "row", marginVertical: "2%", justifyContent: "space-between" }}>
						<View>
							<Text style={styles.subHeaderText}>Skill score</Text>
							<View style={{ marginTop: "10%" }}>
								<Text style={{ textAlign: "center", fontWeight: "500", fontSize: 15 }}>{skillbrief.score}</Text>
							</View>
						</View>
						<View>
							<Text style={styles.subHeaderText}>Hours worked</Text>
							<View style={{ marginTop: "10%" }}>
								<Text style={{ textAlign: "center", fontWeight: "500", fontSize: 15 }}>{skillbrief.workCount}</Text>
							</View>
						</View>
						<View>
							<Text style={styles.subHeaderText}># of works</Text>
							<View style={{ marginTop: "10%" }}>
								<Text style={{ textAlign: "center", fontWeight: "500", fontSize: 15 }}>{skillbrief.workCount}</Text>
							</View>
						</View>
					</View>
				</View>
				<View style={styles.summaryCard}>
					<View style={{ flexDirection: "row", marginVertical: "2%", justifyContent: "space-between" }}>
						<View>
							<Text style={styles.subHeaderText}>Created on</Text>
							<View style={{ marginTop: "10%" }}>
								<Text style={{ textAlign: "center", fontWeight: "500", fontSize: 15 }}>{DateToString(skillbrief.createDate * 1000)}</Text>
							</View>
						</View>
						<View>
							<Text style={styles.subHeaderText}>Last updated</Text>
							<View style={{ marginTop: "10%" }}>
								<Text style={{ textAlign: "center", fontWeight: "500", fontSize: 15 }}>{DateToString(skillbrief.updateDate * 1000)}</Text>
							</View>
						</View>
						<View>
							<Text style={styles.subHeaderText}>Validated</Text>
							<View style={{ marginTop: "10%" }}>
								<Text style={{ textAlign: "center", fontWeight: "500", fontSize: 15 }}>{skillbrief.verified ? "Yes" : "No"}</Text>
							</View>
						</View>
					</View>
				</View>
			</ScrollView>
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	summaryCard: {
		width: "90%",
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	subHeaderText: {
		//color: "rgba(0,0,0,0.5)",
		fontSize: 13,
		fontStyle: "italic"
	},
	skillHeaderText: {
		fontSize: 20,
		fontWeight: "500",
		alignSelf: "center"
	}
})

export default SkillDetailScreen
