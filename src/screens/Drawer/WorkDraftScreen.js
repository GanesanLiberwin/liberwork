import React, { useState, useCallback, useRef } from "react"
import { View, Text, SafeAreaView, StyleSheet, ScrollView, TouchableOpacity, Alert, ActivityIndicator } from "react-native"
import { useDispatch, useSelector } from "react-redux"
import { useFocusEffect } from "@react-navigation/native"
import { addDays, differenceInDays, differenceInCalendarDays } from "date-fns"

import { CustomHeader2 } from "../../components/CustomHeader2"
import WorkTabsList from "../../components/Utilities/WorkTabsList"
import WorkSummary from "../../components/Work/WorkSummary"
import WorkForce from "../../components/WorkDraft/WorkForce"
import WorkType from "../../components/WorkDraft/WorkType"
import WorkFunction from "../../components/WorkDraft/WorkFunction"
import WorkSelectionModel from "../../components/WorkDraft/WorkSelectionModel"
import WorkLocation from "../../components/WorkDraft/WorkLocation"
import WorkSkills from "../../components/WorkDraft/WorkSkills"
import WorkIndustry from "../../components/WorkDraft/WorkIndustry"
import WorkDescription from "../../components/WorkDraft/WorkDescription"
import WorkEffort from "../../components/WorkDraft/WorkEffort"
import WorkSchedule from "../../components/WorkDraft/WorkSchedule"
import WorkReward from "../../components/WorkDraft/WorkReward"
import WorkRewardModel from "../../components/WorkDraft/WorkRewardModel"

import { GetUnixTime } from "../../components/Utilities/DateFormat"

import { workStatusJson, workRoleJson } from "../../constants/PageJson"
import Colors from "../../constants/Colors"
import Regex from "../../constants/Regex"

import * as workActions from "../../store/actions/workActions"
import * as workFlowActions from "../../store/actions/workFlowActions"
import * as walletActions from "../../store/actions/walletActions"
import * as searchActions from "../../store/actions/searchActions"

const WorkDraftScreen = ({ navigation, route }) => {
	const dispatch = useDispatch()
	const scrollViewRef = useRef()
	const isLoading = useSelector(state => state.apiCallsInProgress > 0)

	const account = useSelector(state => state.account.accountDetails)
	const workDraft = useSelector(state => state.workFlow.workDraft)
	const workDetails = useSelector(state => state.workFlow.workDetails)

	const workId = useSelector(state => state.works.selectedDraftId)
	const lstDrafts = useSelector(state => state.works.lstDrafts)
	const [isPostDraftDisabled, setIsPostDraftDisabled] = useState(false)
	const [errorMessage, setErrorMessage] = useState({})
	//category = workbrief.functionalDomain.toLowerCase()

	useFocusEffect(
		useCallback(() => {
			if (workId === "" || workId === 0) {
				onCreateDraftData()
			} else {
				onMapDraftData()
			}
			return () => {
				ClearDraftDetails()
			}
		}, [workId])
	)

	function ClearDraftDetails() {
		try {
			dispatch(workActions.updateDraftId(0))
			dispatch(workFlowActions.clearWorkFlowData())
			dispatch(searchActions.clearSearchSkills())
		} catch (err) {}
	}

	async function onCreateDraftData() {
		try {
			let newWorkId = GetUnixTime(new Date())

			dispatch(workFlowActions.updateWorkFlowFields("workId", newWorkId))
			dispatch(workFlowActions.updateWorkFlowFields("currentStatus", workStatusJson.Draft))
			//dispatch(workFlowActions.updateWorkFlowFields("currentStatus", workStatusJson.DraftNew)) //Testing

			const balance = await GetWalletBalance()
		} catch (err) {
			SendAlert()
		}
	}

	async function onMapDraftData() {
		try {
			const draftIndex = lstDrafts.findIndex(w => w.workId === workId)
			if (draftIndex > -1) {
				if (lstDrafts[draftIndex].workDetails) {
					dispatch(workFlowActions.loadDraftDetails(lstDrafts[draftIndex].workDetails, workRoleJson.Poster))
				}

				if (lstDrafts[draftIndex].workDraft) {
					dispatch(workFlowActions.updateWorkDraftData(lstDrafts[draftIndex].workDraft))
				}

				const balance = await GetWalletBalance()
				const sValidResult = await ValidateDraftSchedule(lstDrafts[draftIndex].workDraft.scheduleValid, lstDrafts[draftIndex].workDraft.workStartDate)
				const rValidResult = await ValidateDraftReward(
					lstDrafts[draftIndex].workDraft.rewardValid,
					lstDrafts[draftIndex].workDetails.rewardUnits,
					balance
				)

				SetNavigation(lstDrafts[draftIndex].workDraft.targetId, sValidResult, rValidResult)
			}
		} catch (err) {
			SendAlert()
		}
	}

	function SendAlert() {
		Alert.alert(
			"New Work",
			"We are unable to connect to server, please try after sometime",
			[
				{
					text: "OK",
					onPress: () => navigation.goBack()
				}
			],
			{ cancelable: false }
		)
	}

	const GetWalletBalance = () => {
		return dispatch(walletActions.getBalance(account.id))
	}

	function ValidateDraftSchedule(scheduleValid, startDate) {
		let valid = scheduleValid
		const currentDate = addDays(new Date(), 1)
		const diffInDays = differenceInCalendarDays(currentDate, new Date(startDate))

		if (diffInDays > 0) {
			valid = false
			dispatch(workFlowActions.updateWorkDraftFields("scheduleValid", false))
			dispatch(workFlowActions.updateWorkDraftFields("scheduleStatus", "please rescehdule the work"))

			SetDateInStore(currentDate, "start")
			SetDateInStore(currentDate, "end")

			dispatch(workFlowActions.updateWorkDraftFields("workEndMinDate", currentDate))
		} else {
			dispatch(workFlowActions.updateWorkDraftFields("workEndMinDate", startDate))
		}

		dispatch(workFlowActions.updateWorkDraftFields("workStartMinDate", currentDate))
		dispatch(workFlowActions.updateWorkDraftFields("workStartMaxDate", addDays(currentDate, 365)))
		dispatch(workFlowActions.updateWorkDraftFields("workEndMaxDate", addDays(currentDate, 365)))

		return valid
	}

	function SetDateInStore(inputDate, type) {
		if (type === "start") {
			inputDate.setHours(0, 0, 0)
			dispatch(workFlowActions.updateWorkDraftFields("workStartDate", inputDate))
			dispatch(workFlowActions.updateWorkFlowFields("plannedStartDate", GetUnixTime(inputDate)))
		} else {
			inputDate.setHours(23, 59, 59)
			dispatch(workFlowActions.updateWorkDraftFields("workEndDate", inputDate))
			dispatch(workFlowActions.updateWorkFlowFields("plannedEndDate", GetUnixTime(inputDate)))
		}
	}

	function ValidateDraftReward(rewardValid, reward, balance) {
		let valid = rewardValid

		if (rewardValid) {
			if (reward > balance) {
				valid = false
				dispatch(workFlowActions.updateWorkDraftFields("rewardValid", false))
				dispatch(workFlowActions.updateWorkDraftFields("rewardStatus", "reward exceeds balance"))
			}
		} else {
			if (Regex.number.test(reward) && reward > 0 && reward <= balance) {
				valid = true
				dispatch(workFlowActions.updateWorkDraftFields("rewardValid", true))
				dispatch(workFlowActions.updateWorkDraftFields("rewardStatus", ""))
			}
		}

		return valid
	}

	function SetNavigation(targetId, sValid, rValid) {
		let target = targetId
		if (target > 2) {
			if (!sValid) {
				target = 2
			}
			if (target > 3 && !rValid) {
				target = 3
			}
			if (target != targetId) {
				dispatch(workFlowActions.updateWorkDraftFields("targetId", target))
			}
		}
	}

	const OnPost = () => {
		setIsPostDraftDisabled(true)
		try {
			setErrorMessage({})

			if (
				workDraft.workForceValid &&
				workDraft.workTypeValid &&
				workDraft.categoryValid &&
				workDraft.selectionModelValid &&
				workDraft.locationValid &&
				workDraft.skillValid &&
				workDraft.descriptionValid &&
				workDraft.outcomeValid &&
				workDraft.effortValid &&
				workDraft.scheduleValid &&
				workDraft.rewardValid &&
				workDraft.rewardModelValid
			) {
				let workPayload = {
					accountId: account.id,
					orgId: account.orgId,
					workforceType: workDetails.workforceType,
					workforceCount: workDetails.workforceCount,
					beginnerAllowed: workDetails.beginnerAllowed,
					workType: workDetails.workType,
					functionalDomain: workDetails.functionalDomain,
					functionalExperience: workDetails.functionalExperience,
					selectionModel: workDetails.selectionModel,
					remoteWorking: workDetails.remoteWorking,
					workLocation: workDetails.workLocation,
					skills: workDetails.skills,
					industryDomain: workDetails.industryDomain,
					industryExperience: workDetails.industryExperience,
					description: workDetails.description,
					estimatedEfforts: workDetails.estimatedEfforts,
					plannedStartDate: workDetails.plannedStartDate,
					plannedEndDate: workDetails.plannedEndDate,
					criticalWork: workDetails.criticalWork,
					outcome: workDetails.outcome,
					attachments: workDetails.attachments,
					rewardUnits: workDetails.rewardUnits,
					rewardType: workDetails.rewardType,
					rewardModel: workDetails.rewardModel,
					delegatePermission: workDetails.delegatePermission,
					delegateAccountId: workDetails.delegateAccountId,
					currentStatus: workStatusJson.Posted
				}

				dispatch(workActions.createWork(workPayload))
					.then(() => {
						dispatch(walletActions.getBalance(account.id))
						OnDeleteDraft()
						setErrorMessage({})
						setIsPostDraftDisabled(false)
					})
					.catch(error => {
						setErrorMessage({ onApiCall: error.message })
						setIsPostDraftDisabled(false)
					})
			} else {
				//display error for respective component
				// setIsPostDraftDisabled(false)
			}
		} catch (err) {
			setErrorMessage({ onApiCall: err.message })
			setIsPostDraftDisabled(false)
		}
	}

	const OnNavigate = i => {
		dispatch(workFlowActions.updateWorkDraftFields("targetId", workDraft.targetId + i))

		if (i > 0 && workDraft.tabProgressId === workDraft.targetId) {
			dispatch(workFlowActions.updateWorkDraftFields("tabProgressId", workDraft.tabProgressId + i))
		}
	}

	const OnCancel = () => {}

	const OnDeleteDraft = () => {
		if (workId && workId > 0) {
			if (lstDrafts.length > 0) {
				let lstDraftsData = [...lstDrafts]
				const draftIndex = lstDraftsData.findIndex(w => w.workId === workId)
				if (draftIndex > -1) {
					//Delete item from lstDrafts
					dispatch(workActions.deleteWorkDrafts(workId))

					lstDraftsData.splice(draftIndex, 1)

					//Update AsyncStorage
					if (lstDraftsData.length > 0) {
						workActions.saveWorkDraft(lstDraftsData)
					} else {
						workActions.clearWorkDraft()
					}
				}
			}
		}

		navigation.goBack()
	}

	const OnSaveDraft = () => {
		try {
			let lstDraftData = [...lstDrafts]

			const draftIndex = lstDraftData.findIndex(w => w.workId === workId)
			if (draftIndex > -1) {
				dispatch(workActions.updateWorkDrafts(draftIndex, workDetails, workDraft))
				lstDraftData[draftIndex].workDetails = workDetails
				lstDraftData[draftIndex].workDraft = workDraft
				workActions.saveWorkDraft(lstDraftData)
			} else {
				const draftNew = { workId: workDetails.workId, accountId: account.id, workDetails: workDetails, workDraft: workDraft }
				dispatch(workActions.addWorkDraft(draftNew))
				lstDraftData = [...lstDrafts, draftNew]
				workActions.saveWorkDraft(lstDraftData)
			}
		} catch (err) {
			// setErrorMessage({ onApiCall: err.message })
			console.log("onSaveDraft", err.message)
		}
		navigation.goBack()
	}

	const OnFinish = () => {
		Alert.alert(
			"New Work",
			"What would you like to do ?",
			[
				{ text: "Continue Work", onPress: () => OnCancel() },
				{ text: "Delete Work", onPress: () => OnDeleteDraft() },
				{ text: "Save as Draft", onPress: () => OnSaveDraft() }
			],
			{ cancelable: true }
		)
	}

	return (
		<SafeAreaView style={styles.containerStyle}>
			<CustomHeader2 title="new work" isClose={true} isBack={false} orgId={account.orgId} navigation={navigation} onPress={OnFinish} />
			<WorkTabsList currTab={workDraft.targetId} progressTab={workDraft.tabProgressId} />
			<ScrollView
				showsVerticalScrollIndicator={false}
				ref={scrollViewRef}
				onContentSizeChange={() => scrollViewRef.current.scrollToEnd({ animated: true })}
			>
				{workDraft.targetId === 0 ? (
					<View>
						<WorkLocation />
						<WorkForce />
						{workDraft.workForceValid && <WorkType />}
						{workDraft.workTypeValid && <WorkFunction />}
						{(workDraft.categoryValid || workDraft.selectionModelValid) && <WorkSelectionModel />}
					</View>
				) : workDraft.targetId === 1 ? (
					<View>
						<WorkSkills />
						{workDraft.skillValid && <WorkIndustry />}
					</View>
				) : workDraft.targetId === 2 ? (
					<View>
						<WorkDescription type="description" />
						{(workDraft.descriptionValid || workDraft.outcomeValid) && <WorkDescription type="outcome" />}
						{(workDraft.outcomeValid || workDraft.effortValid) && <WorkEffort />}
						{workDraft.effortValid && <WorkSchedule />}
					</View>
				) : workDraft.targetId === 3 ? (
					<View>
						<WorkReward />
						{(workDraft.rewardValid || workDraft.rewardModelValid) && <WorkRewardModel />}
					</View>
				) : workDraft.targetId === 4 ? (
					<WorkSummary
						showRewardSkills={true}
						showWorkType={true}
						showWorkDesc={true}
						rewardUnits={workDetails.rewardUnits}
						rewardModel={workDetails.rewardModel}
						rewardType={workDetails.rewardType}
						skills={workDetails.skills}
						industryDomain={workDetails.industryDomain}
						industryDomainExp={workDetails.industryExperience}
						workForce={workDetails.workforceType}
						isBeginner={workDetails.beginnerAllowed}
						workSelection={workDetails.selectionModel}
						workType={workDetails.workType}
						isRemote={workDetails.remoteWorking}
						workLocation={workDetails.workLocation}
						workCategory={workDetails.functionalDomain}
						functionalExp={workDetails.functionalExperience}
						description={workDetails.description}
						outcome={workDetails.outcome}
						effort={workDetails.estimatedEfforts}
						attachments={workDetails.attachments}
						workStartDate={workDetails.plannedStartDate}
						workEndDate={workDetails.plannedEndDate}
						isCritical={workDetails.criticalWork}
						statusDate={workDetails.plannedEndDate}
						status={workDetails.currentStatus}
						statusHead="Ending"
						posterAccountId={workDetails.accountId}
						delegateAccountId={workDetails.delegateAccountId}
						delegatePermission={workDetails.delegatePermission}
						workRole={workRoleJson.Poster}
					/>
				) : null}
			</ScrollView>
			<View style={{ marginHorizontal: "5%" }}>
				{workDraft.targetId === 0 &&
				workDraft.locationValid &&
				workDraft.workForceValid &&
				workDraft.workTypeValid &&
				workDraft.categoryValid &&
				workDraft.selectionModelValid ? (
					<View style={{ width: "45%", borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.blue2, alignSelf: "flex-end" }}>
						<TouchableOpacity onPress={() => OnNavigate(1)}>
							<Text style={{ ...styles.buttonText, padding: "3%" }}>NEXT</Text>
						</TouchableOpacity>
					</View>
				) : workDraft.targetId === 1 ? (
					<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
						<View style={{ width: "45%", borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.blue2 }}>
							<TouchableOpacity onPress={() => OnNavigate(-1)}>
								<Text style={{ ...styles.buttonText, padding: "3%" }}>BACK</Text>
							</TouchableOpacity>
						</View>
						{workDraft.skillValid && (
							<View style={{ width: "45%", borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.blue2 }}>
								<TouchableOpacity onPress={() => OnNavigate(1)}>
									<Text style={{ ...styles.buttonText, padding: "3%" }}>NEXT</Text>
								</TouchableOpacity>
							</View>
						)}
					</View>
				) : workDraft.targetId === 2 ? (
					<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
						<View style={{ width: "45%", borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.blue2 }}>
							<TouchableOpacity onPress={() => OnNavigate(-1)}>
								<Text style={{ ...styles.buttonText, padding: "3%" }}>BACK</Text>
							</TouchableOpacity>
						</View>
						{workDraft.descriptionValid && workDraft.outcomeValid && workDraft.effortValid && workDraft.scheduleValid && (
							<View style={{ width: "45%", borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.blue2 }}>
								<TouchableOpacity onPress={() => OnNavigate(1)}>
									<Text style={{ ...styles.buttonText, padding: "3%" }}>NEXT</Text>
								</TouchableOpacity>
							</View>
						)}
					</View>
				) : workDraft.targetId === 3 ? (
					<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
						<View style={{ width: "45%", borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.blue2 }}>
							<TouchableOpacity onPress={() => OnNavigate(-1)}>
								<Text style={{ ...styles.buttonText, padding: "3%" }}>BACK</Text>
							</TouchableOpacity>
						</View>
						{workDraft.rewardValid && workDraft.rewardModelValid && (
							<View style={{ width: "45%", borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.blue2 }}>
								<TouchableOpacity onPress={() => OnNavigate(1)}>
									<Text style={{ ...styles.buttonText, padding: "3%" }}>NEXT</Text>
								</TouchableOpacity>
							</View>
						)}
					</View>
				) : workDraft.targetId === 4 ? (
					isLoading ? (
						<View style={{ flexDirection: "row", alignSelf: "center" }}>
							<ActivityIndicator size="small" color="rgba(0,0,0,0.5)" />
							<Text style={{ marginLeft: 5 }}>processing</Text>
						</View>
					) : (
						<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
							<View style={{ width: "45%", borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.blue2 }}>
								<TouchableOpacity onPress={() => OnNavigate(-1)}>
									<Text style={{ ...styles.buttonText, padding: "3%" }}>BACK</Text>
								</TouchableOpacity>
							</View>
							<View style={{ width: "45%", borderRadius: 5, marginVertical: "2%", backgroundColor: Colors.green1 }}>
								<TouchableOpacity onPress={() => OnPost()} disabled={isPostDraftDisabled}>
									<Text style={{ ...styles.buttonText, padding: "3%" }}>POST</Text>
								</TouchableOpacity>
							</View>
						</View>
					)
				) : null}
			</View>
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	containerStyle: {
		flex: 1
	},
	buttonText: {
		fontSize: 18,
		fontWeight: "bold",
		textAlign: "center",
		padding: "2%",
		color: "#fff"
	}
})

export default WorkDraftScreen
