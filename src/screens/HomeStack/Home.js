import React, { useState, useEffect, useCallback } from "react"
import { View, SafeAreaView, Text, StyleSheet, TouchableOpacity, ScrollView, RefreshControl, ActivityIndicator } from "react-native"
import { useDispatch, useSelector } from "react-redux"

import { CustomHeader } from "../../components/CustomHeader"
import WorkList from "../../components/Home/WorkList"
import QuickList from "../../components/Home/QuickList"

import * as walletActions from "../../store/actions/walletActions"
import * as workActions from "../../store/actions/workActions"
import * as rewardsActions from "../../store/actions/rewardsActions"

import { pageLabelJson } from "../../constants/PageLabelJson"
import { workStatusJson, workCategoryJson } from "../../constants/PageJson"

const Home = ({ navigation }) => {
	const dispatch = useDispatch()
	const isLoading = useSelector(state => state.apiCallsInProgress > 0)

	const [myPostsHeader, setMyPostsHeader] = useState(workCategoryJson.MyPosts)
	const [myDraftsHeader, setMyDraftsHeader] = useState(workCategoryJson.MyDrafts)
	const [myBidsHeader, setMyBidsHeader] = useState(workCategoryJson.MyBids)
	const [myWorksHeader, setMyWorksHeader] = useState(workCategoryJson.MyWorks)

	const [refreshing, setRefreshing] = useState(false)

	const isAccount = useSelector(state => state.isAccount)
	const account = useSelector(state => state.account.accountDetails)
	const quicks = useSelector(state => state.quicks.userQuicks)

	const lstAvailableWorks = useSelector(state => state.works.lstAvailableWorks)
	const myWorks = useSelector(state =>
		state.works.lstEligibleWorks.filter(
			w =>
				w.allocationStatus === workStatusJson.Allocated &&
				w.currentStatus != workStatusJson.Closed &&
				w.currentStatus != workStatusJson.Cancelled &&
				w.currentStatus != workStatusJson.OptedOut
		)
	)
	const preferedWorks = useSelector(state =>
		state.works.lstEligibleWorks.filter(w => w.allocationStatus === workStatusJson.Published && w.currentStatus === workStatusJson.Published)
	)
	const bidWorks = useSelector(state =>
		state.works.lstEligibleWorks.filter(w => w.allocationStatus === workStatusJson.Applied && w.currentStatus === workStatusJson.Published)
	)
	const recommendedWorks = useSelector(state =>
		state.works.lstEligibleWorks.filter(w => w.currentStatus === workStatusJson.Bidding && w.prefered === "no")
	)

	const accountId = useSelector(state => state.account.selectedAccountId)

	const lstDraftWorks = useSelector(state => state.works.lstDrafts.filter(w => w.accountId === accountId))

	let lstDraftWorksCard = []
	try {
		if (lstDraftWorks && lstDraftWorks.length > 0) {
			lstDraftWorks.forEach(draft => {
				if (draft.workDetails) {
					lstDraftWorksCard.push(draft.workDetails)
				}
			})
		}
	} catch (err) {}

	const businessWalletBalance = useSelector(state =>
		state.wallets.BusinessWallet && state.wallets.BusinessWallet.balance ? state.wallets.BusinessWallet.balance : 0
	)
	const personalWalletBalance = useSelector(state =>
		state.wallets.PersonalWallet && state.wallets.PersonalWallet.balance ? state.wallets.PersonalWallet.balance : 0
	)

	useEffect(() => {
		getServerData()
	}, [dispatch, accountId])

	const getServerData = () => {
		if (accountId !== "") {
			try {
				dispatch(walletActions.getBalance(accountId))
			} catch (err) {}
			try {
				dispatch(workActions.getAvailableWorks(accountId))
			} catch (err) {}
			try {
				dispatch(workActions.getEligibleWorks(accountId))
			} catch (err) {}

			try {
				dispatch(rewardsActions.getAllRewards())
			} catch (err) {}
		}
	}

	const onRefresh = useCallback(() => {
		setRefreshing(true)
		getServerData()
		setRefreshing(false) //determine right place for this
	})

	const OnMyPostsToggle = () => {
		if (myPostsHeader === workCategoryJson.MyPosts) {
			setMyPostsHeader(workCategoryJson.MyDrafts)
			setMyDraftsHeader(workCategoryJson.MyPosts)
		} else {
			setMyPostsHeader(workCategoryJson.MyPosts)
			setMyDraftsHeader(workCategoryJson.MyDrafts)
		}
	}

	const OnMyWorksToggle = () => {
		if (myWorksHeader === workCategoryJson.MyWorks) {
			setMyWorksHeader(workCategoryJson.MyBids)
			setMyBidsHeader(workCategoryJson.MyWorks)
		} else {
			setMyWorksHeader(workCategoryJson.MyWorks)
			setMyBidsHeader(workCategoryJson.MyBids)
		}
	}

	return (
		<SafeAreaView style={styles.viewStyle}>
			<CustomHeader title="home" isHome={true} isSearch={true} isNotification={true} navigation={navigation} orgId={account.orgId} />
			{isAccount ? (
				<ScrollView showsVerticalScrollIndicator={false} refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}>
					<QuickList resultsQuick={quicks} navigation={navigation} businessWallet={businessWalletBalance} personalWallet={personalWalletBalance} />
					{isLoading ? <ActivityIndicator size="small" color="rgba(0,0,0,0.5)" /> : null}
					<View>
						<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "flex-end" }}>
							<Text style={styles.title}>{myPostsHeader}</Text>
							<TouchableOpacity style={styles.toggleButton} onPress={OnMyPostsToggle}>
								<Text style={styles.titleButton}>{myDraftsHeader}</Text>
							</TouchableOpacity>
						</View>
						<WorkList
							results={myPostsHeader === workCategoryJson.MyPosts ? lstAvailableWorks : lstDraftWorksCard}
							workSource={myPostsHeader === workCategoryJson.MyPosts ? "AvailableWorks" : "draftWorks"}
							navigation={navigation}
						/>
					</View>
					<View>
						<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "flex-end" }}>
							<Text style={styles.title}>{myWorksHeader}</Text>
							<TouchableOpacity style={styles.toggleButton} onPress={OnMyWorksToggle}>
								<Text style={styles.titleButton}>{myBidsHeader}</Text>
							</TouchableOpacity>
						</View>
						<WorkList
							results={myWorksHeader === workCategoryJson.MyWorks ? myWorks : bidWorks}
							workSource={myWorksHeader === workCategoryJson.MyWorks ? "myWorks" : "bidWorks"}
							navigation={navigation}
						/>
					</View>
					<View>
						<Text style={styles.title}>{pageLabelJson.Home.preferenceWorks}</Text>
						<WorkList results={preferedWorks} workSource={"preferedWorks"} navigation={navigation} />
					</View>
					<View>
						<Text style={styles.title}>{pageLabelJson.Home.interestedWorks}</Text>
						<WorkList results={recommendedWorks} workSource={"recommendedWorks"} navigation={navigation} />
					</View>
				</ScrollView>
			) : (
				<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
					<Text style={{ fontWeight: "bold" }}>{pageLabelJson.Home.NoAccount}</Text>
				</View>
			)}
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	viewStyle: {
		flex: 1
	},
	iconSearchStyle: {
		//flex: 1,
		fontSize: 25,
		alignSelf: "center"
	},
	iconNotificationStyle: {
		//flex: 1,
		fontSize: 25,
		color: "#F0863D",
		alignSelf: "center"
	},
	logoStyle: {
		//flex: 4,
		height: 50,
		width: 130,
		marginLeft: 15
	},
	title: {
		marginHorizontal: 25,
		marginTop: 30,
		fontSize: 14,
		fontWeight: "bold"
	},
	titleButton: {
		fontSize: 12,
		fontWeight: "600",
		color: "#fff",
		paddingHorizontal: 4,
		paddingVertical: 2
	},
	toggleButton: {
		backgroundColor: "#07b0f9",
		marginHorizontal: 25,
		borderRadius: 5,
		width: "20%",
		alignItems: "center"
	}
})

export default Home
