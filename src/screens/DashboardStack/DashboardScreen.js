import React from "react"
import { View, Text, SafeAreaView } from "react-native"
import { useSelector } from "react-redux"

import { CustomHeader } from "../../components/CustomHeader"

const DashboardScreen = ({ navigation }) => {
	const isAccount = useSelector(state => state.isAccount)
	const account = useSelector(state => state.account.accountDetails)

	return (
		<SafeAreaView style={{ flex: 1 }}>
			<CustomHeader title="dashboard" isHome={false} isSearch={true} isNotification={true} navigation={navigation} orgId={account.orgId} />
			{isAccount ? (
				<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
					<Text style={{ fontWeight: "bold" }}>Dashboard Screen</Text>
				</View>
			) : (
				<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
					<Text style={{ fontWeight: "bold" }}>You have not created an account</Text>
				</View>
			)}
		</SafeAreaView>
	)
}

export default DashboardScreen
