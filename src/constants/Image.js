const IMAGE = {
	ICON_PROFILE: require("../assets/images/Profile.png"),
	ICON_LIBERWIN: require("../assets/images/Liberwin-Logo.png"),
	ICON_LIBERWIN_DM: require("../assets/images/Liberwin-Logo-DM.png"),
	ICON_LIBERWIN_BG: require("../assets/images/Liberwin-BG.jpg"),
	ICON_APP: require("../assets/images/App-Logo.png"),
	ICON_LIBER: require("../assets/images/Liberwin-Icon.png"),
	ICON_LIBERWORK: require("../assets/images/Liberwin-LiberWork.png")
}

export { IMAGE }
