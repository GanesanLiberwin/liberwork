const pageLabelJson = {
  Work: {
    NoWorks: "There are no work items",
    Posted: "Posted"
  },
  Home: {
    preferenceWorks: "New Works matching your preferences",
    interestedWorks: "New Works that may interest you",
    NoAccount: "You have not created an account"
  }
}

export { pageLabelJson }
