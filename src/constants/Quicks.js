const Quicks = [
	{
		id: 4,
		icon: "ICON_LIBER",
		isWallet: true,
		item: "BusinessWallet",
		preferred: "yes",
		title: "business"
	},
	{
		id: 2,
		icon: "ICON_LIBER",
		isWallet: true,
		item: "PersonalWallet",
		preferred: "yes",
		title: "personal"
	},
	{
		id: 6,
		icon: "library",
		isWallet: false,
		item: "Skills",
		preferred: "yes",
		title: "skills"
	},
	{
		id: 5,
		icon: "playlist-check",
		isWallet: false,
		item: "WorksHistory",
		preferred: "yes",
		title: "work history"
	},
	{
		id: 7,
		icon: "playlist-edit",
		isWallet: false,
		item: "WorksActive",
		preferred: "yes",
		title: "work active"
	}
]

export default Quicks
