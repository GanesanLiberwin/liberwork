export const FeedbackQuestion = [
	"Oops, What went wrong?",
	"What went wrong?",
	"What could have been better?",
	"What you liked in the delivery?",
	"Great, What you liked in the delivery?"
]

export const Feedback = [
	["poor1", "poor2", "poor3", "poor4"],
	["fair1", "fair2", "fair3", "fair4"],
	["average1", "average2", "average3", "average4"],
	["good1", "good2", "good3", "good4"],
	["excellent1", "excellent2", "excellent3", "excellent4"]
]
