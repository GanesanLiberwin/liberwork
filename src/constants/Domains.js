export const workCategory = [
	{ id: "0", title: "" },
	{ id: "APIDev", title: "API Development" },
	{ id: "APIDes", title: "API Design" },
	{ id: "SWDev", title: "Software Development" },
	{ id: "SWDes", title: "Software Design" },
	{ id: "SWRev", title: "Software Review" },
	{ id: "SWTst", title: "Software Testing" },
	{ id: "SWRel", title: "Software Release" },
	{ id: "SWFix", title: "Software Fix" },
	{ id: "Sales", title: "Sales" },
	{ id: "Mktg", title: "Marketing" },
	{ id: "DMktg", title: "Digital Marketing" },
	{ id: "HRHrg", title: "HR Hiring" },
	{ id: "HRLng", title: "HR Learning" },
	{ id: "HRCmp", title: "HR Compensation" },
	{ id: "HREng", title: "HR Engagement" }
]

export const industryDomainList = [
	"",
	"Aerospace",
	"Automotive",
	"Banking",
	"FMCG",
	"Chemicals",
	"Construction",
	"Energy",
	"Education",
	"Hospitality",
	"Healthcare",
	"Insurance",
	"Information Tech",
	"Travel",
	"Telecom"
]

export const expList = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"]
