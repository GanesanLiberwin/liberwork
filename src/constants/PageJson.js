export const WalletJson = {
  BusinessWallet: "BusinessWallet",
  PersonalWallet: "PersonalWallet"
}

export const workRoleJson = {
  Poster: "Poster",
  Worker: "Worker",
  delegateOwner: "Del-O",
  delegateEdit: "Del-E",
  delegateRead: "Del-R"
}

export const workStatusJson = {
  Draft: "Draft",
  DraftNew: "DraftNew",
  Posted: "Posted",
  Published: "Published",
  Bidding: "Bidding",
  BidEnded: "Bid-ended",
  Applied: "Applied",
  Allocated: "Allocated",
  Inprogress: "In-progress",
  Submitted: "Submitted",
  Rework: "Rework",
  Resubmitted: "Resubmitted",
  Closed: "Closed",
  Completed: "Completed",
  Cancelling: "Cancelling",
  Cancelled: "Cancelled",
  OptedOut: "Opted-out",
  OptingOut: "Opting-out"
}

export const workCategoryJson = {
  MyPosts: "My Posts",
  MyDrafts: "My Drafts",
  MyBids: "My Bids",
  MyWorks: "My Works"
}

export const lstWorkTabs = [
  { id: 0, name: "work type" },
  { id: 1, name: "skills" },
  { id: 2, name: "work detail" },
  { id: 3, name: "reward" },
  { id: 4, name: "post" }
]
