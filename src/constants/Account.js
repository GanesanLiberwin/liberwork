export const orgRoles = ["Employee", "BU Leader", "HR Leader", "Enterprise Admin", "Executive", "Support Staff"]

export const orgWorkTypes = ["Employee", "Contractor", "Service Provider"]
