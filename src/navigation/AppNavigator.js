import React from "react"
import { useSelector } from "react-redux"
import { NavigationContainer } from "@react-navigation/native"
import { DrawNavigator, AuthStack, ProfileStack } from "./Navigators"
import StartupScreen from "../screens/StartupScreen"

const AppNavigator = props => {
  const isAutoLoginAttempt = useSelector(state => state.auth.isAutoLoginAttempt)
  const isAuth = useSelector(state => state.isUser === true)
  const isProfile = useSelector(state => state.isProfile === true)

  return (
    <NavigationContainer>
      {isAuth && isProfile && <DrawNavigator />}
      {isAuth && !isProfile && <ProfileStack />}
      {!isAuth && isAutoLoginAttempt && <AuthStack />}
      {!isAuth && !isAutoLoginAttempt && <StartupScreen />}
    </NavigationContainer>
  )
}

export default AppNavigator
