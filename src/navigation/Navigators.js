import React from "react"
import { Image, View, SafeAreaView, TouchableOpacity, Text } from "react-native"
import { createStackNavigator } from "@react-navigation/stack"
import { createDrawerNavigator } from "@react-navigation/drawer"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
//import CustomDrawerContent from '../components/CustomDrawerContent';
import { useSelector } from "react-redux"

import AccountCreationScreen from "../screens/Drawer/AccountCreationScreen"
import NotificationsScreen from "../screens/Drawer/NotificationsScreen"
import SearchScreen from "../screens/Drawer/SearchScreen"
import SkillDetailScreen from "../screens/Drawer/SkillDetailScreen"
import WorkDetailScreen from "../screens/Drawer/WorkDetailScreen"
import WorkBidScreen from "../screens/Drawer/WorkBidScreen"
import WorkDraftScreen from "../screens/Drawer/WorkDraftScreen"
import WalletTransactionsScreen from "../screens/Drawer/WalletTransactionsScreen"

import AuthenticationScreen from "../screens/AuthStack/AuthenticationScreen"
import ProfileInitialScreen from "../screens/AuthStack/ProfileInitialScreen"

import Home from "../screens/HomeStack/Home"

import DashboardScreen from "../screens/DashboardStack/DashboardScreen"

import AccountScreen from "../screens/AccountStack/AccountScreen"
import BusinessWalletScreen from "../screens/AccountStack/BusinessWalletScreen"
import PersonalWalletScreen from "../screens/AccountStack/PersonalWalletScreen"
import WorksHistoryScreen from "../screens/AccountStack/WorksHistoryScreen"
import WorksActiveScreen from "../screens/AccountStack/WorksActiveScreen"

import MoreScreen from "../screens/MoreStack/MoreScreen"
import ProfileScreen from "../screens/MoreStack/ProfileScreen"
import SettingsScreen from "../screens/MoreStack/SettingsScreen"
import SkillsScreen from "../screens/MoreStack/SkillsScreen"
import SupportScreen from "../screens/MoreStack/SupportScreen"

import { IMAGE } from "../constants/Image"
import { MaterialIcons, Octicons } from "@expo/vector-icons"
import { LinearGradient } from "expo-linear-gradient"
import Color from "../constants/Colors"

const navOptionHandler = () => ({
	//headerShown: false,
	animationEnabled: false
})

const StackHome = createStackNavigator()

export const HomeStack = () => {
	return (
		<StackHome.Navigator initialRouteName="Home" headerMode="none">
			<StackHome.Screen name="Home" component={Home} options={navOptionHandler} />
			<StackHome.Screen name="BusinessWallet" component={BusinessWalletScreen} options={navOptionHandler} />
			<StackHome.Screen name="PersonalWallet" component={PersonalWalletScreen} options={navOptionHandler} />
			<StackHome.Screen name="WorksHistory" component={WorksHistoryScreen} options={navOptionHandler} />
			<StackHome.Screen name="WorksActive" component={WorksActiveScreen} options={navOptionHandler} />
			<StackHome.Screen name="Skills" component={SkillsScreen} options={navOptionHandler} />
		</StackHome.Navigator>
	)
}

const StackAccount = createStackNavigator()

export const AccountStack = () => {
	return (
		<StackAccount.Navigator initialRouteName="Account" headerMode="none">
			<StackAccount.Screen name="Account" component={AccountScreen} options={navOptionHandler} />
			{/* <StackAccount.Screen name="Works" component={WorksScreen} options={navOptionHandler} />
      <StackAccount.Screen name="BusinessWallet" component={BusinessWalletScreen} options={navOptionHandler} />
      <StackAccount.Screen name="PersonalWallet" component={PersonalWalletScreen} options={navOptionHandler} /> */}
		</StackAccount.Navigator>
	)
}

const StackMore = createStackNavigator()

export const MoreStack = () => {
	return (
		<StackMore.Navigator initialRouteName="More" headerMode="none">
			<StackMore.Screen name="More" component={MoreScreen} options={navOptionHandler} />
			<StackMore.Screen name="Profile" component={ProfileScreen} options={navOptionHandler} />
			<StackMore.Screen name="Skills" component={SkillsScreen} options={navOptionHandler} />
			<StackMore.Screen name="Settings" component={SettingsScreen} options={navOptionHandler} />
			<StackMore.Screen name="Support" component={SupportScreen} options={navOptionHandler} />
		</StackMore.Navigator>
	)
}

const Tab = createBottomTabNavigator()

export const TabNavigator = () => {
	const isAccount = useSelector(state => state.isAccount === true)
	let initialRoutePage = isAccount ? "home" : "more"
	return (
		<Tab.Navigator
			initialRouteName={initialRoutePage}
			screenOptions={({ route }) => ({
				tabBarIcon: ({ focused, color, size }) => {
					let iconName
					color = focused ? Color.blue2 : "grey"
					size = 30

					switch (route.name) {
						case "home":
							iconName = "home"
							break
						case "more":
							iconName = "menu"
							break
						case "dashboard":
							iconName = "dashboard"
							size = 28
							break
						case "account":
							iconName = "grain"
							break
						case "new":
							return (
								<LinearGradient
									style={{
										height: 34,
										width: 34,
										borderRadius: 17,
										justifyContent: "center",
										alignItems: "center",
										alignSelf: "center"
									}}
									colors={["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.5)", "rgba(182, 231, 106, 0.5)", "rgba(182, 231, 106, 1)"]}
								>
									<Octicons name="plus" size={25} color="#fff" />
								</LinearGradient>
							)
						default:
							break
					}

					return <MaterialIcons name={iconName} size={size} color={color} />
				}
			})}
			tabBarOptions={{
				activeTintColor: "#000000",
				inactiveTintColor: "#000000"
				/* style: {
					backgroundColor: "transparent",
					borderTopWidth: 0,
				}, */
			}}
		>
			<Tab.Screen name="home" component={HomeStack} />
			<Tab.Screen name="dashboard" component={DashboardScreen} />
			<Tab.Screen
				name="new"
				component={CreatePlaceHolder}
				options={{
					animationEnabled: true,
					tabBarLabel: ""
				}}
				listeners={({ navigation }) => ({
					tabPress: event => {
						event.preventDefault()
						navigation.toggleDrawer()
					}
				})}
			/>
			<Tab.Screen name="account" component={AccountStack} />
			<Tab.Screen name="more" component={MoreStack} />
		</Tab.Navigator>
	)
}

const CreatePlaceHolder = () => <View style={{ flex: 1, backgroundColor: "blue" }} />

const Drawer = createDrawerNavigator()

export const DrawNavigator = ({ navigation }) => {
	const isAccount = useSelector(state => state.isAccount === true)
	const account = useSelector(state => state.account.accountDetails)
	return (
		<Drawer.Navigator
			initialRouteName="MenuTab"
			hideStatusBar
			drawerPosition="right"
			drawerType="front"
			//drawerContent={() => <CustomDrawerContent navigation={navigation} />}
			drawerContent={props => {
				return (
					<SafeAreaView style={{ flex: 1, justifyContent: "center" }}>
						{isAccount ? (
							<View style={{ alignItems: "center" }}>
								<View style={{ borderRadius: 7, backgroundColor: "#07b0f9" }}>
									<Text style={{ alignSelf: "center", fontSize: 20, fontWeight: "bold", color: "#fff", padding: 5 }}>{account.orgId}</Text>
								</View>
								<TouchableOpacity style={{ marginTop: 25 }} onPress={() => props.navigation.navigate("WorkDraft")}>
									<Text>New Work</Text>
								</TouchableOpacity>
								<TouchableOpacity style={{ marginTop: 25 }}>
									<Text>New Event</Text>
								</TouchableOpacity>
								<TouchableOpacity style={{ marginTop: 25 }}>
									<Text>New Poll</Text>
								</TouchableOpacity>
								<TouchableOpacity style={{ marginTop: 25 }} onPress={() => props.navigation.navigate("WorkDraft")}>
									<Text>New Idea</Text>
								</TouchableOpacity>
							</View>
						) : (
							<View style={{ borderRadius: 7, backgroundColor: "#07b0f9" }}>
								<Text style={{ alignSelf: "center", fontSize: 25, fontWeight: "bold", color: "#fff", padding: 5 }}>no account</Text>
							</View>
						)}
						<TouchableOpacity style={{ marginTop: 70 }} onPress={() => props.navigation.navigate("MenuTab")}>
							<Text style={{ alignSelf: "center", fontSize: 16, fontWeight: "bold" }}>CLOSE</Text>
						</TouchableOpacity>
					</SafeAreaView>
				)
			}}
			drawerStyle={{ backgroundColor: "rgba(192,192,192,0.92)", width: "45%" }}
		>
			<Drawer.Screen name="MenuTab" component={TabNavigator} />
			<Drawer.Screen name="Notifications" component={NotificationsScreen} />
			<Drawer.Screen name="Search" component={SearchScreen} />
			<Drawer.Screen name="SkillDetail" component={SkillDetailScreen} options={{ gestureEnabled: false }} />
			<Drawer.Screen name="WorkDetail" component={WorkDetailScreen} options={{ gestureEnabled: false }} />
			<Drawer.Screen name="WorkBid" component={WorkBidScreen} options={{ gestureEnabled: false }} />
			<Drawer.Screen name="WorkDraft" component={WorkDraftScreen} options={{ gestureEnabled: false }} />
			<Drawer.Screen name="AccountCreate" component={AccountCreationScreen} options={{ gestureEnabled: false }} />
			<Drawer.Screen name="WalletTrans" component={WalletTransactionsScreen} options={{ gestureEnabled: false }} />
		</Drawer.Navigator>
	)
}

const StackAuth = createStackNavigator()

export const AuthStack = () => {
	return (
		<StackAuth.Navigator initialRouteName="SignUp" headerMode="none">
			<StackAuth.Screen name={"SignUp"} component={AuthenticationScreen} options={navOptionHandler} />
		</StackAuth.Navigator>
	)
}

const StackProfile = createStackNavigator()

export const ProfileStack = () => {
	return (
		<StackProfile.Navigator initialRouteName="Profile" headerMode="none">
			<StackProfile.Screen name="Profile" component={ProfileInitialScreen} options={navOptionHandler} />
		</StackProfile.Navigator>
	)
}

export const Splash = () => (
	<SafeAreaView style={{ flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: "#fff" }}>
		<Image style={{ height: 110, width: 110 }} source={IMAGE.ICON_APP} resizeMode="contain" />
	</SafeAreaView>
)
