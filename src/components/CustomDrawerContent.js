import React from "react"
import { View, Text, SafeAreaView, TouchableOpacity, ScrollView } from "react-native"
import { useDispatch } from "react-redux"
import * as authActions from "../store/actions/authActions"

const CustomDrawerContent = props => {
	const dispatch = useDispatch()
	return (
		<SafeAreaView style={{ flex: 1 }}>
			<View>
				<Text style={{ marginLeft: 20, fontSize: 20, fontWeight: "bold" }}>Quick Access</Text>
			</View>
			<ScrollView style={{ marginLeft: 20 }}>
				<TouchableOpacity style={{ marginTop: 20 }} onPress={() => props.navigation.navigate("MenuTab")}>
					<Text>Menu Tab</Text>
				</TouchableOpacity>
				<TouchableOpacity style={{ marginTop: 20 }} onPress={() => props.navigation.navigate("Notifications")}>
					<Text>Notifications</Text>
				</TouchableOpacity>
				<TouchableOpacity style={{ marginTop: 20 }} onPress={() => props.navigation.navigate("Search")}>
					<Text>Search</Text>
				</TouchableOpacity>
				<TouchableOpacity style={{ marginTop: 20 }} onPress={() => props.navigation.navigate("resources")}>
					<Text>Resources</Text>
				</TouchableOpacity>
			</ScrollView>
			<TouchableOpacity
				style={{ marginTop: 20, marginLeft: 25 }}
				onPress={() => {
					dispatch(authActions.logout())
					//navigation.navigate('Auth');
				}}
			>
				<Text>SignOut</Text>
			</TouchableOpacity>
		</SafeAreaView>
	)
}

export default CustomDrawerContent
