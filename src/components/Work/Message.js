import React, { Component } from "react"
import { View, StyleSheet, Text } from "react-native"
import Colors from "../../constants/Colors"

export default class Message extends Component {
	render() {
		const senderType = this.props.workRole
		const textColor = this.props.message.senderType === senderType ? Colors.TextLight : "rgba(0,0,0,0.8)"
		return (
			<View
				style={{
					...styles.messageCard,
					alignSelf: this.props.message.senderType === senderType ? "flex-end" : "flex-start",
					backgroundColor: this.props.message.senderType === senderType ? "rgba(0,0,0,0.4)" : Colors.TextLight
				}}
			>
				{this.props.message.senderType === senderType ? (
					<Text style={{ color: textColor, textAlign: "left", fontSize: 11 }}>You</Text>
				) : (
					<Text style={{ color: textColor, textAlign: "left", fontSize: 11 }}>
						{this.props.message.sender} | {this.props.message.senderType}
					</Text>
				)}
				<Text style={{ color: textColor, paddingVertical: 3 }}>{this.props.message.message}</Text>
				<Text style={{ color: textColor, textAlign: "right", fontSize: 10 }}>{this.props.message.time}</Text>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	messageCard: {
		minWidth: "30%",
		maxWidth: "90%",
		marginVertical: 10,
		padding: 5,
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	}
})
