import React, { useState } from "react"
import {
	View,
	StyleSheet,
	Text,
	TextInput,
	FlatList,
	KeyboardAvoidingView,
	Modal,
	TouchableOpacity,
	TouchableHighlight,
	ScrollView
} from "react-native"
import { useDispatch, useSelector } from "react-redux"
import Colors from "../../constants/Colors"
import * as workFlowActions from "../../store/actions/workFlowActions"

export const WorkSubmissionCard = props => {
	const dispatch = useDispatch()

	const [modalVisible, setModalVisible] = useState(false)
	const [submissionText, setSubmissionText] = useState("")

	const submissions = useSelector(state => state.workFlow.lstSubmission)

	const onTextChange = text => {
		setSubmissionText(text)
		dispatch(workFlowActions.updateWorkSubmission(text, []))
	}
	return (
		<KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : "height"}>
			<View style={styles.workSubmissionCard}>
				<View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: "2%" }}>
					<Text style={{ ...styles.subHeaderText }}>Work submission</Text>
					<TouchableOpacity style={{}} onPress={() => setModalVisible(true)}>
						<Text style={{ ...styles.subHeaderText, fontWeight: "500", color: Colors.blue2 }}>more</Text>
					</TouchableOpacity>
				</View>
				{!props.editable ? (
					<View>
						<Text numberOfLines={3} ellipsizeMode="tail" style={{ ...styles.workSubmissionText, marginTop: "2%" }}>
							{props.submission}
						</Text>
						<View style={{ marginTop: "5%", marginBottom: "2%" }}>
							<FlatList
								horizontal={true}
								showsHorizontalScrollIndicator={false}
								data={props.attachments}
								keyExtractor={result => result}
								renderItem={({ item, index }) => (
									<Text style={{ marginRight: 10, backgroundColor: Colors.GreyBackgroundDark, padding: 2, alignSelf: "center" }}>{item}</Text>
								)}
							/>
						</View>
					</View>
				) : (
					<TextInput
						style={{ ...styles.workSubmissionText, marginVertical: "3%", backgroundColor: Colors.white, borderRadius: 5 }}
						allowFontScaling={false}
						autoCapitalize="none"
						autoCorrect={false}
						textAlignVertical="top"
						placeholder="your submission here"
						placeholderTextColor="rgba(0,0,0,0.25)"
						maxLength={250}
						multiline={true}
						value={submissionText}
						onChangeText={text => onTextChange(text)}
					></TextInput>
				)}
				<Modal
					animationType="slide"
					transparent={true}
					visible={modalVisible}
					onRequestClose={() => {
						setModalVisible(!modalVisible)
					}}
				>
					<View style={styles.centeredView}>
						<View style={styles.modalView}>
							<TouchableHighlight style={{ marginVertical: 10, alignSelf: "flex-end" }} onPress={() => setModalVisible(false)}>
								<Text style={styles.buttonTextStyle}>CLOSE</Text>
							</TouchableHighlight>
							<ScrollView showsVerticalScrollIndicator={false}>
								{submissions.length > 0
									? submissions.map((item, index) => {
											return (
												<View key={index}>
													<Text style={{ ...styles.subHeaderText, color: Colors.TextLight, marginTop: 10 }}>Work submission {index + 1}</Text>
													<Text style={{ ...styles.workSubmissionText, color: Colors.TextLight }}>{item.description}</Text>
												</View>
											)
									  })
									: null}
							</ScrollView>
						</View>
					</View>
				</Modal>
			</View>
		</KeyboardAvoidingView>
	)
}

const styles = StyleSheet.create({
	workSubmissionCard: {
		flex: 1,
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	workSubmissionText: {
		textAlign: "justify",
		padding: "2%"
	},
	subHeaderText: {
		//color: "rgba(0,0,0,0.5)",
		fontSize: 13,
		fontStyle: "italic"
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	modalView: {
		width: "90%",
		minHeight: "25%",
		maxHeight: "80%",
		backgroundColor: Colors.BGDark,
		borderRadius: 7,
		padding: 10
		//alignItems: "center"
	},
	buttonTextStyle: {
		color: Colors.TextLight,
		//fontSize: 12,
		fontWeight: "bold",
		textAlign: "center"
	}
})

export default WorkSubmissionCard
