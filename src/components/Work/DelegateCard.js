import React, { useState } from "react"
import { useDispatch, useSelector } from "react-redux"

import { View, StyleSheet, Text, Modal, TouchableHighlight, TouchableOpacity } from "react-native"
import { LinearGradient } from "expo-linear-gradient"
import { Octicons, MaterialCommunityIcons } from "@expo/vector-icons"

import Colors from "../../constants/Colors"
import { workStatusJson, workRoleJson } from "../../constants/PageJson"

import AccountValidation from "../../components/Account/AccountValidation"
import * as workFlowActions from "../../store/actions/workFlowActions"
import * as accountActions from "../../store/actions/accountActions"

export const DelegateCard = props => {
	const dispatch = useDispatch()

	const orgUnits = useSelector(state => state.orgDetail.orgUnits)
	const verifiedAccountDetails = useSelector(state => state.account.accountVerified.accDetails)
	const accountValidated = useSelector(state => state.account.accountVerified.isValid)
	const delegateStorePermission = useSelector(state => state.workFlow.workDetails.delegatePermission)
	const workId = useSelector(state => state.workFlow.workId)

	const [modalVisible, setModalVisible] = useState(false)
	const [selectPermission, setSelectPermission] = useState(delegateStorePermission)
	const [checkStatus, setCheckStatus] = useState("")
	const [accountRole, setAccountRole] = useState("")
	const [delegateEdit, setDelegateEdit] = useState(false)

	const permissions = ["read", "edit", "owner"]

	const isEdit = delegateStorePermission && (globalThis.isMapPermission === undefined || !globalThis.isMapPermission) ? onMapPermission() : ""

	function onMapPermission() {
		if (delegateStorePermission) {
			setSelectPermission(delegateStorePermission)
		}
		globalThis.isMapPermission = true
	}

	const addDelegate = async () => {
		if (verifiedAccountDetails.id === props.allocatedTo) {
			setCheckStatus("Delegate cannot be the worker")
			return
		}

		try {
			if (props.status === workStatusJson.Draft) {
				dispatch(workFlowActions.updateWorkFlowFields("delegateAccountId", verifiedAccountDetails.id))
				dispatch(workFlowActions.updateWorkFlowFields("delegatePermission", selectPermission))
				setCheckStatus("Delegate added successfully")
			} else {
				dispatch(workFlowActions.updateDelegate(workId, verifiedAccountDetails.id, selectPermission))
					.then(data => {
						setCheckStatus("Delegate added successfully")
					})
					.catch(error => {
						// error msg
					})
			}
		} catch (err) {
			// error msg
		}
	}

	const removeDelegate = () => {
		try {
			if (props.status === workStatusJson.Draft) {
				setDelegateEdit(true)
				dispatch(workFlowActions.updateWorkFlowFields("delegatePermission", ""))
				dispatch(workFlowActions.updateWorkFlowFields("delegateAccountId", "None"))
				dispatch(accountActions.clearAccountVerifiedData())
			} else {
				dispatch(workFlowActions.updateDelegate(workId, "None", ""))
					.then(data => {
						setDelegateEdit(true)
						dispatch(accountActions.clearAccountVerifiedData())
					})
					.catch(error => {
						// error msg
					})
			}
		} catch (err) {
			// error msg
		}
	}

	const delegatePermission = item => {
		setSelectPermission(item)
	}

	const onModalClose = () => {
		setModalVisible(!modalVisible)
		setSelectPermission("")
		setCheckStatus("")
		setAccountRole("")
		setDelegateEdit(false)

		globalThis.isMap = false
		globalThis.isMapPermission = false
	}

	const onShowAccount = role => {
		onGetAccountApiCall(role === "DE" ? props.delegateAccountId : role === "WK" ? props.allocatedTo : props.posterAccountId)
		setAccountRole(role)
		setModalVisible(true)
	}

	function onGetAccountApiCall(accountId) {
		try {
			dispatch(accountActions.getAccountDetails(accountId))
				.then(data => {
					dispatch(accountActions.updateAccountVerifiedData(data, true))
				})
				.catch(error => {})
		} catch (err) {}
	}

	return (
		<View style={styles.delegateCard}>
			<Text style={{ ...styles.subHeaderText, fontSize: 10, position: "absolute", bottom: 0, color: "rgba(0,0,0,0.5)", alignSelf: "center" }}>
				{props.workRole}
			</Text>
			{props.workRole === workRoleJson.Poster ? (
				<View style={{ flexDirection: "row", paddingVertical: "2%" }}>
					<View style={{ width: "50%" }}>
						<Text style={styles.subHeaderText}>Worker</Text>
						{props.allocatedTo != "" && (
							<TouchableOpacity onPress={() => onShowAccount("WK")}>
								<LinearGradient style={styles.linearGradient} colors={["rgba(0, 0, 0, 0.4)", "rgba(0, 0, 0, 0.05)"]}>
									<Text style={{ fontWeight: "500", fontSize: 16 }}>WK</Text>
								</LinearGradient>
							</TouchableOpacity>
						)}
					</View>
					<View style={{ width: "50%" }}>
						<Text style={styles.subHeaderText}>Delegate</Text>
						{props.delegateAccountId === "None" || props.delegateAccountId === "" ? (
							<TouchableOpacity
								onPress={() => {
									setAccountRole("DE"), setModalVisible(true), setDelegateEdit(true)
								}}
							>
								<LinearGradient
									style={styles.linearGradient}
									colors={props.isDelegatable ? ["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.5)"] : ["rgba(0, 0, 0, 0.4)", "rgba(0, 0, 0, 0.05)"]}
								>
									<Octicons name="plus" style={styles.iconPlusStyle} />
								</LinearGradient>
							</TouchableOpacity>
						) : (
							<TouchableOpacity onPress={() => onShowAccount("DE")}>
								<LinearGradient style={styles.linearGradient} colors={["rgba(0, 0, 0, 0.4)", "rgba(0, 0, 0, 0.05)"]}>
									<Text style={{ fontWeight: "500", fontSize: 16 }}>DE</Text>
								</LinearGradient>
							</TouchableOpacity>
						)}
					</View>
				</View>
			) : (
				<View style={{ flexDirection: "row", paddingVertical: "2%" }}>
					<View style={{ width: "50%" }}>
						<Text style={styles.subHeaderText}>Poster</Text>
						<TouchableOpacity onPress={() => onShowAccount("PO")}>
							<LinearGradient style={styles.linearGradient} colors={["rgba(0, 0, 0, 0.4)", "rgba(0, 0, 0, 0.05)"]}>
								<Text style={{ fontWeight: "500", fontSize: 16 }}>PO</Text>
							</LinearGradient>
						</TouchableOpacity>
					</View>
					{props.workRole === workRoleJson.Worker ? (
						<View style={{ width: "50%" }}>
							<Text style={styles.subHeaderText}>Delegate</Text>
							{props.delegateAccountId != "None" && props.delegateAccountId != "" && (
								<TouchableOpacity onPress={() => onShowAccount("DE")}>
									<LinearGradient style={styles.linearGradient} colors={["rgba(0, 0, 0, 0.4)", "rgba(0, 0, 0, 0.05)"]}>
										<Text style={{ fontWeight: "500", fontSize: 16 }}>DE</Text>
									</LinearGradient>
								</TouchableOpacity>
							)}
						</View>
					) : (
						<View style={{ width: "50%" }}>
							<Text style={styles.subHeaderText}>Worker</Text>
							{props.allocatedTo != "" && (
								<TouchableOpacity onPress={() => onShowAccount("WK")}>
									<LinearGradient style={styles.linearGradient} colors={["rgba(0, 0, 0, 0.4)", "rgba(0, 0, 0, 0.05)"]}>
										<Text style={{ fontWeight: "500", fontSize: 16 }}>WK</Text>
									</LinearGradient>
								</TouchableOpacity>
							)}
						</View>
					)}
				</View>
			)}
			<Modal
				animationType="slide"
				transparent={true}
				visible={modalVisible}
				onRequestClose={() => {
					setModalVisible(!modalVisible)
				}}
			>
				<View style={styles.centeredView}>
					<View style={styles.modalView}>
						<TouchableHighlight style={{ marginVertical: 10, alignSelf: "flex-end" }} onPress={() => onModalClose()}>
							<Text style={styles.buttonTextStyle}>CLOSE</Text>
						</TouchableHighlight>
						<View style={{ flexDirection: "row", marginLeft: 10, alignItems: "center" }}>
							<LinearGradient style={{ ...styles.linearGradient, marginTop: 0 }} colors={["rgba(255, 255, 255, 0.4)", "rgba(255, 255, 255, 0.05)"]}>
								<Text style={{ fontWeight: "500", fontSize: 16, color: Colors.TextLight }}>{accountRole}</Text>
							</LinearGradient>
							<Text style={{ marginLeft: 10, color: Colors.TextLight }}>ACCOUNT INFORMATION</Text>
							{props.workRole === "Poster" && props.delegateAccountId != "None" && props.delegateAccountId != "" && (
								<MaterialCommunityIcons
									name="account-remove"
									style={{ ...styles.iconEditStyle, color: Colors.orange1 }}
									onPress={() => removeDelegate()}
								/>
							)}
							{props.workRole === "Poster" && props.delegateAccountId != "None" && props.delegateAccountId != "" && (
								<MaterialCommunityIcons
									name="account-edit"
									style={{ ...styles.iconEditStyle, color: Colors.green1 }}
									onPress={() => setDelegateEdit(!delegateEdit)}
								/>
							)}
						</View>
						{delegateEdit ? (
							props.isDelegatable ? (
								<View>
									<AccountValidation dark={true} accountEmail={verifiedAccountDetails.accountEmail} />
									{accountValidated && (
										<View style={{ marginHorizontal: 15, alignSelf: "flex-end" }}>
											<View style={{ flexDirection: "row", marginTop: 10, justifyContent: "space-between" }}>
												{permissions.map((item, index) => {
													const itemColor = item === selectPermission ? Colors.green1 : Colors.TextLight

													return (
														<View
															key={index}
															style={{
																minWidth: "15%",
																marginHorizontal: 10,
																borderRadius: 5,
																backgroundColor: itemColor
															}}
														>
															<Text style={{ padding: 3, textAlign: "center" }} onPress={() => delegatePermission(item)}>
																{item}
															</Text>
														</View>
													)
												})}
												<TouchableOpacity
													style={{
														//width: "18%",
														marginLeft: 20,
														borderRadius: 5,
														padding: 4,
														backgroundColor: Colors.blue2
													}}
													onPress={() => addDelegate()}
												>
													<Text style={{ ...styles.buttonTextStyle, color: "#fff" }}>DELEGATE</Text>
												</TouchableOpacity>
											</View>
											<Text style={{ marginTop: 10, fontSize: 12, fontStyle: "italic", color: Colors.TextLight, alignSelf: "flex-end" }}>
												{checkStatus}
											</Text>
										</View>
									)}
								</View>
							) : (
								<Text style={{ color: Colors.TextLight, alignSelf: "center", padding: 15 }}>This work can longer be delegated</Text>
							)
						) : (
							<View>
								<View style={{ ...styles.accountBlock, marginTop: 15 }}>
									<Text style={styles.accountTextHeader}>name</Text>
									<Text numberOfLines={1} style={styles.accountText}>
										{verifiedAccountDetails.firstName} {verifiedAccountDetails.lastName}
									</Text>
								</View>
								<View style={{ ...styles.accountBlock, marginTop: 5 }}>
									<Text style={styles.accountTextHeader}>email</Text>
									<Text numberOfLines={1} style={styles.accountText}>
										{verifiedAccountDetails.accountEmail}
									</Text>
								</View>
								<View style={{ ...styles.accountBlock, marginTop: 5 }}>
									<Text style={styles.accountTextHeader}>org unit</Text>
									<Text numberOfLines={1} style={styles.accountText}>
										{orgUnits.filter(units => units.orgUnitId === verifiedAccountDetails.orgUnitId).map(filteredUnit => filteredUnit.orgUnitName)}
									</Text>
								</View>
								<View style={{ ...styles.accountBlock, marginVertical: 5 }}>
									<Text style={styles.accountTextHeader}>org role</Text>
									<Text style={styles.accountText}>{verifiedAccountDetails.orgPersonRole}</Text>
								</View>
							</View>
						)}
					</View>
				</View>
			</Modal>
		</View>
	)
}

const styles = StyleSheet.create({
	delegateCard: {
		width: "34%",
		alignSelf: "center",
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	iconPlusStyle: {
		fontSize: 23,
		color: "#fff"
	},
	iconEditStyle: {
		marginLeft: 15,
		fontSize: 18,
		color: Colors.TextLight
	},
	subHeaderText: {
		//color: "rgba(0,0,0,0.5)",
		fontSize: 13,
		fontStyle: "italic"
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	modalView: {
		width: "90%",
		minHeight: "25%",
		backgroundColor: Colors.BGDark,
		borderRadius: 7,
		padding: 10
		//alignItems: "center"
	},
	buttonTextStyle: {
		color: Colors.TextLight,
		//fontSize: 12,
		fontWeight: "bold",
		textAlign: "center"
	},
	linearGradient: {
		height: 34,
		width: 34,
		borderRadius: 17,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "center",
		marginTop: "7%"
	},
	accountBlock: {
		flexDirection: "row",
		marginLeft: 10,
		alignContent: "center"
	},
	accountTextHeader: {
		fontStyle: "italic",
		color: Colors.TextLight,
		width: "22%"
	},
	accountText: {
		color: Colors.TextLight,
		width: "75%"
	}
})

export default DelegateCard
