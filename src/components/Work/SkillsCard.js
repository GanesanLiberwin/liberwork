import React from "react"
import { View, StyleSheet, Text } from "react-native"
import Colors from "../../constants/Colors"

export const SkillsCard = props => {
	return (
		<View style={styles.skillsCard}>
			<Text style={styles.subHeaderText}>Skills</Text>
			<View style={{ marginTop: "5%" }}>
				{props.skills.map(item => {
					return (
						<View key={item.skill} style={{ flexDirection: "row", justifyContent: "space-between", paddingVertical: 1 }}>
							<Text numberOfLines={1} style={{ fontSize: 14, width: "70%" }}>
								{item.skill}
							</Text>
							<Text style={{ fontSize: 12 }}>{item.required ? "MUST" : "NICE"}</Text>
						</View>
					)
				})}
			</View>
			{props.industryDomain != "" && (
				<View style={{ flexDirection: "row", justifyContent: "space-between", paddingVertical: 1, marginTop: "5%" }}>
					<Text numberOfLines={1} style={{ fontSize: 14, width: "50%" }}>
						{props.industryDomain}
					</Text>
					<Text style={{ fontSize: 13 }}>Exp - {props.industryDomainExp !== "0" ? props.industryDomainExp + ` yrs` : `Any`}</Text>
				</View>
			)}
		</View>
	)
}

const styles = StyleSheet.create({
	skillsCard: {
		width: "50%",
		padding: "2%",
		marginHorizontal: "5%",
		marginTop: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	subHeaderText: {
		//color: "rgba(0,0,0,0.5)",
		fontSize: 13,
		fontStyle: "italic"
	}
})

export default SkillsCard
