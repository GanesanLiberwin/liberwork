import React, { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { View, StyleSheet, Text, TextInput, TouchableOpacity, Modal, SectionList, KeyboardAvoidingView } from "react-native"
import { MaterialIcons } from "@expo/vector-icons"
import { format } from "date-fns"

import Colors from "../../constants/Colors"
import Message from "./Message"
import * as messageActions from "../../store/actions/messageActions"

export const CommunicationCard = props => {
	const dispatch = useDispatch()
	const isLoading = useSelector(state => state.apiCallsInProgress > 0)

	const account = useSelector(state => state.account.accountDetails)
	const workId = useSelector(state => state.workFlow.workId)
	const workRole = useSelector(state => state.workFlow.workRole)
	const comMessages = useSelector(state => state.workFlow.Communication.messages)
	const [errorMessage, setErrorMessage] = useState({})

	const [modalVisible, setModalVisible] = useState(false)
	const [messageText, setMessageText] = useState("")

	const [messageList, setMessageList] = useState([])

	const onTextChange = text => {
		setMessageText(text)
	}

	const getSortedMesssages = lstMessages => {
		let lstSortedMessages = []
		try {
			let lstGroupMessages = lstMessages.reduce((r, a) => {
				r[a.date] = [...(r[a.date] || []), a]
				return r
			}, {})

			Object.keys(lstGroupMessages).forEach(function (key) {
				lstSortedMessages.push({ title: key, data: lstGroupMessages[key] })
			})

			return lstSortedMessages
		} catch (err) {
			console.log(err)
			return lstSortedMessages
		}
	}

	const fetchMessageList = async () => {
		if (workId !== "") {
			try {
				setErrorMessage({})
				dispatch(messageActions.getMessages(workId))
					.then(lstMessages => {
						if (lstMessages && lstMessages.length > 0) {
							let lstSortedMessages = getSortedMesssages(lstMessages)
							setMessageList(lstSortedMessages)
						}
						setErrorMessage({})
						setModalVisible(true)
					})
					.catch(error => {
						setErrorMessage({ onApiCall: error.message })
						setModalVisible(true)
					})
			} catch (err) {
				setErrorMessage({ onApiCall: err.message })
				setModalVisible(true)
			}
		}
	}

	const getUpdatedMesssages = (senderType, senderName, messageText) => {
		let lstUpdatedMessages = []
		try {
			let lstMessages = []

			if (comMessages && comMessages.length > 0) {
				// lstUpdatedMessages = getSortedMesssages(Communication.messages)
				// setMessageList(lstUpdatedMessages)
				lstMessages = [...comMessages]
			}
			const msgDateTime = new Date()
			lstMessages.unshift({
				date: "Today",
				time: format(msgDateTime, "p"),
				sender: senderName,
				senderType: senderType,
				message: messageText,
				msgDateTime: msgDateTime,
				isToday: true,
				isYesterday: false
			})
			lstUpdatedMessages = getSortedMesssages(lstMessages)
			return lstUpdatedMessages
		} catch (err) {
			console.log("getUpdatedMesssages => ", err)
			return lstUpdatedMessages
		}
	}

	const sendMessage = async () => {
		if (messageText.trim().length > 0 && !isLoading) {
			try {
				setErrorMessage({})
				const senderName = account.firstName + " " + account.lastName
				const senderId = account.id
				const senderType = workRole

				dispatch(messageActions.postMessage(workId, senderId, senderType, senderName, "text", messageText))
					.then(() => {
						setErrorMessage({})
						setMessageText("")
						let lstUpdatedMessages = getUpdatedMesssages(senderType, senderName, messageText)
						setMessageList(lstUpdatedMessages)
					})
					.catch(error => {
						//need to handle error
						setErrorMessage({ onApiCall: "send failed, please retry" })
					})
			} catch (err) {
				// setErrorMessage({ onApiCall: err.message })
				setErrorMessage({ onApiCall: "send failed, please retry" })
			}
		} else {
			setErrorMessage({ onApiCall: "Please enter test message" })
		}
	}

	return (
		<View style={styles.communicationCard}>
			<View style={{ flexDirection: "row", justifyContent: "space-between", marginVertical: "2%" }}>
				<Text style={{ ...styles.subHeaderText }}>Messages - no new messages</Text>
				{isLoading ? (
					<Text style={{ ...styles.subHeaderText }}>loading</Text>
				) : (
					<TouchableOpacity onPress={() => fetchMessageList()}>
						<Text style={{ ...styles.subHeaderText, fontWeight: "500", color: Colors.blue2 }}>more</Text>
					</TouchableOpacity>
				)}
			</View>
			<Modal
				animationType="slide"
				transparent={true}
				visible={modalVisible}
				onRequestClose={() => {
					setModalVisible(!modalVisible)
				}}
			>
				<KeyboardAvoidingView style={styles.centeredView} behavior={Platform.OS == "ios" ? "padding" : "height"}>
					<View style={styles.modalView}>
						<TouchableOpacity
							style={{ marginVertical: 10, alignSelf: "flex-end" }}
							onPress={() => {
								setModalVisible(!modalVisible)
							}}
						>
							<Text style={styles.buttonTextStyle}>CLOSE</Text>
						</TouchableOpacity>
						<SectionList
							showsVerticalScrollIndicator={false}
							inverted
							sections={messageList}
							renderItem={({ item, section }) => <Message message={item} workRole={props.workRole} />}
							renderSectionFooter={({ section }) => (
								<View style={styles.sectionFooter}>
									<Text style={styles.footerText}>{section.title}</Text>
								</View>
							)}
							keyExtractor={(item, index) => item + index}
						/>
						{props.editable && (
							<View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 20 }}>
								<TextInput
									allowFontScaling={false}
									autoCapitalize="none"
									autoCorrect={false}
									textAlignVertical="top"
									placeholder="your message here"
									placeholderTextColor="rgba(192,192,192, 0.5)"
									maxLength={100}
									multiline={true}
									value={messageText}
									onChangeText={text => onTextChange(text)}
									style={{
										padding: 4,
										fontSize: 15,
										width: messageText != "" ? "90%" : "100%",
										borderWidth: 1,
										borderRadius: 5,
										color: Colors.TextLight,
										borderColor: Colors.GreyBackgroundDark
									}}
								/>
								{messageText != "" && (
									<MaterialIcons
										name="send"
										style={styles.iconPlusStyle}
										onPress={() => {
											sendMessage(messageText)
										}}
									/>
								)}
							</View>
						)}
						{errorMessage.onApiCall ? (
							<View style={{ marginTop: 10, alignSelf: "center" }}>
								<Text style={{ color: Colors.red }}>{errorMessage.onApiCall}</Text>
							</View>
						) : null}
					</View>
				</KeyboardAvoidingView>
			</Modal>
		</View>
	)
}

const styles = StyleSheet.create({
	communicationCard: {
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	subHeaderText: {
		//color: "rgba(0,0,0,0.5)",
		fontSize: 13,
		fontStyle: "italic"
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	modalView: {
		width: "90%",
		minHeight: "50%",
		maxHeight: "80%",
		backgroundColor: Colors.BGDark,
		borderRadius: 7,
		padding: 10
		//alignItems: "center"
	},
	buttonTextStyle: {
		color: Colors.TextLight,
		fontWeight: "bold",
		textAlign: "center"
	},
	messageCard: {
		minWidth: "30%",
		maxWidth: "85%",
		marginVertical: 10,
		padding: 5,
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	sectionFooter: {
		width: "30%",
		marginTop: 10,
		borderRadius: 5,
		backgroundColor: "rgba(0,0,0,0.4)",
		alignSelf: "center"
	},
	footerText: {
		fontSize: 12,
		textAlign: "center",
		padding: 2,
		color: Colors.TextLight
	},
	item: {
		padding: 10,
		fontSize: 18,
		height: 44
	},
	iconPlusStyle: {
		fontSize: 28,
		alignSelf: "center",
		color: Colors.blue2
	}
})

export default CommunicationCard
