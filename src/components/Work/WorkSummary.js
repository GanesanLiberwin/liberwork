import React from "react"
import { View } from "react-native"
import RewardCard from "./RewardCard"
import SkillsCard from "./SkillsCard"
import WorkTypeCard from "./WorkTypeCard"
import WorkDescriptionCard from "./WorkDescriptionCard"
import StatusCard from "./StatusCard"
import DelegateCard from "./DelegateCard"
import { workStatusJson } from "../../constants/PageJson"

export const WorkSummary = props => {
	return (
		<View>
			{props.showRewardSkills && (
				<View style={{ flexDirection: "row" }}>
					<RewardCard rewardUnits={props.rewardUnits} rewardModel={props.rewardModel} rewardType={props.rewardType} navigation={props.navigation} />
					<SkillsCard skills={props.skills} industryDomain={props.industryDomain} industryDomainExp={props.industryDomainExp} />
				</View>
			)}
			{props.showWorkType && (
				<WorkTypeCard
					workForce={props.workForce}
					isBeginner={props.isBeginner}
					workSelection={props.workSelection}
					workType={props.workType}
					isRemote={props.isRemote}
					workLocation={props.workLocation}
					workCategory={props.workCategory}
					functionalExp={props.functionalExp}
				/>
			)}
			{props.showWorkDesc && (
				<WorkDescriptionCard
					description={props.description}
					outcome={props.outcome}
					effort={props.effort}
					attachments={props.attachments}
					workStartDate={props.workStartDate}
					workEndDate={props.workEndDate}
					isCritical={props.isCritical}
				/>
			)}
			<View style={{ flexDirection: "row" }}>
				<StatusCard statusDate={props.statusDate} status={props.status} statusHead={props.statusHead} />
				<DelegateCard
					delegateAccountId={props.delegateAccountId}
					posterAccountId={props.posterAccountId}
					workRole={props.workRole}
					status={props.status}
					allocatedTo={props.allocatedTo ? props.allocatedTo : ""}
					isDelegatable={
						props.status === workStatusJson.Cancelling ||
						props.status === workStatusJson.OptingOut ||
						props.status === workStatusJson.Completed ||
						props.status === workStatusJson.Closed ||
						props.status === workStatusJson.Cancelled ||
						props.status === workStatusJson.OptedOut
							? false
							: true
					}
				/>
			</View>
		</View>
	)
}

export default WorkSummary
