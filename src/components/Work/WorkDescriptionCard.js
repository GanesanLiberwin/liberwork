import React, { useState } from "react"
import { View, StyleSheet, Text, FlatList, TouchableOpacity, TouchableHighlight, Modal } from "react-native"
import { LinearGradient } from "expo-linear-gradient"

import Colors from "../../constants/Colors"
import { DateToString, TimeToString } from "../../components/Utilities/DateFormat"

export const WorkDescriptionCard = props => {
	const [modalVisible, setModalVisible] = useState(false)

	return (
		<View style={styles.workDetailsCard}>
			<View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: "2%" }}>
				<Text style={{ ...styles.subHeaderText }}>Work description</Text>
				<TouchableOpacity style={{}} onPress={() => setModalVisible(true)}>
					<Text style={{ ...styles.subHeaderText, fontWeight: "500", color: Colors.blue2 }}>outcome & more</Text>
				</TouchableOpacity>
			</View>
			<Text numberOfLines={3} ellipsizeMode="tail" style={{ ...styles.workDescriptionText, marginTop: "2%" }}>
				{props.description}
			</Text>
			<View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: "5%" }}>
				<View style={{}}>
					<Text style={styles.subHeaderText}>Effort (hrs)</Text>
					<LinearGradient
						style={{
							height: 46,
							width: 46,
							borderRadius: 23,
							justifyContent: "center",
							alignItems: "center",
							alignSelf: "center",
							marginTop: "7%"
						}}
						colors={["rgba(0, 0, 0, 0.4)", "rgba(0, 0, 0, 0.05)"]}
					>
						<Text style={{ fontWeight: "bold", fontSize: 15 }}>{props.effort.toString().padStart(2, "0")}</Text>
					</LinearGradient>
				</View>
				<View>
					<Text style={styles.subHeaderText}>Start</Text>
					<View style={{ justifyContent: "center", flex: 1 }}>
						<Text style={{ fontWeight: "bold", fontSize: 14 }}>{DateToString(props.workStartDate * 1000)}</Text>
						<Text style={{ fontSize: 11 }}>{TimeToString(props.workStartDate * 1000)}</Text>
					</View>
				</View>
				<View>
					<Text style={styles.subHeaderText}>End</Text>
					<View style={{ justifyContent: "center", flex: 1 }}>
						<Text style={{ fontWeight: "bold", fontSize: 14 }}>{DateToString(props.workEndDate * 1000)}</Text>
						<Text style={{ fontSize: 11 }}>{TimeToString(props.workEndDate * 1000)}</Text>
					</View>
				</View>
				<View>
					<Text style={styles.subHeaderText}>Criticality</Text>
					<View style={{ justifyContent: "center", flex: 1 }}>
						<Text style={{ fontWeight: "bold", fontSize: 15, textAlign: "center" }}>{props.isCritical ? "Yes" : `No`}</Text>
					</View>
				</View>
			</View>
			<View style={{ marginTop: "5%", marginBottom: "2%" }}>
				<FlatList
					horizontal={true}
					showsHorizontalScrollIndicator={false}
					data={props.attachments}
					keyExtractor={result => result}
					renderItem={({ item, index }) => (
						<Text style={{ marginHorizontal: 5, backgroundColor: Colors.GreyBackgroundDark, padding: 2, alignSelf: "center" }}>{item}</Text>
					)}
				/>
			</View>
			<Modal
				animationType="slide"
				transparent={true}
				visible={modalVisible}
				onRequestClose={() => {
					setModalVisible(!modalVisible)
				}}
			>
				<View style={styles.centeredView}>
					<View style={styles.modalView}>
						<TouchableHighlight style={{ marginVertical: 10, alignSelf: "flex-end" }} onPress={() => setModalVisible(false)}>
							<Text style={styles.buttonTextStyle}>CLOSE</Text>
						</TouchableHighlight>
						<View>
							<Text style={{ ...styles.subHeaderText, color: Colors.TextLight, marginTop: 10 }}>Work description</Text>
							<Text style={{ ...styles.workDescriptionText, color: Colors.TextLight }}>{props.description}</Text>
							<Text style={{ ...styles.subHeaderText, color: Colors.TextLight, marginTop: 10 }}>Work outcome</Text>
							<Text style={{ ...styles.workDescriptionText, color: Colors.TextLight }}>{props.outcome}</Text>
						</View>
					</View>
				</View>
			</Modal>
		</View>
	)
}

const styles = StyleSheet.create({
	workDetailsCard: {
		flex: 1,
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	workDescriptionText: {
		textAlign: "justify",
		padding: "2%"
	},
	subHeaderText: {
		//color: "rgba(0,0,0,0.5)",
		fontSize: 13,
		fontStyle: "italic"
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	modalView: {
		width: "90%",
		minHeight: "25%",
		maxHeight: "80%",
		backgroundColor: Colors.BGDark,
		borderRadius: 7,
		padding: 10
		//alignItems: "center"
	},
	buttonTextStyle: {
		color: Colors.TextLight,
		//fontSize: 12,
		fontWeight: "bold",
		textAlign: "center"
	}
})

export default WorkDescriptionCard
