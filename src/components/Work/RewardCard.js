import React, { useState } from "react"
import { View, StyleSheet, Text, Image, TouchableOpacity, Modal, ScrollView, Alert } from "react-native"
import { useDispatch, useSelector } from "react-redux"

import { IMAGE } from "../../constants/Image"
import Colors from "../../constants/Colors"

import * as rewardsActions from "../../store/actions/rewardsActions"

export const RewardCard = props => {
	const dispatch = useDispatch()
	const isLoading = useSelector(state => state.apiCallsInProgress > 0)
	// const ModelData = useSelector(state => state.rewardModels.ModelData)
	const lstModels = useSelector(state => state.rewardModels.lstModels)

	const [modelDetails, setModelDetails] = useState({})
	const [errorMessage, setErrorMessage] = useState({})
	const [modalVisible, setModalVisible] = useState(false)

	const openRewardsModel = modelId => {
		let isModelData = false
		if (lstModels && lstModels.length > 0 && modelId !== "") {
			const modelIndex = lstModels.findIndex(item => item.id === modelId)
			if (modelIndex > -1) {
				dispatch(rewardsActions.loadSelectedRewardModel(lstModels[modelIndex]))
				setModelDetails(lstModels[modelIndex])
				setModalVisible(true)
				isModelData = true
			}
		}

		if (modelId !== "" && !isModelData) {
			try {
				dispatch(rewardsActions.getReward(modelId))
					.then(data => {
						setModelDetails(data)
						setModalVisible(true)
						setErrorMessage({})
					})
					.catch(error => {
						Alert.alert("Rewards Model", "Sorry, Rewards Model data not found.", [{ text: "OK" }], { cancelable: false })
						// setErrorMessage({ onApiCall: error.message })
					})
			} catch (err) {
				Alert.alert("Rewards Model", "Sorry, Rewards Model data not found.", [{ text: "OK" }], { cancelable: false })
				// setErrorMessage({ onApiCall: err.message })
			}
		}
	}

	return (
		<View style={styles.rewardCard}>
			<View>
				<Text style={styles.subHeaderText}>Reward</Text>
				<View style={{ flexDirection: "row", marginTop: "15%", justifyContent: "space-around" }}>
					<Image style={{ ...styles.icon }} source={IMAGE["ICON_LIBER"]} resizeMode="contain" />
					<Text style={styles.rewardText}>{props.rewardUnits}</Text>
				</View>
				<View style={{ marginTop: "15%", alignItems: "center" }}>
					<TouchableOpacity onPress={() => openRewardsModel(props.rewardModel)}>
						<Text style={{ ...styles.subHeaderText, fontWeight: "500", padding: "2%", color: Colors.blue2 }}>Model - {props.rewardModel}</Text>
					</TouchableOpacity>
				</View>
			</View>
			<Modal
				animationType="slide"
				transparent={true}
				visible={modalVisible}
				onRequestClose={() => {
					setModalVisible(!modalVisible)
				}}
			>
				<View style={styles.centeredView} behavior={Platform.OS == "ios" ? "padding" : "height"}>
					<View style={styles.modalView}>
						<TouchableOpacity style={{ marginVertical: 10, alignSelf: "flex-end" }} onPress={() => setModalVisible(!modalVisible)}>
							<Text style={styles.buttonTextStyle}>CLOSE</Text>
						</TouchableOpacity>
						<ScrollView showsVerticalScrollIndicator={false}>
							{modelDetails.rules && modelDetails.rules.length > 0 && (
								<View>
									<View
										style={{
											flexDirection: "row",
											justifyContent: "space-around",
											marginVertical: 10,
											borderRadius: 5,
											backgroundColor: Colors.GreyBackgroundDark,
											padding: 4
										}}
									>
										<Text style={{ ...styles.textLight, fontWeight: "bold" }}>Reward Model</Text>
										<Text style={{ ...styles.textLight, fontWeight: "bold" }}>{modelDetails.id ? modelDetails.id : "NONE"}</Text>
									</View>
									{modelDetails.rules.map((rule, key) => {
										if (rule.action === "close") {
											return (
												<View
													key={rule.action}
													style={{ backgroundColor: Colors.GreyBackgroundDark, borderRadius: 5, padding: 5, marginVertical: 5 }}
												>
													<View style={{ flexDirection: "row", justifyContent: "space-around" }}>
														<Text style={styles.textLight}>{"ACTION: " + rule.action}</Text>
														<Text style={styles.textLight}>{"STAGE: " + rule.actionStage}</Text>
													</View>
													<View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 10, marginHorizontal: 20 }}>
														<Text style={styles.textLight}>rating</Text>
														<Text style={styles.textLight}>payout</Text>
													</View>
													{rule.conditions.map((condition, key) => {
														return (
															<View
																key={condition + key}
																style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 10, marginHorizontal: 20 }}
															>
																<Text style={styles.textLight}>{condition.rewardFactor}</Text>
																<Text style={styles.textLight}>{condition.workerShare + "%"}</Text>
															</View>
														)
													})}
												</View>
											)
										} else if (rule.action === "cancel") {
											return (
												<View
													key={rule.action}
													style={{ backgroundColor: Colors.GreyBackgroundDark, borderRadius: 5, padding: 5, marginVertical: 5 }}
												>
													<View style={{ flexDirection: "row", justifyContent: "space-around" }}>
														<Text style={styles.textLight}>{"ACTION: " + rule.action}</Text>
														<Text style={styles.textLight}>{"STAGE: " + rule.actionStage}</Text>
													</View>
													<View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 10, marginHorizontal: 20 }}>
														<Text style={styles.textLight}>range</Text>
														<Text style={styles.textLight}>payout</Text>
													</View>
													{rule.conditions.map((condition, key) => {
														return (
															<View
																key={condition + key}
																style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 10, marginHorizontal: 20 }}
															>
																<Text style={styles.textLight}>
																	{condition.factorValueFrom}% - {condition.factorValueTo}%
																</Text>
																<Text style={styles.textLight}>{condition.workerShare + "%"}</Text>
															</View>
														)
													})}
												</View>
											)
										}
									})}
								</View>
							)}
						</ScrollView>
					</View>
				</View>
			</Modal>
		</View>
	)
}

const styles = StyleSheet.create({
	rewardCard: {
		width: "30%",
		padding: "2%",
		marginHorizontal: "5%",
		marginTop: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	icon: {
		height: 23, //30
		width: 23,
		alignSelf: "center"
	},
	rewardText: {
		//marginTop: 8,
		fontSize: 20, //22
		alignSelf: "center"
	},
	subHeaderText: {
		//color: "rgba(0,0,0,0.5)",
		fontSize: 13,
		fontStyle: "italic"
	},
	modelText: {
		marginTop: "5%",
		alignSelf: "center"
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	modalView: {
		width: "90%",
		minHeight: "25%",
		maxHeight: "80%",
		backgroundColor: Colors.BGDark,
		borderRadius: 7,
		padding: 10
		//alignItems: "center"
	},
	buttonTextStyle: {
		color: Colors.TextLight,
		fontWeight: "bold",
		textAlign: "center"
	},
	textLight: {
		color: Colors.TextLight
	}
})

export default RewardCard
