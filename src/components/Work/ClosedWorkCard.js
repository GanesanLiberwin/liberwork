import React, { Component } from "react"
import { View, StyleSheet, Text } from "react-native"
import Colors from "../../constants/Colors"
import { DateToString } from "../../components/Utilities/DateFormat"

export default class ClosedWorkCard extends Component {
	render() {
		return (
			<View style={styles.workCard}>
				<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
					<View style={{ width: "90%" }}>
						<Text numberOfLines={1} style={styles.header}>
							{this.props.header}
						</Text>
					</View>
				</View>
				<View>
					<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
						<Text style={styles.itemText}>{this.props.status === "Published" ? "Bidding" : this.props.status}</Text>
						<Text style={styles.itemText}>{this.props.category}</Text>
						<Text style={styles.itemText}>{DateToString(this.props.date * 1000)}</Text>
					</View>
				</View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	workCard: {
		marginTop: 15,
		padding: 10,
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	header: {
		fontSize: 15,
		fontWeight: "400"
	},
	itemText: {
		fontSize: 12,
		fontStyle: "italic",
		marginTop: 5
	}
})
