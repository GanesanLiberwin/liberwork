import React from "react"
import { View, StyleSheet, Text } from "react-native"

import Colors from "../../constants/Colors"
import Rating from "../../components/Utilities/Ratings"

export const WorkClosureCard = props => {
	return (
		<View style={styles.workClosureCard}>
			<Text style={{ ...styles.subHeaderText, marginTop: "2%" }}>Work Closure</Text>
			<Rating
				editable={props.editable}
				rating={props.rating}
				numberOfStars={5}
				starColor={Colors.green1}
				rewardUnits={props.rewardUnits}
				rewardModel={props.rewardModel}
				rewardType={props.rewardType}
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	workClosureCard: {
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	subHeaderText: {
		//color: "rgba(0,0,0,0.5)",
		fontSize: 13,
		fontStyle: "italic"
	}
})

export default WorkClosureCard
