import React from "react"
import { View, StyleSheet, Text } from "react-native"
import Colors from "../../constants/Colors"

export const WorkTypeCard = props => {
	return (
		<View style={styles.workTypeCard}>
			<View style={{ flexDirection: "row", marginVertical: "2%" }}>
				<View style={{ width: "33%" }}>
					<Text style={styles.subHeaderText}>Perform by</Text>
					<View style={{ marginTop: "5%" }}>
						<Text style={{ fontWeight: "bold", fontSize: 15, textTransform: "capitalize" }}>
							{props.workForce} {props.isBeginner && `(B)`}
						</Text>
						<Text numberOfLines={1} style={{ fontSize: 13, textTransform: "capitalize" }}>
							{props.workSelection}
						</Text>
					</View>
				</View>
				<View style={{ width: "30%", marginLeft: "1%" }}>
					<Text style={styles.subHeaderText}>Work type</Text>
					<View style={{ marginTop: "5%" }}>
						<Text style={{ fontWeight: "bold", fontSize: 15, textTransform: "capitalize" }}>{props.workType}</Text>
						<Text numberOfLines={1} style={{ fontSize: 13 }}>
							{props.isRemote ? `Remote` : props.workLocation}
						</Text>
					</View>
				</View>
				<View style={{ width: "36%", marginHorizontal: "1%" }}>
					<Text style={styles.subHeaderText}>Category</Text>
					<View style={{ marginTop: "5%" }}>
						<Text numberOfLines={1} style={{ fontWeight: "bold", fontSize: 15 }}>
							{props.workCategory}
						</Text>
						<Text style={{ fontSize: 13 }}>Exp - {props.functionalExp !== "0" ? props.functionalExp + ` yrs` : `Any`}</Text>
					</View>
				</View>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	workTypeCard: {
		width: "90%",
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	subHeaderText: {
		//color: "rgba(0,0,0,0.5)",
		fontSize: 13,
		fontStyle: "italic"
	}
})

export default WorkTypeCard
