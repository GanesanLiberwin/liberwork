import React from "react"
import { View, StyleSheet, Text } from "react-native"
import Colors from "../../constants/Colors"
import { DateToString, TimeToString } from "../../components/Utilities/DateFormat"

export const StatusCard = props => {
	return (
		<View style={styles.statusCard}>
			<View style={{ flexDirection: "row", paddingVertical: "4%" }}>
				<Text style={{ ...styles.subHeaderText, width: "43%" }}>Status</Text>
				<Text style={{ fontWeight: "bold", fontSize: 13 }}>{props.status === "Published" ? "BIDDING" : props.status.toUpperCase()}</Text>
			</View>
			<View style={{ flexDirection: "row" }}>
				<Text style={{ ...styles.subHeaderText, width: "43%", alignSelf: "center" }}>{props.statusHead}</Text>
				<View style={{}}>
					<Text style={{ fontWeight: "bold", fontSize: 14 }}>{DateToString(props.statusDate * 1000)}</Text>
					<Text style={{ fontSize: 11 }}>{TimeToString(props.statusDate * 1000)}</Text>
				</View>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	statusCard: {
		width: "46%",
		alignSelf: "center",
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	subHeaderText: {
		//color: "rgba(0,0,0,0.5)",
		fontSize: 13,
		fontStyle: "italic"
	}
})

export default StatusCard
