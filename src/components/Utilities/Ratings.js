import React, { useState, useCallback } from "react"
import { useFocusEffect } from "@react-navigation/native"
import { View, StyleSheet, Text, TouchableWithoutFeedback, Animated, Easing } from "react-native"
import { useDispatch, useSelector } from "react-redux"

import Colors from "../../constants/Colors"
import RewardCard from "../../components/Work/RewardCard"
import Star from "./Star"
import { FeedbackQuestion, Feedback } from "../../constants/RatingFeedback"
import * as workFlowActions from "../../store/actions/workFlowActions"

export const Ratings = props => {
	const dispatch = useDispatch()

	const [rating, setRating] = useState(0)
	const [feedback, setFeedback] = useState([])
	const [feedbackQuestion, setFeedbackQuestion] = useState("")
	const [selectedList, setSelectedList] = useState([])
	const [animation, setAnimation] = useState(new Animated.Value(1))

	const Closure = useSelector(state => state.workFlow.Closure)

	useFocusEffect(
		useCallback(() => {
			setRating(props.rating)

			if (props.editable) {
				dispatch(workFlowActions.updatePosterRewards(parseInt(props.rewardUnits)))
			}

			return () => {
				setFeedback([])
				setFeedbackQuestion("")
			}
		}, [])
	)

	const rate = star => {
		if (props.editable) {
			setRating(star)
			setSelectedList([])

			switch (star) {
				case 1:
					setFeedbackQuestion(FeedbackQuestion[0])
					getFeedbackList(0)
					dispatch(workFlowActions.updatePosterRating(star, "Poor"))
					dispatch(workFlowActions.updatePosterRewards(Math.ceil(parseInt(props.rewardUnits) * 0.25)))
					break
				case 2:
					setFeedbackQuestion(FeedbackQuestion[1])
					getFeedbackList(1)
					dispatch(workFlowActions.updatePosterRating(star, "Fair"))
					dispatch(workFlowActions.updatePosterRewards(Math.ceil(parseInt(props.rewardUnits) * 0.5)))
					break
				case 3:
					setFeedbackQuestion(FeedbackQuestion[2])
					getFeedbackList(2)
					dispatch(workFlowActions.updatePosterRating(star, "Average"))
					dispatch(workFlowActions.updatePosterRewards(Math.ceil(parseInt(props.rewardUnits) * 0.75)))
					break
				case 4:
					setFeedbackQuestion(FeedbackQuestion[3])
					getFeedbackList(3)
					dispatch(workFlowActions.updatePosterRating(star, "Good"))
					dispatch(workFlowActions.updatePosterRewards(Math.ceil(parseInt(props.rewardUnits) * 1)))
					break
				case 5:
					setFeedbackQuestion(FeedbackQuestion[4])
					getFeedbackList(4)
					dispatch(workFlowActions.updatePosterRating(star, "Excellent"))
					dispatch(workFlowActions.updatePosterRewards(Math.ceil(parseInt(props.rewardUnits) * 1)))
					break

				default:
					break
			}

			animate()
		}
	}

	const getFeedbackList = index => {
		let newArray = []
		newArray = Feedback[index]
		setFeedback(newArray)
	}

	const animate = () => {
		Animated.timing(animation, {
			toValue: 2,
			duration: 400,
			easing: Easing.ease,
			useNativeDriver: true
		}).start(() => {
			animation.setValue(1)
		})
	}

	const animateScale = animation.interpolate({
		inputRange: [1, 1.5, 2],
		outputRange: [1, 1.4, 1]
	})

	const animateOpacity = animation.interpolate({
		inputRange: [1, 1.2, 2],
		outputRange: [1, 0.5, 1]
	})

	const animateWobble = animation.interpolate({
		inputRange: [1, 1.25, 1.75, 2],
		outputRange: ["0deg", "-3deg", "3deg", "0deg"]
	})

	const animationStyle = {
		transform: [{ scale: animateScale }, { rotate: animateWobble }],
		opacity: animateOpacity
	}

	let stars = []

	for (let x = 1; x <= (props.numberOfStars ?? 5); x++) {
		stars.push(
			<TouchableWithoutFeedback key={x} onPress={() => rate(x)}>
				<Animated.View style={x <= rating ? animationStyle : ""}>
					<Star filled={x <= rating ? true : false} color={props.starColor ?? Colors.blue2} />
				</Animated.View>
			</TouchableWithoutFeedback>
		)
	}

	const selectItem = item => {
		const index = selectedList.findIndex(s => s === item)

		if (index < 0) {
			const newArray = selectedList.concat(item)
			setSelectedList(newArray)
			dispatch(workFlowActions.updatePosterFeedback(newArray))
		} else {
			const newArray = selectedList.filter(s => s != item)
			setSelectedList(newArray)
			dispatch(workFlowActions.updatePosterFeedback(newArray))
		}
	}

	const determineColor = item => {
		if (selectedList.some(s => s === item)) {
			return Colors.green1
		} else {
			return Colors.GreyInactive
		}
	}

	return (
		<View>
			<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
				<View style={{ justifyContent: "center", alignItems: "center" }}>
					<View style={{ flexDirection: "row", marginVertical: "5%", alignItems: "center" }}>{stars}</View>
					<Text style={{ fontSize: 15, fontStyle: "italic" }}>
						{Closure.posterRating != 0 && Closure.posterRating} {Closure.posterRatingText}
					</Text>
				</View>
				<RewardCard rewardUnits={Closure.rewardUnitsFinal} rewardModel={props.rewardModel} rewardType={props.rewardType} />
			</View>
			<Text style={{ alignSelf: "center", marginTop: 15 }}>{feedbackQuestion}</Text>
			<View style={styles.itemList}>
				{feedback.map((item, index) => {
					return (
						<TouchableWithoutFeedback key={index} onPress={() => selectItem(item)}>
							<View style={{ margin: 7, borderRadius: 5, backgroundColor: determineColor(item) }}>
								<Text style={{ padding: 3 }}>{item}</Text>
							</View>
						</TouchableWithoutFeedback>
					)
				})}
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	itemList: {
		flexDirection: "row",
		justifyContent: "center",
		margin: 15,
		flexWrap: "wrap"
	}
})

export default Ratings
