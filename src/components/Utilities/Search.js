import React, { useState } from "react"
import { StyleSheet, Text, View, TouchableOpacity, TextInput, ActivityIndicator, Keyboard } from "react-native"
import { useDispatch, useSelector } from "react-redux"

import Colors from "../../constants/Colors"
import { LinearGradient } from "expo-linear-gradient"
import { MaterialIcons, MaterialCommunityIcons } from "@expo/vector-icons"

import * as searchActions from "../../store/actions/searchActions"
import * as workToSkillActions from "../../store/actions/workToSkillActions"
import * as workFlowActions from "../../store/actions/workFlowActions"

export const Search = props => {
	const dispatch = useDispatch()

	const account = useSelector(state => state.account.accountDetails)
	const Skills = useSelector(state => state.search.lstFetchSkills)
	const workDraft = useSelector(state => state.workFlow.workDraft)

	const [serverText, setServerText] = useState("")
	const [filteredList, setFilteredList] = useState([])
	const [selectedList, setSelectedList] = useState(props.selectedList ?? [])
	const [searchText, setSearchText] = useState("")

	const isLoading = useSelector(state => state.apiCallsInProgress > 0)
	const [errorMessage, setErrorMessage] = useState({})

	const searchList = async () => {
		if (serverText === searchText.trim()) {
			return
		}
		setServerText(searchText.trim())

		if (searchText.trim().length > 0) {
			try {
				setErrorMessage({})
				dispatch(searchActions.fetchSkills(searchText.trim()))
					.then(searchedSkills => {
						if (searchedSkills && searchedSkills.length > 0) {
							setErrorMessage({})
							setFilteredList(searchedSkills)
						} else {
							setErrorMessage({ onApiCall: "No skills found for this search" })
						}
					})
					.catch(error => {
						setErrorMessage({ onApiCall: error.message })
					})
			} catch (err) {
				setErrorMessage({ onApiCall: err.message })
			}
		} else {
			setErrorMessage({ onApiCall: "Please enter search skills" })
		}

		Keyboard.dismiss()
	}

	const onUpdateRequired = index => {
		if (props.numberOfSelections > 1) {
			let newArray = [...selectedList]
			newArray[index].required = !newArray[index].required

			setAndDispatchList(newArray)
		}
	}

	const onRemoveItem = selectedItem => {
		const newArray = selectedList.filter(item => selectedItem != item.skill)

		setAndDispatchList(newArray)
	}

	const onSelectItem = selectedItem => {
		if (selectedList.length < props.numberOfSelections && !selectedList.some(s => s.skill === selectedItem.skillName)) {
			const newArray = [
				...selectedList,
				{
					skill: selectedItem.skillName,
					id: selectedItem.skillId,
					required: true
				}
			]

			setAndDispatchList(newArray)
		}
	}

	const setAndDispatchList = list => {
		list.sort(x => (x.required ? -1 : 1))
		setSelectedList(list)
		dispatch(workFlowActions.updateWorkDraftFields("profileCount", ""))
		dispatch(searchActions.loadSelectedSkills(list))
	}

	const searchFilter = text => {
		setErrorMessage({})
		setSearchText(text)

		if (text.trim().length > 0 && Skills.length > 0) {
			const mapSkills = Skills.filter(s => s.skillName.toLowerCase().startsWith(text.trim().toLowerCase()))
			setFilteredList(mapSkills)
		} else {
			setFilteredList([])
		}
	}

	const clearText = () => {
		setSearchText("")
		setFilteredList([])
	}

	const onProfileSkillCount = () => {
		try {
			let payload = { accountId: account.id, orgId: account.orgId, skills: selectedList, count: true }
			dispatch(workToSkillActions.checkMatchingCount(payload))
				.then(count => {
					dispatch(workFlowActions.updateWorkDraftFields("profileCount", count.toString()))
				})
				.catch(error => {})
		} catch (err) {}
	}

	return (
		<View>
			<Text style={{ ...styles.textLight, marginTop: 10, marginHorizontal: "5%" }}>
				Search and Add {props.searchType} (max {props.numberOfSelections})
			</Text>
			<View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginTop: 10, width: "90%" }}>
				<View
					style={{
						flexDirection: "row",
						justifyContent: "space-between",
						borderWidth: 1,
						borderRadius: 5,
						borderColor: Colors.GreyBackgroundDark,
						width: "70%"
					}}
				>
					<TextInput
						style={{ fontSize: 15, alignSelf: "center", padding: 4, color: Colors.TextLight }}
						value={searchText}
						onChangeText={text => searchFilter(text)}
						placeholder="pls type first few chars"
						placeholderTextColor="rgba(192,192,192, 0.5)"
						selectionColor="rgba(192,192,192, 1)"
						editable={selectedList.length < props.numberOfSelections}
						allowFontScaling={false}
						autoCapitalize="none"
						autoCorrect={false}
					/>
					{searchText != "" && (
						<TouchableOpacity onPress={() => clearText()}>
							<MaterialIcons name="clear" size={18} color={Colors.TextLight} style={{ padding: 4 }} />
						</TouchableOpacity>
					)}
				</View>
				{isLoading ? (
					<ActivityIndicator size="small" color={Colors.TextLight} />
				) : (
					<View style={{ borderRadius: 5, backgroundColor: Colors.blue2 }}>
						<TouchableOpacity onPress={() => searchList()}>
							<Text style={{ fontWeight: "bold", color: "#fff", padding: 4 }}>SEARCH</Text>
						</TouchableOpacity>
					</View>
				)}
			</View>
			{errorMessage.onApiCall ? (
				<View style={{ marginTop: 10, alignSelf: "center" }}>
					<Text style={{ color: Colors.white, fontSize: 12, fontStyle: "italic" }}>{errorMessage.onApiCall}</Text>
				</View>
			) : null}
			<View style={styles.itemList}>
				{filteredList.map((item, index) => {
					return (
						<TouchableOpacity key={item + index} onPress={() => onSelectItem(item)}>
							<Text
								style={{
									margin: 7,
									fontSize: 12,
									color: Colors.TextLight,
									borderWidth: 1,
									borderRadius: 5,
									borderColor: Colors.TextLight,
									padding: 2
								}}
							>
								{item.skillName}
							</Text>
						</TouchableOpacity>
					)
				})}
			</View>
			<View style={{ marginTop: 20, width: "90%", alignSelf: "center" }}>
				{selectedList.map((item, index) => {
					return (
						<View key={item.skill + index} style={{ flexDirection: "row" }}>
							<TouchableOpacity onPress={() => onRemoveItem(item.skill)} style={{ margin: 7 }}>
								<MaterialIcons name="remove-circle" size={27} color={Colors.orange1} />
							</TouchableOpacity>
							<View style={{ margin: 7, borderRadius: 5, backgroundColor: Colors.GreyBackgroundDark, width: "55%" }}>
								<Text style={{ fontSize: 15, color: Colors.TextLight, paddingVertical: 4, paddingHorizontal: 6 }}>{item.skill}</Text>
							</View>
							{props.preferenceSelection && (
								<TouchableOpacity
									onPress={() => onUpdateRequired(index)}
									style={{ margin: 7, borderRadius: 5, backgroundColor: Colors.TextLight, width: "20%" }}
								>
									<Text style={{ fontSize: 13, paddingVertical: 5, alignSelf: "center" }}>{item.required ? "MUST" : "NICE"}</Text>
								</TouchableOpacity>
							)}
						</View>
					)
				})}
			</View>
			{props.preferenceSelection && selectedList.length > 0 && (
				<View style={{ flexDirection: "row", marginTop: 20, alignItems: "center", alignSelf: "center" }}>
					<TouchableOpacity onPress={() => onProfileSkillCount()}>
						<LinearGradient style={styles.linearGradient} colors={["rgba(255, 255, 255, 1)", "rgba(255, 255, 255, 0.05)"]}>
							<MaterialCommunityIcons name="target-account" style={styles.iconCountStyle} />
						</LinearGradient>
					</TouchableOpacity>
					{workDraft.profileCount != "" && (
						<Text style={{ ...styles.textLight, fontStyle: "italic", fontWeight: "500", fontSize: 16, marginLeft: 10 }}>
							{workDraft.profileCount + "  profile(s)"}
						</Text>
					)}
				</View>
			)}
		</View>
	)
}

const styles = StyleSheet.create({
	itemList: {
		flexDirection: "row",
		justifyContent: "center",
		margin: 15,
		flexWrap: "wrap"
	},
	linearGradient: {
		height: 34,
		width: 34,
		borderRadius: 17,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "center"
	},
	iconCountStyle: {
		fontSize: 25,
		color: "rgba(0,0,0,0.8)"
	},
	textLight: {
		color: Colors.TextLight
	}
})

export default Search
