import React from "react"
import { View, Text, StyleSheet } from "react-native"
import Colors from "../../constants/Colors"
import Icons from "../Icons"
import { lstWorkTabs } from "../../constants/PageJson"

const WorkTabsList = props => {
	return (
		<View style={{ marginHorizontal: "3%", marginTop: 10 }}>
			<View style={styles.tabRowContainer}>
				{lstWorkTabs.map((item, key) => {
					const isActive = item.id === props.currTab ? true : false
					const itemColor = item.id < props.progressTab ? Colors.green1 : item.id === props.progressTab ? Colors.blue2 : Colors.grey

					return (
						<View key={item.id} style={{ width: "19%" }}>
							<View style={{ backgroundColor: itemColor, alignItems: "center", borderRadius: 4 }}>
								<Text style={{ padding: 2, fontSize: 11, color: "#fff", fontStyle: "italic" }}>{item.name}</Text>
							</View>
							{isActive && (
								<View style={styles.currentTabContainer}>
									<Icons
										name={"triangle"}
										iconType="MaterialCommunityIcons"
										color={itemColor}
										size={8}
										style={{ transform: [{ rotate: "180deg" }] }}
									/>
								</View>
							)}
						</View>
					)
				})}
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	tabRowContainer: {
		flexDirection: "row",
		justifyContent: "space-evenly"
	},
	currentTabContainer: {
		alignSelf: "center",
		marginTop: -2
	}
})

export default WorkTabsList
