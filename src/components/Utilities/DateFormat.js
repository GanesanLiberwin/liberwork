import {
	format,
	parseJSON,
	formatDistanceToNow,
	formatDistanceToNowStrict,
	differenceInSeconds,
	differenceInMinutes,
	differenceInYears,
	fromUnixTime,
	getUnixTime,
	toDate,
	isToday,
	isTomorrow,
	isPast
} from "date-fns"

const dateToString = date => {
	return format(date, "do MMM yy")
}

const dateToString2 = date => {
	return format(date, "do MMM yyyy")
}

const timeToString = date => {
	return format(date, "p")
}

const datetimeToString = date => {
	return format(date, "do MMM yy  p")
}

const dateToDBString = date => {
	return format(date, "yyyy-MM-dd")
}

export const DateToString = inputDate => dateToString(inputDate)

export const DateToString2 = inputDate => dateToString2(inputDate)

export const TimeToString = inputDate => timeToString(inputDate)

export const DateTimeToString = inputDate => datetimeToString(inputDate)

export const DateToDBString = inputDate => dateToDBString(inputDate)

export const ISOStringToDate = inputString => parseJSON(inputString)

export const DistanceToNowStrict = inputDate => {
	if (isToday(inputDate)) {
		return "today"
	} else if (isTomorrow(inputDate)) {
		return "tomorrow"
	} else if (isPast(inputDate)) {
		return "work due"
	}
	return formatDistanceToNowStrict(inputDate, {
		addSuffix: false
	})
}

export const DistanceToNow = inputDate => {
	if (isToday(inputDate)) {
		return "today"
	} else if (isTomorrow(inputDate)) {
		return "tomorrow"
	} else if (isPast(inputDate)) {
		return "past due"
	}
	return formatDistanceToNow(inputDate, {
		addSuffix: false
	})
}

export const TimeToString2 = inputDate => {
	if (isToday(inputDate) || isTomorrow(inputDate)) {
		return timeToString(inputDate)
	}
	return ""
}

export const ToDate = inputDate => toDate(inputDate)

export const FromUnixTime = inputDate => fromUnixTime(inputDate)

export const GetUnixTime = inputDate => getUnixTime(inputDate)

export const DifferenceInSeconds = (date1, date2) => differenceInSeconds(date1, date2)

export const DifferenceInMinutes = (date1, date2) => differenceInMinutes(date1, date2)

export const DifferenceInYears = (date1, date2) => differenceInYears(date1, date2)
