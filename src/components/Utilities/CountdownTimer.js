import React, { useState, useCallback } from "react"
import { GetUnixTime } from "./DateFormat"
import { useFocusEffect } from "@react-navigation/native"

export const CountDownTimer = props => {
	const [timerText, setTimerText] = useState("CHECKING....")

	useFocusEffect(
		useCallback(() => {
			const counter = setTimeout(() => updateCountdown(), 1000)
			return () => {
				clearTimeout(counter)
			}
		}, [timerText])
	)

	const updateCountdown = () => {
		const currentTime = GetUnixTime(new Date())
		const distanceToDate = props.bidDate - currentTime
		//console.log(distanceToDate)

		if (distanceToDate > 0) {
			let days = Math.floor(distanceToDate / (60 * 60 * 24))
			let hours = Math.floor((distanceToDate / (60 * 60)) % 24)
			let minutes = Math.floor((distanceToDate / 60) % 60)
			let seconds = Math.floor(distanceToDate % 60)

			let hms = hours.toString().padStart(2, "0") + " : " + minutes.toString().padStart(2, "0") + " : " + seconds.toString().padStart(2, "0")

			if (days === 0) {
				setTimerText("ENDS   " + hms)
			} else {
				setTimerText("ENDS   " + days + "d  " + hms)
			}
		} else {
			setTimerText("BID PERIOD OVER")
		}
	}

	return timerText
}

export default CountDownTimer
