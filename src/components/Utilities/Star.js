import React from "react"
import { FontAwesome } from "@expo/vector-icons"

export const Star = props => {
	return <FontAwesome name={props.filled === true ? "star" : "star-o"} color={props.color} size={32} style={{ marginHorizontal: 6 }} />
}

export default Star
