import { workStatusJson } from "../../constants/PageJson"
import { TimeToString2, DistanceToNowStrict } from "./DateFormat"
import Colors from "../../constants/Colors"

export const getWorkRole = (AcctId, PosterAcctId, DelegateAcctId, Permission) => {
	let workRole = ""
	if (AcctId === PosterAcctId) {
		workRole = "Poster"
	} else if (AcctId === DelegateAcctId) {
		if (Permission === "owner") {
			workRole = "Del-O"
		} else if (Permission === "edit") {
			workRole = "Del-E"
		} else if (Permission === "read") {
			workRole = "Del-R"
		}
	} else {
		workRole = "Worker"
	}
	return workRole
}

export const getWorkCardDetails = selCard => {
	let cardInfo = {
		header: "",
		subHeader: "",
		body: "",
		subBody: "",
		status: "",
		icon: "ICON_LIBER",
		iconStatus: false,
		critical: false,
		delegate: false,
		message: false,
		gradientBase: Colors.BlueCard
	}

	try {
		if (selCard) {
			if (selCard.functionalDomain) {
				cardInfo.header = selCard.functionalDomain
			}

			if (selCard.criticalWork) {
				cardInfo.critical = true
			}
			if (selCard.delegateWork) {
				cardInfo.delegate = true
			}
			if (selCard.message) {
				cardInfo.message = true
			}

			if (selCard.skills && selCard.skills.length > 0) {
				try {
					if (selCard.currentStatus === workStatusJson.Draft || selCard.currentStatus === workStatusJson.DraftNew) {
						cardInfo.subHeader = selCard.skills[0].skill
					} else {
						cardInfo.subHeader = selCard.skills[0]
					}
				} catch (err) {
					console.log("Error on parsing skills => ", err)
				}
			}

			cardInfo.status = selCard.currentStatus === workStatusJson.Published ? "Bidding" : selCard.currentStatus

			if (selCard.icon) {
				cardInfo.icon = selCard.icon
			}

			if (selCard.allocationStatus && selCard.allocationStatus === workStatusJson.Published) {
				cardInfo.iconStatus = true
			}

			//body - rewardUnits / coming 😄 / bye ✋🏼 / biddingEndDateTime / plannedEndDate
			if (selCard.allocationStatus && selCard.allocationStatus === workStatusJson.Published) {
				cardInfo.body = selCard.rewardUnits
			} else if (selCard.currentStatus === workStatusJson.Draft || selCard.currentStatus === workStatusJson.DraftNew) {
				cardInfo.body = "coming 😄"
			} else if (
				selCard.currentStatus === workStatusJson.Cancelling ||
				selCard.currentStatus === workStatusJson.OptingOut ||
				selCard.currentStatus === workStatusJson.Completed
			) {
				cardInfo.body = "bye ✋🏼"
			} else if (
				//selCard.currentStatus === workStatusJson.Posted ||
				selCard.currentStatus === workStatusJson.Published ||
				selCard.currentStatus === workStatusJson.BidEnded
			) {
				cardInfo.body = DistanceToNowStrict(selCard.biddingEndDateTime * 1000)
			} else {
				cardInfo.body = DistanceToNowStrict(selCard.plannedEndDate * 1000)
			}
			//subBody => / biddingEndDateTime / plannedEndDate
			if (selCard.allocationStatus && selCard.allocationStatus === workStatusJson.Published) {
				cardInfo.subBody = ""
			} else if (
				selCard.currentStatus === workStatusJson.Draft ||
				selCard.currentStatus === workStatusJson.DraftNew ||
				selCard.currentStatus === workStatusJson.Cancelling ||
				selCard.currentStatus === workStatusJson.OptingOut
			) {
				cardInfo.subBody = ""
			} else if (selCard.currentStatus === workStatusJson.Completed) {
				cardInfo.subBody = TimeToString2(selCard.plannedEndDate * 1000)
			} else if (
				//selCard.currentStatus === workStatusJson.Posted ||
				selCard.currentStatus === workStatusJson.Published ||
				selCard.currentStatus === workStatusJson.BidEnded
			) {
				cardInfo.subBody = TimeToString2(selCard.biddingEndDateTime * 1000) + "  Bid"
			} else {
				cardInfo.subBody = TimeToString2(selCard.plannedEndDate * 1000)
			}

			//gradientBase
			if (selCard.allocationStatus && selCard.allocationStatus === workStatusJson.Published) {
				cardInfo.gradientBase = Colors.BrownCard
			} else if (
				selCard.currentStatus === workStatusJson.Draft ||
				selCard.currentStatus === workStatusJson.DraftNew ||
				selCard.currentStatus === workStatusJson.Cancelling ||
				selCard.currentStatus === workStatusJson.OptingOut
			) {
				cardInfo.gradientBase = Colors.GreyBackground
			} else if (selCard.currentStatus === workStatusJson.Rework || selCard.currentStatus === workStatusJson.Resubmitted) {
				cardInfo.gradientBase = Colors.OrangeCard
			} else if (selCard.currentStatus === workStatusJson.Submitted || selCard.currentStatus === workStatusJson.Completed) {
				cardInfo.gradientBase = Colors.GreenCard
			} else {
				cardInfo.gradientBase = Colors.BlueCard
			}
		}
		return cardInfo
	} catch (err) {
		return cardInfo
	}
}
