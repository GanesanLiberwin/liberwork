import React, { useState } from "react"
import { View, StyleSheet, Text, TextInput, TouchableOpacity, Keyboard, ActivityIndicator } from "react-native"
import { useDispatch, useSelector } from "react-redux"

import Colors from "../../constants/Colors"
import Regex from "../../constants/Regex"

import * as accountActions from "../../store/actions/accountActions"

export const AccountValidation = props => {
	const dispatch = useDispatch()
	const isLoading = useSelector(state => state.apiCallsInProgress > 0)
	const account = useSelector(state => state.account.accountDetails)

	const verifiedAccountDetails = useSelector(state => state.account.accountVerified.accDetails)
	const [accountEmail, setAccountEmail] = useState(props.accountEmail)

	const [verifyStatus, setVerifyStatus] = useState("")

	const onEmailChange = text => {
		setAccountEmail(text)
		setVerifyStatus("")
	}

	const onVerifyAccount = async () => {
		Keyboard.dismiss()

		if (Regex.email.test(accountEmail)) {
			if (accountEmail === account.accountEmail) {
				setVerifyStatus("The email id should be different from yours")
				return
			} else if (accountEmail === verifiedAccountDetails.accountEmail) {
				setVerifyStatus("The email id is already verified") //check the message
				return
			}
		} else {
			setVerifyStatus("Please enter a valid email id")
			return
		}

		try {
			dispatch(accountActions.isValidDomainEmail(accountEmail, account.orgId))
				.then(data => {
					if (data.isValid && data.accounts && data.accounts.length > 0) {
						const account = data.accounts[0]
						setVerifyStatus(`The email belongs to ${account.firstName} ${account.lastName}`)
						dispatch(accountActions.createAccountVerifiedSuccess(data))
					} else {
						setVerifyStatus("There seems to be a problem with email")
					}
				})
				.catch(error => {
					setVerifyStatus("There seems to be a problem with email")
				})
		} catch (err) {
			setVerifyStatus("There seems to be a problem with email")
		}
	}

	return (
		<View style={styles.containerStyle}>
			<View
				style={{
					flexDirection: "row",
					justifyContent: "space-between",
					alignItems: "center",
					width: "100%"
				}}
			>
				<View style={{ width: "70%" }}>
					<TextInput
						style={{
							fontSize: 15,
							color: props.dark && Colors.TextLight,
							padding: 4,

							borderColor: Colors.GreyBackgroundDark,
							borderWidth: 1,
							borderRadius: 5
						}}
						value={accountEmail}
						onChangeText={text => onEmailChange(text)}
						placeholder="enter account email to verify"
						placeholderTextColor={props.dark ? "rgba(192,192,192, 0.5)" : "rgba(0,0,0, 0.5)"}
						editable={true}
						allowFontScaling={false}
						autoCapitalize="none"
						autoCorrect={false}
						keyboardType="email-address"
					/>
				</View>
				<View style={{ width: "30%" }}>
					{isLoading ? (
						<ActivityIndicator size="small" color={props.dark ? "rgba(220,220,220, 1)" : "rgba(0,0,0, 0.5)"} />
					) : (
						<TouchableOpacity
							style={{ marginLeft: 20, borderRadius: 5, padding: 4, backgroundColor: Colors.blue2 }}
							onPress={() => onVerifyAccount()}
						>
							<Text style={{ ...styles.buttonTextStyle, color: "#fff" }}>VERIFY</Text>
						</TouchableOpacity>
					)}
				</View>
			</View>
			<Text style={{ marginTop: 10, fontSize: 12, fontStyle: "italic", color: props.dark && Colors.TextLight, alignSelf: "flex-end" }}>
				{verifyStatus}
			</Text>
		</View>
	)
}

const styles = StyleSheet.create({
	containerStyle: {
		margin: 15
	},
	buttonTextStyle: {
		fontWeight: "bold",
		textAlign: "center"
	}
})

export default AccountValidation
