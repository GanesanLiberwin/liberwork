import React, { useState } from "react"
import { View, StyleSheet, Text, TextInput, Image, TouchableOpacity, Keyboard } from "react-native"
import { useDispatch, useSelector } from "react-redux"

import { IMAGE } from "../../constants/Image"
import Colors from "../../constants/Colors"
import * as walletActions from "../../store/actions/walletActions"

export const WalletTransferCard = props => {
  const dispatch = useDispatch()

  const myAccount = useSelector(state => state.account.accountDetails)
  const toAccount = useSelector(state => state.account.accountVerified.accDetails)

  const [transferValue, setTransferValue] = useState("")
  const [transferStatus, setTransferStatus] = useState("")
  const [errorMessage, setErrorMessage] = useState({})

  const onValueChange = text => {
    //check respective wallet balance - props.type
    setTransferValue(text)
  }

  const onSend = async () => {
    //wallet transfer api - props.type ="business"
    Keyboard.dismiss()
    if (transferValue > 0) {
      try {
        setErrorMessage({})

        dispatch(
          walletActions.walletTransfer(
            myAccount.orgId,
            myAccount.id,
            toAccount.id,
            myAccount.firstName + " " + myAccount.lastName,
            toAccount.firstName + " " + toAccount.lastName,
            transferValue,
            "points",
            props.type
          )
        )
          .then(() => {
            setTransferStatus("Transfer Successful")
            setTransferValue("")
          })
          .catch(error => {
            //capture error
            setErrorMessage({ onApiCall: error.message })
          })
      } catch (err) {
        //capture error
        setErrorMessage({ onApiCall: err.message })
      }
    }
  }

  return (
    <View style={styles.transferCard}>
      <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-end", width: "100%" }}>
        <Image style={styles.icon} source={IMAGE["ICON_LIBER"]} resizeMode="contain" />
        <TextInput
          style={{
            width: "25%",
            marginLeft: 5,
            fontSize: 15,
            padding: 4,
            borderColor: Colors.GreyBackgroundDark,
            borderWidth: 1,
            borderRadius: 5
          }}
          value={transferValue}
          onChangeText={text => onValueChange(text)}
          placeholder="value"
          placeholderTextColor="rgba(0,0,0, 0.5)"
          editable={true}
          allowFontScaling={false}
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType="decimal-pad"
          returnKeyType="done"
        />
        <TouchableOpacity
          style={{ width: "20%", marginLeft: 20, borderRadius: 5, padding: 4, backgroundColor: Colors.blue2 }}
          onPress={() => onSend()}
        >
          <Text style={{ ...styles.buttonTextStyle, color: "#fff" }}>SEND</Text>
        </TouchableOpacity>
      </View>
      <Text style={{ marginTop: 10, fontSize: 12, fontStyle: "italic", alignSelf: "flex-end" }}>{transferStatus}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  transferCard: {
    marginHorizontal: 15
    //borderRadius: 7
    //backgroundColor: Colors.GreyBackground
  },
  headerText: {
    //color: "rgba(0,0,0,0.5)",
    fontSize: 14,
    fontStyle: "italic"
  },
  buttonTextStyle: {
    fontWeight: "bold",
    textAlign: "center"
  },
  icon: {
    height: 23,
    width: 23
  }
})

export default WalletTransferCard
