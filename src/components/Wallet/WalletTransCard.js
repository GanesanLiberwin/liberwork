import React, { Component } from "react"
import { View, StyleSheet, Text } from "react-native"
import { DateTimeToString } from "../../components/Utilities/DateFormat"
import Colors from "../../constants/Colors"

export default class WalletTransCard extends Component {
	render() {
		return (
			<View style={styles.walletCard}>
				<View>
					<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
						<View style={{ width: "80%" }}>
							<Text numberOfLines={1} style={styles.firstLineText}>
								{this.props.type} for {this.props.reason}
							</Text>
						</View>
						<View style={{ width: "20%", alignItems: "flex-end" }}>
							<Text
								style={{
									...styles.firstLineText,
									color: this.props.type != "Reserve" ? (this.props.transAmount < 0 ? "red" : "green") : Colors.black
								}}
							>
								{Math.abs(this.props.transAmount)}
							</Text>
						</View>
					</View>
				</View>
				<View>
					<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
						{/* <Text style={styles.secondLineText}>{DateTimeToString(FromUnixTime(this.props.date))}</Text> */}
						<Text style={styles.secondLineText}>
							{DateTimeToString(this.props.date > 1606307796 ? this.props.date / 1000000 : this.props.date * 1000)}
						</Text>
						<Text style={styles.secondLineText}>{this.props.balance}</Text>
					</View>
				</View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	walletCard: {
		marginTop: 15,
		padding: 10,
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	firstLineText: {
		fontSize: 15,
		fontWeight: "400"
	},
	secondLineText: {
		fontSize: 12,
		fontStyle: "italic",
		marginTop: 5
	}
})
