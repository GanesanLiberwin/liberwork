import React, { Component } from "react"
import { View, StyleSheet, Text, Image } from "react-native"
import { IMAGE } from "../../constants/Image"
import Colors from "../../constants/Colors"

export default class WalletCard extends Component {
	render() {
		return (
			<View style={styles.walletCard}>
				<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
					<View style={{ width: "50%" }}>
						<Text style={styles.headerText}>My Balance</Text>
						<View style={{ flexDirection: "row", marginTop: 10 }}>
							<Image style={styles.icon} source={IMAGE["ICON_LIBER"]} resizeMode="contain" />
							<Text style={styles.balanceText}>{this.props.curBalance}</Text>
						</View>
					</View>
					<View style={{ width: "40%", flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
						<View>
							<Text style={styles.subHeaderText}>{this.props.subHeader1}</Text>
							<Text style={styles.reserveText}>{this.props.resBalance}</Text>
						</View>
						<View>
							<Text style={styles.subHeaderText}>{this.props.subHeader2}</Text>
							<Text style={styles.reserveText}>{this.props.curBalance + this.props.resBalance}</Text>
						</View>
					</View>
				</View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	walletCard: {
		padding: 20,
		//width: "90%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	headerText: {
		color: "rgba(0,0,0,0.5)",
		fontSize: 14,
		fontStyle: "italic"
	},
	subHeaderText: {
		color: "rgba(0,0,0,0.5)",
		fontSize: 12,
		fontStyle: "italic"
	},
	balanceText: {
		fontSize: 30,
		marginLeft: 15,
		alignSelf: "center"
	},
	reserveText: {
		marginTop: 7,
		marginLeft: 7,
		fontSize: 16
	},
	icon: {
		height: 46,
		width: 46
	}
})
