import React from "react"
import { View, StyleSheet, Text, TextInput, KeyboardAvoidingView } from "react-native"
import { useDispatch, useSelector } from "react-redux"
import Colors from "../../constants/Colors"

import * as workFlowActions from "../../store/actions/workFlowActions"

export const WorkDescription = props => {
	const dispatch = useDispatch()
	const workDetails = useSelector(state => state.workFlow.workDetails)

	const onTextChange = text => {
		dispatch(workFlowActions.updateWorkFlowFields(props.type, text))

		const valid = text === "" ? false : true

		if (props.type === "description") {
			dispatch(workFlowActions.updateWorkDraftFields("descriptionValid", valid))
		} else {
			dispatch(workFlowActions.updateWorkDraftFields("outcomeValid", valid))
		}
	}
	return (
		<KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : "height"}>
			<View style={styles.container}>
				<View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: "2%" }}>
					<Text style={{ ...styles.subHeaderText }}>
						{props.type === "description" ? "Please describe your work" : "Please specify the work outcome"}
					</Text>
				</View>
				<TextInput
					style={{ ...styles.workDescriptionText, marginVertical: "3%", backgroundColor: Colors.white, borderRadius: 5 }}
					allowFontScaling={false}
					autoCapitalize="none"
					autoCorrect={false}
					textAlignVertical="top"
					placeholder={props.type === "description" ? "please describe your work" : "please specify the work deliverables "}
					placeholderTextColor="rgba(0,0,0,0.25)"
					maxLength={250}
					multiline={true}
					value={props.type === "description" ? workDetails.description : workDetails.outcome}
					onChangeText={text => onTextChange(text)}
				></TextInput>
			</View>
		</KeyboardAvoidingView>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	workDescriptionText: {
		textAlign: "justify",
		padding: "2%"
	},
	subHeaderText: {
		//color: "rgba(0,0,0,0.5)",
		fontSize: 13,
		fontStyle: "italic"
	}
})

export default WorkDescription
