import React, { useState, useCallback } from "react"
import { View, StyleSheet, Text, TouchableOpacity } from "react-native"
import { useDispatch, useSelector } from "react-redux"
import { useFocusEffect } from "@react-navigation/native"
import { addDays, differenceInDays, differenceInCalendarDays } from "date-fns"

import { GetUnixTime } from "../Utilities/DateFormat"
import CustomDatePicker from "../../components/Picker/CustomDatePicker"
import { LinearGradient } from "expo-linear-gradient"
import Colors from "../../constants/Colors"

import * as workFlowActions from "../../store/actions/workFlowActions"

export const WorkSchedule = props => {
	const dispatch = useDispatch()
	const workDetails = useSelector(state => state.workFlow.workDetails)
	const workDraft = useSelector(state => state.workFlow.workDraft)

	useFocusEffect(
		useCallback(() => {
			if (workDetails.plannedStartDate === 0 || workDetails.plannedEndDate === 0) {
				const currentDate = addDays(new Date(), 1)
				setDateInStore(currentDate, "start")
				dispatch(workFlowActions.updateWorkDraftFields("workStartMinDate", currentDate))
				dispatch(workFlowActions.updateWorkDraftFields("workStartMaxDate", addDays(currentDate, 365)))

				setDateInStore(currentDate, "end")
				dispatch(workFlowActions.updateWorkDraftFields("workEndMinDate", currentDate))
				dispatch(workFlowActions.updateWorkDraftFields("workEndMaxDate", addDays(currentDate, 365)))
			}
			return () => {}
		}, [])
	)

	const onOK = () => {
		validateSchedule()
	}

	const validateSchedule = () => {
		let scheduleHours = (differenceInCalendarDays(new Date(workDraft.workEndDate), new Date(workDraft.workStartDate)) + 1) * 8
		let status = ""
		let valid = false

		if (scheduleHours < workDetails.estimatedEfforts) {
			//valid = false
			status = "schedule is less for the planned effort"
		} else {
			valid = true
			status = "schedule is good to go"
		}

		dispatch(workFlowActions.updateWorkDraftFields("scheduleValid", valid))
		dispatch(workFlowActions.updateWorkDraftFields("scheduleStatus", status))
	}

	const onStartDateChange = selectDate => {
		setDateInStore(selectDate, "start")

		if (GetUnixTime(selectDate) > workDetails.plannedEndDate) {
			setDateInStore(selectDate, "end")
		}
		dispatch(workFlowActions.updateWorkDraftFields("workEndMinDate", selectDate))
	}

	const onEndDateChange = selectDate => {
		setDateInStore(selectDate, "end")
	}

	const setDateInStore = (inputDate, type) => {
		if (workDraft.scheduleValid || workDraft.scheduleStatus != "") {
			dispatch(workFlowActions.updateWorkDraftFields("scheduleValid", false))
			dispatch(workFlowActions.updateWorkDraftFields("scheduleStatus", ""))
		}

		if (type === "start") {
			inputDate.setHours(0, 0, 0)
			dispatch(workFlowActions.updateWorkDraftFields("workStartDate", inputDate))
			dispatch(workFlowActions.updateWorkFlowFields("plannedStartDate", GetUnixTime(inputDate)))
		} else {
			inputDate.setHours(23, 59, 59)
			dispatch(workFlowActions.updateWorkDraftFields("workEndDate", inputDate))
			dispatch(workFlowActions.updateWorkFlowFields("plannedEndDate", GetUnixTime(inputDate)))
		}
	}

	return (
		<View style={styles.container}>
			<View style={{ flexDirection: "row", alignItems: "center" }}>
				<Text style={{ ...styles.subHeaderText, marginRight: "3%" }}>Please select dates and press</Text>
				<TouchableOpacity onPress={() => onOK()}>
					<LinearGradient style={styles.linearGradient} colors={["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.5)"]}>
						<Text style={styles.okStyle}>OK</Text>
					</LinearGradient>
				</TouchableOpacity>
			</View>
			{workDraft.scheduleStatus != "" && (
				<Text style={{ ...styles.subHeaderText, marginTop: "2%", textAlign: "right", color: workDraft.scheduleValid ? Colors.blue2 : Colors.red }}>
					{workDraft.scheduleStatus}
				</Text>
			)}
			<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: "3%" }}>
				<Text style={{ ...styles.subHeaderText, width: "25%" }}>Start date</Text>
				<View style={{ width: "75%" }}>
					<CustomDatePicker
						displayDate={workDraft.workStartDate}
						onDateChange={onStartDateChange}
						minimumDate={workDraft.workStartMinDate}
						maximumDate={workDraft.workStartMaxDate}
					/>
				</View>
			</View>
			<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginVertical: "3%" }}>
				<Text style={{ ...styles.subHeaderText, width: "25%" }}>End date</Text>
				<View style={{ width: "75%" }}>
					<CustomDatePicker
						displayDate={workDraft.workEndDate}
						onDateChange={onEndDateChange}
						minimumDate={workDraft.workEndMinDate}
						maximumDate={workDraft.workEndMaxDate}
					/>
				</View>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	linearGradient: {
		height: 28,
		width: 28,
		borderRadius: 14,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "center"
	},
	okStyle: {
		fontSize: 12,
		fontWeight: "bold",
		color: "#fff"
	},
	subHeaderText: {
		fontSize: 13,
		fontStyle: "italic"
	}
})

export default WorkSchedule
