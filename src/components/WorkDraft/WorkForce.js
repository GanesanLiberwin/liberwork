import React from "react"
import { View, StyleSheet, Text, TouchableOpacity, Switch } from "react-native"
import { useDispatch, useSelector } from "react-redux"

import { FontAwesome } from "@expo/vector-icons"
import Colors from "../../constants/Colors"

import * as workFlowActions from "../../store/actions/workFlowActions"

export const WorkForce = props => {
	const dispatch = useDispatch()
	const workDetails = useSelector(state => state.workFlow.workDetails)
	const workDraft = useSelector(state => state.workFlow.workDraft)

	const OnWorkPerformed = workForce => {
		dispatch(workFlowActions.updateWorkFlowFields("workforceType", workForce))
		dispatch(workFlowActions.updateWorkDraftFields("workForceValid", true))
	}

	const beginnerToggle = () => {
		dispatch(workFlowActions.updateWorkFlowFields("beginnerAllowed", !workDetails.beginnerAllowed))
	}

	return (
		<View style={styles.workTypeCard}>
			<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
				<Text style={{ ...styles.subHeaderText, width: "60%" }}>Work performed by?</Text>
				<TouchableOpacity style={{ alignItems: "center" }} onPress={() => OnWorkPerformed("individual")}>
					<FontAwesome name="user" size={34} color={workDetails.workforceType === "individual" ? Colors.blue2 : Colors.GreyInactive} />
					<Text style={{ fontSize: 9, textAlign: "center" }}>individual</Text>
				</TouchableOpacity>
				<TouchableOpacity style={{ alignItems: "center" }} onPress={() => OnWorkPerformed("team")}>
					<FontAwesome name="users" size={34} color={workDetails.workforceType === "team" ? Colors.blue2 : Colors.GreyInactive} />
					<Text style={{ fontSize: 9, textAlign: "center" }}>team</Text>
				</TouchableOpacity>
			</View>
			{workDraft.workForceValid && (
				<View style={{ flexDirection: "row", marginTop: "5%", justifyContent: "space-between", alignItems: "center" }}>
					<Text style={{ ...styles.subHeaderText, width: "70%" }}>Ok with beginners? (optional)</Text>
					<Switch
						style={{ transform: [{ scaleX: 0.7 }, { scaleY: 0.7 }] }}
						trackColor={{ false: Colors.GreyInactive, true: Colors.green1 }}
						thumbColor={Colors.white}
						ios_backgroundColor={Colors.GreyInactive}
						onValueChange={beginnerToggle}
						value={workDetails.beginnerAllowed}
					/>
				</View>
			)}
		</View>
	)
}

const styles = StyleSheet.create({
	workTypeCard: {
		flex: 1,
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	subHeaderText: {
		fontSize: 13,
		fontStyle: "italic"
	}
})

export default WorkForce
