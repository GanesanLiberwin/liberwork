//**********REPLACED WITH WorkFunction components***********
import React, { useState } from "react"
import { View, StyleSheet, Text, TouchableOpacity, Platform, ActionSheetIOS, Modal } from "react-native"
import { Picker } from "@react-native-picker/picker"

import { useDispatch, useSelector } from "react-redux"

import { Octicons } from "@expo/vector-icons"
import { LinearGradient } from "expo-linear-gradient"
import Colors from "../../constants/Colors"
import * as WorkPostData from "../../constants/Domains"
import Search from "../../components/Utilities/Search"

import * as workFlowActions from "../../store/actions/workFlowActions"

export const WorkCategory = props => {
	const dispatch = useDispatch()
	const workDetails = useSelector(state => state.workFlow.workDetails)
	const SelectedCategory = useSelector(state => state.search.lstCategory)

	const [modalVisible, setModalVisible] = useState(false)

	const onFunctionExpIOS = () =>
		ActionSheetIOS.showActionSheetWithOptions(
			{
				options: [...WorkPostData.expList, "Cancel"],
				cancelButtonIndex: WorkPostData.expList.length,
				title: "Years of experience"
			},
			buttonIndex => {
				WorkPostData.expList.filter((exp, key) => key === buttonIndex).map(filteredExp => OnFunctionExperience(filteredExp))
			}
		)

	const OnFunctionDomain = functionalDomain => {
		dispatch(workFlowActions.updateWorkFlowFields("functionalDomain", functionalDomain))
		dispatch(workFlowActions.updateWorkDraftFields("categoryValid", true))
	}

	const OnFunctionExperience = functionalExperience => {
		dispatch(workFlowActions.updateWorkFlowFields("functionalExperience", functionalExperience))
	}

	const onModalOpen = () => {
		setModalVisible(true)
	}

	const onModalClose = () => {
		setModalVisible(!modalVisible)
		//dispatch(searchActions.clearSearchCategory())
	}

	return (
		<View style={styles.container}>
			<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
				<Text style={{ ...styles.subHeaderText, width: "75%" }}>Please add a work category</Text>
				<TouchableOpacity onPress={() => onModalOpen()}>
					<LinearGradient style={styles.linearGradient} colors={["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.5)"]}>
						<Octicons name="plus" style={styles.iconPlusStyle} />
					</LinearGradient>
				</TouchableOpacity>
			</View>
			{workDetails.functionalDomain != "" && (
				<View style={{ flexDirection: "row", margin: "2%", alignItems: "center" }}>
					<View style={{ margin: 7, borderRadius: 5, backgroundColor: "rgba(192, 192, 192, 0.5)", width: "60%" }}>
						<Text numberOfLines={1} style={{ fontSize: 14, color: "#000", paddingVertical: 5, paddingHorizontal: 6 }}>
							{workDetails.functionalDomain}
						</Text>
					</View>
					{Platform.OS === "ios" ? (
						<TouchableOpacity onPress={onFunctionExpIOS}>
							<Text style={{ marginLeft: 7 }}>{"Exp  " + workDetails.functionalExperience + " yrs"}</Text>
						</TouchableOpacity>
					) : (
						<Picker
							mode="dropdown"
							selectedValue={workDetails.functionalExperience}
							style={{ width: "25%", height: 20 }}
							itemStyle={{ height: 20 }}
							onValueChange={OnFunctionExperience}
						>
							{WorkPostData.expList.map((item, key) => (
								<Picker.Item label={item} value={item} key={key} />
							))}
						</Picker>
					)}
				</View>
			)}
			<View style={styles.centeredView}>
				<Modal
					animationType="slide"
					transparent={true}
					visible={modalVisible}
					onRequestClose={() => {
						setModalVisible(!modalVisible)
					}}
				>
					<View style={styles.centeredView}>
						<View style={styles.modalView}>
							<TouchableOpacity style={{ marginVertical: 10, alignSelf: "flex-end" }} onPress={() => onModalClose()}>
								<Text style={styles.buttonTextStyle}>CLOSE</Text>
							</TouchableOpacity>
							<Search numberOfSelections={1} searchType="Category" selectedList={SelectedCategory} preferenceSelection={false} />
						</View>
					</View>
				</Modal>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	modalView: {
		width: "90%",
		height: "70%",
		backgroundColor: Colors.BGDark,
		borderRadius: 7,
		padding: 10,
		alignItems: "center"
	},
	linearGradient: {
		height: 28,
		width: 28,
		borderRadius: 14,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "center"
	},
	iconPlusStyle: {
		fontSize: 20,
		color: "#fff"
	},
	buttonTextStyle: {
		color: Colors.TextLight,
		//fontSize: 12,
		fontWeight: "bold",
		textAlign: "center"
	},
	subHeaderText: {
		fontSize: 13,
		fontStyle: "italic"
	}
})

export default WorkCategory
