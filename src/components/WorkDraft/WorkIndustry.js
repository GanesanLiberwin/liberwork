import React from "react"
import { View, StyleSheet, Text, TouchableOpacity, Platform, ActionSheetIOS } from "react-native"
import { Picker } from "@react-native-picker/picker"
import { useDispatch, useSelector } from "react-redux"

import { Octicons } from "@expo/vector-icons"
import { LinearGradient } from "expo-linear-gradient"
import Colors from "../../constants/Colors"
import * as WorkPostData from "../../constants/Domains"

import * as workFlowActions from "../../store/actions/workFlowActions"

export const WorkIndustry = props => {
	const dispatch = useDispatch()
	const workDetails = useSelector(state => state.workFlow.workDetails)

	const onIndusDomainIOS = () =>
		ActionSheetIOS.showActionSheetWithOptions(
			{
				options: [...WorkPostData.industryDomainList, "Cancel"],
				cancelButtonIndex: WorkPostData.industryDomainList.length,
				title: "Select an Industry Domain"
			},
			buttonIndex => {
				WorkPostData.industryDomainList
					.filter((indusDomain, key) => key === buttonIndex)
					.map(filteredIndusDomain => OnIndustryDomain(filteredIndusDomain))
			}
		)

	const onDomainExpIOS = () =>
		ActionSheetIOS.showActionSheetWithOptions(
			{
				options: [...WorkPostData.expList, "Cancel"],
				cancelButtonIndex: WorkPostData.expList.length,
				title: "Years of Experience"
			},
			buttonIndex => {
				WorkPostData.expList.filter((exp, key) => key === buttonIndex).map(filteredExp => OnIndustryExperience(filteredExp))
			}
		)

	const OnIndustryDomain = industryDomain => {
		dispatch(workFlowActions.updateWorkFlowFields("industryDomain", industryDomain))
	}

	const OnIndustryExperience = industryExperience => {
		dispatch(workFlowActions.updateWorkFlowFields("industryExperience", industryExperience))
	}

	return (
		<View style={styles.container}>
			<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
				<Text style={{ ...styles.subHeaderText, width: "75%" }}>Please select an industry (optional)</Text>
				{Platform.OS === "ios" ? (
					<TouchableOpacity onPress={onIndusDomainIOS}>
						<LinearGradient style={styles.linearGradient} colors={["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.5)"]}>
							<Octicons name="plus" style={styles.iconPlusStyle} />
						</LinearGradient>
					</TouchableOpacity>
				) : (
					<Text style={{ ...styles.subHeaderText }}>Exp (yrs)</Text>
				)}
			</View>
			{Platform.OS === "ios" ? (
				workDetails.industryDomain != "" && (
					<View style={{ flexDirection: "row", margin: "2%", alignItems: "center" }}>
						<View style={{ margin: 7, borderRadius: 5, backgroundColor: "rgba(192, 192, 192, 0.5)", width: "60%" }}>
							<Text numberOfLines={1} style={{ fontSize: 14, color: "#000", paddingVertical: 5, paddingHorizontal: 6 }}>
								{workDetails.industryDomain}
							</Text>
						</View>
						<TouchableOpacity onPress={onDomainExpIOS}>
							<Text style={{ marginLeft: 7 }}>{"Exp  " + workDetails.industryExperience + " yrs"}</Text>
						</TouchableOpacity>
					</View>
				)
			) : (
				<View style={{ flexDirection: "row", margin: "2%", justifyContent: "space-between", alignItems: "center" }}>
					<Picker
						mode="dropdown"
						selectedValue={workDetails.industryDomain}
						style={{ width: "60%", height: 20 }}
						itemStyle={{ height: 5 }}
						onValueChange={OnIndustryDomain}
					>
						{WorkPostData.industryDomainList.map((item, key) => (
							<Picker.Item label={item} value={item} key={key} />
						))}
					</Picker>
					{workDetails.industryDomain != "" && (
						<Picker
							mode="dropdown"
							selectedValue={workDetails.industryExperience}
							style={{ width: "25%", height: 20 }}
							itemStyle={{ height: 20 }}
							onValueChange={OnIndustryExperience}
						>
							{WorkPostData.expList.map((item, key) => (
								<Picker.Item label={item} value={item} key={key} />
							))}
						</Picker>
					)}
				</View>
			)}
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	linearGradient: {
		height: 28,
		width: 28,
		borderRadius: 14,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "center"
	},
	iconPlusStyle: {
		fontSize: 20,
		color: "#fff"
	},
	subHeaderText: {
		fontSize: 13,
		fontStyle: "italic"
	}
})

export default WorkIndustry
