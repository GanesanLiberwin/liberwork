import React from "react"
import { View, StyleSheet, Text, TouchableOpacity } from "react-native"
import { useDispatch, useSelector } from "react-redux"

import Colors from "../../constants/Colors"

import * as workFlowActions from "../../store/actions/workFlowActions"

export const WorkRewardModel = props => {
	const dispatch = useDispatch()
	const workDetails = useSelector(state => state.workFlow.workDetails)
	const lstRewardsModel = useSelector(state => state.rewardModels.lstModels)

	const onModelSelection = model => {
		dispatch(workFlowActions.updateWorkFlowFields("rewardModel", model))
		dispatch(workFlowActions.updateWorkDraftFields("rewardModelValid", true))
	}

	return (
		<View style={styles.container}>
			<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: "2%" }}>
				<Text style={{ ...styles.subHeaderText }}>Please choose a reward model</Text>
			</View>
			<View style={{ flexDirection: "row", justifyContent: "space-around", marginTop: 10 }}>
				{lstRewardsModel.map((item, key) => {
					return (
						<View
							key={item.id}
							style={{
								width: "40%",
								borderRadius: 5,
								marginVertical: "2%",
								backgroundColor: workDetails.rewardModel === item.id ? Colors.blue2 : Colors.GreyInactive
							}}
						>
							<TouchableOpacity onPress={() => onModelSelection(item.id)}>
								<Text style={{ ...styles.modelText, padding: "3%" }}>{item.id}</Text>
							</TouchableOpacity>
						</View>
					)
				})}
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	subHeaderText: {
		fontSize: 13,
		fontStyle: "italic"
	},
	oneModel: {
		width: "40%",
		alignItems: "center",
		//padding: "4%",
		borderRadius: 7
	},
	modelText: {
		fontSize: 18,
		fontWeight: "bold",
		textAlign: "center",
		padding: "2%",
		color: "#fff"
	}
})

export default WorkRewardModel
