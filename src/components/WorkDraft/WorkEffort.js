import React from "react"
import { View, StyleSheet, Text, TextInput, Switch } from "react-native"
import { useDispatch, useSelector } from "react-redux"

import Colors from "../../constants/Colors"
import Regex from "../../constants/Regex"

import * as workFlowActions from "../../store/actions/workFlowActions"

export const WorkEffort = props => {
	const dispatch = useDispatch()
	const workDetails = useSelector(state => state.workFlow.workDetails)
	const workDraft = useSelector(state => state.workFlow.workDraft)

	const criticalToggle = () => {
		dispatch(workFlowActions.updateWorkFlowFields("criticalWork", !workDetails.criticalWork))
	}

	const onTextChange = effort => {
		dispatch(workFlowActions.updateWorkFlowFields("estimatedEfforts", effort))
		dispatch(workFlowActions.updateWorkDraftFields("scheduleValid", false))
		dispatch(workFlowActions.updateWorkDraftFields("scheduleStatus", ""))

		let status = ""
		let valid = false

		if (!Regex.number.test(effort) || effort <= 0) {
			status = "enter valid effort"
		} else {
			valid = true
			status = ""
		}

		dispatch(workFlowActions.updateWorkDraftFields("effortValid", valid))
		dispatch(workFlowActions.updateWorkDraftFields("effortStatus", status))
	}

	return (
		<View style={styles.container}>
			<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
				<Text style={{ ...styles.subHeaderText, width: "60%" }}>Please enter work effort (hours)</Text>
				<TextInput
					numberOfLines={1}
					style={{ width: "25%", padding: 3, textAlign: "center", marginVertical: "3%", backgroundColor: Colors.white, borderRadius: 5 }}
					allowFontScaling={false}
					autoCapitalize="none"
					autoCorrect={false}
					placeholder="hours"
					placeholderTextColor="rgba(0,0,0,0.25)"
					maxLength={5}
					value={workDetails.estimatedEfforts}
					onChangeText={text => onTextChange(text)}
					keyboardType="number-pad"
					returnKeyType="done"
					clearButtonMode="while-editing"
				></TextInput>
			</View>
			{!workDraft.effortValid && <Text style={{ ...styles.subHeaderText, color: Colors.red, textAlign: "right" }}>{workDraft.effortStatus}</Text>}
			<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginVertical: "2%" }}>
				<Text style={{ ...styles.subHeaderText, width: "60%" }}>Is your work critical?</Text>
				<Switch
					style={{ transform: [{ scaleX: 0.7 }, { scaleY: 0.7 }] }}
					trackColor={{ false: Colors.GreyInactive, true: Colors.orange1 }}
					thumbColor={Colors.white}
					ios_backgroundColor={Colors.GreyInactive}
					onValueChange={criticalToggle}
					value={workDetails.criticalWork}
				/>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	subHeaderText: {
		fontSize: 13,
		fontStyle: "italic"
	}
})

export default WorkEffort
