import React from "react"
import { View, StyleSheet, Text, TextInput, Image } from "react-native"
import { useDispatch, useSelector } from "react-redux"

import { IMAGE } from "../../constants/Image"
import Colors from "../../constants/Colors"
import Regex from "../../constants/Regex"

import * as workFlowActions from "../../store/actions/workFlowActions"

export const WorkReward = props => {
	const dispatch = useDispatch()
	const workDetails = useSelector(state => state.workFlow.workDetails)
	const workDraft = useSelector(state => state.workFlow.workDraft)

	const curBalance = useSelector(state =>
		state.wallets.BusinessWallet && state.wallets.BusinessWallet.balance ? state.wallets.BusinessWallet.balance : 0
	)

	const onTextChange = reward => {
		dispatch(workFlowActions.updateWorkFlowFields("rewardUnits", reward))

		let status = ""
		let valid = false

		if (!Regex.number.test(reward) || reward <= 0) {
			status = "enter valid reward"
		} else if (reward > curBalance) {
			status = "reward exceeds balance"
		} else {
			valid = true
			status = ""
		}

		dispatch(workFlowActions.updateWorkDraftFields("rewardValid", valid))
		dispatch(workFlowActions.updateWorkDraftFields("rewardStatus", status))
	}

	return (
		<View style={styles.container}>
			<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
				<Text style={{ ...styles.subHeaderText, width: "60%" }}>Please enter reward value</Text>
				<Image style={styles.icon} source={IMAGE["ICON_LIBER"]} resizeMode="contain" />
				<TextInput
					numberOfLines={1}
					style={{ width: "25%", padding: 3, textAlign: "center", marginVertical: "3%", backgroundColor: Colors.white, borderRadius: 5 }}
					allowFontScaling={false}
					autoCapitalize="none"
					autoCorrect={false}
					placeholder={curBalance.toString()}
					placeholderTextColor="rgba(0,0,0,0.25)"
					maxLength={5}
					value={workDetails.rewardUnits}
					onChangeText={text => onTextChange(text)}
					keyboardType="number-pad"
					returnKeyType="done"
					clearButtonMode="while-editing"
					editable={curBalance > 0 ? true : false}
				></TextInput>
			</View>
			<View style={{ flexDirection: "row", marginVertical: "3%", justifyContent: "space-between", alignItems: "center" }}>
				<Text style={{ ...styles.subHeaderText, width: "45%", color: curBalance > 0 ? Colors.black : Colors.red }}>
					{curBalance > 0 ? "curr balance is  " + curBalance.toString() : "load wallet to proceed"}
				</Text>
				{workDraft.rewardValid ? (
					<Text style={{ ...styles.subHeaderText, textAlign: "right" }}>
						{"rate per hr is  " + Math.ceil(workDetails.rewardUnits / workDetails.estimatedEfforts).toString()}
					</Text>
				) : (
					<Text style={{ ...styles.subHeaderText, color: Colors.red, textAlign: "right" }}>{workDraft.rewardStatus}</Text>
				)}
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	subHeaderText: {
		fontSize: 13,
		fontStyle: "italic"
	},
	icon: {
		height: 23,
		width: 23
	}
})

export default WorkReward
