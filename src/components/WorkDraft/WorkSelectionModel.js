import React from "react"
import { View, StyleSheet, Text, TouchableOpacity } from "react-native"
import { useDispatch, useSelector } from "react-redux"

import Colors from "../../constants/Colors"

import * as workFlowActions from "../../store/actions/workFlowActions"

export const WorkSelectionModel = props => {
	const dispatch = useDispatch()
	const workDetails = useSelector(state => state.workFlow.workDetails)

	//const SelectionModels = ["I SELECT", "I SHORTLIST AUTO SELECT", "AUTO SHORTLIST I SELECT", "AUTO SELECT"]
	const SelectionModels = ["I SELECT", "AUTO SELECT"]

	const onModelSelection = model => {
		dispatch(workFlowActions.updateWorkFlowFields("selectionModel", model))
		dispatch(workFlowActions.updateWorkDraftFields("selectionModelValid", true))
	}

	return (
		<View style={styles.container}>
			<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: "2%" }}>
				<Text style={{ ...styles.subHeaderText }}>Please choose a selection model</Text>
			</View>

			<View style={{ flexDirection: "row", justifyContent: "space-around", marginTop: 10, flexWrap: "wrap" }}>
				{SelectionModels.map((item, key) => {
					return (
						<View
							key={item}
							style={{
								width: "40%",
								borderRadius: 5,
								marginVertical: "2%",
								backgroundColor: workDetails.selectionModel === item ? Colors.blue2 : Colors.GreyInactive
							}}
						>
							<TouchableOpacity onPress={() => onModelSelection(item)}>
								<Text style={{ ...styles.modelText }}>{item}</Text>
							</TouchableOpacity>
						</View>
					)
				})}
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	subHeaderText: {
		fontSize: 13,
		fontStyle: "italic"
	},
	modelText: {
		fontSize: 13,
		//fontWeight: "bold",
		textAlign: "center",
		padding: "5%",
		color: "#fff"
	}
})

export default WorkSelectionModel
