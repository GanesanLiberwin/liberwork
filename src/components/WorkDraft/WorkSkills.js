import React, { useState } from "react"
import { View, StyleSheet, Text, TouchableOpacity, Modal, Alert } from "react-native"
import { useDispatch, useSelector } from "react-redux"

import { Octicons } from "@expo/vector-icons"
import { LinearGradient } from "expo-linear-gradient"
import Colors from "../../constants/Colors"
import Search from "../../components/Utilities/Search"

import * as workFlowActions from "../../store/actions/workFlowActions"
import * as searchActions from "../../store/actions/searchActions"

export const WorkSkills = props => {
	const dispatch = useDispatch()
	const workDetails = useSelector(state => state.workFlow.workDetails)
	const SelectedSkills = useSelector(state => state.search.lstSkills)

	const [modalVisible, setModalVisible] = useState(false)

	const onModalOpen = () => {
		setModalVisible(true)
	}

	const onModalClose = () => {
		if (!SelectedSkills.some(s => s.required === true) && SelectedSkills.length != 0) {
			showAlert()
		} else {
			setModalVisible(!modalVisible)
			dispatch(workFlowActions.updateWorkFlowFields("skills", SelectedSkills))

			if (SelectedSkills.length > 0) {
				dispatch(workFlowActions.updateWorkDraftFields("skillValid", true))
			} else {
				dispatch(workFlowActions.updateWorkDraftFields("skillValid", false))
			}

			dispatch(searchActions.clearFetchSkills())
			//dispatch(searchActions.clearSearchSkills()) ==> Draft Close
		}
	}

	const showAlert = () =>
		Alert.alert(
			"Select Skill",
			"Atleast one skill with MUST required",
			[
				{
					text: "OK"
				}
			],
			{ cancelable: false }
		)

	return (
		<View style={styles.container}>
			<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
				<Text style={{ ...styles.subHeaderText, width: "60%" }}>Please add skills</Text>
				<TouchableOpacity style={{}} onPress={() => onModalOpen()}>
					<LinearGradient style={styles.linearGradient} colors={["rgba(77, 192, 243, 1)", "rgba(77, 192, 243, 0.5)"]}>
						<Octicons name="plus" style={styles.iconPlusStyle} />
					</LinearGradient>
				</TouchableOpacity>
			</View>
			<View style={{ margin: "2%" }}>
				{workDetails.skills.map((item, index) => {
					return (
						<View key={item.skill + index} style={{ flexDirection: "row" }}>
							<View style={{ margin: 7, borderRadius: 5, backgroundColor: "rgba(192, 192, 192, 0.5)", width: "60%" }}>
								<Text style={{ fontSize: 14, color: "#000", paddingVertical: 5, paddingHorizontal: 6 }}>{item.skill}</Text>
							</View>
							<View style={{ margin: 7, borderRadius: 5, backgroundColor: "rgba(192, 192, 192, 0.5)", width: "20%" }}>
								<Text style={{ fontSize: 13, color: "#000", padding: 5, alignSelf: "center" }}>{item.required ? "MUST" : "NICE"}</Text>
							</View>
						</View>
					)
				})}
			</View>
			<View style={styles.centeredView}>
				<Modal
					animationType="slide"
					transparent={true}
					visible={modalVisible}
					onRequestClose={() => {
						setModalVisible(!modalVisible)
					}}
				>
					<View style={styles.centeredView}>
						<View style={styles.modalView}>
							<TouchableOpacity style={{ marginVertical: 10, alignSelf: "flex-end" }} onPress={() => onModalClose()}>
								<Text style={styles.buttonTextStyle}>CLOSE</Text>
							</TouchableOpacity>
							<Search numberOfSelections={3} searchType="Skills" selectedList={workDetails.skills} preferenceSelection={true} />
						</View>
					</View>
				</Modal>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	centeredView: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	modalView: {
		width: "90%",
		height: "70%",
		backgroundColor: Colors.BGDark,
		borderRadius: 7,
		padding: 10,
		alignItems: "center"
	},
	linearGradient: {
		height: 28,
		width: 28,
		borderRadius: 14,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "center"
	},
	iconPlusStyle: {
		fontSize: 20,
		color: "#fff"
	},
	buttonTextStyle: {
		color: Colors.TextLight,
		//fontSize: 12,
		fontWeight: "bold",
		textAlign: "center"
	},
	subHeaderText: {
		fontSize: 13,
		fontStyle: "italic"
	}
})

export default WorkSkills
