import React from "react"
import { View, StyleSheet, Text, TextInput, Switch } from "react-native"
import { useDispatch, useSelector } from "react-redux"

import Colors from "../../constants/Colors"

import * as workFlowActions from "../../store/actions/workFlowActions"

export const WorkLocation = props => {
	const dispatch = useDispatch()
	const workDetails = useSelector(state => state.workFlow.workDetails)

	const locationToggle = () => {
		const remoteWorking = !workDetails.remoteWorking
		dispatch(workFlowActions.updateWorkFlowFields("remoteWorking", !workDetails.remoteWorking))

		if (remoteWorking) {
			dispatch(workFlowActions.updateWorkFlowFields("workLocation", ""))
			dispatch(workFlowActions.updateWorkDraftFields("locationValid", true))
		} else {
			dispatch(workFlowActions.updateWorkDraftFields("locationValid", false))
		}
	}

	const onTextChange = workLocation => {
		dispatch(workFlowActions.updateWorkFlowFields("workLocation", workLocation))

		if (workLocation === "") {
			dispatch(workFlowActions.updateWorkDraftFields("locationValid", false))
		} else {
			dispatch(workFlowActions.updateWorkDraftFields("locationValid", true))
		}
	}

	return (
		<View style={styles.container}>
			<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
				<Text style={{ ...styles.subHeaderText, width: "60%" }}>Work remotely?</Text>
				<Switch
					style={{ transform: [{ scaleX: 0.7 }, { scaleY: 0.7 }] }}
					trackColor={{ false: Colors.GreyInactive, true: Colors.green1 }}
					thumbColor={Colors.white}
					ios_backgroundColor={Colors.GreyInactive}
					onValueChange={locationToggle}
					value={workDetails.remoteWorking}
				/>
			</View>
			{!workDetails.remoteWorking && (
				<TextInput
					numberOfLines={1}
					style={{ width: "90%", padding: "2%", alignSelf: "center", marginVertical: "3%", backgroundColor: Colors.white, borderRadius: 5 }}
					allowFontScaling={false}
					autoCapitalize="none"
					autoCorrect={false}
					textAlignVertical="top"
					placeholder="please enter your location"
					placeholderTextColor="rgba(0,0,0,0.25)"
					maxLength={60}
					value={workDetails.workLocation}
					onChangeText={text => onTextChange(text)}
				></TextInput>
			)}
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	subHeaderText: {
		fontSize: 13,
		fontStyle: "italic"
	}
})

export default WorkLocation
