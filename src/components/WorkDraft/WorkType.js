import React from "react"
import { View, StyleSheet, Text, TouchableOpacity } from "react-native"
import { useDispatch, useSelector } from "react-redux"

import { MaterialCommunityIcons } from "@expo/vector-icons"
import Colors from "../../constants/Colors"

import * as workFlowActions from "../../store/actions/workFlowActions"

export const WorkType = props => {
	const dispatch = useDispatch()
	const workDetails = useSelector(state => state.workFlow.workDetails)

	const OnWorkType = workType => {
		dispatch(workFlowActions.updateWorkFlowFields("workType", workType))
		dispatch(workFlowActions.updateWorkDraftFields("workTypeValid", true))
	}

	return (
		<View style={styles.workTypeCard}>
			<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
				<Text style={{ ...styles.subHeaderText, width: "60%" }}>Type of work?</Text>
				<TouchableOpacity style={{ alignItems: "center" }} onPress={() => OnWorkType("outcome")}>
					<MaterialCommunityIcons name="file-document" size={34} color={workDetails.workType === "outcome" ? Colors.blue2 : Colors.GreyInactive} />
					<Text style={{ fontSize: 9, textAlign: "center" }}>outcome</Text>
				</TouchableOpacity>
				<TouchableOpacity style={{ alignItems: "center" }} onPress={() => OnWorkType("duration")}>
					<MaterialCommunityIcons name="clock" size={34} color={workDetails.workType === "duration" ? Colors.blue2 : Colors.GreyInactive} />
					<Text style={{ fontSize: 9, textAlign: "center" }}>duration</Text>
				</TouchableOpacity>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	workTypeCard: {
		flex: 1,
		padding: "2%",
		marginTop: "5%",
		marginHorizontal: "5%",
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	subHeaderText: {
		fontSize: 13,
		fontStyle: "italic"
	}
})

export default WorkType
