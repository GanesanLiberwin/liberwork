import React, { Component } from "react"
import { Platform, StatusBar } from "react-native"
import { View, Image, Text, StyleSheet, TouchableOpacity } from "react-native"
import { IMAGE } from "../constants/Image"
import { Ionicons } from "@expo/vector-icons"

export class CustomHeader2 extends Component {
	render() {
		let { isLogo, isBack, isClose, title, navigation, orgId } = this.props
		return (
			<View style={styles.headerContainer}>
				{isBack && (
					<TouchableOpacity style={{ justifyContent: "center" }} onPress={() => navigation.goBack()}>
						<Ionicons style={styles.iconStyle} name="ios-arrow-back" />
					</TouchableOpacity>
				)}
				<View style={{ flex: 1, flexDirection: "row" }}>
					{isLogo ? (
						<Image style={styles.appLogoStyle} source={IMAGE.ICON_APP} resizeMode="contain" />
					) : (
						<View style={styles.viewOrgIdStyle}>
							<Text style={styles.textOrgIdStyle}>{orgId}</Text>
						</View>
					)}
					<Text style={styles.titleStyle}>{title}</Text>
				</View>
				{isClose && (
					<TouchableOpacity style={{ justifyContent: "center" }} onPress={this.props.onPress}>
						<Ionicons style={styles.iconStyle} name="md-close" />
					</TouchableOpacity>
				)}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	headerContainer: {
		flexDirection: "row",
		height: 40,
		//backgroundColor: "grey",
		...Platform.select({
			android: {
				marginTop: StatusBar.currentHeight
			}
		})
	},
	iconStyle: {
		fontSize: 27,
		paddingHorizontal: 15
	},
	viewOrgIdStyle: {
		marginLeft: 15,
		borderRadius: 5,
		alignSelf: "center",
		backgroundColor: "#07b0f9"
	},
	textOrgIdStyle: {
		fontSize: 16,
		fontWeight: "bold",
		color: "#fff",
		padding: 3
	},
	titleStyle: {
		fontSize: 24,
		fontWeight: "500",
		marginLeft: 10,
		alignSelf: "center"
	},
	appLogoStyle: {
		height: 30,
		width: 32,
		marginLeft: 15,
		alignSelf: "center"
	}
})
