import React, { Component } from "react"
import { Platform, StatusBar } from "react-native"
import { View, Image, Text, StyleSheet, TouchableOpacity } from "react-native"

import { IMAGE } from "../constants/Image"
import { Feather } from "@expo/vector-icons"

export class CustomHeader extends Component {
	render() {
		let { isHome, isSearch, isNotification, title, navigation, orgId } = this.props

		return (
			<View style={styles.headerContainer}>
				<View style={{ flex: 1 }}>
					{isHome ? (
						<View style={{ flexDirection: "row", marginLeft: 20 }}>
							<Image style={styles.logoStyle} source={IMAGE.ICON_LIBERWORK} resizeMode="contain" />
							<View style={{ alignSelf: "center", flex: 1 }}>
								<Text style={{ alignSelf: "center", fontSize: 20, fontWeight: "bold", color: "rgba(0,0,0,0.15)" }}>{orgId}</Text>
							</View>
						</View>
					) : (
						<View style={{ flexDirection: "row", marginLeft: 15 }}>
							{orgId ? (
								<View style={styles.viewOrgIdStyle}>
									<Text style={styles.textOrgIdStyle}>{orgId}</Text>
								</View>
							) : null}
							<Text style={styles.titleStyle}>{title}</Text>
						</View>
					)}
				</View>
				{isSearch && (
					<TouchableOpacity style={styles.iconContainer} onPress={() => navigation.navigate("Search")}>
						<Feather style={styles.iconSearchStyle} name="search" />
					</TouchableOpacity>
				)}
				{isNotification && (
					<TouchableOpacity style={styles.iconContainer} onPress={() => navigation.navigate("Notifications")}>
						<Feather style={styles.iconNotificationStyle} name="bell" />
					</TouchableOpacity>
				)}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	iconContainer: {
		justifyContent: "center",
		marginRight: 20
	},
	headerContainer: {
		flexDirection: "row",
		//backgroundColor: "grey",
		alignItems: "center",
		height: 40,
		...Platform.select({
			android: {
				marginTop: StatusBar.currentHeight
			}
		})
	},
	iconSearchStyle: {
		fontSize: 25
	},
	iconNotificationStyle: {
		fontSize: 25,
		color: "#F0863D"
	},
	logoStyle: {
		width: 110,
		height: 40
		//marginLeft: 20
	},
	viewOrgIdStyle: {
		//marginLeft: 15,
		borderRadius: 5,
		alignSelf: "center",
		backgroundColor: "#07b0f9"
	},
	textOrgIdStyle: {
		fontSize: 16,
		fontWeight: "bold",
		color: "#fff",
		padding: 3
	},
	titleStyle: {
		fontSize: 24,
		fontWeight: "500",
		marginLeft: 10,
		alignSelf: "center"
	}
	/* appLogoStyle: {
		height: 40,
		width: 32,
		marginLeft: 15,
	}, */
})
