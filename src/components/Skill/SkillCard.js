import React, { Component } from "react"
import { View, StyleSheet, Text } from "react-native"
import Colors from "../../constants/Colors"
import { MaterialCommunityIcons } from "@expo/vector-icons"

export default class SkillCard extends Component {
	render() {
		return (
			<View style={styles.skillCard}>
				<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
					<View style={{ width: "80%" }}>
						<Text numberOfLines={1} style={styles.skillText}>
							{this.props.skill}
						</Text>
					</View>
					<View style={{ alignItems: "flex-end" }}>
						{
							<MaterialCommunityIcons
								style={{ ...styles.iconValidated, color: this.props.isValidated ? Colors.green1 : Colors.orange1 }}
								name="circle"
							/>
						}
					</View>
				</View>
				<View>
					<View style={{ flexDirection: "row", justifyContent: "space-between" }}>
						<Text style={styles.statsText}>{"score " + this.props.score}</Text>
						<Text style={styles.statsText}>{"#works " + this.props.numberOfWorks}</Text>
						<Text style={styles.statsText}>{"#hours " + this.props.numberOfHours}</Text>
					</View>
				</View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	skillCard: {
		marginTop: 15,
		padding: 10,
		borderRadius: 7,
		backgroundColor: Colors.GreyBackground
	},
	skillText: {
		fontSize: 16,
		fontWeight: "400"
	},
	statsText: {
		fontSize: 12,
		fontStyle: "italic",
		marginTop: 5
	},
	iconValidated: {
		fontSize: 11,
		marginHorizontal: 4,
		alignSelf: "center"
	}
})
