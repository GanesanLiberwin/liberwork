import React, { Component } from "react";
import { AntDesign } from "@expo/vector-icons";

const AntDesignIcons = (props) => {
  return (
    <AntDesign
      name={props.name}
      size={props.size}
      color={props.color}
      {...props}
    />
  );
};

export default AntDesignIcons;
