import React, { Component } from "react";
import { FontAwesome } from "@expo/vector-icons";

const FontAwesomeIcons = (props) => {
  return (
    <FontAwesome
      name={props.name}
      size={props.size}
      color={props.color}
      {...props}
    />
  );
};

export default FontAwesomeIcons;
