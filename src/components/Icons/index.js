import React from "react"

import FeatherIcons from "./Feather"
import AntDesignIcons from "./AntDesign"
import FontAwesomeIcons from "./FontAwesome"
import FontAwesome5Icons from "./FontAwesome5"
import FontistoIcons from "./Fontisto"
import IoniconsIcons from "./Ionicons"
import SimpleLineIcons from "./SimpleLineIcons"
import MaterialCommunityIcon from "./MaterialCommunityIcons"
import MaterialIcons from "./MaterialIcons"

const Icons = props => {
  switch (props.iconType) {
    case "Feather":
      return <FeatherIcons {...props} />
    case "AntDesign":
      return <AntDesignIcons {...props} />
    case "FontAwesome":
      return <FontAwesomeIcons {...props} />
    case "FontAwesome5":
      return <FontAwesome5Icons {...props} />
    case "Fontisto":
      return <FontistoIcons {...props} />
    case "Ionicons":
      return <IoniconsIcons {...props} />
    case "SimpleLineIcons":
      return <SimpleLineIcons {...props} />
    case "MaterialCommunityIcons":
      return <MaterialCommunityIcon {...props} />
    case "MaterialIcons":
      return <MaterialIcons {...props} />
    default:
      return <IoniconsIcons {...props} />
  }
}

export default Icons
