import React, { Component } from "react";
import { Feather } from "@expo/vector-icons";

const FeatherIcons = (props) => {
  return (
    <Feather
      name={props.name}
      size={props.size}
      color={props.color}
      {...props}
    />
  );
};

export default FeatherIcons;
