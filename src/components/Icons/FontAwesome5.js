import React from "react";
import { FontAwesome5 } from "@expo/vector-icons";

const FontAwesome5Icons = (props) => {
  return (
    <FontAwesome5
      name={props.name}
      size={props.size}
      color={props.color}
      {...props}
    />
  );
};

export default FontAwesome5Icons;
