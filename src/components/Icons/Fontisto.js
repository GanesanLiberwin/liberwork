import React, { Component } from "react";
import { Fontisto } from "@expo/vector-icons";

const FontistoIcons = (props) => {
  return (
    <Fontisto
      name={props.name}
      size={props.size}
      color={props.color}
      {...props}
    />
  );
};

export default FontistoIcons;
