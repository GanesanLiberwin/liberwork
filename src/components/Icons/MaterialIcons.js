import React from "react"
import { MaterialIcons } from "@expo/vector-icons"

const MaterialIcon = props => {
  return <MaterialIcons name={props.name} size={props.size} color={props.color} {...props} />
}

export default MaterialIcon
