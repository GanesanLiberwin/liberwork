import React from "react"
import { MaterialCommunityIcons } from "@expo/vector-icons"

const MaterialCommunityIcon = props => {
  return <MaterialCommunityIcons name={props.name} size={props.size} color={props.color} {...props} />
}

export default MaterialCommunityIcon
