import React, { Component } from "react";
import { Ionicons } from "@expo/vector-icons";

const IoniconsIcons = (props) => {
  return (
    <Ionicons
      name={props.name}
      size={props.size}
      color={props.color}
      {...props}
    />
  );
};

export default IoniconsIcons;
