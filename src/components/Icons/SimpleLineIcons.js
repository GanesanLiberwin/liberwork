import React, { Component } from "react";
import { SimpleLineIcons } from "@expo/vector-icons";

const SimpleLine = (props) => {
  return (
    <SimpleLineIcons
      name={props.name}
      size={props.size}
      color={props.color}
      {...props}
    />
  );
};

export default SimpleLine;
