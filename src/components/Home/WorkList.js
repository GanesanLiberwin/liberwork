import React from "react"
import { useDispatch } from "react-redux"
import { Text, StyleSheet, FlatList, TouchableOpacity } from "react-native"

import WorkCard from "./WorkCard"
import { pageLabelJson } from "../../constants/PageLabelJson"
import { workStatusJson } from "../../constants/PageJson"

import * as workActions from "../../store/actions/workActions"
import * as workFlowActions from "../../store/actions/workFlowActions"

const WorkList = ({ results, workSource, navigation }) => {
	const dispatch = useDispatch()

	const NavigateWork = item => {
		switch (item.currentStatus) {
			case workStatusJson.Draft:
				dispatch(workActions.updateDraftId(item.workId))
				navigation.navigate("WorkDraft")
				break

			//Testing New Draft flow
			case workStatusJson.DraftNew:
				dispatch(workActions.updateDraftId(item.workId))
				navigation.navigate("WorkDraft")
				break

			case workStatusJson.BidEnded:
			case workStatusJson.Published:
			case workStatusJson.Posted:
				dispatch(workFlowActions.updateWorkFlowId(item.workId, workSource))
				navigation.navigate("WorkBid", { item: item })
				break

			default:
				dispatch(workFlowActions.updateWorkFlowId(item.workId, workSource))
				navigation.navigate("WorkDetail", { item: item })
				break
		}
	}

	return (
		<FlatList
			horizontal={true}
			showsHorizontalScrollIndicator={false}
			data={results}
			// keyExtractor={result => result.workId}
			keyExtractor={result => (typeof result.workId === "string" ? result.workId : result.workId.toString())}
			ListEmptyComponent={() => <Text style={styles.emptyTextStyle}>{pageLabelJson.Work.NoWorks}</Text>}
			renderItem={({ item, index }) => (
				<TouchableOpacity onPress={() => NavigateWork(item)}>
					<WorkCard workCardDetails={item} count={results.length} index={index + 1} />
				</TouchableOpacity>
			)}
		/>
	)
}

const styles = StyleSheet.create({
	emptyTextStyle: {
		marginVertical: 10,
		marginLeft: 25,
		fontWeight: "300",
		fontStyle: "italic"
	}
})

export default WorkList
