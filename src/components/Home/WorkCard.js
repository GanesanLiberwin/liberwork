import React, { Component } from "react"
import { View, StyleSheet, Text, Image } from "react-native"

import { MaterialCommunityIcons } from "@expo/vector-icons"
import { LinearGradient } from "expo-linear-gradient"

import { IMAGE } from "../../constants/Image"
import Colors from "../../constants/Colors"
import { getWorkCardDetails } from "../../components/Utilities/WorkUtils"

export default class WorkCard extends Component {
	render() {
		const cardDetails = getWorkCardDetails(this.props.workCardDetails)
		let { header, subHeader, body, subBody, status, icon, iconStatus, critical, delegate, message, gradientBase } = cardDetails
		const listIndex = this.props.count > 2 ? `${this.props.index}/${this.props.count}` : ""

		return (
			<LinearGradient
				style={{ borderRadius: 5, alignSelf: "center", marginHorizontal: 10, marginVertical: 14 }}
				colors={["rgba(255, 255, 255, 1)", "rgba(255, 255, 255, 0.7)", "rgba(255, 255, 255, 0.5)", gradientBase]} //rgba(140, 198, 63, 0.85) rgba(253, 123, 24, 0.85) rgba(142, 113, 78, 0.85)
			>
				<View style={styles.workCard}>
					<View style={styles.topStrip} />
					<View style={{ flexDirection: "row", alignItems: "center" }}>
						{critical ? <MaterialCommunityIcons style={styles.iconCritical} name="circle" /> : <View style={styles.dummy} />}
						{delegate && <Text style={{ fontSize: 11, marginHorizontal: 4 }}>D</Text>}
						{message && <MaterialCommunityIcons style={styles.iconMessage} name="email-variant" />}
					</View>
					<Text numberOfLines={1} style={styles.header}>
						{header}
					</Text>
					<Text numberOfLines={1} style={styles.subHeader}>
						{subHeader}
					</Text>
					<View style={{ justifyContent: "center", alignItems: "center", flex: 1 }}>
						<View style={styles.bodyContainer}>
							{iconStatus && <Image style={styles.icon} source={IMAGE[icon]} resizeMode="contain" />}
							<Text numberOfLines={1} style={styles.bodyText}>
								{body}
							</Text>
						</View>
						{subBody != "" && <Text style={styles.subBodyText}>{subBody}</Text>}
					</View>
					<View style={styles.footerContainer}>
						<Text style={{ ...styles.footer, marginLeft: 4, flex: 1 }}>{status}</Text>
						{listIndex !== "" && <Text style={{ ...styles.footer, marginRight: 4 }}>{listIndex}</Text>}
					</View>
				</View>
			</LinearGradient>
		)
	}
}

const styles = StyleSheet.create({
	workCard: {
		//elevation: 3,
		shadowOffset: { width: 1, height: 1 },
		shadowColor: "#333",
		shadowOpacity: 0.3,
		shadowRadius: 5,
		height: 120,
		width: 120
	},
	topStrip: {
		padding: 1,
		borderTopWidth: 6,
		borderRadius: 5,
		borderTopColor: "rgba(0,0,0,0.15)"
	},
	dummy: {
		height: 10
	},
	iconCritical: {
		fontSize: 11,
		color: Colors.orange1,
		marginHorizontal: 4,
		alignSelf: "center"
	},
	iconMessage: {
		fontSize: 11,
		marginHorizontal: 4,
		alignSelf: "center"
	},
	iconDelegate: {
		fontSize: 11,
		marginHorizontal: 4,
		alignSelf: "center"
	},
	header: {
		paddingTop: 2,
		paddingHorizontal: 2,
		width: "100%",
		textAlign: "center",
		fontSize: 14
	},
	subHeader: {
		padding: 2,
		width: "100%",
		textAlign: "center",
		fontSize: 13,
		fontStyle: "italic"
	},
	bodyContainer: {
		flexDirection: "row",
		alignItems: "center"
	},
	bodyText: {
		fontSize: 18
	},
	subBodyText: {
		fontStyle: "italic",
		fontSize: 9
	},
	icon: {
		height: 18,
		width: 18,
		marginRight: 4
	},
	footer: {
		fontStyle: "italic",
		fontSize: 11
	},
	footerContainer: {
		padding: 2,
		flexDirection: "row"
	}
})
