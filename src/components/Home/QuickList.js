import React from "react"
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from "react-native"
import QuickCard from "./QuickCard"

const QuickList = ({ title, resultsQuick, navigation, businessWallet, personalWallet }) => {
	return (
		<View style={{ alignItems: "center" }}>
			<Text style={styles.title}>{title}</Text>
			<FlatList
				horizontal={true}
				showsHorizontalScrollIndicator={false}
				data={resultsQuick}
				keyExtractor={result => result.id.toString()}
				renderItem={({ item }) => (
					<TouchableOpacity onPress={() => navigation.navigate(item.item)}>
						<QuickCard
							icon={item.icon}
							isWallet={item.isWallet}
							title={item.title}
							item={item.item}
							businessWallet={businessWallet}
							personalWallet={personalWallet}
						/>
					</TouchableOpacity>
				)}
			/>
		</View>
	)
}

const styles = StyleSheet.create({
	title: {
		fontSize: 18,
		fontWeight: "bold"
	}
})

export default QuickList
