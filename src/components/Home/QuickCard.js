import React, { Component } from "react"
import { View, StyleSheet, Text, Image } from "react-native"
import { LinearGradient } from "expo-linear-gradient"
import { MaterialCommunityIcons } from "@expo/vector-icons"

import { IMAGE } from "../../constants/Image"
import { WalletJson } from "../../constants/PageJson"

export default class QuickCard extends Component {
	render() {
		return (
			<View>
				<LinearGradient
					style={styles.linearGradient}
					colors={["rgba(0, 0, 0, 0.15)", "rgba(255, 255, 255, 0.1)", "rgba(255, 255, 255, 0.1)", "rgba(0, 0, 0, 0.15)"]}
				>
					{this.props.isWallet ? (
						<View>
							<Image style={styles.icon} source={IMAGE[this.props.icon]} resizeMode="contain" />
							<Text style={styles.body}>{this.props.item === WalletJson.BusinessWallet ? this.props.businessWallet : this.props.personalWallet}</Text>
						</View>
					) : (
						<MaterialCommunityIcons style={styles.iconStyle} name={this.props.icon} />
					)}
				</LinearGradient>
				<Text style={styles.titleStyle}>{this.props.title}</Text>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	linearGradient: {
		height: 70,
		width: 70,
		borderRadius: 35,
		marginHorizontal: 7,
		marginVertical: 5,
		justifyContent: "center"
		//alignItems: "center"
		//alignSelf: "center"
	},
	icon: {
		height: 23,
		width: 23,
		alignSelf: "center"
	},
	iconStyle: {
		fontSize: 40,
		color: "rgba(0, 0, 0, 0.5)",
		alignSelf: "center"
	},
	body: {
		marginTop: 8,
		textAlign: "center",
		fontWeight: "600"
	},
	titleStyle: {
		alignSelf: "center",
		fontSize: 11
	}
})
