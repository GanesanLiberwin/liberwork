import React, { Component } from "react"
import { View, Text, Modal, FlatList, StyleSheet, SafeAreaView, TouchableWithoutFeedback, TouchableOpacity } from "react-native"

import CountryDataJson from "./CountriesData"
import Colors from "../../constants/Colors"

// Default render of country flag
const defaultFlag = CountryDataJson.filter(obj => obj.name === "India")[0].flag

const defaultCountry = CountryDataJson.filter(obj => obj.name === "India")[0].name

export default class CountryPicker extends Component {
	constructor(props) {
		super(props)
		this.state = {
			modalVisible: false,
			countryName: defaultCountry,
			flag: defaultFlag
		}
	}

	showModal() {
		this.setState({ modalVisible: true })
	}
	hideModal() {
		this.setState({ modalVisible: false })
		// Refocus on the Input field after selecting the country code
		// this.refs.PhoneInput._root.focus();
	}

	async getCountry(country) {
		const countryData = await CountryDataJson
		try {
			const countryText = await countryData.filter(obj => obj.name === country)[0].name
			const countryFlag = await countryData.filter(obj => obj.name === country)[0].flag
			// Set data from user choice of country
			this.setState({ countryName: countryText, flag: countryFlag })
			this.props.onPickerSelected(countryText)
			await this.hideModal()
		} catch (err) {
			console.log(err)
		}
	}

	// Login screen design
	render() {
		//let { flag } = this.state
		const countryData = CountryDataJson

		return (
			<View style={{ ...styles.itemStyle, ...this.props.style }}>
				{/* country flag */}

				<TouchableOpacity onPress={() => this.showModal()}>
					<View style={{ flexDirection: "row", alignItems: "center", marginLeft: 10 }}>
						<Text style={{ fontSize: 20 }}>{this.state.flag}</Text>
						<Text style={styles.input}>{this.state.countryName}</Text>
					</View>
				</TouchableOpacity>

				{/* Modal for country code and flag */}
				<Modal
					animationType="slide" // fade
					transparent={false}
					visible={this.state.modalVisible}
				>
					<SafeAreaView style={{ flex: 1, margin: 20, marginTop: 30 }}>
						<View style={{ flex: 10 }}>
							<FlatList
								data={countryData}
								showsVerticalScrollIndicator={false}
								keyExtractor={(item, index) => index.toString()}
								renderItem={({ item }) => (
									<TouchableWithoutFeedback onPress={() => this.getCountry(item.name)}>
										<View
											style={[
												styles.countryStyle,
												{
													flexDirection: "row",
													alignItems: "center",
													justifyContent: "space-between"
												}
											]}
										>
											<Text style={{ fontSize: 14, color: "#01579b" }}>{item.name}</Text>
											<Text style={{ fontSize: 20 }}>{item.flag}</Text>
										</View>
									</TouchableWithoutFeedback>
								)}
							/>
						</View>
						<TouchableOpacity onPress={() => this.hideModal()} style={styles.closeButtonStyle}>
							<Text style={styles.textStyle}>CLOSE</Text>
						</TouchableOpacity>
					</SafeAreaView>
				</Modal>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	input: {
		marginLeft: 5,
		fontSize: 14,
		color: Colors.black
	},

	itemStyle: {
		width: "100%",
		justifyContent: "flex-end"
	},
	textStyle: {
		padding: 5,
		fontSize: 20,
		color: "#fff",
		fontWeight: "bold"
	},
	countryStyle: {
		flex: 1,
		backgroundColor: "#fafafa",
		borderTopColor: "#4fc3f7",
		borderTopWidth: 0.6,
		padding: 8
	},
	closeButtonStyle: {
		marginTop: "2%",
		borderRadius: 5,
		alignItems: "center",
		backgroundColor: Colors.blue2 //"#4fc3f7"
	}
})
