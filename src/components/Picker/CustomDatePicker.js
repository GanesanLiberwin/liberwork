import React, { useState } from "react"
import { View, Text, TouchableOpacity, Platform } from "react-native"
import DateTimePicker from "@react-native-community/datetimepicker"

import { DateToString } from "../../components/Utilities/DateFormat"
import Colors from "../../constants/Colors"
import Icons from "../Icons"

export const CustomDatePicker = props => {
	const [show, setShow] = useState(false)

	const showDatepicker = () => {
		setShow(!show)
	}

	const onChange = (event, selectedDate) => {
		const changedDate = selectedDate //|| props.displayDate
		setShow(Platform.OS === "ios")
		props.onDateChange(changedDate)
	}

	return (
		<View>
			<TouchableOpacity onPress={showDatepicker} style={{ flexDirection: "row", alignItems: "center" }}>
				<View style={{ width: "80%", alignItems: "flex-end" }}>
					<Text style={{}}> {DateToString(new Date(props.displayDate))} </Text>
				</View>
				<View style={{ width: "20%", alignItems: "flex-end" }}>
					<Icons name={"calendar-edit"} iconType="MaterialCommunityIcons" color={Colors.blue2} size={22} />
				</View>
			</TouchableOpacity>
			{show && (
				<DateTimePicker
					value={props.displayDate}
					onChange={onChange}
					mode={"date"}
					is24Hour={true}
					display={Platform.OS === "ios" ? "spinner" : "default"}
					minimumDate={props.minimumDate}
					maximumDate={props.maximumDate}
				/>
			)}
		</View>
	)
}

export default CustomDatePicker

/* IN IMPORT FILE

<CustomDatePicker  onDateChange={props.onDateChange}
 displayDate={props.displayDate}  minimumDate={props.minimumDate} maximumDate={props.maximumDate} />
 
 */
