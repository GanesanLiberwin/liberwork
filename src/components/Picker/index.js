import React from "react"
import { Text } from "react-native"

import DialCodePicker from "./DialCodePicker"
import CountryPicker from "./CountryPicker"

const Picker = props => {
  switch (props.pickerType) {
    case "DialCodePicker":
      return <DialCodePicker {...props} />
    case "CountryPicker":
      return <CountryPicker {...props} />
    default:
      return <Text>No Picker was passed via pickerType props</Text>
  }
}

export default Picker
