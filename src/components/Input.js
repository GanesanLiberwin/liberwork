import React, { useState } from "react"
import { View, Text, TextInput, StyleSheet, TouchableOpacity } from "react-native"

import Colors from "../constants/Colors"
import Icons from "../components/Icons"
import Picker from "../components/Picker"

const Input = props => {
	const [isHidden, setIsHidden] = useState(props.textHidden)

	return (
		<View style={styles.formControl}>
			{props.label && <Text style={styles.label}>{props.label}</Text>}
			<View style={{ flexDirection: "row", alignItems: "center" }}>
				{props.icon.name && <Icons name={props.icon.name} iconType={props.icon.type} color={props.icon.color} size={props.icon.size} />}
				{props.pickerWhenToEnable && props.pickerType && props.PickerEnable && (
					<Picker pickerType={props.pickerType} onPickerSelected={props.onPickerSelected}></Picker>
				)}
				{props.TextInputEnable && (
					<View style={styles.inputContainer}>
						<TextInput
							{...props}
							style={styles.input}
							placeholderTextColor={Colors.grey}
							allowFontScaling={false}
							editable={props.editable}
							secureTextEntry={isHidden}
							value={props.value}
							onChangeText={props.onChangeText}
							onBlur={props.onBlur}
						/>
						{props.securedTextIcon && (
							<TouchableOpacity
								onPress={() => {
									setIsHidden(prevState => !prevState)
								}}
							>
								<Icons name={isHidden ? "eye-slash" : "eye"} iconType="FontAwesome" color="rgba(0,0,0,0.7)" size={16} />
							</TouchableOpacity>
						)}
						{props.otpResend && (
							<TouchableOpacity>
								<Icons name={"repeat"} iconType="MaterialCommunityIcons" color="rgba(0,0,0,0.7)" size={18} />
							</TouchableOpacity>
						)}
					</View>
				)}
			</View>
			{props.errorText.length > 0 && (
				<View style={styles.errorContainer}>
					<Text style={styles.errorText}>{props.errorText}</Text>
				</View>
			)}
		</View>
	)
}

const styles = StyleSheet.create({
	formControl: {},
	label: {
		//fontFamily: 'open-sans-bold',
		//marginVertical: 8,
	},
	input: {
		paddingHorizontal: 5,
		paddingVertical: 6,
		width: "100%"
	},
	inputContainer: {
		flexDirection: "row",
		alignItems: "center",
		width: "85%",
		marginLeft: 10
	},
	errorContainer: {
		marginVertical: 5
	},
	errorText: {
		color: Colors.red,
		fontSize: 12
	}
})

export default Input
