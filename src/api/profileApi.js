import * as serviceUtil from "./apiServiceUtils"
import { envConfigs } from "./environmentConfigs"

export function isProfileExist(userId) {
  return serviceUtil.get(`${envConfigs.Profile.Service}/${envConfigs.Profile.Verify}/${userId}`)
}

export function createProfile(payload) {
  return serviceUtil.post(`${envConfigs.Profile.Service}`, JSON.stringify(payload))
}

export function getProfile(userId) {
  return serviceUtil.get(`${envConfigs.Profile.Service}/${userId}`)
}

export function getProfileSkills(userId) {
  ///dev/profile/1eaf28fc-e059-11ea-9baa-426abf0a4f5c/skill
  return serviceUtil.get(`${envConfigs.Profile.Service}/${userId}/${envConfigs.Profile.AddSkills}`)
}

export function addSkillToProfile(profileId, payload) {
  // /dev/profile/1eaf28fc-e059-11ea-9baa-426abf0a4f5c/skill
  return serviceUtil.post(`${envConfigs.Profile.Service}/${profileId}/${envConfigs.Profile.AddSkills}`, JSON.stringify(payload))
}
