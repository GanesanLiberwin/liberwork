import * as serviceUtil from "./apiServiceUtils"
import { envConfigs } from "./environmentConfigs"

export function isUserExist(payload) {
  return serviceUtil.post(`${envConfigs.User.Service}/${envConfigs.User.Signup}`, JSON.stringify(payload))
}

export function createUser(payload) {
  return serviceUtil.post(`${envConfigs.User.Service}/${envConfigs.User.Signup}`, JSON.stringify(payload))
}

export function getUser(payload) {
  return serviceUtil.post(`${envConfigs.User.Service}/${envConfigs.User.Signin}`, JSON.stringify(payload))
}
