import * as serviceUtil from "./apiServiceUtils"
import { envConfigs } from "./environmentConfigs"

export function getBalance(accountId) {
  return serviceUtil.get(`${envConfigs.Wallet.Service}/account/${accountId}`)
}

export function checkBalance(walletId) {
  return serviceUtil.get(`${envConfigs.Wallet.Service}/${walletId}`)
}

export function getTransaction(walletId) {
  // https://he7t9odb7l.execute-api.us-west-2.amazonaws.com/dev/wallet/CTS_1eaf28fc-e059-11ea-9baa-426abf0a4f5c_WB/transaction
  return serviceUtil.get(`${envConfigs.Wallet.Service}/${walletId}/${envConfigs.Wallet.Transaction}`)
}

export function manageWallet(payload) {
  return serviceUtil.post(`${envConfigs.Wallet.Service}/${envConfigs.Wallet.Manage}`, JSON.stringify(payload))
}

export function transfer(payload) {
  // /dev/wallet/transfer
  return serviceUtil.post(`${envConfigs.Wallet.Service}/${envConfigs.Wallet.Transfer}`, JSON.stringify(payload))
}
