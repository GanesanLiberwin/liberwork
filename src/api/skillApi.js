import * as serviceUtil from "./apiServiceUtils"
import { envConfigs } from "./environmentConfigs"

export function addSkillToProfile(payload) {
  return serviceUtil.post(`${envConfigs.Skills.Service}/`, JSON.stringify(payload))
}

export function searchSkill(searchText) {
  // /dev/skill/search?term=java
  return serviceUtil.get(`${envConfigs.Skills.Service}/${envConfigs.Skills.Search}?term=${searchText}`)
}

export function addSkillsToSkillMaster(payload) {
  return serviceUtil.post(`${envConfigs.Skills.Service}`, JSON.stringify(payload))
}
