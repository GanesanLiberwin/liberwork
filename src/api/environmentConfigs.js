const envConfigs = {
	SERVER_DOMAIN: "https://liberage.com/",
	isEnableLog: true,
	Account: {
		Service: "account",
		Verify: "exist"
	},
	Message: {
		Service: "message"
	},
	Profile: {
		Service: "profile",
		Verify: "verify",
		AddSkills: "skill"
	},
	Skills: {
		Service: "skill",
		Search: "search"
	},
	User: {
		Service: "user",
		Signin: "signin",
		Signup: "signup"
	},
	Wallet: {
		Service: "wallet",
		Balance: "balance",
		Manage: "manageWallet",
		Transaction: "transaction",
		Transfer: "transfer"
	},
	Work: {
		Service: "work",
		Details: "workdetails",
		Apply: "apply",
		Eligible: "eligible",
		Feedback: "feedback",
		Cancel: "cancel",
		Closed: "closed",
		Submit: "submission"
	},
	WorkToSkill: {
		Service: "work2skill"
	},
	Rewards: {
		Service: "reward",
		Model: "model"
	}
}

export { envConfigs }
