import { envConfigs } from "./environmentConfigs"

const SERVER_DOMAIN = envConfigs.SERVER_DOMAIN
const isEnableLog = envConfigs.isEnableLog

export function handleApiError(errMessage, path, data) {
  if (isEnableLog) {
    console.log("Api Url: ", `${SERVER_DOMAIN}${path}`)
    if (data) {
      console.log("Api Data: ", JSON.stringify(data))
    }
    console.log("Error Message: ", errMessage)
  }
}
