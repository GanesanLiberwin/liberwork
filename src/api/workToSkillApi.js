import * as serviceUtil from "./apiServiceUtils"
import { envConfigs } from "./environmentConfigs"

export function checkMatchingCount(payload) {
  // { accountId: accountId, orgId: orgId, skills: skills, count: true }
  return serviceUtil.post(`${envConfigs.WorkToSkill.Service}`, JSON.stringify(payload))
}
