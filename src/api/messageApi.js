import * as serviceUtil from "./apiServiceUtils"
import { envConfigs } from "./environmentConfigs"

export function postMessage(payload) {
  return serviceUtil.post(`${envConfigs.Message.Service}`, JSON.stringify(payload))
}

export function getMessages(workId) {
  return serviceUtil.get(`${envConfigs.Message.Service}/${workId}`)
}
