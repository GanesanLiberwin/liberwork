import * as serviceUtil from "./apiServiceUtils"
import { envConfigs } from "./environmentConfigs"

export function isAccountExist(profileId) {
	let payload = { profile: profileId }
	return serviceUtil.getWithParams(`${envConfigs.Account.Service}/${envConfigs.Account.Verify}`, payload)
}

export function isAccountAvailable(emailId) {
	let payload = { email: emailId }
	return serviceUtil.getWithParams(`${envConfigs.Account.Service}/${envConfigs.Account.Verify}`, payload)
}

export function isValidDomainEmail(emailId, orgId) {
	// /dev/account/exist?email=vijay@ibm.com&org=CTS
	let payload = { email: emailId, org: orgId }
	return serviceUtil.getWithParams(`${envConfigs.Account.Service}/${envConfigs.Account.Verify}`, payload)
}

export function createAccount(payload) {
	return serviceUtil.post(`${envConfigs.Account.Service}`, JSON.stringify(payload))
}

export function updateAccount(payload, accountId) {
	return serviceUtil.put(`${envConfigs.Account.Service}/${accountId}`, JSON.stringify(payload))
}

export function getAccount(accountId) {
	return serviceUtil.get(`${envConfigs.Account.Service}/${accountId}`)
}
