import * as serviceUtil from "./apiServiceUtils"
import { envConfigs } from "./environmentConfigs"

export function getReward(modelId) {
  // dev/reward/model/M2
  return serviceUtil.get(`${envConfigs.Rewards.Service}/${envConfigs.Rewards.Model}/${modelId}`)
}

export function getAllReward() {
  // dev/reward/model/
  return serviceUtil.get(`${envConfigs.Rewards.Service}/${envConfigs.Rewards.Model}/`)
}
