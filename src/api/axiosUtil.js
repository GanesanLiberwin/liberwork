import axios from "axios"
import AsyncStorage from "@react-native-async-storage/async-storage"

import { envConfigs } from "./environmentConfigs"

const SERVER_DOMAIN = envConfigs.SERVER_DOMAIN

export default axiosInstance = axios.create({ baseURL: SERVER_DOMAIN })

// Skip for few Apis
const isAuthCheckNeeded = apiUrl => {
	try {
		const lstDisableApis = [`${envConfigs.User.Service}/${envConfigs.User.Signin}`, `${envConfigs.User.Service}/${envConfigs.User.Signup}`]

		for (let index = 0; index < lstDisableApis.length; index++) {
			if (apiUrl.indexOf(lstDisableApis[index]) > -1) {
				return true
			}
		}
		return false
	} catch (error) {
		return false
	}
}

const isUserAvailable = async () => {
	try {
		const value = await AsyncStorage.getItem("userData")
		if (value !== null) {
			const userData = JSON.parse(value)
			if (userData != null && userData.accesstoken) {
				return userData
			} else {
				return null
			}
		}
	} catch (error) {
		return null
	}
}

axiosInstance.interceptors.request.use(
	async config => {
		const conf = config
		if (!isAuthCheckNeeded(conf.url)) {
			const userData = await isUserAvailable()

			if (userData != null) {
				if (userData.accesstoken) {
					config.headers.Authorization = `Bearer ${userData.accesstoken}`
				}
				if (userData.userId) {
					config.headers["User-Id"] = userData.userId
				}
			}
		}
		//return the request configurations
		return conf
	},
	error => Promise.reject(error)
)
