import * as serviceUtil from "./apiServiceUtils"
import { envConfigs } from "./environmentConfigs"

export function createWork(payload) {
	return serviceUtil.post(`${envConfigs.Work.Service}`, JSON.stringify(payload))
}

export function updateWork(payload, workId) {
	return serviceUtil.put(`${envConfigs.Work.Service}/${workId}`, JSON.stringify(payload))
}

export function updateWorkStatus(payload, workId) {
	return serviceUtil.put(`${envConfigs.Work.Service}/${workId}`, JSON.stringify(payload))
}

export function getWorkByWorkId(payload) {
	return serviceUtil.put(`${envConfigs.Work.Service}/${envConfigs.Work.Details}`, JSON.stringify(payload))
}

export function getAvailableWorks(payload) {
	// work/workdetails
	// payload : { accountId: accountId }
	return serviceUtil.put(`${envConfigs.Work.Service}/${envConfigs.Work.Details}`, JSON.stringify(payload))
}

export function getEligibleWorks(accountId) {
	// work/eligible?accountId=accountid
	let payload = { accountId: accountId }
	return serviceUtil.getWithParams(`${envConfigs.Work.Service}/${envConfigs.Work.Eligible}`, payload)
}

export function getClosedWorks(accountId) {
	// dev/work/closed?accountId=accountid
	let payload = { accountId: accountId }
	return serviceUtil.getWithParams(`${envConfigs.Work.Service}/${envConfigs.Work.Closed}`, payload)
}

export function applyWork(payload) {
	// work/apply
	//"action": "apply"  //"action": "decline"
	//{ "accountId": "", "workId": "",  "action": "apply", "allocationStatus": "Applied", "comment": "test" }
	return serviceUtil.post(`${envConfigs.Work.Service}/${envConfigs.Work.Apply}`, JSON.stringify(payload))
}

export function submitWork(payload) {
	// /work/submission

	// {
	//   "workId": "",
	//   "accountId": "",
	//   "status": "submitted",
	//   "submissions": { "description": "submitted1", "attachment": ["d", "dd"] }
	// }

	return serviceUtil.post(`${envConfigs.Work.Service}/${envConfigs.Work.Submit}`, JSON.stringify(payload))
}

export function getSubmissionData(workId) {
	// dev/work/submission/{workId}
	return serviceUtil.get(`${envConfigs.Work.Service}/${envConfigs.Work.Submit}/${workId}`)
}

export function cancelWork(payload) {
	// work/apply
	//"action": "apply"  //"action": "decline"
	//{ "accountId": "", "workId": "",  "role": "poster","rewardUnits": 500}
	return serviceUtil.post(`${envConfigs.Work.Service}/${envConfigs.Work.Cancel}`, JSON.stringify(payload))
}

export function addFeedback(payload) {
	// /work/feedback/
	// { "accountId": "", "workId": "", "role": "poster", "rating": "4", "feedback": "good", "feedbackStage": "S" }
	return serviceUtil.post(`${envConfigs.Work.Service}/${envConfigs.Work.Feedback}`, JSON.stringify(payload))
}

// using workId and accountId. This will return single feedback item.
export function getFeedbackByAccount(workId, accountId) {
	//work/feedback?accountId=&workId=
	let payload = { accountId: accountId, workId: workId }
	return serviceUtil.getWithParams(`${envConfigs.Work.Service}/${envConfigs.Work.Feedback}`, payload)
}

// using workId alone. This will return all the feedback given for that work.
export function getFeedbackAll(workId) {
	//work/feedback?workId=
	let payload = { workId: workId }
	return serviceUtil.getWithParams(`${envConfigs.Work.Service}/${envConfigs.Work.Feedback}`, payload)
}

export function deleteWork(workId) {
	return serviceUtil.get(`${envConfigs.Work.Service}/${workId}`)
}
