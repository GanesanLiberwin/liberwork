import axiosInstance from "./axiosUtil"
import { handleApiError } from "./apiErrorLogUtils"

// const SERVER_DOMAIN = envConfigs.SERVER_DOMAIN
// const isEnableLog = envConfigs.isEnableLog

const getHeaders = () => {
	return {
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json"
		}
	}
}

// HTTP GET Request - Returns Resolved or Rejected Promise
export const get = path => {
	return new Promise((resolve, reject) => {
		axiosInstance
			.get(`${path}`, getHeaders())
			.then(response => {
				resolve(response.data)
			})
			.catch(error => {
				const errMessage = error.response && error.response.data && error.response.data.errMessage ? error.response.data.errMessage : error
				handleApiError(errMessage, `${path}`)
				reject(errMessage)
				// reject(error)
			})
	})
}

// HTTP GET Request - Returns Resolved or Rejected Promise | {  params: { ID: 12345 } }
export const getWithParams = (path, paramsData) => {
	return new Promise((resolve, reject) => {
		axiosInstance
			.get(`${path}`, { params: paramsData }, getHeaders())
			.then(response => {
				resolve(response.data)
			})
			.catch(error => {
				const errMessage = error.response && error.response.data && error.response.data.errMessage ? error.response.data.errMessage : error
				handleApiError(errMessage, `${path}`, paramsData)
				reject(errMessage)
				// reject(error)
			})
	})
}

// HTTP PATCH Request - Returns Resolved or Rejected Promise
export const patch = (path, data) => {
	return new Promise((resolve, reject) => {
		axiosInstance
			.patch(`${path}`, data, getHeaders())
			.then(response => {
				resolve(response.data)
			})
			.catch(error => {
				const errMessage = error.response && error.response.data && error.response.data.errMessage ? error.response.data.errMessage : error
				handleApiError(errMessage, `${path}`, data)
				reject(errMessage)
				// reject(error)
			})
	})
}

// HTTP POST Request - Returns Resolved or Rejected Promise
export const post = (path, data) => {
	return new Promise((resolve, reject) => {
		axiosInstance
			.post(`${path}`, data, getHeaders())
			.then(response => {
				resolve(response.data)
			})
			.catch(error => {
				const errMessage = error.response && error.response.data && error.response.data.errMessage ? error.response.data.errMessage : error
				handleApiError(errMessage, `${path}`, data)

				reject(errMessage)
				// reject(error)
			})
	})
}

// HTTP PUT Request - Returns Resolved or Rejected Promise
export const put = (path, data) => {
	return new Promise((resolve, reject) => {
		axiosInstance
			.put(`${path}`, data, getHeaders())
			.then(response => {
				resolve(response.data)
			})
			.catch(error => {
				const errMessage = error.response && error.response.data && error.response.data.errMessage ? error.response.data.errMessage : error
				handleApiError(errMessage, `${path}`, data)

				reject(errMessage)
				// reject(error)
			})
	})
}

// HTTP DELETE Request - Returns Resolved or Rejected Promise
export const del = path => {
	return new Promise((resolve, reject) => {
		axiosInstance
			.delete(`${path}`, getHeaders())
			.then(response => {
				resolve(response.data)
			})
			.catch(error => {
				const errMessage = error.response && error.response.data && error.response.data.errMessage ? error.response.data.errMessage : error
				handleApiError(errMessage, `${path}`)

				reject(errMessage)
				// reject(error)
			})
	})
}

// HTTP DELETE Request - Returns Resolved or Rejected Promise
export const delWithParams = (path, paramsData) => {
	return new Promise((resolve, reject) => {
		axiosInstance
			.delete(`${path}`, { params: paramsData }, getHeaders())
			.then(response => {
				resolve(response.data)
			})
			.catch(error => {
				const errMessage = error.response && error.response.data && error.response.data.errMessage ? error.response.data.errMessage : error
				handleApiError(errMessage, `${path}`, paramsData)

				reject(errMessage)
				// reject(error)
			})
	})
}
