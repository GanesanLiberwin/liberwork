const NODE_ENV = "dev"
// if (process.env.NODE_ENV === "production")
if (NODE_ENV === "production") {
  module.exports = require("./configureStore-prod")
} else {
  module.exports = require("./configureStore-dev")
}
