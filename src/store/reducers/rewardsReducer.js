import * as types from "../actions/actionTypes"
import initialState from "./initialState"

export default (state = initialState.rewardModels, action) => {
  switch (action.type) {
    case types.LOAD_ALL_REWARDS:
    case types.LOAD_ALL_REWARDS_SUCCESS:
      return { ...state, lstModels: action.lstModels }

    case types.LOAD_REWARD_MODEL:
    case types.LOAD_REWARD_MODEL_SUCCESS:
      return { ...state, ModelData: action.rewardModel }

    case types.CLEAR_REWARD_MODELS:
      return {
        lstModels: [],
        ModelData: {}
      }

    default:
      return state
  }
}
