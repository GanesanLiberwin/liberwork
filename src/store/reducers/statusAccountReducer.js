import * as types from "../actions/actionTypes"
import initialState from "./initialState"

export default function statusAccountReducer(state = initialState.isAccount, action) {
  if (action.type == types.STATUS_ACCOUNT) {
    return action.isAccount
  }
  return state
}
