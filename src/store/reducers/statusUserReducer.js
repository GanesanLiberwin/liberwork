import * as types from "../actions/actionTypes"
import initialState from "./initialState"

export default function statusUserReducer(state = initialState.isUser, action) {
  if (action.type == types.STATUS_USER) {
    return action.isUser
  }
  return state
}
