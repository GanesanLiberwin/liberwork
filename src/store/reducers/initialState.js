import Quicks from "../../constants/Quicks"

export default {
	isUser: false,
	isProfile: false,
	isAccount: false,
	apiCallsInProgress: 0,
	auth: {
		accesstoken: null,
		userId: null,
		userName: "",
		userNameType: "",
		isAutoLoginAttempt: false
	},
	profile: {
		id: "",
		firstName: "",
		lastName: "",
		dob: "",
		nationality: "",
		gender: "",
		personalEmail: "",
		mobileNumber: "",
		lstProfileSkills: []
	},
	account: {
		selectedAccountId: "",
		lstAccounts: [],
		accountDetails: {
			id: "",
			profileId: "",
			firstName: "",
			lastName: "",
			orgId: "",
			accountStatus: "",
			orgUnitId: "",
			workerType: "",
			orgPersonId: "",
			accountEmail: "",
			supervisorEmail: "",
			orgPersonRole: "",
			personLocation: ""
		},
		accountVerified: {
			isValid: false,
			accDetails: {
				id: "",
				profileId: "",
				firstName: "",
				lastName: "",
				orgId: "",
				accountStatus: "",
				orgUnitId: "",
				workerType: "",
				orgPersonId: "",
				accountEmail: "",
				supervisorEmail: "",
				orgPersonRole: "",
				personLocation: ""
			}
		}
	},

	orgDetail: {
		orgId: "",
		orgName: "",
		orgType: "",
		orgUnits: [],
		validity: ""
	},
	wallets: {
		accountId: "",
		currency: "",
		createdAt: "",
		BusinessWallet: {
			id: "",
			balance: 0,
			reservedAmount: 0,
			trxHistory: []
		},
		PersonalWallet: {
			id: "",
			balance: 0,
			reservedAmount: 0,
			trxHistory: []
		},
		trxHistory: []
	},
	quicks: {
		availableQuicks: Quicks,
		userQuicks: Quicks.filter(q => q.preferred === "yes")
	},
	search: {
		lstSkills: [],
		lstCategory: [],
		lstIndustry: [],
		lstSearchedSkills: [],
		lstFetchSkills: []
	},
	works: {
		selectedDraftId: 0,
		lstDrafts: [],
		lstDraftWorksCard: [],
		lstDraftWorksDetails: [],
		skillsLimit: 3,
		lstSelectedSkills: [],
		lstAvailableWorks: [],
		lstEligibleWorks: [],
		closedWorks: {
			postedWorks: [],
			delegatedWorks: [],
			allocatedWorks: []
		}
	},
	workFlow: {
		workId: "",
		workSource: "",
		workRole: "",
		workDetails: {
			// for Card Starts
			// skills: skills.length > 0 ? [skills[0].skill] : [""],
			// icon: null,
			// iconStatus: false,
			// prefered: "yes",
			// for Card Ends
			workId: "",
			accountId: "",
			orgId: "",
			//Type
			workforceType: "",
			beginnerAllowed: false,
			workType: "",
			functionalDomain: "",
			functionalExperience: "0",
			selectionModel: "",
			remoteWorking: true,
			workLocation: "",
			// Skills
			skills: [],
			industryDomain: "",
			industryExperience: "0",
			//Work Description
			description: "",
			estimatedEfforts: "",
			plannedStartDate: 0,
			plannedEndDate: 0,
			criticalWork: false,
			outcome: "",
			attachments: [],
			//Reward
			rewardUnits: "",
			rewardType: "points",
			rewardModel: "",
			//delegate
			delegatePermission: "",
			delegateAccountId: "None",
			// Status
			currentStatus: "",
			biddingEndDateTime: 0,
			//
			updatedAt: 0,
			postedDate: 0,
			workforceCount: 0,
			statusTransition: [
				{
					date: 0,
					changedBy: "",
					status: ""
				}
			],
			currentStatusDate: 0
		},
		Bidding: {
			allocationStatus: "",
			comment: ""
		},
		Communication: {
			messages: [],
			lastReadMessage: ""
		},
		Submission: {
			submission: "",
			attachments: []
		},
		Closure: {
			posterRating: 0,
			posterRatingText: "",
			posterFeedbackList: [],
			workerRating: 0,
			workerRatingText: "",
			workerFeedbackList: [],
			rewardUnitsFinal: 0
		},
		lstFeedbacks: [],
		lstSubmission: [],
		workDraft: {
			targetId: 0,
			tabProgressId: 0,
			workForceValid: false,
			workTypeValid: false,
			categoryValid: false,
			selectionModelValid: false,
			locationValid: true,
			skillValid: false,
			industryValid: false,
			descriptionValid: false,
			outcomeValid: false,
			effortValid: false,
			scheduleValid: false,
			rewardValid: false,
			rewardModelValid: false,
			workStartDate: new Date(),
			workStartMinDate: new Date(),
			workStartMaxDate: new Date(),
			workEndDate: new Date(),
			workEndMinDate: new Date(),
			workEndMaxDate: new Date(),
			differenceInDays: 0,
			profileCount: "",
			effortStatus: "",
			scheduleStatus: "",
			rewardStatus: "",
			rewardRate: ""
		}
	},
	rewardModels: {
		lstModels: [],
		ModelData: {}
	}
}
