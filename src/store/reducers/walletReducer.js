import * as types from "../actions/actionTypes"
import initialState from "./initialState"

export default (state = initialState.wallets, action) => {
  switch (action.type) {
    case types.CLEAR_WALLET_DATA:
      return {
        ...state,
        accountId: "",
        currency: "",
        createdAt: "",
        BusinessWallet: {},
        PersonalWallet: {},
        trxHistory: []
      }
    case types.LOAD_BALANCE_SUCCESS:
    case types.LOAD_BALANCE:
      return {
        ...state,
        accountId: action.walletDetail.accountId,
        currency: action.walletDetail.currency,
        createdAt: action.walletDetail.createdAt,
        BusinessWallet: action.walletDetail.BusinessWallet,
        PersonalWallet: action.walletDetail.PersonalWallet
      }

    case types.LOAD_ALL_TRANSACTION:
    case types.LOAD_ALL_TRANSACTION_SUCCESS:
      return {
        ...state,
        trxHistory: action.trxHistory
      }

    case types.CLEAR_ALL_TRANSACTION:
      return {
        ...state,
        trxHistory: []
      }

    default:
      return state
  }
}
