import * as types from "../actions/actionTypes"
import initialState from "./initialState"

export default (state = initialState.account, action) => {
  switch (action.type) {
    case types.CREATE_ACCOUNT_SUCCESS:
      let lstAccountsUpdated = state.lstAccounts.concat(action.account)
      return {
        ...state,
        accountDetails: action.account,
        selectedAccountId: action.account.id,
        lstAccounts: lstAccountsUpdated
      }
    case types.CREATE_ACCOUNT_VERIFIED_SUCCESS:
      let verifiedAccountDetails = {
        isValid: action.account.isValid,
        accDetails: action.account.accounts[0]
      }
      return {
        ...state,
        accountVerified: verifiedAccountDetails
      }
    case types.UPDATE_ACCOUNT_VERIFIED_DATA:
    case types.UPDATE_ACCOUNT_VERIFIED_DATA_SUCCESS:
      let updateAccountDetails = {
        isValid: action.isValid,
        accDetails: action.account
      }
      return {
        ...state,
        accountVerified: updateAccountDetails
      }

    case types.LOAD_ACCOUNT_SUCCESS:
    case types.LOAD_ACCOUNT_DATA:
      return {
        ...state,
        accountDetails: action.account,
        selectedAccountId: action.account.id
      }
    case types.ACCOUNT_ID_UPDATE:
      return {
        ...state,
        selectedAccountId: action.AccountId
      }
    case types.LOAD_ACCOUNT_EXIST_SUCCESS:
    case types.LOAD_EXISTING_ACCOUNT:
      //lstAccounts: [...state.lstAccounts, action.lstAccounts],
      let lstAccountsNew = state.lstAccounts.concat(action.lstAccounts)
      return {
        ...state,
        lstAccounts: lstAccountsNew,
        selectedAccountId: action.accountId
      }

    case types.CLEAR_ACCOUNT_VERIFIED_DATA:
      return {
        ...state,
        accountVerified: {
          isValid: false,
          accDetails: {
            id: "",
            profileId: "",
            firstName: "",
            lastName: "",
            orgId: "",
            accountStatus: "",
            orgUnitId: "",
            workerType: "",
            orgPersonId: "",
            accountEmail: "",
            supervisorEmail: "",
            orgPersonRole: "",
            personLocation: ""
          }
        }
      }

    case types.CLEAR_ACCOUNT_DATA:
      return {
        ...state,
        selectedAccountId: "",
        lstAccounts: [],
        accountDetails: {
          id: "",
          profileId: "",
          firstName: "",
          lastName: "",
          orgId: "",
          accountStatus: "",
          orgUnitId: "",
          workerType: "",
          orgPersonId: "",
          accountEmail: "",
          supervisorEmail: "",
          orgPersonRole: "",
          personLocation: ""
        },
        accountVerified: {
          isValid: false,
          accDetails: {
            id: "",
            profileId: "",
            firstName: "",
            lastName: "",
            orgId: "",
            accountStatus: "",
            orgUnitId: "",
            workerType: "",
            orgPersonId: "",
            accountEmail: "",
            supervisorEmail: "",
            orgPersonRole: "",
            personLocation: ""
          }
        }
      }
    default:
      return state
  }
}
