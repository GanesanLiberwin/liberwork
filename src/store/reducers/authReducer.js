import * as types from "../actions/actionTypes"
import initialState from "./initialState"

export default (state = initialState.auth, action) => {
  switch (action.type) {
    case types.CREATE_USER_SUCCESS:
    case types.LOAD_USER_SUCCESS:
    case types.LOAD_USER_DATA:
      return {
        userId: action.userDetails.userId,
        userName: action.userDetails.userName,
        userNameType: action.userDetails.userNameType,
        accesstoken: action.userDetails.accesstoken,
        isAutoLoginAttempt: true
      }

    case types.CLEAR_USER_DATA:
      return {
        accesstoken: "",
        userId: "",
        userName: "",
        userNameType: "",
        isAutoLoginAttempt: true
      }
    case types.LOGOUT:
    case types.SET_AUTO_LOGIN:
      return { ...state, isAutoLoginAttempt: true }
    default:
      return state
  }
}
