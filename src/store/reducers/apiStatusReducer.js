import * as types from "../actions/actionTypes"
import initialState from "./initialState"

function actionTypeEndsInSuccess(type) {
  return type.substring(type.length - 8) === "_SUCCESS"
}

export default function apiCallStatusReducer(state = initialState.apiCallsInProgress, action) {
  // By convention, actions that end in "_SUCCESS" are assumed to have been the result of a completed API call.
  // apiCallsInProgress is reduced by 1 on "_SUCCESS" actions
  // apiStatusActions.beginApiCall() => apiCallsInProgress + 1
  // apiStatusActions.apiCallError() => apiCallsInProgress - 1
  // apiStatusActions.apiCallSuccess() => apiCallsInProgress - 1 // on few case we are not dispatch any action

  if (action.type == types.API_CALL_STARTS) {
    return state + 1
  } else if (action.type === types.API_CALL_ERROR || action.type === types.API_CALL_ENDS) {
    return state - 1
  } else if (actionTypeEndsInSuccess(action.type)) {
    return state - 1
  } else if (action.type === types.CLEAR_API_COUNT) {
    return 0
  }
  return state
}
