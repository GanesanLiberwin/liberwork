import * as types from "../actions/actionTypes"
import initialState from "./initialState"

export default function statusProfileReducer(state = initialState.isProfile, action) {
  if (action.type == types.STATUS_PROFILE) {
    return action.isProfile
  }
  return state
}
