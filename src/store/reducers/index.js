import { combineReducers } from "redux"

import apiCallsInProgress from "./apiStatusReducer"
import isUser from "./statusUserReducer"
import isProfile from "./statusProfileReducer"
import isAccount from "./statusAccountReducer"

import authReducer from "./authReducer"
import accountReducer from "./accountReducer"
import profileReducer from "./profileReducer"
import orgDetailReducer from "./orgDetailReducer"

import quicksReducer from "./quicksReducer"
import worksReducer from "./worksReducer"
import worksFlowReducer from "./worksFlowReducer"

import walletReducer from "./walletReducer"
import searchReducer from "./searchReducer"
import rewardsReducer from "./rewardsReducer"

const rootReducer = combineReducers({
  isUser,
  isAccount,
  isProfile,
  apiCallsInProgress,
  auth: authReducer,
  account: accountReducer,
  quicks: quicksReducer,
  works: worksReducer,
  workFlow: worksFlowReducer,
  profile: profileReducer,
  wallets: walletReducer,
  search: searchReducer,
  orgDetail: orgDetailReducer,
  rewardModels: rewardsReducer
})

export default rootReducer
