import * as types from "../actions/actionTypes"
import initialState from "./initialState"

export default (state = initialState.profile, action) => {
  switch (action.type) {
    case types.CREATE_PROFILE_SUCCESS:
    case types.LOAD_PROFILE_SUCCESS:
    case types.LOAD_PROFILE_DATA:
      return {
        ...state,
        id: action.profile.id,
        firstName: action.profile.firstName,
        lastName: action.profile.lastName,
        dob: action.profile.dob,
        nationality: action.profile.nationality,
        gender: action.profile.gender,
        personalEmail: action.profile.personalEmail,
        mobileNumber: action.profile.mobileNumber
      }

    case types.LOAD_PROFILE_SKILLS:
    case types.LOAD_PROFILE_SKILLS_SUCCESS:
      return { ...state, lstProfileSkills: action.lstSkills }

    case types.ADD_PROFILE_SKILLS_SUCCESS:
      let lstSkillsNew = state.lstProfileSkills.concat(action.lstSkills)
      return {
        ...state,
        lstProfileSkills: lstSkillsNew
      }

    case types.LOAD_PROFILE_EXIST_SUCCESS:
      return { ...state, firstName: action.firstName, lastName: action.lastName }

    case types.CLEAR_PROFILE_DATA:
      return {
        id: "",
        firstName: "",
        lastName: "",
        dob: "",
        nationality: "",
        gender: "",
        personalEmail: "",
        mobileNumber: "",
        lstProfileSkills: []
      }

    default:
      return state
  }
}
