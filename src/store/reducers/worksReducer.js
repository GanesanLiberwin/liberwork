import * as types from "../actions/actionTypes"
import initialState from "./initialState"
import { workStatusJson } from "../../constants/PageJson"

export default (state = initialState.works, action) => {
  switch (action.type) {
    case types.LOAD_AVAILABLE_WORKS:
    case types.LOAD_AVAILABLE_WORKS_SUCCESS:
      return {
        ...state,
        lstAvailableWorks: action.lstWorks
      }

    case types.LOAD_ELIGIBLE_WORKS_SUCCESS:
    case types.LOAD_ELIGIBLE_WORKS:
      return {
        ...state,
        lstEligibleWorks: action.lstWorks
      }

    case types.LOAD_CLOSED_WORKS_SUCCESS:
    case types.LOAD_CLOSED_WORKS:
      return {
        ...state,
        closedWorks: action.lstWorks
      }

    case types.LOAD_MYPOSTWORKCARD_DATA:
      return {
        ...state,
        lstAvailableWorks: state.lstAvailableWorks.concat(action.MyPostWorkCard)
      }

    case types.LOAD_DRAFTWORK_DATA:
      return {
        ...state,
        lstDraftWorksCard: state.lstDraftWorksCard.concat(action.draftWorkCard),
        lstDraftWorksDetails: state.lstDraftWorksDetails.concat(action.draftWork)
      }

    case types.LOAD_DRAFT_DATA:
      let lstDraftWorkDetails = []
      return {
        ...state,
        lstDraftWorksDetails: lstDraftWorkDetails.concat(action.draftWork)
      }

    case types.LOAD_DRAFTCARD_DATA:
      let lstDraftWorksCards = []
      return {
        ...state,
        lstDraftWorksCard: lstDraftWorksCards.concat(action.draftWorkCard)
      }

    case types.UPDATE_DRAFTWORK_DATA:
      let listDraftWorks = [...state.lstDraftWorksDetails]
      listDraftWorks[action.workIndex] = action.draftWork
      let listDraftWorksCard = [...state.lstDraftWorksCard]
      listDraftWorksCard[action.cardIndex] = action.draftWorkCard
      return {
        ...state,
        lstDraftWorksCard: listDraftWorksCard,
        lstDraftWorksDetails: listDraftWorks
      }

    case types.DELETE_DRAFTWORK_DATA:
      return {
        ...state,
        lstDraftWorksCard: state.lstDraftWorksCard.filter(item => item.workId !== action.draftId),
        lstDraftWorksDetails: state.lstDraftWorksDetails.filter(item => item.workDraftId !== action.draftId)
      }

    case types.UPDATE_SELECTED_DRAFT_ID:
      return {
        ...state,
        selectedDraftId: action.draftId
      }

    case types.CLEAR_WORKS_DATA:
      return {
        ...state,
        selectedDraftId: 0,
        lstDrafts: [],
        lstDraftWorksCard: [],
        lstDraftWorksDetails: [],
        skillsLimit: 3,
        lstSelectedSkills: [],
        lstAvailableWorks: [],
        lstEligibleWorks: [],
        closedWorks: {
          postedWorks: [],
          delegatedWorks: [],
          allocatedWorks: []
        }
        // availableWorks: []
        // postWorks: [],
        // myWorks: [],
        // preferedWorks: [],
        // recommendedWorks: []
      }

    case types.WORK_APPLY:
      const applyIndex = state.lstEligibleWorks.findIndex(w => w.workId === action.workId)
      let lstWorksApply = [...state.lstEligibleWorks]
      if (applyIndex > -1) {
        lstWorksApply[applyIndex].allocationStatus = "Applied"
        return {
          ...state,
          lstEligibleWorks: lstWorksApply
        }
      } else {
        return { state }
      }

    case types.WORK_DECLINE:
      const declineWorkIndex = state.lstEligibleWorks.findIndex(w => w.workId === action.workId)
      if (declineWorkIndex > -1) {
        return {
          ...state,
          lstEligibleWorks: state.lstEligibleWorks.filter(w => w.workId !== action.workId)
        }
      } else {
        return { state }
      }

    case types.UPDATE_WORK_STATUS:
      if (action.workStatus === workStatusJson.Inprogress) {
        const eligibleWorksStatusIndex = state.lstEligibleWorks.findIndex(w => w.workId === action.workId)
        let lstEligibleWorksStatus = [...state.lstEligibleWorks]
        if (eligibleWorksStatusIndex > -1) {
          lstEligibleWorksStatus[eligibleWorksStatusIndex].currentStatus = action.workStatus
          return {
            ...state,
            lstEligibleWorks: lstEligibleWorksStatus
          }
        } else {
          return { state }
        }
      } else if (action.workStatus === workStatusJson.Rework || action.workStatus === workStatusJson.Completed) {
        const availableWorkStatusIndex = state.lstAvailableWorks.findIndex(w => w.workId === action.workId)
        let lstAvailableWorksStatus = [...state.lstAvailableWorks]
        if (availableWorkStatusIndex > -1) {
          lstAvailableWorksStatus[availableWorkStatusIndex].currentStatus = action.workStatus
          return {
            ...state,
            lstAvailableWorks: lstAvailableWorksStatus
          }
        } else {
          return { state }
        }
      } else {
        return { state }
      }

    case types.WORK_CANCEL:
      if (action.workRole === "Poster" || action.workRole === "Del-O") {
        const cancelAvailableIndex = state.lstAvailableWorks.findIndex(w => w.workId === action.workId)
        let lstWorksAvailableCancel = [...state.lstAvailableWorks]
        if (cancelAvailableIndex > -1) {
          lstWorksAvailableCancel[cancelAvailableIndex].currentStatus = workStatusJson.Cancelling
          return {
            ...state,
            lstAvailableWorks: lstWorksAvailableCancel
          }
        } else {
          return { state }
        }
      } else if (action.workRole === "Worker") {
        const cancelEligibleIndex = state.lstEligibleWorks.findIndex(w => w.workId === action.workId)
        let lstEligibleWorksCancel = [...state.lstEligibleWorks]
        if (cancelEligibleIndex > -1) {
          lstEligibleWorksCancel[cancelEligibleIndex].currentStatus = workStatusJson.OptingOut
          return {
            ...state,
            lstEligibleWorks: lstEligibleWorksCancel
          }
        } else {
          return { state }
        }
      } else {
        return { state }
      }

    case types.WORK_SUBMIT_SUCCESS:
      const submitIndex = state.lstEligibleWorks.findIndex(w => w.workId === action.workId)
      let lstWorksSubmit = [...state.lstEligibleWorks]
      if (submitIndex > -1) {
        lstWorksSubmit[submitIndex].currentStatus = action.status
        return {
          ...state,
          lstEligibleWorks: lstWorksSubmit
        }
      } else {
        return { state }
      }

    case types.WORK_CREATE_SUCCESS:
    case types.LOAD_WORK_SUCCESS:
    case types.LOAD_WORK_DATA:
      return state

    // case types.DELETE_WORK_DATA:
    //   return state

    //Draft work new Starts
    case types.ADD_WORK_DRAFT:
      return {
        ...state,
        lstDrafts: state.lstDrafts.concat(action.draft)
      }

    case types.LOAD_WORK_DRAFTS:
      let lstDrafts = []
      return {
        ...state,
        lstDrafts: lstDrafts.concat(action.draft)
      }

    case types.UPDATE_WORK_DRAFTS:
      let lstDraftData = [...state.lstDrafts]
      try {
        lstDraftData[action.workIndex].workDetails = action.workDetails
        lstDraftData[action.workIndex].workDraft = action.workDraft
      } catch (err) {}

      return {
        ...state,
        lstDrafts: lstDraftData
      }

    case types.DELETE_WORK_DRAFTS:
      return {
        ...state,
        lstDrafts: state.lstDrafts.filter(item => item.workId !== action.workId)
      }
    //Draft work new Ends

    default:
      return state
  }
}
