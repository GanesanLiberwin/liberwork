import * as types from "../actions/actionTypes"
import initialState from "./initialState"

export default (state = initialState.search, action) => {
  switch (action.type) {
    case types.LOAD_SELECTED_SKILLS:
    case types.LOAD_SKILLS:
    case types.LOAD_SKILLS_SUCCESS:
      let lstNewSkills = []
      return {
        ...state,
        lstSkills: lstNewSkills.concat(action.searchContent)
      }

    case types.UPDATE_SKILLS:
      let lstSkillsNew = state.lstSkills.concat(action.searchContent)
      return {
        ...state,
        lstSkills: lstSkillsNew
      }

    case types.LOAD_SELECTED_CATEGORY:
      let lstNewCategory = []
      return {
        ...state,
        lstCategory: lstNewCategory.concat(action.searchContent)
      }

    case types.LOAD_SELECTED_INDUSTRY:
      let lstNewIndustry = []
      return {
        ...state,
        lstIndustry: lstNewIndustry.concat(action.searchContent)
      }

    case types.LOAD_SKILLS_SEARCHED:
    case types.LOAD_SEARCHEDSKILLS_SUCCESS:
      let lstSearchedSkills = []
      return {
        ...state,
        lstSearchedSkills: lstSearchedSkills.concat(action.searchContent)
      }

    case types.FETCH_SKILLS:
    case types.FETCH_SKILLS_SUCCESS:
      let lstFetchSkills = []
      return {
        ...state,
        lstFetchSkills: lstFetchSkills.concat(action.searchContent)
      }

    case types.CLEAR_SELECTED_DATA:
      return {
        ...state,
        lstSkills: [],
        lstCategory: [],
        lstIndustry: [],
        lstFetchSkills: []
      }
    case types.CLEAR_SELECTED_SKILLS:
      return {
        ...state,
        lstSkills: []
      }
    case types.CLEAR_SELECTED_CATEGORY:
      return {
        ...state,
        lstCategory: []
      }
    case types.CLEAR_SELECTED_INDUSTRY:
      return {
        ...state,
        lstIndustry: []
      }
    case types.CLEAR_SEARCHED_SKILLS:
      return {
        ...state,
        lstSearchedSkills: []
      }

    case types.CLEAR_FETCH_SKILLS:
      return {
        ...state,
        lstFetchSkills: []
      }
    default:
      return state
  }
}
