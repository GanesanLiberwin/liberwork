import * as types from "../actions/actionTypes"
import initialState from "./initialState"

export default (state = initialState.orgDetail, action) => {
  switch (action.type) {
    case types.LOAD_ORG_DETAIL:
      return {
        ...state,
        orgId: action.orgDetails.orgId,
        orgName: action.orgDetails.orgName,
        orgType: action.orgDetails.orgType,
        orgUnits: action.orgDetails.orgUnits,
        validity: action.orgDetails.validity
      }
    case types.CLEAR_ORG_DETAIL:
      return { ...state, orgId: "", orgName: "", orgType: "", orgUnits: [], validity: "" }
    default:
      return state
  }
}
