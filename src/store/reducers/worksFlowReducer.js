import * as types from "../actions/actionTypes"
import initialState from "./initialState"

export default (state = initialState.workFlow, action) => {
	switch (action.type) {
		case types.UPDATE_WORK_FLOW_ID:
			return {
				...state,
				workId: action.workId,
				workSource: action.workSource
			}

		case types.LOAD_WORK_FLOW_DATA:
		case types.LOAD_WORK_FLOW_DATA_SUCCESS:
			return {
				...state,
				workRole: action.workRole,
				workDetails: action.workDetails
			}

		case types.UPDATE_WORKFLOW_STATUS:
			let workDetailStatus = state.workDetails
			workDetailStatus.currentStatus = action.workStatus

			return { ...state, workDetails: workDetailStatus }

		case types.UPDATE_WORK_FLOW_FIELDS:
			let workDetailsInfo = { ...state.workDetails }
			switch (action.fieldName) {
				case "workId":
					workDetailsInfo.workId = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "currentStatus":
					workDetailsInfo.currentStatus = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "remoteWorking":
					workDetailsInfo.remoteWorking = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "workLocation":
					workDetailsInfo.workLocation = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "workforceType":
					workDetailsInfo.workforceType = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "workforceCount":
					workDetailsInfo.workforceCount = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "beginnerAllowed":
					workDetailsInfo.beginnerAllowed = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "workType":
					workDetailsInfo.workType = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "functionalDomain":
					workDetailsInfo.functionalDomain = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "functionalExperience":
					workDetailsInfo.functionalExperience = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "selectionModel":
					workDetailsInfo.selectionModel = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "skills":
					workDetailsInfo.skills = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "industryDomain":
					workDetailsInfo.industryDomain = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "industryExperience":
					workDetailsInfo.industryExperience = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "description":
					workDetailsInfo.description = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "outcome":
					workDetailsInfo.outcome = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "criticalWork":
					workDetailsInfo.criticalWork = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "estimatedEfforts":
					workDetailsInfo.estimatedEfforts = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "plannedStartDate":
					workDetailsInfo.plannedStartDate = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "plannedEndDate":
					workDetailsInfo.plannedEndDate = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "rewardUnits":
					workDetailsInfo.rewardUnits = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "rewardType":
					workDetailsInfo.rewardType = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "rewardModel":
					workDetailsInfo.rewardModel = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "attachments":
					workDetailsInfo.attachments = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "delegatePermission":
					workDetailsInfo.delegatePermission = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				case "delegateAccountId":
					workDetailsInfo.delegateAccountId = action.fieldValue
					return { ...state, workDetails: workDetailsInfo }
				default:
					return state
			}

		case types.UPDATE_DELEGATE:
			let workDetailInfo = state.workDetails
			workDetailInfo.delegateAccountId = action.delegateAccountId
			workDetailInfo.delegatePermission = action.delegatePermission

			return { ...state, workDetails: workDetailInfo }

		case types.UPDATE_BIDDING:
			let Bidding = { ...state.Bidding }
			Bidding.allocationStatus = action.allocationStatus
			Bidding.comment = action.comment
			return { ...state, Bidding: Bidding }

		case types.LOAD_MESSAGES:
		case types.LOAD_MESSAGES_SUCCESS:
			let Communication = { ...state.Communication }
			Communication.messages = action.lstMessages
			return { ...state, Communication: Communication }

		case types.UPDATE_MESSAGES:
		case types.UPDATE_MESSAGES_SUCCESS:
			const newMessage = [action.newMessage]
			let MessageInfo = { ...state.Communication }
			MessageInfo.messages = MessageInfo.messages.concat(newMessage)
			return { ...state, Communication: MessageInfo }

		case types.UPDATE_SUBMISSION:
			let Submission = { ...state.Submission }
			Submission.submission = action.submission
			Submission.attachments = action.attachments
			return { ...state, Submission: Submission }

		case types.LOAD_SUBMISSION_LIST:
			return {
				...state,
				lstSubmission: action.lstSubmission
			}

		case types.UPDATE_POSTER_RATING:
			let posterRatings = { ...state.Closure }
			posterRatings.posterRating = action.posterRating
			posterRatings.posterRatingText = action.posterRatingText
			return { ...state, Closure: posterRatings }

		case types.UPDATE_POSTER_FEEDBACK:
			let posterRatingsFeedback = { ...state.Closure }
			posterRatingsFeedback.posterFeedbackList = action.posterFeedbackList
			return { ...state, Closure: posterRatingsFeedback }

		case types.LOAD_FEEDBACK:
			return {
				...state,
				lstFeedbacks: action.lstFeedback
			}

		case types.UPDATE_POSTER_REWARDS:
			let posterRewards = { ...state.Closure }
			posterRewards.rewardUnitsFinal = action.rewardUnitsFinal
			return { ...state, Closure: posterRewards }

		case types.UPDATE_WORKER_RATING:
			let workerRatings = { ...state.Closure }
			workerRatings.workerRating = action.workerRating
			workerRatings.workerRatingText = action.workerRatingText
			return { ...state, Closure: workerRatings }

		case types.UPDATE_WORKER_FEEDBACK:
			let workerFeedback = { ...state.Closure }
			workerFeedback.workerFeedbackList = action.workerFeedbackList //need to check array list
			return { ...state, Closure: workerFeedback }

		case types.CLEAR_WORK_FLOW_DATA:
			let currentDate = new Date()
			return {
				workId: "",
				workSource: "",
				workRole: "",
				workDetails: {
					workId: "",
					accountId: "",
					orgId: "",
					//Type
					workforceType: "",
					beginnerAllowed: false,
					workType: "",
					functionalDomain: "",
					functionalExperience: "0",
					selectionModel: "",
					remoteWorking: true,
					workLocation: "",
					// Skills
					skills: [],
					industryDomain: "",
					industryExperience: "0",
					//Work Description
					description: "",
					estimatedEfforts: "",
					plannedStartDate: 0,
					plannedEndDate: 0,
					criticalWork: false,
					outcome: "",
					attachments: [],
					//Reward
					rewardUnits: "",
					rewardType: "points",
					rewardModel: "",
					//delegate
					delegatePermission: "",
					delegateAccountId: "None",
					// Status
					currentStatus: "",
					biddingEndDateTime: 0,
					//
					updatedAt: 0,
					postedDate: 0,
					workforceCount: 0,
					statusTransition: [
						{
							date: 0,
							changedBy: "",
							status: ""
						}
					],
					currentStatusDate: 0
				},
				Bidding: {
					allocationStatus: "",
					comment: ""
				},
				Communication: {
					messages: [],
					lastReadMessage: ""
				},
				Submission: {
					submission: "",
					attachments: []
				},
				Closure: {
					posterRating: 0,
					posterRatingText: "",
					posterFeedbackList: [],
					workerRating: 0,
					workerRatingText: "",
					workerFeedbackList: [],
					rewardUnitsFinal: 0
				},
				lstFeedbacks: [],
				lstSubmission: [],
				workDraft: {
					targetId: 0,
					tabProgressId: 0,
					workForceValid: false,
					workTypeValid: false,
					categoryValid: false,
					selectionModelValid: false,
					locationValid: true,
					skillValid: false,
					industryValid: false,
					descriptionValid: false,
					outcomeValid: false,
					effortValid: false,
					scheduleValid: false,
					rewardValid: false,
					rewardModelValid: false,
					workStartDate: currentDate,
					workStartMinDate: currentDate,
					workStartMaxDate: currentDate,
					workEndDate: currentDate,
					workEndMinDate: currentDate,
					workEndMaxDate: currentDate,
					differenceInDays: 0,
					profileCount: "",
					effortStatus: "",
					scheduleStatus: "",
					rewardStatus: "",
					rewardRate: ""
				}
			}

		case types.UPDATE_WORK_DRAFT_DATA:
			return {
				...state,
				workDraft: action.workDraftData
			}

		case types.UPDATE_WORK_DRAFT_FIELDS:
			let workDraftInfo = { ...state.workDraft }
			switch (action.fieldName) {
				case "targetId":
					workDraftInfo.targetId = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "tabProgressId":
					workDraftInfo.tabProgressId = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "workForceValid":
					workDraftInfo.workForceValid = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "workTypeValid":
					workDraftInfo.workTypeValid = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "categoryValid":
					workDraftInfo.categoryValid = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "selectionModelValid":
					workDraftInfo.selectionModelValid = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "locationValid":
					workDraftInfo.locationValid = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "skillValid":
					workDraftInfo.skillValid = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "industryValid":
					workDraftInfo.industryValid = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "descriptionValid":
					workDraftInfo.descriptionValid = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "outcomeValid":
					workDraftInfo.outcomeValid = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "effortValid":
					workDraftInfo.effortValid = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "scheduleValid":
					workDraftInfo.scheduleValid = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "rewardValid":
					workDraftInfo.rewardValid = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "rewardModelValid":
					workDraftInfo.rewardModelValid = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "workStartDate":
					workDraftInfo.workStartDate = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "workStartMinDate":
					workDraftInfo.workStartMinDate = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "workStartMaxDate":
					workDraftInfo.workStartMaxDate = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "workEndDate":
					workDraftInfo.workEndDate = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "workEndMinDate":
					workDraftInfo.workEndMinDate = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "workEndMaxDate":
					workDraftInfo.workEndMaxDate = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "differenceInDays":
					workDraftInfo.differenceInDays = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "profileCount":
					workDraftInfo.profileCount = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "effortStatus":
					workDraftInfo.effortStatus = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "scheduleStatus":
					workDraftInfo.scheduleStatus = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "rewardStatus":
					workDraftInfo.rewardStatus = action.fieldValue
					return { ...state, workDraft: workDraftInfo }
				case "rewardRate":
					workDraftInfo.rewardRate = action.fieldValue
					return { ...state, workDraft: workDraftInfo }

				default:
					return state
			}

		default:
			return state
	}
}
