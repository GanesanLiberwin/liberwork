//import AsyncStorage from '@react-native-community/async-storage';
//import { AsyncStorage } from "react-native"
import AsyncStorage from "@react-native-async-storage/async-storage"

export const saveDataStorage = (dataKey, data) => {
	AsyncStorage.setItem(dataKey, data)
}

export const removeDataStorage = dataKey => {
	AsyncStorage.removeItem(dataKey)
}

export const multiRemoveDataStorage = keys => {
	AsyncStorage.multiRemove(keys, err => {})
}

export const multiSetDataStorage = keys => {
	AsyncStorage.multiSet(keys, err => {})
}
