//import { AsyncStorage } from "react-native"
import AsyncStorage from "@react-native-async-storage/async-storage"

import * as types from "./actionTypes"
import * as accountApi from "../../api/accountApi"
import * as apiStatusActions from "./apiStatusActions"
import * as workActions from "./workActions"

import { multiSetDataStorage, multiRemoveDataStorage, saveDataStorage } from "../asyncStorageUtil"

export function createAccountSuccess(account) {
	return { type: types.CREATE_ACCOUNT_SUCCESS, account }
}

export function createAccountVerifiedSuccess(account) {
	return { type: types.CREATE_ACCOUNT_VERIFIED_SUCCESS, account }
}

export function loadAccountData(account, isSilent = false) {
	return { type: isSilent ? types.LOAD_ACCOUNT_DATA : types.LOAD_ACCOUNT_SUCCESS, account }
}

export function loadExistingAccount(lstAccounts, accountId, isSilent = false) {
	return { type: isSilent ? types.LOAD_EXISTING_ACCOUNT : types.LOAD_ACCOUNT_EXIST_SUCCESS, lstAccounts, accountId }
}

export function updateAccountVerifiedData(account, isValid, isSilent) {
	return { type: isSilent ? types.UPDATE_ACCOUNT_VERIFIED_DATA : types.UPDATE_ACCOUNT_VERIFIED_DATA_SUCCESS, account, isValid }
}

export function clearAccountVerifiedData() {
	return { type: types.CLEAR_ACCOUNT_VERIFIED_DATA }
}

export const userUpdated = isUser => {
	return { type: types.STATUS_USER, isUser }
}

export const profileUpdated = isProfile => {
	return { type: types.STATUS_PROFILE, isProfile }
}

export const accountUpdated = isAccount => {
	return { type: types.STATUS_ACCOUNT, isAccount }
}

export const accountIdUpdated = AccountId => {
	return { type: types.ACCOUNT_ID_UPDATE, AccountId }
}

export function loadOrgDetails(orgDetails) {
	return { type: types.LOAD_ORG_DETAIL, orgDetails }
}

export const loadUserData = user => {
	return { type: types.LOAD_USER_SUCCESS, userName: user.userName, userId: user.userId, accesstoken: user.accesstoken }
}

export function isAccountExist(userData) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return accountApi
			.isAccountExist(userData.userId)
			.then(data => {
				if (data) {
					dispatch(accountUpdated(true))
					dispatch(profileUpdated(true))
					dispatch(userUpdated(true))

					let lstAccounts = data
					if (lstAccounts && lstAccounts.length > 0) {
						dispatch(getDraftWorks())
						//set first account as selected account
						let selectedAccount = lstAccounts[0]
						dispatch(loadExistingAccount(data, selectedAccount.id, false))

						//save the default account details
						dispatch(loadAccountData(selectedAccount, true))
						saveDatatoStorage("accountData", selectedAccount)
						saveDatatoStorage("accountList", lstAccounts)

						if (userData.userNameType === "M" && selectedAccount && selectedAccount.accountEmail !== "") {
							//fetch OrgDetails for default account on Mobile signin flow
							dispatch(getOrgDetail("testM" + selectedAccount.accountEmail))
						}
					}
				} else {
					dispatch(profileUpdated(true))
					dispatch(userUpdated(true))
					dispatch(apiStatusActions.apiCallSuccess())
				}
			})
			.catch(error => {
				dispatch(profileUpdated(true))
				dispatch(userUpdated(true))
				dispatch(apiStatusActions.apiCallError())
			})
	}
}

export function isAccountAvailable(accountName) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return accountApi
			.isAccountAvailable(accountName)
			.then(data => {
				dispatch(apiStatusActions.apiCallSuccess())
				if (data) {
					if (data.accountExist) {
						throw new Error("account name already exists")
					} else {
						if (data.orgDetails) {
							saveOrgDetailtoStorage(data.orgDetails)
							dispatch(loadOrgDetails(data.orgDetails))
						}
						return true
					}
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function isValidDomainEmail(accountName, orgId) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return accountApi
			.isValidDomainEmail(accountName, orgId)
			.then(data => {
				dispatch(apiStatusActions.apiCallSuccess())
				if (data) {
					return data
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function getOrgDetail(accountName) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return accountApi
			.isAccountAvailable(accountName)
			.then(data => {
				if (data) {
					if (!data.accountExist && data.orgDetails) {
						dispatch(loadOrgDetails(data.orgDetails))
					}
					dispatch(apiStatusActions.apiCallSuccess())
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function createAccount(
	userId,
	accountName,
	orgId,
	selectedOrgUnit,
	empID,
	selectedOrgRole,
	supEmail,
	empLocation,
	selectedWorkerType,
	firstName,
	lastName
) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return accountApi
			.createAccount({
				profileId: userId,
				orgId: orgId,
				accountStatus: "active",
				orgUnitId: selectedOrgUnit,
				workerType: selectedWorkerType,
				orgPersonId: empID,
				accountEmail: accountName,
				supervisorEmail: supEmail,
				orgPersonRole: selectedOrgRole,
				personLocation: empLocation,
				firstName: firstName,
				lastName: lastName
			})
			.then(data => {
				if (data) {
					dispatch(createAccountSuccess(data))
					dispatch(accountUpdated(true))
					if (data.id) {
						dispatch(accountIdUpdated(data.id))
					}
					saveDatatoStorage("accountData", data)
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function updateAccount(selectedOrgUnit, selectedWorkerType, empID, supEmail, selectedOrgRole, empLocation, accountId) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return accountApi
			.updateAccount(
				{
					orgUnitId: selectedOrgUnit,
					workerType: selectedWorkerType,
					orgPersonId: empID,
					supervisorEmail: supEmail,
					orgPersonRole: selectedOrgRole,
					personLocation: empLocation,
					accountStatus: "active"
				},
				accountId
			)
			.then(data => {
				if (data) {
					dispatch(loadAccountData(data, false))
					saveDatatoStorage("accountData", data)
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function getAccount(accountId) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return accountApi
			.getAccount(accountId)
			.then(data => {
				if (data) {
					dispatch(loadAccountData(data, false))
					saveDatatoStorage("accountData", data)

					if (data.accountEmail) {
						//get OrgDetail for accountEmail
						dispatch(getOrgDetail(data.accountEmail))
					}
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function getAccountDetails(accountId) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return accountApi
			.getAccount(accountId)
			.then(data => {
				if (data) {
					return data
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

const saveDatatoStorage = (key, data) => {
	try {
		let multi_set_pairs = [
			["isAccount", "Y"],
			[key, JSON.stringify(data)]
		]
		multiSetDataStorage(multi_set_pairs)
	} catch (err) {
		console.log(err)
	}
}

const saveOrgDetailtoStorage = data => {
	try {
		saveDataStorage("orgDetail", JSON.stringify(data))
	} catch (err) {
		console.log(err)
	}
}

const deleteDatatoStorage = () => {
	try {
		let keys = ["isAccount", "accountData"]
		multiRemoveDataStorage(keys)
	} catch (err) {
		console.log(err)
	}
}

//Fetch DraftWorks from AsycStorage on signin
export function getDraftWorks() {
	return function (dispatch) {
		try {
			let lstStoreData = ["draftWorkData", "draftCardData", "WorkDrafts"]
			AsyncStorage.multiGet(lstStoreData, (err, stores) => {
				stores.map((result, i, store) => {
					let key = store[i][0]
					let val = store[i][1]
					if (val) {
						switch (key) {
							case "draftWorkData":
								const draftWorkData = JSON.parse(val)
								if (draftWorkData && draftWorkData.draft) {
									dispatch(workActions.loadDraftData(draftWorkData.draft))
								}
								break
							case "draftCardData":
								const draftCardData = JSON.parse(val)
								if (draftCardData && draftCardData.draftCard) {
									dispatch(workActions.loadDraftCardData(draftCardData.draftCard))
								}
								break

							case "WorkDrafts":
								const draftsData = JSON.parse(val)
								if (draftsData) {
									dispatch(workActions.loadWorkDrafts(draftsData))
								}
								break

							default:
								break
						}
					}
				})
			})
		} catch (err) {}
	}
}
