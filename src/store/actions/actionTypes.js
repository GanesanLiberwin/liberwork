// By convention, actions that end in "_SUCCESS" are assumed to have been the result of a completed API call.
// apiCallsInProgress is reduced by 1 on "_SUCCESS" actions

export const API_CALL_STARTS = "API_CALL_STARTS"
export const API_CALL_ERROR = "API_CALL_ERROR"
export const API_CALL_ENDS = "API_CALL_ENDS"
export const CLEAR_API_COUNT = "CLEAR_API_COUNT"

//isUser, isAccount, isProfile, - status update
export const STATUS_USER = "STATUS_USER"
export const STATUS_ACCOUNT = "STATUS_ACCOUNT"
export const STATUS_PROFILE = "STATUS_PROFILE"

// Auth Reducer
export const CLEAR_USER_DATA = "CLEAR_USER_DATA"
export const CREATE_USER_SUCCESS = "CREATE_USER_SUCCESS"
export const LOAD_USER_SUCCESS = "LOAD_USER_SUCCESS"
export const LOAD_USER_DATA = "LOAD_USER_DATA"
export const LOGOUT = "LOGOUT"
export const SET_AUTO_LOGIN = "SET_AUTO_LOGIN"

//Profile
export const CLEAR_PROFILE_DATA = "CLEAR_PROFILE_DATA"
export const CREATE_PROFILE_SUCCESS = "CREATE_PROFILE_SUCCESS"
export const LOAD_PROFILE_SUCCESS = "LOAD_PROFILE_SUCCESS"
export const LOAD_PROFILE_EXIST_SUCCESS = "LOAD_PROFILE_EXIST_SUCCESS"
export const LOAD_PROFILE_DATA = "LOAD_PROFILE_DATA"
export const LOAD_PROFILE_SKILLS = "LOAD_PROFILE_SKILLS"
export const LOAD_PROFILE_SKILLS_SUCCESS = "LOAD_PROFILE_SKILLS_SUCCESS"
export const ADD_PROFILE_SKILLS_SUCCESS = "ADD_PROFILE_SKILLS_SUCCESS"

//Account
export const CLEAR_ACCOUNT_DATA = "CLEAR_ACCOUNT_DATA"
export const CREATE_ACCOUNT_SUCCESS = "CREATE_ACCOUNT_SUCCESS"
export const LOAD_ACCOUNT_SUCCESS = "LOAD_ACCOUNT_SUCCESS"
export const LOAD_ACCOUNT_DATA = "LOAD_ACCOUNT_DATA"
export const LOAD_ACCOUNT_EXIST_SUCCESS = "LOAD_ACCOUNT_EXIST_SUCCESS"
export const LOAD_EXISTING_ACCOUNT = "LOAD_EXISTING_ACCOUNT"

export const ACCOUNT_ID_UPDATE = "ACCOUNT_ID_UPDATE"
export const CREATE_ACCOUNT_VERIFIED_SUCCESS = "CREATE_ACCOUNT_VERIFIED_SUCCESS"
export const UPDATE_ACCOUNT_VERIFIED_DATA = "UPDATE_ACCOUNT_VERIFIED_DATA"
export const UPDATE_ACCOUNT_VERIFIED_DATA_SUCCESS = "UPDATE_ACCOUNT_VERIFIED_DATA_SUCCESS"
export const CLEAR_ACCOUNT_VERIFIED_DATA = "CLEAR_ACCOUNT_VERIFIED_DATA"

//orgDetail
export const LOAD_ORG_DETAIL = "LOAD_ORG_DETAIL"
export const CLEAR_ORG_DETAIL = "CLEAR_ORG_DETAIL"

//wallet
export const CLEAR_WALLET_DATA = "CLEAR_WALLET_DATA"
export const CLEAR_ALL_TRANSACTION = "CLEAR_ALL_TRANSACTION"
export const LOAD_BALANCE_SUCCESS = "LOAD_BALANCE_SUCCESS"
export const LOAD_BALANCE = "LOAD_BALANCE"
export const LOAD_TRANSACTION = "LOAD_TRANSACTION"
export const LOAD_ALL_TRANSACTION = "LOAD_ALL_TRANSACTION"
export const LOAD_ALL_TRANSACTION_SUCCESS = "LOAD_ALL_TRANSACTION_SUCCESS"

//Work
export const LOAD_MYPOSTWORKCARD_DATA = "LOAD_MYPOSTWORKCARD_DATA"
export const LOAD_WORK_SUCCESS = "LOAD_WORK_SUCCESS"
export const LOAD_WORK_DATA = "LOAD_WORK_DATA"
// export const DELETE_WORK_DATA = "DELETE_WORK_DATA"
export const DELETE_DRAFTWORK_DATA = "DELETE_DRAFTWORK_DATA"

export const CLEAR_WORKS_DATA = "CLEAR_WORKS_DATA"
export const WORK_CREATE_SUCCESS = "WORK_CREATE_SUCCESS"
export const WORK_APPLY = "WORK_APPLY"
export const WORK_DECLINE = "WORK_DECLINE"
export const WORK_CANCEL = "WORK_CANCEL"
export const WORK_SUBMIT_SUCCESS = "WORK_SUBMIT_SUCCESS"

export const LOAD_AVAILABLE_WORKS_SUCCESS = "LOAD_AVAILABLE_WORKS_SUCCESS"
export const LOAD_AVAILABLE_WORKS = "LOAD_AVAILABLE_WORKS"

export const LOAD_ELIGIBLE_WORKS_SUCCESS = "LOAD_ELIGIBLE_WORKS_SUCCESS"
export const LOAD_ELIGIBLE_WORKS = "LOAD_ELIGIBLE_WORKS"
export const LOAD_CLOSED_WORKS_SUCCESS = "LOAD_CLOSED_WORKS_SUCCESS"
export const LOAD_CLOSED_WORKS = "LOAD_CLOSED_WORKS"

export const UPDATE_WORK_STATUS = "UPDATE_WORK_STATUS"
export const UPDATE_WORKFLOW_STATUS = "UPDATE_WORKFLOW_STATUS"

//WorkDraft
export const UPDATE_WORK_DRAFT_FIELDS = "UPDATE_WORK_DRAFT_FIELDS"
export const UPDATE_WORK_DRAFT_DATA = "UPDATE_WORK_DRAFT_DATA"

//WorkFlow
export const UPDATE_WORK_FLOW_ID = "UPDATE_WORK_FLOW_ID"
export const LOAD_WORK_FLOW_DATA = "LOAD_WORK_FLOW_DATA"
export const LOAD_WORK_FLOW_DATA_SUCCESS = "LOAD_WORK_FLOW_DATA_SUCCESS"
export const CLEAR_WORK_FLOW_DATA = "CLEAR_WORK_FLOW_DATA"
export const UPDATE_WORK_FLOW_FIELDS = "UPDATE_WORK_FLOW_FIELDS"
export const UPDATE_BIDDING = "UPDATE_BIDDING"

export const UPDATE_MESSAGES = "UPDATE_MESSAGES"
export const UPDATE_MESSAGES_SUCCESS = "UPDATE_MESSAGES_SUCCESS"
export const LOAD_MESSAGES = "LOAD_MESSAGES"
export const LOAD_MESSAGES_SUCCESS = "LOAD_MESSAGES_SUCCESS"

export const LOAD_SUBMISSION_LIST = "LOAD_SUBMISSION_LIST"
export const UPDATE_SUBMISSION = "UPDATE_SUBMISSION"
export const UPDATE_POSTER_RATING = "UPDATE_POSTER_RATING"
export const UPDATE_POSTER_FEEDBACK = "UPDATE_POSTER_FEEDBACK"
export const LOAD_FEEDBACK = "LOAD_FEEDBACK"
export const UPDATE_POSTER_REWARDS = "UPDATE_POSTER_REWARDS"
export const UPDATE_WORKER_RATING = "UPDATE_WORKER_RATING"
export const UPDATE_WORKER_FEEDBACK = "UPDATE_WORKER_FEEDBACK"
export const UPDATE_DELEGATE = "UPDATE_DELEGATE"

export const LOAD_DRAFTWORK_DATA = "LOAD_DRAFWORK_DATA"
export const LOAD_DRAFT_DATA = "LOAD_DRAFT_DATA"
export const LOAD_DRAFTCARD_DATA = "LOAD_DRAFTCARD_DATA"
export const UPDATE_DRAFTWORK_DATA = "LOAD_DRAFTWORKUPDATE_DATA"
export const UPDATE_SELECTED_DRAFT_ID = "UPDATE_SELECTED_DRAFT_ID"

export const ADD_WORK_DRAFT = "ADD_WORK_DRAFT"
export const LOAD_WORK_DRAFTS = "LOAD_WORK_DRAFTS"
export const UPDATE_WORK_DRAFTS = "UPDATE_WORK_DRAFTS"
export const DELETE_WORK_DRAFTS = "DELETE_WORK_DRAFTS"

export const CLEAR_SELECTED_DATA = "CLEAR_SELECTED_DATA"
export const CLEAR_SELECTED_SKILLS = "CLEAR_SELECTED_SKILLS"
export const CLEAR_SELECTED_CATEGORY = "CLEAR_SELECTED_CATEGORY"
export const CLEAR_SELECTED_INDUSTRY = "CLEAR_SELECTED_INDUSTRY"
export const LOAD_SELECTED_SKILLS = "LOAD_SELECTED_SKILLS"
export const LOAD_SELECTED_CATEGORY = "LOAD_SELECTED_CATEGORY"
export const LOAD_SELECTED_INDUSTRY = "LOAD_SELECTED_INDUSTRY"

export const LOAD_SKILLS = "LOAD_SKILLS"
export const LOAD_SKILLS_SUCCESS = "LOAD_SKILLS_SUCCESS"
export const UPDATE_SKILLS = "UPDATE_SKILLS"

//Search
export const LOAD_SKILLS_SEARCHED = "LOAD_SKILLS_SEARCHED"
export const LOAD_SEARCHEDSKILLS_SUCCESS = "LOAD_SEARCHEDSKILLS_SUCCESS"
export const CLEAR_SEARCHED_SKILLS = "CLEAR_SEARCHED_SKILLS"

export const FETCH_SKILLS = "FETCH_SKILLS"
export const FETCH_SKILLS_SUCCESS = "FETCH_SKILLS_SUCCESS"
export const CLEAR_FETCH_SKILLS = "CLEAR_FETCH_SKILLS"

export const LOAD_ALL_REWARDS = "LOAD_ALL_REWARDS"
export const LOAD_ALL_REWARDS_SUCCESS = "LOAD_ALL_REWARDS_SUCCESS"
export const LOAD_REWARD_MODEL = "LOAD_REWARD_MODEL"
export const LOAD_REWARD_MODEL_SUCCESS = "LOAD_REWARD_MODEL_SUCCESS"
export const CLEAR_REWARD_MODELS = "CLEAR_REWARD_MODELS"
