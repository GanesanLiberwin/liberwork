import * as types from "./actionTypes"
import * as loginApi from "../../api/loginApi"
import * as apiStatusActions from "./apiStatusActions"
import * as profileActions from "../../store/actions/profileActions"
import { saveDataStorage, multiSetDataStorage, multiRemoveDataStorage } from "../asyncStorageUtil"

export const setAutoLoginAttempt = () => {
  return { type: types.SET_AUTO_LOGIN }
}

export const createUserSuccess = user => {
  return { type: types.CREATE_USER_SUCCESS, userDetails: user }
}

export function loadUserData(user, isSilent = false) {
  return { type: isSilent ? types.LOAD_USER_DATA : types.LOAD_USER_SUCCESS, userDetails: user }
}

export function loadOrgDetails(orgDetails) {
  return { type: types.LOAD_ORG_DETAIL, orgDetails }
}

export const userUpdated = isUser => {
  return { type: types.STATUS_USER, isUser }
}

export const profileUpdated = isProfile => {
  return { type: types.STATUS_PROFILE, isProfile }
}

export const accountUpdated = isAccount => {
  return { type: types.STATUS_ACCOUNT, isAccount }
}

export const LogoutSession = () => {
  return { type: types.LOGOUT }
}

export const ClearUserData = () => {
  return { type: types.CLEAR_USER_DATA }
}

export const ClearProfileData = () => {
  return { type: types.CLEAR_PROFILE_DATA }
}

export const ClearAccountData = () => {
  return { type: types.CLEAR_ACCOUNT_DATA }
}

export const ClearWalletData = () => {
  return { type: types.CLEAR_WALLET_DATA }
}

export const ClearWorkData = () => {
  return { type: types.CLEAR_WORKS_DATA }
}

export const ClearApiCount = () => {
  return { type: types.CLEAR_API_COUNT }
}

export const ClearOrgDetail = () => {
  return { type: types.CLEAR_ORG_DETAIL }
}

export function clearSearchedSkills() {
  return { type: types.CLEAR_SEARCHED_SKILLS }
}

export function isUserExist(userName, userNameType) {
  return function (dispatch) {
    dispatch(apiStatusActions.beginApiCall())
    return loginApi
      .isUserExist({ userName: userName, userNameType: userNameType, isExistsUser: true })
      .then(data => {
        if (data) {
          dispatch(apiStatusActions.apiCallSuccess())
          if (userNameType === "E" && !data.isUserExists && data.domainValidity) {
            if (data.orgDetails) {
              dispatch(loadOrgDetails(data.orgDetails))
              saveOrgDetail(data)
            }
            return true
          } else if (userNameType === "M" && !data.isUserExists && !data.domainValidity) {
            return true
          } else {
            return false
          }
        }
      })
      .catch(error => {
        dispatch(apiStatusActions.apiCallError())
        throw new Error(error)
      })
  }
}

export function createUser(userName, password, userNameType, dialCode, otp) {
  return function (dispatch) {
    dispatch(apiStatusActions.beginApiCall())
    return loginApi
      .createUser({
        userName: userName,
        password: password,
        userNameType: userNameType,
        dialCode: dialCode,
        secretPhrase: "test",
        isExistsUser: false,
        otp: otp
      })
      .then(data => {
        if (data) {
          dispatch(createUserSuccess(data, userNameType))
          saveUserDatatoStorage(data)
          dispatch(profileActions.profileUpdated(false))
          dispatch(profileActions.isProfileExist(data))
        }
      })
      .catch(error => {
        dispatch(apiStatusActions.apiCallError())
        throw new Error(error)
      })
  }
}

export function loginUser(userName, password, userNameType) {
  return function (dispatch) {
    dispatch(apiStatusActions.beginApiCall())
    return loginApi
      .getUser({ userName: userName, password: password, userNameType: userNameType })
      .then(data => {
        if (data) {
          if (data.userId) {
            if (data.orgDetails) {
              saveDatatoStorage(data)
            } else {
              saveUserDatatoStorage(data)
            }

            if (userNameType === "E" && data.orgDetails) {
              dispatch(loadOrgDetails(data.orgDetails))
            }
            dispatch(loadUserData(data, false))
            dispatch(profileActions.profileUpdated(false))
            //check if Profile exist on login success, check AccountExist on Profile success
            dispatch(profileActions.isProfileExist(data))
          } else {
            dispatch(apiStatusActions.apiCallSuccess())
          }
        }
      })
      .catch(error => {
        dispatch(apiStatusActions.apiCallError())
        throw new Error(error)
      })
  }
}

export const logout = () => {
  return function (dispatch) {
    dispatch(accountUpdated(false))
    dispatch(profileUpdated(false))
    dispatch(userUpdated(false))

    dispatch(LogoutSession())
    dispatch(ClearUserData())
    dispatch(ClearProfileData())
    dispatch(ClearAccountData())
    dispatch(ClearWalletData())
    dispatch(ClearOrgDetail())
    dispatch(ClearApiCount())

    dispatch(ClearWorkData())
    dispatch(clearSearchedSkills())

    try {
      let keys = ["userData", "isProfile", "profileData", "isAccount", "accountData", "accountList", "orgDetail"]
      multiRemoveDataStorage(keys)
    } catch (err) {
      console.log(err)
    }

    // to clean up drafts
    // clearDraftWorks()
  }
}

const clearDraftWorks = () => {
  try {
    let draftKeys = ["draftWorkData", "draftCardData", "WorkDrafts"]
    multiRemoveDataStorage(draftKeys)
  } catch (err) {
    console.log(err)
  }
}

const saveDatatoStorage = user => {
  try {
    const userData = JSON.stringify({
      userName: user.userName,
      userId: user.userId,
      userNameType: user.userNameType,
      accesstoken: user.accesstoken
    })

    let multi_set_pairs = [
      ["userData", userData],
      ["orgDetail", JSON.stringify(user.orgDetails)]
    ]
    multiSetDataStorage(multi_set_pairs)
  } catch (err) {
    console.log(err)
  }
}

const saveUserDatatoStorage = user => {
  try {
    const userData = JSON.stringify({
      userName: user.userName,
      userId: user.userId,
      userNameType: user.userNameType,
      accesstoken: user.accesstoken
    })
    saveDataStorage("userData", userData)
  } catch (err) {
    console.log(err)
  }
}

const saveOrgDetail = data => {
  try {
    saveDataStorage("orgDetail", JSON.stringify(data.orgDetails))
  } catch (err) {
    console.log(err)
  }
}
