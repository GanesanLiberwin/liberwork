import * as types from "./actionTypes"
import * as workApi from "../../api/workApi"
import * as apiStatusActions from "./apiStatusActions"

// import { DateToDBString } from "../../components/Utilities/DateFormat"
import { getWorkRole } from "../../components/Utilities/WorkUtils"
import { workStatusJson } from "../../constants/PageJson"

// import { saveDataStorage, removeDataStorage, multiSetDataStorage, multiRemoveDataStorage } from "../asyncStorageUtil"

export function updateWorkFlowId(workId, workSource) {
	return { type: types.UPDATE_WORK_FLOW_ID, workId, workSource }
}

export function applyWorkData(workId) {
	return { type: types.WORK_APPLY, workId }
}

export function declineWorkData(workId) {
	return { type: types.WORK_DECLINE, workId }
}

export function cancelWorkData(workId, workRole) {
	return { type: types.WORK_CANCEL, workId, workRole }
}

export function updateWorkStatusData(workId, workStatus) {
	return { type: types.UPDATE_WORK_STATUS, workId, workStatus }
}

export function updateWorkFlowStatusData(workStatus) {
	return { type: types.UPDATE_WORKFLOW_STATUS, workStatus }
}

export function submitWorkData(workId, status) {
	return { type: types.WORK_SUBMIT_SUCCESS, workId, status }
}

export function loadWorkFlowData(workDetails, accountId, isSilent = false) {
	let workRole = getWorkRole(accountId, workDetails.accountId, workDetails.delegateAccountId, workDetails.delegatePermission)
	// workRole = "Worker" // To test work submit in local
	return { type: isSilent ? types.LOAD_WORK_FLOW_DATA : types.LOAD_WORK_FLOW_DATA_SUCCESS, workDetails, workRole }
}

export function loadDraftDetails(workDetails, workRole) {
	return { type: types.LOAD_WORK_FLOW_DATA, workDetails, workRole }
}

export function updateWorkFlowFields(fieldName, fieldValue) {
	return { type: types.UPDATE_WORK_FLOW_FIELDS, fieldName, fieldValue }
}

export function updateWorkBidding(allocationStatus, comment) {
	return { type: types.UPDATE_BIDDING, allocationStatus, comment }
}

export function updateWorkSubmission(submission, attachments) {
	return { type: types.UPDATE_SUBMISSION, submission, attachments }
}

export function loadWorkSubmissionList(lstSubmission) {
	return { type: types.LOAD_SUBMISSION_LIST, lstSubmission }
}

export function updatePosterRating(posterRating, posterRatingText) {
	return { type: types.UPDATE_POSTER_RATING, posterRating, posterRatingText }
}

export function updatePosterFeedback(posterFeedbackList) {
	return { type: types.UPDATE_POSTER_FEEDBACK, posterFeedbackList }
}

export function loadFeedback(lstFeedback) {
	return { type: types.LOAD_FEEDBACK, lstFeedback }
}

export function updatePosterRewards(rewardUnitsFinal) {
	return { type: types.UPDATE_POSTER_REWARDS, rewardUnitsFinal }
}

export function updateWorkerRating(workerRating, workerRatingText) {
	return { type: types.UPDATE_WORKER_RATING, workerRating, workerRatingText }
}

export function updateWorkerFeedback(workerFeedbackList) {
	return { type: types.UPDATE_WORKER_FEEDBACK, workerFeedbackList }
}

export function updateDelegateData(delegateAccountId, delegatePermission) {
	return { type: types.UPDATE_DELEGATE, delegateAccountId: delegateAccountId, delegatePermission: delegatePermission }
}

export function clearWorkFlowData() {
	return { type: types.CLEAR_WORK_FLOW_DATA }
}

// export function updateWorkData(work, isSilent = false) {
//   return { type: isSilent ? types.LOAD_WORK_DATA : types.LOAD_WORK_SUCCESS, work }
// }

// export function deleteWorkData(workId) {
//   return { type: types.DELETE_WORK_DATA, workId }
// }

export function updateWorkDraftFields(fieldName, fieldValue) {
	return { type: types.UPDATE_WORK_DRAFT_FIELDS, fieldName, fieldValue }
}

export function updateWorkDraftData(workDraftData) {
	return { type: types.UPDATE_WORK_DRAFT_DATA, workDraftData }
}

export function getWorkByWorkId(workId, accountId) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return workApi
			.getWorkByWorkId({ workId: workId })
			.then(data => {
				if (data) {
					dispatch(loadWorkFlowData(data, accountId))
					if (data.currentStatus) {
						let currentStatus = data.currentStatus

						if (
							currentStatus === workStatusJson.Submitted ||
							currentStatus === workStatusJson.Resubmitted ||
							currentStatus === workStatusJson.Rework ||
							currentStatus === workStatusJson.Completed ||
							currentStatus === workStatusJson.Closed
						) {
							dispatch(getSubmissionData(workId))
						}

						if (currentStatus === workStatusJson.Completed || currentStatus === workStatusJson.Closed) {
							let posterAccountId = data.accountId
							dispatch(getFeedbackByAccount(workId, posterAccountId))
						}
					}
					// To test work submit in local
					// dispatch(updateWorkFlowStatusData(workStatusJson.Posted, true))
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function updateDelegate(workId, delegateId, selectPermission) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		// {
		//   "delegateAccountId": "", "delegatePermission": "read"/"None"
		// }
		return workApi
			.updateWork(
				{
					delegateAccountId: delegateId,
					delegatePermission: selectPermission
				},
				workId
			)
			.then(data => {
				if (data) {
					dispatch(apiStatusActions.apiCallSuccess())
					dispatch(updateDelegateData(delegateId, selectPermission))
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function updateWorkStatus(workId, workStatus, payload) {
	// {  "accountId": "", "status": "InProgress" } InProgress/Rework
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return workApi
			.updateWorkStatus(payload, workId)
			.then(data => {
				if (data) {
					dispatch(apiStatusActions.apiCallSuccess())

					dispatch(updateWorkStatusData(workId, workStatus))
					dispatch(updateWorkFlowStatusData(workStatus))
					// InProgress/Rework
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function submitWork(workId, accountId, status, desc, attachments) {
	/*
  {
    "workId":"", "accountId": "", "status": "submitted",
    "submissions": { "description": "submitted1", "attachment": ["d", "dd"] }
  }*/
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return workApi
			.submitWork({
				workId: workId,
				accountId: accountId,
				status: status,
				submissions: { description: desc, attachment: attachments }
			})
			.then(data => {
				if (data) {
					dispatch(submitWorkData(workId, status))
					dispatch(updateWorkFlowStatusData(status))
					dispatch(updateWorkSubmission(desc, attachments))
					if (data.submissions) {
						dispatch(loadWorkSubmissionList(data.submissions))
					}
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function getSubmissionData(workId) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return workApi
			.getSubmissionData(workId)
			.then(data => {
				if (data && data.length > 0) {
					dispatch(apiStatusActions.apiCallSuccess())
					dispatch(updateWorkSubmission(data[data.length - 1].description, data[data.length - 1].attachment))
					dispatch(loadWorkSubmissionList(data))
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function applyWork(workId, accountId, action, comment) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return workApi
			.applyWork({ action: action, workId: workId, accountId: accountId, comment: comment })
			.then(data => {
				if (data) {
					dispatch(apiStatusActions.apiCallSuccess())

					if (action === "apply") {
						dispatch(applyWorkData(workId))
					} else if (action === "decline") {
						// OnRemove/OnOptOut
						dispatch(declineWorkData(workId))
					}
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function cancelWork(workId, accountId, workRole, rewardUnits, cancelStatus) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		// role : "poster"
		return workApi
			.cancelWork({ workId: workId, accountId: accountId, role: workRole, rewardUnits: rewardUnits })
			.then(data => {
				//cancelWork only returns status
				// if (data) {
				dispatch(apiStatusActions.apiCallSuccess())
				// dispatch(updateWorkStatusData(workId, cancelStatus))
				dispatch(updateWorkFlowStatusData(cancelStatus))
				dispatch(cancelWorkData(workId, workRole))
				// }
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

//complete work
export function addFeedback(workId, accountId, role, rating, feedback, rewardUnitsFinal) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return workApi
			.addFeedback({
				accountId: accountId,
				workId: workId,
				role: role,
				rating: rating,
				feedback: feedback,
				feedbackStage: "S",
				rewardUnits: rewardUnitsFinal
			})
			.then(data => {
				if (data) {
					dispatch(apiStatusActions.apiCallSuccess())
					// userRole => Completed: "Completed",
					// workStatus === "Submitted" || workStatus === "Resubmitted"
					// workRole === "Poster" || workRole === "Del-O"
					dispatch(updateWorkStatusData(workId, workStatusJson.Completed))
					dispatch(updateWorkFlowStatusData(workStatusJson.Completed))
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function getFeedbackByAccount(workId, accountId) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return workApi
			.getFeedbackByAccount(workId, accountId)
			.then(data => {
				if (data) {
					dispatch(apiStatusActions.apiCallSuccess())
					dispatch(loadFeedback(data))

					dispatch(updatePosterRating(data.rating, data.feedback))
					dispatch(updatePosterRewards(data.rewardUnits))
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function getFeedbackAll(workId) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return workApi
			.getFeedbackAll(workId)
			.then(data => {
				if (data) {
					dispatch(apiStatusActions.apiCallSuccess())
					dispatch(loadFeedback(data))
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function deleteWork(workId) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return workApi
			.deleteWork(workId)
			.then(data => {
				if (data) {
					dispatch(apiStatusActions.apiCallError())
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}
