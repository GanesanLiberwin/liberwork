import * as types from "./actionTypes"
import * as workToSkillApi from "../../api/workToSkillApi"
import * as apiStatusActions from "./apiStatusActions"

export function checkMatchingCount(payload) {
  return function (dispatch) {
    dispatch(apiStatusActions.beginApiCall())
    return workToSkillApi
      .checkMatchingCount(payload)
      .then(data => {
        if (data) {
          dispatch(apiStatusActions.apiCallSuccess())
          return data.count
        }
      })
      .catch(error => {
        dispatch(apiStatusActions.apiCallError())
        throw new Error(error)
      })
  }
}
