import { isToday, isYesterday } from "date-fns"
import { DateToString, FromUnixTime, TimeToString } from "../../components/Utilities/DateFormat"

import * as types from "./actionTypes"
import * as messageApi from "../../api/messageApi"
import * as apiStatusActions from "./apiStatusActions"

export function loadMessageData(lstMessages, isSilent = false) {
	return { type: isSilent ? types.LOAD_MESSAGES : types.LOAD_MESSAGES_SUCCESS, lstMessages: lstMessages }
}

export function updateWorkCommunication(newMessage, isSilent = false) {
	return { type: isSilent ? types.UPDATE_MESSAGES : types.UPDATE_MESSAGES_SUCCESS, newMessage: newMessage }
}

export function getMessages(workId) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return messageApi
			.getMessages(workId)
			.then(data => {
				if (data) {
					if (data.length > 0) {
						let lstMessages = []
						let msgDateTime = ""
						let msgDate = ""
						let msgTime = ""

						data.forEach(msg => {
							msgDateTime = msg.createdAt * 1000
							msgDate = DateToString(msgDateTime)
							msgTime = TimeToString(msgDateTime)

							lstMessages.push({
								date: isToday(msgDateTime) ? "Today" : isYesterday(msgDateTime) ? "Yesterday" : msgDate,
								time: msgTime,
								sender: msg.senderName,
								senderType: msg.senderType,
								message: msg.message,
								msgDateTime: msgDateTime,
								isToday: isToday(msgDateTime),
								isYesterday: isYesterday(msgDateTime)
							})
						})

						// dispatch(loadMessageData(data))
						dispatch(loadMessageData(lstMessages))
						// dispatch(loadMessageData(lstSortedMessages))
						return lstMessages
					} else {
						dispatch(apiStatusActions.apiCallSuccess())
						return []
					}
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function postMessage(workId, senderId, senderType, senderName, messageType, messageText) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return messageApi
			.postMessage({
				workId: workId,
				sender: senderId,
				senderType: senderType,
				senderName: senderName,
				messageType: messageType,
				message: messageText
			})
			.then(data => {
				// dispatch(apiStatusActions.apiCallSuccess())
				const msgDateTime = new Date()
				dispatch(
					updateWorkCommunication({
						date: "Today",
						time: TimeToString(msgDateTime),
						sender: senderName,
						senderType: senderType,
						message: messageText,
						msgDateTime: msgDateTime,
						isToday: true,
						isYesterday: false
					})
				)
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}
