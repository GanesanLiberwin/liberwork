import * as types from "./actionTypes"
import * as apiStatusActions from "./apiStatusActions"
import * as skillApi from "../../api/skillApi"
import { saveDataStorage } from "../asyncStorageUtil"

export function loadSelectedSkills(skills) {
  // skills = { name: "", skillId: "", isRequired: false }
  return { type: types.LOAD_SELECTED_SKILLS, searchContent: skills }
}

export function loadSkills(skills, isSilent = false) {
  return { type: isSilent ? types.LOAD_SKILLS : types.LOAD_SKILLS_SUCCESS, searchContent: skills }
}

export function updateSkills(skills) {
  return { type: types.UPDATE_SKILLS, searchContent: skills }
}

export function loadSelectedCategory(Category) {
  return { type: types.LOAD_SELECTED_CATEGORY, searchContent: Category }
}

export function loadSelectedIndustry(Industry) {
  return { type: types.LOAD_SELECTED_INDUSTRY, searchContent: Industry }
}

export function loadSearchedSkills(searchedSkills, isSilent) {
  return { type: isSilent ? types.LOAD_SKILLS_SEARCHED : types.LOAD_SEARCHEDSKILLS_SUCCESS, searchContent: searchedSkills }
}

export function clearSearchData() {
  return { type: types.CLEAR_SELECTED_DATA }
}

export function clearSearchSkills() {
  return { type: types.CLEAR_SELECTED_SKILLS }
}

export function clearSearchCategory() {
  return { type: types.CLEAR_SELECTED_CATEGORY }
}

export function clearSearchIndustry() {
  return { type: types.CLEAR_SELECTED_INDUSTRY }
}

export function loadFetchSkills(searchedSkills, isSilent) {
  return { type: isSilent ? types.FETCH_SKILLS : types.FETCH_SKILLS_SUCCESS, searchContent: searchedSkills }
}

export function clearFetchSkills() {
  return { type: types.CLEAR_FETCH_SKILLS }
}

export function fetchSkills(searchText) {
  return function (dispatch) {
    dispatch(apiStatusActions.beginApiCall())
    return skillApi
      .searchSkill(searchText)
      .then(data => {
        if (data) {
          if (data.length > 0) {
            dispatch(loadFetchSkills(data))
          } else {
            dispatch(apiStatusActions.apiCallSuccess())
          }
          return data
        }
      })
      .catch(error => {
        dispatch(apiStatusActions.apiCallError())
        throw new Error(error)
      })
  }
}

export function searchSkills(searchText, storeSkillData) {
  return function (dispatch) {
    dispatch(apiStatusActions.beginApiCall())
    return skillApi
      .searchSkill(searchText)
      .then(data => {
        if (data) {
          if (data.length > 0) {
            let searchedSkills = [...data]
            let storeSkills = [...storeSkillData]
            searchedSkills.forEach(searchedSkillsItem => {
              storeSkills.forEach((skillItem, index) => {
                if (searchedSkillsItem.skillId === skillItem.skillId) {
                  storeSkills.splice(index, 1)
                }
              })
            })
            let mergedSkills = storeSkills.concat(searchedSkills)
            dispatch(loadSearchedSkills(mergedSkills))
            saveSearchedSkillstoStorage(mergedSkills)
            return data
          } else {
            dispatch(apiStatusActions.apiCallSuccess())
            return data
          }
        }
      })
      .catch(error => {
        dispatch(apiStatusActions.apiCallError())
        throw new Error(error)
      })
  }
}

const saveSearchedSkillstoStorage = data => {
  try {
    saveDataStorage("searchedSkill", JSON.stringify({ data }))
  } catch (err) {
    console.log(err)
  }
}
