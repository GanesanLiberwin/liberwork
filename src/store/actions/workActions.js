import * as types from "./actionTypes"
import * as workApi from "../../api/workApi"
import * as apiStatusActions from "./apiStatusActions"
// import * as workFlowActions from "../../store/actions/workFlowActions"
// import { DateToDBString } from "../../components/Utilities/DateFormat"
import { workStatusJson } from "../../constants/PageJson"

import { saveDataStorage, removeDataStorage, multiSetDataStorage, multiRemoveDataStorage } from "../asyncStorageUtil"

export function createWorkSuccess(work) {
  return { type: types.WORK_CREATE_SUCCESS, work }
}

export function loadAvailableWorks(lstWorks, isSilent = false) {
  return { type: isSilent ? types.LOAD_AVAILABLE_WORKS : types.LOAD_AVAILABLE_WORKS_SUCCESS, lstWorks }
}

export function loadEligibleWorks(lstWorks, isSilent = false) {
  return { type: isSilent ? types.LOAD_ELIGIBLE_WORKS : types.LOAD_ELIGIBLE_WORKS_SUCCESS, lstWorks }
}

export function loadClosedWorks(lstWorks, isSilent = false) {
  return { type: isSilent ? types.LOAD_CLOSED_WORKS : types.LOAD_CLOSED_WORKS_SUCCESS, lstWorks }
}

//Load My posts card data on work post success
export function loadMyPostWorkCardData(MyPostWorkCard) {
  return { type: types.LOAD_MYPOSTWORKCARD_DATA, MyPostWorkCard }
}

//Load From form
export function loadDraftWorkData(draftWork, draftWorkCard) {
  return { type: types.LOAD_DRAFTWORK_DATA, draftWork, draftWorkCard }
}

//update From form
export function updateDraftWorkData(draftWork, draftWorkCard, workIndex, cardIndex) {
  return { type: types.UPDATE_DRAFTWORK_DATA, draftWork, draftWorkCard, workIndex, cardIndex }
}

//Load From asyncstorage
export function loadDraftData(draftWork) {
  return { type: types.LOAD_DRAFT_DATA, draftWork }
}

//Load From asyncstorage
export function loadDraftCardData(draftWorkCard) {
  return { type: types.LOAD_DRAFTCARD_DATA, draftWorkCard }
}
// Delete in store from form
export function deleteDraftWorkData(draftId) {
  return { type: types.DELETE_DRAFTWORK_DATA, draftId }
}

export function updateDraftId(draftId) {
  return { type: types.UPDATE_SELECTED_DRAFT_ID, draftId }
}

export function addWorkDraft(draft) {
  return { type: types.ADD_WORK_DRAFT, draft }
}

//Load From form
export function loadWorkDrafts(draft) {
  return { type: types.LOAD_WORK_DRAFTS, draft }
}

//update From form
export function updateWorkDrafts(workIndex, workDetails, workDraft) {
  return { type: types.UPDATE_WORK_DRAFTS, workIndex, workDetails, workDraft }
}

export function deleteWorkDrafts(workId) {
  return { type: types.DELETE_WORK_DRAFTS, workId }
}

export function createWork(workPayload) {
  return function (dispatch) {
    dispatch(apiStatusActions.beginApiCall())

    return workApi
      .createWork(workPayload)
      .then(data => {
        if (data) {
          const myPostCardData = {
            workId: data.workId,
            currentStatus: data.currentStatus,
            delegateWork: false,
            biddingEndDateTime: data.biddingEndDateTime,
            functionalDomain: data.functionalDomain,
            skills: data.skills.length > 0 ? [data.skills[0].skill] : [""],
            criticalWork: data.criticalWork,
            rewardUnits: data.rewardUnits,
            plannedStartDate: data.plannedStartDate,
            plannedEndDate: data.plannedEndDate
          }

          dispatch(loadMyPostWorkCardData(myPostCardData))
          dispatch(createWorkSuccess(data))
          // saveDatatoStorage(data)
        }
      })
      .catch(error => {
        dispatch(apiStatusActions.apiCallError())
        throw new Error(error)
      })
  }
}

export function getAvailableWorks(accountId) {
  return function (dispatch) {
    dispatch(apiStatusActions.beginApiCall())
    return workApi
      .getAvailableWorks({ accountId: accountId })
      .then(data => {
        if (data && data.Works) {
          dispatch(loadAvailableWorks(data.Works))
        } else {
          dispatch(apiStatusActions.apiCallSuccess())
        }
      })
      .catch(error => {
        dispatch(apiStatusActions.apiCallError())
        // throw new Error(error)
      })
  }
}

export function getEligibleWorks(accountId) {
  return function (dispatch) {
    dispatch(apiStatusActions.beginApiCall())
    // Published, applied and allocated
    return workApi
      .getEligibleWorks(accountId)
      .then(data => {
        if (data && data.Works) {
          dispatch(loadEligibleWorks(data.Works))
        } else {
          dispatch(apiStatusActions.apiCallSuccess())
        }
      })
      .catch(error => {
        dispatch(apiStatusActions.apiCallError())
        throw new Error(error)
      })
  }
}

export function getClosedWorks(accountId) {
  return function (dispatch) {
    dispatch(apiStatusActions.beginApiCall())
    return workApi
      .getClosedWorks(accountId)
      .then(data => {
        if (data) {
          if (
            data.Works &&
            ((data.Works.postedWorks && data.Works.postedWorks.length > 0) ||
              (data.Works.delegatedWorks && data.Works.delegatedWorks.length > 0) ||
              (data.Works.allocatedWorks && data.Works.allocatedWorks.length > 0))
          ) {
            dispatch(loadClosedWorks(data.Works))
          } else {
            dispatch(apiStatusActions.apiCallSuccess())
          }
        }
      })
      .catch(error => {
        dispatch(apiStatusActions.apiCallError())
        // throw new Error(error)
      })
  }
}

// work draft create,Delete,Update in Asyncstorage
export const saveDrafttoStorage = (draft, draftCard) => {
  try {
    let multi_set_pairs = [
      ["draftWorkData", JSON.stringify({ draft })],
      ["draftCardData", JSON.stringify({ draftCard })]
    ]
    multiSetDataStorage(multi_set_pairs)
  } catch (err) {
    console.log(err)
  }
}

export const saveWorkDraft = data => {
  try {
    saveDataStorage("WorkDrafts", JSON.stringify(data))
  } catch (err) {
    console.log(err)
  }
}

export const clearWorkDraft = () => {
  try {
    removeDataStorage("WorkDrafts")
  } catch (err) {
    console.log(err)
  }
}
