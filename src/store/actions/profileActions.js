import * as types from "./actionTypes"
import * as profileApi from "../../api/profileApi"
import * as accountActions from "../../store/actions/accountActions"
import * as apiStatusActions from "./apiStatusActions"
import * as searchActions from "../../store/actions/searchActions"
import { multiSetDataStorage, multiRemoveDataStorage } from "../asyncStorageUtil"

export function createProfileSuccess(profile) {
	return { type: types.CREATE_PROFILE_SUCCESS, profile }
}

export function getProfileData(profile, isSilent = false) {
	return { type: isSilent ? types.LOAD_PROFILE_DATA : types.LOAD_PROFILE_SUCCESS, profile }
}

export function loadProfileSkillsData(lstSkills, isSilent = false) {
	return { type: isSilent ? types.LOAD_PROFILE_SKILLS : types.LOAD_PROFILE_SKILLS_SUCCESS, lstSkills }
}

export function addProfileSkillsSuccess(lstSkills) {
	return { type: types.ADD_PROFILE_SKILLS_SUCCESS, lstSkills }
}

export function profileExistSuccess(profile) {
	return { type: types.LOAD_PROFILE_EXIST_SUCCESS, firstName: profile.firstName, lastName: profile.lastName }
}

export const loadUserData = user => {
	return { type: types.LOAD_USER_SUCCESS, userDetails: user }
}

export const profileUpdated = isProfile => {
	return { type: types.STATUS_PROFILE, isProfile }
}

export const userUpdated = isUser => {
	return { type: types.STATUS_USER, isUser }
}

export function isProfileExist(userData) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return profileApi
			.isProfileExist(userData.userId)
			.then(data => {
				if (data && data.isProfileExist) {
					saveDatatoStorage(data)
					//check AccountExist on Profile success
					dispatch(accountActions.isAccountExist(userData))
					dispatch(profileExistSuccess(data))
				} else {
					dispatch(userUpdated(true))
					dispatch(apiStatusActions.apiCallSuccess())
				}
			})
			.catch(error => {
				dispatch(profileUpdated(false))
				dispatch(userUpdated(true))
				dispatch(apiStatusActions.apiCallError())
			})
	}
}

export function createProfile(data) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return profileApi
			.createProfile({
				userId: data.userId,
				firstName: data.firstName,
				lastName: data.lastName,
				dob: data.dob,
				nationality: data.nationality,
				personalEmail: data.personalEmail,
				mobileNumber: data.mobileNumber,
				gender: data.gender
			})
			.then(data => {
				if (data) {
					dispatch(createProfileSuccess(data))
					dispatch(profileUpdated(true))
					saveDatatoStorage(data)
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function getProfile(userId, isSilent) {
	return function (dispatch) {
		if (!isSilent) {
			dispatch(apiStatusActions.beginApiCall())
		}
		return profileApi
			.getProfile(userId)
			.then(data => {
				if (data) {
					dispatch(getProfileData(data, isSilent))
					dispatch(profileUpdated(true))
					saveDatatoStorage(data)
				}
			})
			.catch(error => {
				if (!isSilent) {
					dispatch(apiStatusActions.apiCallError())
				}
				throw new Error(error)
			})
	}
}

export function getProfileSkills(userId, isSilent = false) {
	return function (dispatch) {
		if (!isSilent) {
			dispatch(apiStatusActions.beginApiCall())
		}
		return profileApi
			.getProfileSkills(userId)
			.then(data => {
				if (data) {
					dispatch(loadProfileSkillsData(data, isSilent))
				} else if (!isSilent) {
					dispatch(apiStatusActions.apiCallError())
				}
			})
			.catch(error => {
				if (!isSilent) {
					dispatch(apiStatusActions.apiCallError())
				}
				// throw new Error(error)
			})
	}
}

export function addSkillToProfile(profileId, lstSkills, certifications) {
	// {
	//   "profileId": "1eaf28fc",
	//   "profileSkils": [
	//     { "skillId": "93795c06", "skillName": "ReactJs" },
	//     { "skillId": "fc1834d2", "skillName": "vb.net" }
	//   ],
	//   "certifications": ""
	// }

	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return profileApi
			.addSkillToProfile(profileId, {
				profileId: profileId,
				profileSkils: lstSkills,
				certifications: certifications
			})
			.then(data => {
				if (data) {
					dispatch(addProfileSkillsSuccess(data))
					// clearSearchSkills on add success
					dispatch(searchActions.clearSearchSkills())
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				// throw new Error(error)
			})
	}
}

const saveDatatoStorage = data => {
	try {
		let multi_set_pairs = [
			["isProfile", "Y"],
			["profileData", JSON.stringify(data)]
		]

		multiSetDataStorage(multi_set_pairs)
	} catch (err) {
		console.log(err)
	}
}

const deleteDatatoStorage = () => {
	try {
		let keys = ["profileData", "isProfile"]
		multiRemoveDataStorage(keys)
	} catch (err) {
		console.log(err)
	}
}
