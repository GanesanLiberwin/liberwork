import * as types from "./actionTypes"
import * as apiStatusActions from "./apiStatusActions"
import * as rewardsApi from "../../api/rewardsApi"
// import { saveDataStorage } from "../asyncStorageUtil"

export function loadAllRewardModel(rewardModels, isSilent = false) {
  return { type: isSilent ? types.LOAD_ALL_REWARDS : types.LOAD_ALL_REWARDS_SUCCESS, lstModels: rewardModels }
}

export function loadSelectedRewardModel(rewardModel, isSilent = false) {
  return { type: isSilent ? types.LOAD_REWARD_MODEL : types.LOAD_REWARD_MODEL_SUCCESS, rewardModel: rewardModel }
}

export function getReward(modelId) {
  return function (dispatch) {
    dispatch(apiStatusActions.beginApiCall())
    return rewardsApi
      .getReward(modelId)
      .then(data => {
        dispatch(loadSelectedRewardModel(data))
        return data
      })
      .catch(error => {
        dispatch(apiStatusActions.apiCallError())
        throw new Error(error)
      })
  }
}

export function getAllRewards() {
  return function (dispatch) {
    dispatch(apiStatusActions.beginApiCall())
    return rewardsApi
      .getAllReward()
      .then(data => {
        if (data) {
          dispatch(loadAllRewardModel(data))
        }
      })
      .catch(error => {
        dispatch(apiStatusActions.apiCallError())
        throw new Error(error)
      })
  }
}
