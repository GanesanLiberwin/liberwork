import * as types from "./actionTypes"

export function beginApiCall() {
  return { type: types.API_CALL_STARTS }
}

export function apiCallError() {
  return { type: types.API_CALL_ERROR }
}

export function apiCallSuccess() {
  return { type: types.API_CALL_ENDS }
}

export function userStatusUpdate(isUser) {
  return { type: types.STATUS_USER, isUser }
}

export function profileStatusUpdate(isProfile) {
  return { type: types.STATUS_PROFILE, isProfile }
}

export function accountStatusUpdate(isAccount) {
  return { type: types.STATUS_ACCOUNT, isAccount }
}
