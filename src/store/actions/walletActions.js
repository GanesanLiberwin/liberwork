import * as types from "./actionTypes"
import * as walletApi from "../../api/walletApi"
import * as apiStatusActions from "./apiStatusActions"
import { saveDataStorage } from "../asyncStorageUtil"

export function loadBalanceData(walletDetail, isSilent = false) {
	return { type: isSilent ? types.LOAD_BALANCE : types.LOAD_BALANCE_SUCCESS, walletDetail: walletDetail }
}

export function loadTransactionData(transactionDetail) {
	return { type: types.LOAD_TRANSACTION, TransactionDetail: transactionDetail }
}

export function loadAllTransaction(lstTransaction, isSilent = false) {
	return { type: isSilent ? types.LOAD_ALL_TRANSACTION : types.LOAD_ALL_TRANSACTION_SUCCESS, trxHistory: lstTransaction }
}

export function clearAllTransaction() {
	return { type: types.CLEAR_ALL_TRANSACTION }
}

export function getBalance(accountId) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return walletApi
			.getBalance(accountId)
			.then(data => {
				if (data) {
					if (data.WalletDetail) {
						dispatch(loadBalanceData(data.WalletDetail, false))
						saveDatatoStorage("balanceData", data.WalletDetail)
						return data.WalletDetail.BusinessWallet.balance
					} else {
						dispatch(apiStatusActions.apiCallError())
						//return -9999
					}
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function checkBalance(walletId, rewardValue) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return walletApi
			.checkBalance(walletId)
			.then(data => {
				if (data) {
					dispatch(apiStatusActions.apiCallSuccess())
					if (data && parseInt(data.balance) >= parseInt(rewardValue)) {
						return true
					} else {
						throw new Error("insufficient balance in business wallet")
					}
				} else {
					dispatch(apiStatusActions.apiCallError())
					return false
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function getTransaction(walletId) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())
		return walletApi
			.getTransaction(walletId)
			.then(data => {
				if (data) {
					dispatch(loadAllTransaction(data))
					return data
				} else {
					dispatch(apiStatusActions.apiCallError())
					return []
				}
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

export function walletTransfer(orgId, senderId, receiverId, transferFrom, transferTo, amount, currencyType, walletType) {
	return function (dispatch) {
		dispatch(apiStatusActions.beginApiCall())

		return walletApi
			.transfer({
				orgId: orgId,
				sendWalId: walletType === "business" ? senderId + "_WB" : senderId + "_WP",
				recvWalId: walletType === "business" ? receiverId + "_WB" : receiverId + "_WP",
				type: walletType === "business" ? "Business" : "Redeem",
				from: transferFrom,
				to: transferTo,
				amount: amount,
				currency: currencyType,
				comments: walletType === "business" ? "Business Transfer" : "Personal Transfer"
			})
			.then(data => {
				dispatch(apiStatusActions.apiCallSuccess())
				dispatch(getBalance(senderId))
			})
			.catch(error => {
				dispatch(apiStatusActions.apiCallError())
				throw new Error(error)
			})
	}
}

const saveDatatoStorage = (key, data) => {
	try {
		saveDataStorage(key, JSON.stringify(data))
	} catch (err) {
		console.log(err)
	}
}
